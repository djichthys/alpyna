#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import weakref
import Func_Hdr
import For_Metadata as For_Meta
import Extract_Memory_Dependency as Mem_Dep

class Flat_Record:
    def __init__(self, lvl, node, pred_ast=None):
        self.node = node
        self.lineno = self.node.lineno
        self.lvl = lvl
        self.ntype = None
        self.gen_record = {
            None: None,
            getattr(ast, "FunctionDef"): self._gen_ast_record_FunctionDef,
            getattr(ast, "For"): self._gen_ast_record_For,
            getattr(ast, "Assign"): self._gen_ast_record_Assign
        }

        self.rec_copy = {
            None: None,
            getattr(ast, "FunctionDef"): self._rcopy_FunctionDef,
            getattr(ast, "For"): self._rcopy_For,
            getattr(ast, "Assign"): self._rcopy_Assign
        }
        self.record = None

        # Used to store predicated execution for this assignment
        self.predicate_ast = pred_ast
        self.predicate_rec = None

    @property
    def node(self):
        return self._node()

    @node.setter
    def node(self, _node):
        _ref = lambda obj: obj() if callable(obj) else obj
        self._node = weakref.ref(_ref(_node))

    @property
    def lvl(self):
        return self._lvl

    @lvl.setter
    def lvl(self, lvl):
        self._lvl = lvl

    @property
    def lineno(self):
        return self._lineno

    @lineno.setter
    def lineno(self, lineno):
        self._lineno = lineno

    def _gen_ast_record_FunctionDef(self, node):
        _func = Func_Hdr.Func_Boundary()
        _new_node = _func.visit(node)
        _new_node = ast.copy_location(_new_node, node)
        self.node = _new_node
        self.lineno = _new_node.lineno

        self.record = _func
        self.ntype = "FunctionDef"
        return self.node

    def _gen_ast_record_For(self, node):
        self.record = dict()
        self.ntype = "For"
        self.record["for_limits"] = For_Meta.For_Limits(getattr(node, "iter", None))
        self.record["for_targets"] = For_Meta.For_Targets(getattr(node, "target", None))
        return node

    def _gen_ast_record_Assign(self, node):
        self.record = {"src": [], "trgt": [], "pred": []}
        _src = Mem_Dep.Assign_MemAccess(node.value)
        self.node.value = _src.visit(node.value)
        self.record["src"] = _src

        _tgt_list = []
        for trgt_idx, _trgt in enumerate(node.targets):
            _xtrct_trgt = Mem_Dep.Assign_MemAccess(_trgt)
            self.node.targets[trgt_idx] = _xtrct_trgt.visit(_trgt)
            _tgt_list.append(_xtrct_trgt)

        for _tgt in _tgt_list:
            for _dep in _tgt.depends:
                self.record["trgt"].append(_dep)

        #  Append predicated execution to the Assign record
        if self.predicate_ast :
            self.predicate_rec = Mem_Dep.Assign_MemAccess(self.predicate_ast)
            self.predicate_ast = self.predicate_rec.visit(self.predicate_ast)
            self.record["pred"] = self.predicate_rec.depends

        _new_node = ast.Assign(node.targets, node.value)
        _new_node = ast.fix_missing_locations(node)
        self.node = _new_node
        self.ntype = "Assign"
        return _new_node

    def gen_ast_record(self, node):
        _func = self.gen_record.get(type(node), None)
        if _func == None:
            return node, None
        return _func(node)

    def _rcopy_dict(self, rdict):
        for _key in rdict.keys():
            _rcopy_func = getattr(rdict[_key], "rcopy", None)
            if _rcopy_func:
                rdict[_key] = _rcopy_func()
            elif type(self.node) is ast.Assign and _key == "trgt":
                ## Major hack due to very bad early design decision
                ## change entire codebase --- MUPPET!
                rdict[_key] = Mem_Dep.assign_rcopy(rdict[_key])

    def _rcopy_FunctionDef(self):
        _stringify = lambda x: x.s if type(x) is ast.Str else x
        _rec = Func_Hdr.Func_Boundary()
        _rec.node = self.node
        _func_args = self.record.arguments
        _rec.arguments = {_key: _stringify(_func_args[_key]) for _key in _func_args.keys()}
        return ("FunctionDef", _rec)

    def _rcopy_For(self):
        # shallow copy dictionary
        # & replace with deep-copy of runtime relevant data structures
        _rec = self.record.copy()
        self._rcopy_dict(_rec)
        return ("For", _rec)

    def _rcopy_Assign(self):
        # shallow copy dictionary
        # & replace with deep-copy of runtime relevant data structures
        _rec = self.record.copy()
        self._rcopy_dict(_rec)
        return ("Assign", _rec)

    def rcopy(self):
        _rec = Flat_Record(self.lvl, self.node)
        _func = self.rec_copy[type(self.node)]
        if _func == None:
            return node, None
        _rec.ntype, _rec.record = _func()
        return _rec
