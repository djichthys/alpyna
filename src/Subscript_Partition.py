#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import weakref
import Utils as flt_util
#import Flat_Record as flt_rec
import FlattenAST as flt_ast
import Dependency_Solver as Solve
import Generate_DAG as dag


class Boundary:
    def __init__(self, start, end, sub_elem=None):
        self.start = start
        self.end = end
        self.sub_elem = sub_elem

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, start):
        self._start = start

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, end):
        self._end = end

    @property
    def sub_elem(self):
        return self._sub_elem

    @sub_elem.setter
    def sub_elem(self, _sub):
        if _sub is None:
            _sub = []
        self._sub_elem = _sub

    # Should hold a list of Loop_Subscript_Extractor objects
    @property
    def loop_truss(self):
        return self._loop_truss

    # Should hold a list of Loop_Subscript_Extractor objects
    @loop_truss.setter
    def loop_truss(self, val):
        if type(val) is list:
            if val and type(val[0]) is Loop_Subscript_Extractor:
                self._loop_truss = val
            else:
                self._loop_truss = []
        elif type(val) is Loop_Subscript_Extractor:
            self._loop_truss = [val]
        else:
            self._loop_truss = []

    def display(self):
        print(
            "{0} Start<lvl={1}, lineno={2}, type={3} ::: End<lvl={4}, lineno={5}, type={6}>".format(
                ('-' * self.start.lvl) + '>', self.start.lvl, self.start.lineno, self.start.ntype,
                self.end.lvl, self.end.lineno, self.end.ntype))
        for _sub in self.sub_elem:
            _sub.display()


class Loop_Aggregator(flt_util.Record_Parser):
    def __init__(self, skel_obj):
        self.skel = skel_obj
        super().__init__(self.skel.skeleton)
        self.functions = []

    @property
    def skel(self):
        return self._skel

    @skel.setter
    def skel(self, skeleton):
        self._skel = skeleton

    def _find_loop_nest_end(self, start):
        end = start

        _tok = self.consume()
        while _tok:
            #Search for end of loop nest
            _next = self.look_ahead()
            if _next:
                #tag end of loop nest to return to caller
                if _next.lvl <= start.lvl:
                    end = _tok
                    break
            else:
                end = _tok
                break
            _tok = self.consume()

        return Boundary(start, end)

    def _find_function_end(self, start):
        _sub_elements = []
        end = start

        _tok = self.consume()
        while _tok:
            # Look for loop nests within a function
            if _tok.ntype == 'For':
                _nest_end = self._find_loop_nest_end(_tok)
                _sub_elements.append(_nest_end)
                _next = self.look_ahead()
                if _next:
                    #tag end of function return to caller
                    if _next.lvl <= start.lvl:
                        end = _nest_end.end
                        break
                else:
                    end = _nest_end.end
                    break
            _tok = self.consume()

        return Boundary(start, end, _sub_elements)

    def _find_functions(self):
        # Read through each record and populate boundaries
        _tok = self.consume()
        while _tok:
            if _tok.ntype == 'FunctionDef':
                _func = self._find_function_end(_tok)
                self.functions.append(_func)

            _tok = self.consume()

    def aggregate(self):
        self._find_functions()

    def display(self, boundary=None):
        if boundary is None:
            boundary = self.functions

        if type(boundary) is Boundary:
            boundary.display()
        else:
            for _func_obj in boundary:
                _func_obj.display()


class Variable_Binding:
    # To be used only with Assign statments
    # _var_ref is to find out position of variable
    # in the list of variables of either target or source
    #def __init__(self, index_list, skel_obj, var_ref, dag_node , var_idx=None, tgt_or_src=None):
    def __init__(self, index_list, skel_obj_node, var_ref, var_idx=None, tgt_or_src=None):
        self.index_list = index_list.copy()
        self.skel_obj_node = skel_obj_node
        self.var_ref = var_ref

        self.tgt_or_src = None
        if self.skel_obj_node.payload.assign_stmt.ntype == 'Assign':
            self.tgt_or_src = tgt_or_src
        self.var_idx = var_idx
        self.subscript_list = []

    @property
    def index_list(self):
        return self._idx_list


    @index_list.setter
    def index_list(self, index_list):
        self._idx_list = index_list

    @property
    def skel_obj_node(self):
        return self._skel_obj_graph_node()

    @skel_obj_node.setter
    def skel_obj_node(self, val):
        self._skel_obj_graph_node = weakref.ref(val)

    @property
    def tgt_or_src(self):
        return self._tgt_or_src

    @tgt_or_src.setter
    def tgt_or_src(self, _obj):
        #_lvl, _line, _type, _payload = self.skel_obj

        if _obj in ['target', 'Target', 'tgt', 'Tgt', 'trgt', 'Trgt']:
            self._tgt_or_src = 'trgt'
        elif _obj in ['source', 'Source', 'src', 'Src']:
            self._tgt_or_src = 'src'
        else:
            self._tgt_or_src = None

    @property
    def var_ref(self):
        return self._var_ref

    @var_ref.setter
    def var_ref(self, _ref):
        self._var_ref = _ref

    @property
    def var_idx(self):
        return self._var_idx

    @var_idx.setter
    def var_idx(self, _ref):
        if type(_ref) is int:
            self._var_idx = _ref
        elif type(_ref) is list:
            self._var_idx = self.var_ref.index(_ref)
        else:
            self._var_idx = 0

    @property
    def subscript_list(self):
        return self._subscript_list

    @subscript_list.setter
    def subscript_list(self, _list):
        if type(_list) is list:
            self._subscript_list = _list
        else:
            self._subscript_list = [_list]

    def get_subscript_objects(self, _user_subscript_list=None):
        if _user_subscript_list is None:
            _user_subscript_list = []

        if len(self.var_ref['axes']) == 0:
            _sub = Subscript(self, None)
            self.subscript_list.append(_sub)
            _user_subscript_list.append(weakref.ref(_sub))
        else:
            for _idx, _sub in enumerate(self.var_ref['axes']):
                _sub = Subscript(self, _idx)
                self.subscript_list.append(_sub)
                _user_subscript_list.append(weakref.ref(_sub))

        return _user_subscript_list


class Subscript:
    # To be used only with Assign statments

    # _idx_ref is the weakref - to find out position of variable
    # in the list of variables of either target or source
    def __init__(self, _var, _sub_ref):
        self.subscripted_var = _var
        self.subscript_ref = _sub_ref

    @property
    def subscripted_var(self):
        return self._subscripted_var()

    @subscripted_var.setter
    def subscripted_var(self, _var_ref):
        self._subscripted_var = weakref.ref(_var_ref)

    @property
    def subscript_ref(self):
        return self._subscript_ref

    @subscript_ref.setter
    def subscript_ref(self, _idx):
        if type(_idx) is int or _idx == None:
            self._subscript_ref = _idx    # if idx is None, it indicates a scalar variable
        else:
            self._subscript_ref = self.subscipted_var.index(_idx)


class Variable_Set(flt_util.Variable_List):
    def __init__(self, _items, _compare=None):
        self.items = _items
        self._compare = _compare
        super().__init__(self.items, _compare)

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, _items):
        if type(_items) is list:
            self._items = _items.copy()
        else:
            self._items = [_items]

    def amalgamate(self, cb):
        _combined = super()._union(cb)
        return Variable_Set(_combined.elements)

    def cross_map(self, cb):
        _combined = super()._cross_map(cb)
        return Variable_Set(_combined.elements)

    def unique_pairs(self, _vlist):
        pair_list = super()._unique_pairs(_vlist)
        return Variable_Set(pair_list.elements)

    def intersection(self, cb):
        _combined = super()._intersect(cb)
        return Variable_Set(_combined.elements)


class Subscript_Group:
    def __init__(self, _pairs):
        self.pairs = _pairs
        self.common_loops = []

    @property
    def pairs(self):
        return self._pairs

    @pairs.setter
    def pairs(self, _pairs):
        if type(_pairs) is list:
            self._pairs = [weakref.ref(_it) for _it in _pairs]
        else:
            self._pairs = [weakref.ref(_pairs)]

    @property
    def dependence(self):
        return self._dependence

    @dependence.setter
    def dependence(self, dep):
        self._dependence = dep

    def finalize_loops(self):
        _compare = lambda x, y: True if x == y else False
        _loops = Variable_Set(self.pairs[0]().loop_list, _compare)
        for _nxt_loop_grp in self.pairs[1:]:
            _loops = _loops.amalgamate(Variable_Set(_nxt_loop_grp().loop_list, _compare))

        self.common_loops = _loops.items
        self.common_loops.sort(key=lambda x: x.lvl)


class Variable_Pair:
    def __init__(self, var_a, var_b):
        self.var_a = var_a
        self.var_b = var_b
        self.subscript_pairs = self._gen_subscript_pairs()
        if len(self.subscript_pairs) > 0:
            self.coupling_list = self._subscript_partition()
        else:
            self.coupling_list = None, None

        #self.display_coupled_groups()

    @property
    def var_a(self):
        return self._var_a()

    @var_a.setter
    def var_a(self, _val):
        if type(_val) is Variable_Binding:
            self._var_a = weakref.ref(_val)
        else:
            self._var_a = None

    @property
    def var_b(self):
        return self._var_b()

    @var_b.setter
    def var_b(self, _val):
        if type(_val) is Variable_Binding:
            self._var_b = weakref.ref(_val)
        else:
            self._var_b = None

    @property
    def coupling_list(self):
        return (self._separable, self._coupled)

    @coupling_list.setter
    def coupling_list(self, val):
        if type(val) is tuple:
            if len(val) < 2:
                self._separable, self._coupled = None, None
            else:
                self._separable, self._coupled = val[0], val[1]

    @property
    def var_dep(self):
        return self._var_dep

    @var_dep.setter
    def var_dep(self, dep):
        assert type(dep) == Solve.Variable_Dependence
        self._var_dep = dep

    def _gen_subscript_pairs(self):
        num_subs = min(len(self.var_a.subscript_list), len(self.var_b.subscript_list))
        subscript_pair_list = []
        for _idx in range(0, num_subs, 1):
            subscript_pair_list.append(
                Subscript_Pair(self.var_a.subscript_list[_idx], self.var_b.subscript_list[_idx]))

        # if variable pair is a pair of scalars, then return an empty list
        if len(subscript_pair_list) == 1 and subscript_pair_list[0].scalar == True:
            return []

        return subscript_pair_list

    def _init_subscript_partition(self):
        return [Subscript_Group(_it) for _it in self.subscript_pairs]

    def _amalgamate_coupled_subscripts(self, grp_a, grp_b):
        # Function assumes that list is populated by atleast one
        # which is True if this is a vector. This function should
        # never be reached for pairs of scalar variables
        def _looplist_SetOp_Create(_list):
            _looplist = Variable_Set(_list[0]().loop_list)
            if len(_list) > 1:
                for pair in _list[1:]:
                    temp = Variable_Set(pair().loop_list)
                    _looplist = _looplist.amalgamate(temp)
            return _looplist

        a_unified_looplist = _looplist_SetOp_Create(grp_a.pairs)
        b_unified_looplist = _looplist_SetOp_Create(grp_b.pairs)
        intersected_loop_list = a_unified_looplist.intersection(b_unified_looplist)
        if len(intersected_loop_list.items) > 0:
            _pairs_a = Variable_Set([pa() for pa in grp_a.pairs])
            _pairs_b = Variable_Set([pb() for pb in grp_b.pairs])
            _coupled_pairs = _pairs_a.amalgamate(_pairs_b)
            _coupled_pairs.items.sort(key=lambda x: x.sub_a.subscript_ref)
            return Subscript_Group(_coupled_pairs.items)
        return None

    def _subscript_partition(self):
        # Create a parition where each subscript parition
        # is in a group of its own
        _separable = []
        _coupled = []
        _subscript_groups = self._init_subscript_partition()
        for o_idx, grp in enumerate(_subscript_groups):
            # Any ZIV subscripts will be a group on its own
            # and will be separable by definition
            if len(grp.pairs) == 1 and grp.pairs[0]().complexity == 'ZIV':
                _separable.append(grp)
                continue

            # check for every remaining subscript
            _done = True
            for i_idx in range(o_idx + 1, len(_subscript_groups), 1):
                _new_grp = self._amalgamate_coupled_subscripts(grp, _subscript_groups[i_idx])
                if _new_grp != None:
                    _done = False
                    _subscript_groups[i_idx] = _new_grp

            # if all the subscripts are identified, identify corresponding loop
            # headers for all the loops in each group and append to the separable
            # or the coupled list of subscript_groups
            if _done:
                grp.finalize_loops()
                if len(grp.pairs) == 1:
                    _separable.append(grp)
                else:
                    _coupled.append(grp)

        return (_separable, _coupled)

    def display_coupled_groups(self):
        _sep, _coupled = self.coupling_list
        print("\n>>>>>>>>>>>>>>\nVariable-Pair ::: {0} <#+#+#> {1}".format(
            self.var_a.var_ref, self.var_b.var_ref))
        print("=========\nSeparated subscript list")
        if _sep:
            for _gno, grp in enumerate(_sep):
                print(" --------GroupNo[{0}]-------".format(_gno))
                #print(" Included loops = {0}".format([
                #    "for {0} in range({1})".format(_forloop.record["for_targets"].target,
                #                                   _forloop.record["for_limits"].limits)
                #    for _forloop in grp.common_loops
                #]))
                for _pno, pair in enumerate(grp.pairs):
                    print(" Pair[{0}] -> pairs(n) = {1}, complexity = {2}".format(
                        _pno, (pair().sub_a.subscript_ref, pair().sub_b.subscript_ref),
                        pair().complexity))
                    print("         -> pairs(ref) = {0} || {1}".format(
                        pair().sub_a.subscripted_var.var_ref['axes'][pair().sub_a.subscript_ref],
                        pair().sub_b.subscripted_var.var_ref['axes'][pair().sub_b.subscript_ref]))
        print("=========\nCoupled subscript list")
        if _coupled:
            for _gno, grp in enumerate(_coupled):
                print(" --------GroupNo[{0}]-------".format(_gno))
                #print(" Included loops = {0}".format([
                #    "for {0} in range({1})".format(_forloop.record["for_targets"].target,
                #                                   _forloop.record["for_limits"].limits)
                #    for _forloop in grp.common_loops
                #]))
                for _pno, pair in enumerate(grp.pairs):
                    print(" Pair[{0}] -> pairs(n) = {1}, complexity = {2}".format(
                        _pno, (pair().sub_a.subscript_ref, pair().sub_b.subscript_ref),
                        pair().complexity))
                    print("         -> pairs(ref) = {0} || {1}  ]".format(
                        pair().sub_a.subscripted_var.var_ref['axes'][pair().sub_a.subscript_ref],
                        pair().sub_b.subscripted_var.var_ref['axes'][pair().sub_b.subscript_ref]))
        print(">>>>>>>>>>>>>>\n")

    def is_scalar(self):
        _separable, _coupled = self.coupling_list
        if _separable is not None or _coupled is not None:
            return False
        return True


class Subscript_Pair(flt_util.Variable_List):
    def __init__(self, sub_a, sub_b, _compare=None):
        self.sub_a = sub_a
        self.sub_b = sub_b
        self.compare_func = _compare
        self.scalar = False
        self.loop_list = None
        self.complexity = self.classify_complexity()

    @property
    def sub_a(self):
        return self._sub_a()

    @sub_a.setter
    def sub_a(self, _val):
        if type(_val) is Subscript:
            self._sub_a = weakref.ref(_val)
        else:
            self._sub_a = None

    @property
    def sub_b(self):
        return self._sub_b()

    @sub_b.setter
    def sub_b(self, _val):
        if type(_val) is Subscript:
            self._sub_b = weakref.ref(_val)
        else:
            self._sub_b = None

    @property
    def complexity(self):
        return self._complexity

    @complexity.setter
    def complexity(self, _val):
        if _val in ('ZIV', 'SIV', 'RDIV', 'MIV'):
            self._complexity = _val
        else:
            self._complexity = None

    def cross_map(self, cb, compare_override=None):
        _combined = super()._cross_map(cb, compare_override)
        return Variable_Set(_combined.elements)

    def unique_pairs(self, _vlist, compare_override=None):
        pair_list = super()._unique_pairs(_vlist, compare_override)
        return Variable_Set(pair_list.elements)

    # param sub should be of class Subscript
    def _index_refs(self, sub):
        # Check if this is a scalar variable
        if len(sub.subscripted_var.var_ref['axes']) == 0:
            self.scalar = True
            return self.scalar, None

        enc_loop_refs = []    # List of loop refs within
        idx_list = sub.subscripted_var.index_list
        idx_in_sub = sub.subscripted_var.var_ref['axes'][sub.subscript_ref]
        trgt_var_stack = []
        for _idx_name in idx_in_sub.keys():    # each index within subscript
            for _iter in range(len(idx_list) - 1, -1, -1):
                # unpack For-loop skel object
                #_lvl, _line, _type, _rec = idx_list[_iter]
                if _idx_name not in idx_list[_iter].record["for_targets"].target:
                    continue

                # check that if the same variable is used in
                # in the inner loop and an outer loop, then
                # only the inner-most loop target is used
                if _idx_name not in trgt_var_stack:
                    enc_loop_refs.append(idx_list[_iter])
                    trgt_var_stack.append(_idx_name)

        return self.scalar, enc_loop_refs

    # classify subscript-pairs based on their index loops
    # start from the inner-most loop-index and go to the outermost
    # because the target index of a loop could be overwritten
    # by another target index in an inner loop
    # eg. for idxi in range() :
    #       for idxi in range() :
    # Here idxi is overwritten in the inside loop. We ameliorate this
    # by versioning the target index variable. We achieve this by storing
    # a reference to the target index of the actual for-loop
    def classify_complexity(self):
        _a_scalar, _a_indices, = self._index_refs(self.sub_a)
        _b_scalar, _b_indices = self._index_refs(self.sub_b)

        if _a_scalar or _b_scalar:
            return    # scalar - nothing else to do for subscript classification

        # create containers for the loop refs to
        _compare = lambda x, y: True if x == y else False
        vl_a, vl_b = Variable_Set(_a_indices, _compare), Variable_Set(_b_indices, _compare)
        self.loop_list = vl_a.amalgamate(vl_b).items

        _complexity = None
        if len(self.loop_list) == 0:
            _complexity = 'ZIV'
        elif len(self.loop_list) == 1:
            _complexity = 'SIV'
        elif self.sub_a.subscripted_var.index_list == self.sub_b.subscripted_var.index_list and len(
                self.loop_list) == 2 and (len(_a_indices) == 0 or len(_b_indices) == 0 or
                                          (len(_a_indices) == 1 and len(_b_indices) == 1)):
            _complexity = 'RDIV'
        else:
            _complexity = 'MIV'

        return _complexity


class Loop_Subscript_Extractor(flt_util.Record_Parser):
    def __init__(self, skel_obj, boundary, func_name):
        self.skel_obj = skel_obj
        self.boundary = boundary
        super().__init__(self.skel_obj.skeleton, boundary.start, boundary.end)
        self.subscript_list = []

        self.variable_pairs = None

        self.graph_builder = self._loop_nest_DAG_init( func_name )
        self.code_gen = None

        self.variable_list = []
        self.target_vars = []
        self.source_vars = []

    @property
    def skel_obj(self):
        return self._skel_obj()

    @skel_obj.setter
    def skel_obj(self, _obj):
        self._skel_obj = weakref.ref(_obj)

    @property
    def boundary(self):
        return self._boundary()

    @boundary.setter
    def boundary(self, _boundary):
        #_slvl, _sline, _stype, _sobj = _boundary.start
        if _boundary.start.ntype == 'For':
            self._boundary = weakref.ref(_boundary)
        else:
            self._boundary = None

    @property
    def subscript_list(self):
        return self._sub_list

    @subscript_list.setter
    def subscript_list(self, val):
        self._sub_list = val

    @property
    def variable_pairs(self):
        return self._x_variable_pairs, self._op_variable_pairs

    @variable_pairs.setter
    def variable_pairs(self, val):
        if type(val) is tuple:
            if len(val) < 2:
                self._x_variable_pairs, self._op_variable_pairs = None, None
            else:
                self._x_variable_pairs, self._op_variable_pairs = val[0], val[1]

    @property
    def graph_builder(self):
        return self._graph_builder

    @graph_builder.setter
    def graph_builder(self, dag_constructor):
        self._graph_builder = dag_constructor


    @property
    def code_gen(self):
        return self._code_generator

    @code_gen.setter
    def code_gen(self, code_gen):
        self._code_generator = code_gen

    # Gather all loop indices relevant to the current loop-nest
    # Pair every Variable with the loop-nest indices
    def _loop_nest_subscripts(self):
        def _loop_nest_idx_push(stack, idx_var):
            stack.append(idx_var)

        def _loop_nest_idx_pop(stack):
            return stack.pop()

        def _loop_nest_idx_top(stack):
            return stack[-1]

        def _loop_nest_top_level(stack):
            if stack:
                return stack[-1].lvl
            else:
                return 0

        def _enveloping_loop_idx(stack):
            return stack.copy()

        idx_var_stack = []    # Stack to store list of enveloping indices
        self.rewind()
        _rec = self.consume()
        while _rec:
            # Adjust the enveloping loop-var stack according to the level of the statement
            # Note : Adssumes that the end of the outer-most loop has been
            #        calculated correctly. This is not checked for here.
            while _rec.lvl <= _loop_nest_top_level(idx_var_stack):
                _loop_nest_idx_pop(idx_var_stack)

            #Search for "For" construct headers within the loop nests
            if _rec.ntype == 'For':
                # Push loop index onto stack to get the enveloping loop indices
                _loop_nest_idx_push(idx_var_stack, _rec)
                _rec = self.consume()
                continue

            _idx_list = _enveloping_loop_idx(idx_var_stack)
            #print("stack :::: {0}  :::::: {1} ".format( idx_var_stack , _idx_list ))
            #flt_util.display_record( _rec )
            self._extract_variables(self.subscript_list, self.variable_list, _idx_list, _rec)
            _rec = self.consume()

    def _extract_variables(self, _subscript_list, _var_list, index_list, skel_obj):
        _node = self.graph_builder.map_node(skel_obj)
        _node.payload.enclosing_loops = index_list 
        # sweep in all the target variables in an assign statement
        for _idx, _trgt in enumerate(skel_obj.record["trgt"]):
            _var = Variable_Binding(index_list, _node , _trgt, var_idx=_idx, tgt_or_src='trgt')
            _subscript_list = _var.get_subscript_objects(_subscript_list)
            _var_list.append(_var)

        # append all source variables in an assign statement
        for _idx, _src in enumerate(skel_obj.record["src"].depends):
            _var = Variable_Binding(index_list, _node,  _src, var_idx=_idx, tgt_or_src='src')
            _subscript_list = _var.get_subscript_objects(_subscript_list)
            _var_list.append(_var)

        # append all source variables in a conditional predicate of the statement
        _acc_idx = len(skel_obj.record["src"].depends)
        for _idx, _src in enumerate(skel_obj.record["pred"]):
            _var = Variable_Binding(index_list, _node,  _src, var_idx=_idx + _acc_idx, tgt_or_src='src')
            _subscript_list = _var.get_subscript_objects(_subscript_list)
            _var_list.append(_var)

    # group target and source variables together. If we do not provide
    # the lists, then the default list is the class variable elements
    def _group_tgt_src_vars(self, _var_list=None, _tgt_vars=None, _src_vars=None):
        if _var_list == None:
            _var_list = self.variable_list
        if _tgt_vars == None:
            _tgt_vars = self.target_vars
        if _src_vars == None:
            _src_vars = self.source_vars

        for _var in _var_list:
            if _var.tgt_or_src == 'trgt':
                _tgt_vars.append(_var)
            elif _var.tgt_or_src == 'src':
                _src_vars.append(_var)

    # Given 3 separate lists, it creates a list of subscript
    # pairs to prepare for dependency checking
    def create_subscript_pairs(self, op, x_trgt, x_src):
        # for each pair of variables within loop nest create a tuple
        # of target <--> source variable mappings for flow and anti-dendences
        _x_var_pair_list = [
            Variable_Pair(_tgt, _src) for _tgt in x_trgt for _src in x_src
            if _tgt.var_ref['var'] == _src.var_ref['var']
        ]

        # for each pair of variables within loop nest create a tuple
        # of target <--> source variable mappings for output dependence
        check_dependence = lambda _a, _b: True if _a.var_ref['var'] == _b.var_ref['var'] else False
        _tgt_vars = Variable_Set(self.target_vars, check_dependence)

        _op_var_pair_list = [
            Variable_Pair(_tgt, _src) for _tgt, _src in _tgt_vars.unique_pairs(_tgt_vars).items
        ]

        return (_x_var_pair_list, _op_var_pair_list)

    # Prune out variables that will not have any dependences
    def _prune_independent_vars(self):
        def _sort_lines(_var_obj):
            # unpack skeleton obj
            #_lvl, _line, _ty, _rec = _var_obj.skel_obj
            return _var_obj.skel_obj_node.payload.assign_stmt.lineno

        # For each unique-variable in target
        # check the variable is in source
        check_dependence = lambda _a, _b: True if _a.var_ref['var'] == _b.var_ref['var'] else False
        _tgt_vars = Variable_Set(self.target_vars, check_dependence)
        _src_vars = Variable_Set(self.source_vars, check_dependence)

        #print("+=+=+=+=+=+=++=+=+=+=+=+=+=\ntarget-variables")
        #for _var in _tgt_vars.items:
        #    print("target-variable = {0}".format(_var.var_ref))
        #print("+=+=+=+=+=+=++=+=+=+=+=+=+=\nsource-variables")
        #for _var in _src_vars.items:
        #    print("source-variable = {0}".format(_var.var_ref))

        cross_dependency_list = _tgt_vars.cross_map(_src_vars)
        cross_dependency_list.items.sort(key=_sort_lines)
        _cross_tgt_list, _cross_src_list = [], []
        self._group_tgt_src_vars(
            _var_list=cross_dependency_list.items,
            _tgt_vars=_cross_tgt_list,
            _src_vars=_cross_src_list)
        return (_tgt_vars.items, _cross_tgt_list, _cross_src_list)

    def classify_subscripts(self):
        self._loop_nest_subscripts()
        for _sub in self.subscript_list:
            _item = _sub().subscripted_var
            #print(
            #    ">>> Var -- Index-List = <<{0}>>,".format(
            #        [_rec.record["for_targets"].target for _rec in _item.index_list]),
            #    end=" ")
            #print(
            #    "variable-ref = <<{0}>> , tgt<->src = [{1}] , var-index = {2} >>>> Subscript = {3}".
            #    format(_item.var_ref, _item.tgt_or_src, _item.var_idx,
            #           _sub().subscript_ref))

        # Group all the target and source variables together within this loop-nest
        self._group_tgt_src_vars()

        # Remove variables that will not cause a Dependence
        # Read-only variables from source list
        # NOTE : to-do : Aliasing - put into a runtime-check list
        _op_dependence_vars, _xd_trgt, _xd_src = self._prune_independent_vars()

        # Generate Variable and Subscript Pairs and classify them as SIV/MIV/RDIV/ZIV
        self.variable_pairs = self.create_subscript_pairs(_op_dependence_vars, _xd_trgt, _xd_src)

    # This function is placed here to associate a DAG 
    # representation with this loop structure
    def _loop_nest_DAG_init(self, func_name):
        _dag_build = dag.DAG_Builder( self, func_name )
        _dag_build.parse_stmts()
        return _dag_build 

    def recParser_display(self):
        init = rec = self.consume()
        while rec:
            print(">>>>> <<<<<< >>>>>> <<<<<<<")
            flt_util.display_record(rec)
            rec = self.consume()

def partition(obj):
    #print("========================+")

    _loop_aggregator = Loop_Aggregator(obj)
    _loop_aggregator.aggregate()
    #_loop_aggregator.display()

    # test getting subscript pair
    #print("========================+")
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("========================+")

    for _idx_f, _func in enumerate(_loop_aggregator.functions):
        _func.loop_truss = []
        for _idx_l, _nest in enumerate(_func.sub_elem):
            _func.loop_truss.append(Loop_Subscript_Extractor(obj, _nest \
                                                    , _func.start.node.name + "_lnest_{0:03d}".format(_idx_l)))
        #print("========================+")

    for _idx_f, _func in enumerate(_loop_aggregator.functions):
        for _idx_l, item in enumerate(_func.loop_truss):
            #print("\n\n\###### Function {0} :: Loop Nest {1} #######\n".format(_idx_f, _idx_l))
            item.classify_subscripts()
    return _loop_aggregator


if __name__ == '__main__':
    val_list = [i for i in range(0, 100, 5) if i % 2 == 0 and i % 5 == 0]
    dummy = Dummy(val_list)
    dummy.do_parse()
