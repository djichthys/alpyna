#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

import sys
import Static_Analysis_Driver as parloop

import Utils as flt_util
import numpy as np
import os
import json
import datetime as dt


def gen2D_data(size=1024, arr_type='uint32'):
    _arr0 = np.arange(0, size, 1, dtype=arr_type)
    _arr = np.array(
        [np.arange(0, size, 1, dtype=arr_type) for _rmax in range(size)], dtype=arr_type, copy=True)
    return _arr


def gen1D_data(arr_type='uint32', size=1024):
    return np.arange(0, size, 1, dtype=arr_type)


# Helper function : print human-readable
# size numbers to print out as keys
# within the result matrix
def format_size(size):
    if size >= (1024 * 1024):
        return "{0}M".format(size // (1024 * 1024))
    if size >= 1024:
        return "{0}K".format(size // (1024))
    return str(size)


# Helper function : Convert a start and end
# domain-size to a tuple where the range can
# be built in the form  (start * 2**n )
def gen_range(start, end):
    return (start, (int(np.log2(end)) - int(np.log2(start)) + 1), 2)


# Helper function : Insert profile values &
#            cost model values into result matrix
# alp_obj : Object returned by ALPyNA static
#           parsing and IR generation
# res_map : The first time this function is called,
#           pass an empty dict into the res_map argument
#           This will also be returned by the function.
#           Subsequent calls can use the returned dict
#           object to accrue results
# benchmark : string to use as key within result-map
# dom-size  : string to use as key within result-map
#             - domain size of loop-nest
# exec_time : resultant time from ALPyNA profiling
# overheads : analysis and compilation times
# skip_vm : Assume that VM speeds are much slower
#           and compare cpu-gpu times
# vm_skipdom : Assume that VM experiment was halted
#            at smaller domain size 
def insert_result(alp_obj, res_map, benchmark, dom_size, exec_time, overheads, skip_vm, vm_skipdom):
    # If not already generated key-map for benchmark
    # create it here
    if benchmark not in res_map:
        res_map[benchmark] = {}

    if dom_size not in res_map[benchmark]:
        res_map[benchmark][dom_size] = {}

    _devices = ["interpreter", "cpu", "gpu"] if not skip_vm else ["cpu", "gpu"]
    for _dev in _devices:
        if _dev in "interpreter" and vm_skipdom :
            continue 

        if _dev not in res_map[benchmark][dom_size]:
            _xfer_cost = alp_obj.mod_opts.xfer_cost[
                _dev] if _dev in alp_obj.mod_opts.xfer_cost else 0
            res_map[benchmark][dom_size][_dev] = {
                "raw": np.mean(exec_time[_dev]),
                "raw-err": np.std(exec_time[_dev]),
                "exec-time-data": exec_time[_dev],
                "xfer-cost": _xfer_cost,
                "exec-cost": alp_obj.mod_opts.cost_estimate[_dev],
                "total-cost": alp_obj.mod_opts.cost_estimate[_dev] + _xfer_cost,
                "compile-cost": 0.0 if _dev == 'interpreter' else np.mean(overheads[_dev]["compilation"]),
                "analysis-cost": 0.0 if _dev == 'interpreter' else np.mean(overheads[_dev]["analysis"]),
                "compile-cost-raw": 0.0 if _dev == 'interpreter' else overheads[_dev]["compilation"],
                "analysis-cost-raw": 0.0 if _dev == 'interpreter' else overheads[_dev]["analysis"]
            }

    # Calculate accuracy of prediction for interpreter
    _predict = lambda a, b: False if (a <= 1 and b > 1) or (b <= 1 and a > 1) else True

    metrics = res_map[benchmark][dom_size]

    # Get CPU-VM predictions
    if skip_vm or vm_skipdom :
        metrics['cpu-vm cost'] = None
        metrics['cpu-vm profile'] = None
        metrics['cpu-vm prediction'] = None

        # Get GPU-VM predictions
        metrics['gpu-vm cost'] = None
        metrics['gpu-vm profile'] = None
        metrics['gpu-vm prediction'] = None

        # Set-up misprediction penalty calculation
        _min_cost = metrics['cpu']['total-cost']
        _min_cost_dev = 'cpu'
        _min_prof = metrics['cpu']['raw']
        _min_prof_dev = 'cpu'
    else:
        metrics['cpu-vm cost'] = metrics['cpu']['total-cost'] / metrics['interpreter']['total-cost']
        metrics['cpu-vm profile'] = metrics['cpu']['raw'] / metrics['interpreter']['raw']
        metrics['cpu-vm prediction'] = _predict(metrics['cpu-vm cost'], metrics['cpu-vm profile'])

        # Get GPU-VM predictions
        metrics['gpu-vm cost'] = metrics['gpu']['total-cost'] / metrics['interpreter']['total-cost']
        metrics['gpu-vm profile'] = metrics['gpu']['raw'] / metrics['interpreter']['raw']
        metrics['gpu-vm prediction'] = _predict(metrics['gpu-vm cost'], metrics['gpu-vm profile'])

        # Set-up misprediction penalty calculation
        _min_cost = metrics['interpreter']['total-cost']
        _min_cost_dev = 'interpreter'
        _min_prof = metrics['interpreter']['raw']
        _min_prof_dev = 'interpreter'

    # Get GPU-CPU predictions
    metrics['gpu-cpu cost'] = metrics['gpu']['total-cost'] / metrics['cpu']['total-cost']
    metrics['gpu-cpu profile'] = metrics['gpu']['raw'] / metrics['cpu']['raw']
    metrics['gpu-cpu prediction'] = _predict(metrics['gpu-cpu cost'], metrics['gpu-cpu profile'])

    # Calculate penalty for mispredicting
    metrics['mispredict-penalty'] = 1
    metrics['predicted dev'] = metrics['real min dev'] = _min_prof_dev
    for _dev in _devices:
        if _dev in "interpreter" and vm_skipdom :
            continue 

        if metrics[_dev]['total-cost'] < _min_cost:
            _min_cost, _min_cost_dev = (metrics[_dev]['total-cost'], _dev)
        if metrics[_dev]['raw'] < _min_prof:
            _min_prof, _min_prof_dev = (metrics[_dev]['raw'], _dev)
    else:
        if _min_prof_dev != _min_cost_dev:
            metrics['mispredict-penalty'] = metrics[_min_cost_dev]['raw'] / metrics[_min_prof_dev][
                'raw']
        metrics['predicted dev'] = _min_cost_dev
        metrics['real min dev'] = _min_prof_dev


# Test for Conway's Game of Life
def test_conway(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    def init_board(size):
        '''construct a square board (2d list of 0s) with side-length size'''
        return np.zeros((size, size), dtype=('uint32'))

    def printBoard(board):
        '''print board to stdout as a table'''
        for i in range(len(board)):
            for j in range(len(board[i])):
                print("%d" % board[i][j], end='')
            print()
        print()

    assert results != None, "Testing Conway -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]
    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                board_a = init_board(_dom)
                board_b = init_board(_dom)

                # a glider
                board_a[4][1] = 1
                board_a[4][2] = 1
                board_a[4][3] = 1
                board_a[3][3] = 1
                board_a[2][2] = 1

                for n in range(1):
                    alp_obj.conway(board_a, board_b, _dom)
                    board_a, board_b = board_b, board_a
                    _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                    _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                    _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _dev : _exec_times[_dev] for _dev in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Convolution-2D
def test_convolution2D(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Convolution-2D -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                _k_size = 3
                _out = gen2D_data(size=_dom, arr_type='float32')
                _in = gen2D_data(size=_dom, arr_type='float32')
                _h = gen2D_data(size=_k_size, arr_type='float32')

                alp_obj.conv2d(_out, _in, _h)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for GEMVER
def test_gemver(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Gemver -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                _alpha, _beta = (2, 1)
                _A = gen2D_data(size=_dom, arr_type='uint32')
                _u1 = gen1D_data(size=_dom, arr_type='uint32')
                _u2 = gen1D_data(size=_dom, arr_type='uint32')
                _v1 = gen1D_data(size=_dom, arr_type='uint32')
                _v2 = gen1D_data(size=_dom, arr_type='uint32')
                _w = np.zeros(_dom, dtype='uint32')
                _x = np.zeros(_dom, dtype='uint32')
                _y = gen1D_data(size=_dom, arr_type='uint32')
                _z = gen1D_data(size=_dom, arr_type='uint32')

                alp_obj.gemver(_alpha, _beta, _A, _u1, _u2, _v1, _v2, _w, _x, _y, _z)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Mandelbrot
def test_mandelbrot(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Mandelbrot -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                _c = np.array(
                    [[complex(float(x) / 100000,
                              float(y) / 100000) for y in range(_dom)] for x in range(_dom)],
                    dtype='complex64')
                _img = np.zeros((_dom, _dom), dtype='uint8')

                alp_obj.mandelbrot(_img, _c, 100, 2)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _key : np.mean(_exec_times[_key]) for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Black-Scholes
def test_blackscholes(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    def randfloat(rand_var, low, high):
        return ((1.0 - rand_var) * low) + (rand_var * high)

    assert results != None, "Testing BlackScholes -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                _call = np.zeros(_dom)
                _put = -np.ones(_dom)
                _stock_price = randfloat(np.random.random(_dom), 5.0, 30.0)
                _strike_price = randfloat(np.random.random(_dom), 1.0, 100.0)
                _years = randfloat(np.random.random(_dom), 0.25, 10.0)
                _risk_free = 0.02
                _volatility = 0.30


                _output = alp_obj.black_scholes( _call, _put, \
                                                 _stock_price, _strike_price ,\
                                                 _years, _risk_free, _volatility, 1 )
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Saxpy -- gets it wrong every single time
def test_saxpy(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Saxpy -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                arr_y = gen1D_data(arr_type='float32', size=_dom)
                arr_x = gen1D_data(arr_type='float32', size=_dom)
                const_a = 10.0

                alp_obj.saxpy(arr_y, arr_x, const_a)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test harness for GEMM - naive matrix multiply
def test_matmul(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing GEMM -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                mat_A = gen2D_data(size=_dom, arr_type='float32')
                mat_B = gen2D_data(size=_dom, arr_type='float32')
                mat_C = np.zeros((_dom, _dom), dtype='float32')

                alp_obj.matmul(mat_A, mat_B, mat_C)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Jacobi relaxation code
def test_jacobi(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Jacobi -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                orig_coeff = gen2D_data(size=_dom, arr_type='float32')
                new_coeff = np.zeros((_dom, _dom), dtype='float32')
                err = np.zeros((_dom, _dom), dtype='float32')

                alp_obj.jacobi_relax_core(new_coeff, orig_coeff, err)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Vector-add
def test_vector_add(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Vector-Add -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                arr_a = gen1D_data(arr_type='float32', size=_dom)
                arr_b = gen1D_data(arr_type='float32', size=_dom)
                arr_c = np.zeros((_dom, ), dtype='float32')

                alp_obj.vector_add(arr_c, arr_a, arr_a)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Hilbert-Matrix
def test_hilbert(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Hilbert Matrix-- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                _output = alp_obj.hilbert_matrix(_dom)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


# Test for Filterbank correlation
def test_fbcorr(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Filterbank Correlation-- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        for _num_filters in [8,16]:
            _exec_times = {}
            _overheads = {}
            _vm_skipdom = False
            for _dev in devices:
                if _dev == "interpreter" and vm_cutoff != None and _num_filters > vm_cutoff :
                    _vm_skipdom = True
                    continue

                _exec_times[_dev] = []
                _overheads[_dev] = { "analysis" : [], "compilation" : [] }
                alp_obj.mod_opts.profile_dev = _dev
                for _attempt in range(attempts):
                    _num_imgs, _vec_size, _ksize = (16, _dom, 5)
                    imgs = np.random.randn(_num_imgs, _dom, _dom, 3)
                    filters = np.random.randn(_num_filters, _ksize, _ksize, 3)
                    output = np.zeros((_num_imgs, _num_filters, _dom, _dom))

                    alp_obj.fbcorr(imgs, filters, output)
                    _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                    _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                    _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
            else:
                insert_result(alp_obj, results, benchmark \
                            , "{0}x{1}x{2}x{2}".format(16,format_size(_num_filters), format_size(_dom))\
                            , { _key : _exec_times[_key] for _key in _exec_times }\
                            , { _dev : { \
                                        "analysis" : _overheads[_dev]["analysis"],\
                                        "compilation" : _overheads[_dev]["compilation"] \
                                       }\
                                     for _dev in _overheads }\
                            , skip_vm, _vm_skipdom)
    return results


# Test for Syr2K
def test_syr2k(benchmark, alp_obj, results, domain, attempts, skip_vm=False, vm_cutoff=None):
    assert results != None, "Testing Jacobi -- output map is null"
    _start, _end, _mult = domain
    domains = [(_start * _mult**_i) for _i in range(_end)]
    devices = ["cpu", "gpu"] if skip_vm else ["interpreter", "cpu", "gpu"]

    for _dom in domains:
        _exec_times = {}
        _overheads = {}
        _vm_skipdom = False
        for _dev in devices:
            if _dev == "interpreter" and vm_cutoff != None and _dom > vm_cutoff :
                _vm_skipdom = True
                continue

            _exec_times[_dev] = []
            _overheads[_dev] = { "analysis" : [], "compilation" : [] }
            alp_obj.mod_opts.profile_dev = _dev
            for _attempt in range(attempts):
                _alpha, _beta = (1, 2)
                _A = gen2D_data(size=_dom, arr_type='float32')
                _B = gen2D_data(size=_dom, arr_type='float32')
                _C = gen2D_data(size=_dom, arr_type='float32')

                alp_obj.syr2k(_alpha, _beta, _C, _A, _B)
                _exec_times[_dev].append(alp_obj.mod_opts.timer.exec_time)
                _overheads[_dev]["analysis"].append(alp_obj.mod_opts.timer.analysis_time)
                _overheads[_dev]["compilation"].append(alp_obj.mod_opts.timer.compile_time)
        else:
            insert_result(alp_obj, results, benchmark \
                        , "{0} x {0}".format(format_size(_dom))\
                        , { _key : _exec_times[_key] for _key in _exec_times }\
                        , { _dev : { \
                                    "analysis" : _overheads[_dev]["analysis"],\
                                    "compilation" : _overheads[_dev]["compilation"] \
                                   }\
                                 for _dev in _overheads }\
                        , skip_vm, _vm_skipdom)
    return results


#####################################
# Initialise ALPyNA module and
# do Static Analaysis + IR generation
#####################################
def init_test_harness(filename):
    with open(filename, mode="r") as fd:
        _opts = flt_util.ALPyNA_Options()
        _opts.read_config()

        code = fd.read()
        _as_mod = parloop.static_analyse(code, _opts)
        return _as_mod
    return None


def save_results(file_name, results):
    with open(file_name, 'w') as fd:
        json.dump(results, fd, indent=2, sort_keys=True)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Usage : Please pass name of file on command-line")
        sys.exit(-1)

    if len(sys.argv) > 2:
        prof_file = sys.argv[2]
    else:
        time = dt.datetime.today()
        prof_file = 'alpyna_harness_{0}-{1:02d}-{2:02d}_{3:02d}{4:02d}.json'.format(
            time.year, time.month, time.day, time.hour, time.minute, time.second)

    filename = sys.argv[1]
    alp_obj = init_test_harness(filename)
    if alp_obj == None:
        print("Could not initialise ALPyNA for provided kernels")
        sys.exit(-1)

    if os.path.isfile(prof_file):
        with open(prof_file) as p_fd:
            results = json.load(p_fd)
    else:
        results = {}

    results = test_conway("conway", alp_obj, results, gen_range(16, 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_convolution2D(
        "convolution-2d", alp_obj, results, gen_range(16, 16 * 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_gemver("gemver", alp_obj, results, gen_range(8, 16 * 1024), 5, skip_vm=True, vm_cutoff=8*1024)
    save_results(prof_file, results)

    results = test_mandelbrot("mandelbrot", alp_obj, results, gen_range(8, 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_blackscholes(
        "black-scholes", alp_obj, results, gen_range(32 * 1024, 16 * 1024 * 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_saxpy(
        "saxpy", alp_obj, results, gen_range(1024, 32 * 1024 * 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_matmul("gemm", alp_obj, results, gen_range(16, 2 * 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_jacobi("jacobi", alp_obj, results, gen_range(4, 2 * 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_vector_add(
        "vadd", alp_obj, results, gen_range(1024, 16 * 1024 * 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_hilbert("hilbert", alp_obj, results, gen_range(16, 4 * 1024), 5, skip_vm=True)
    save_results(prof_file, results)

    results = test_fbcorr("fbcorr", alp_obj, results, gen_range(256, 1024), 5, skip_vm=True, vm_cutoff=16)
    save_results(prof_file, results)

    results = test_syr2k("syr2k", alp_obj, results, gen_range(8, 4 * 1024), 5, skip_vm=True, vm_cutoff=1024)
    save_results(prof_file, results)

    print("Final results = \n{0}".format(json.dumps(results, indent=2, sort_keys=True)))
