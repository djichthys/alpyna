#!/home/dejice/work/python-tutorial/ast-venv/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import weakref
import Utils as flt_util
import astor
import functools


# Global class to hold the status
# of our Analysis-Phases
# Note : Do not instantiate this class
class Analysis_Phase:
    def __init__(self):
        pass

    # These are global across entire program
    comp_stage_strings = ["aot", "runtime"]
    compilation_stages = {_key: _val for _val, _key in enumerate(comp_stage_strings)}
    curr_stg = compilation_stages["aot"]

    def get_curr_stg():
        return Analysis_Phase.curr_stg

    def get_curr_stg_sym():
        return Analysis_Phase.comp_stage_strings[Analysis_Phase.curr_stg]

    def transition_phase():
        Analysis_Phase.curr_stg += 1

    def get_next_stg():
        if Analysis_Phase.curr_stg < (len(Analysis_Phase.comp_stage_strings)-1):
            return Analysis_Phase.curr_stg + 1
        else : 
            return Analysis_Phase.curr_stg

    def get_next_stg_sym():
        if Analysis_Phase.curr_stg < (len(Analysis_Phase.comp_stage_strings)-1):
            return Analysis_Phase.comp_stage_strings[Analysis_Phase.curr_stg + 1]
        else : 
            return Analysis_Phase.comp_stage_strings[Analysis_Phase.curr_stg]


#Alias Analysis_Phase class to shorten code
phase = Analysis_Phase


class Peel_Iter:
    def __init__(self, peel_iters):
        self.peel_iters = peel_iters

    @property
    def peel_iters(self):
        return self._peel_iters

    @peel_iters.setter
    def peel_iters(self, iters):
        self._set_iters(iters)

    def _set_iters(self, iters):
        if getattr(self, "_peel_iters", None) == None:
            self._peel_iters = []

        if type(iters) is list:
            for _it in iters:
                self._set_iters(_it)
        else:
            self._peel_iters.append(iters)


class Split_Iter:
    def __init__(self, iteration):
        self.split_iter = iteration

    @property
    def split_iter(self):
        return self._split_iter

    @split_iter.setter
    def split_iter(self, loop_iter):
        self._split_iter = loop_iter


def dep_vector_expand(loop_list, dep_list):
    _expanded_list = dep_list
    for _col in range(0, len(loop_list)):
        _stale_entries = []
        for _row in _expanded_list:
            if _row[_col] != '*':
                continue

            _anchor = _expanded_list.index(_row)
            if _anchor >= (len(_expanded_list) - 1):
                _expanded_list.append(_row[0:_col] + ['<'] + _row[_col + 1:])
                _expanded_list.append(_row[0:_col] + ['='] + _row[_col + 1:])
                _expanded_list.append(_row[0:_col] + ['>'] + _row[_col + 1:])
            else:
                _expanded_list.insert(_anchor + 1, _row[0:_col] + ['<'] + _row[_col + 1:])
                _expanded_list.insert(_anchor + 2, _row[0:_col] + ['='] + _row[_col + 1:])
                _expanded_list.insert(_anchor + 3, _row[0:_col] + ['>'] + _row[_col + 1:])
            _stale_entries.append(_row)

        for _stale in _stale_entries:
            _expanded_list.remove(_stale)
    return _expanded_list


class Dependency:
    def __init__(self, enclosing_loops, direction=None, distance=None, dependent=None):
        self.enclosing_loops = enclosing_loops
        # initially set user-defined direction and expand recursively
        self.direction = direction
        self.direction = self._expand(self.direction)
        self.rdirection = None

        self.distance = distance
        self.rdistance = self.distance

        self.dependent = dependent
        self.rdependent = self.dependent

        self.brk_action = None
        self.rbrk_action = None
        self._recurrance_brk = {"split_loop": Split_Iter, "peel_loop": Peel_Iter}

    @property
    def dependent(self):
        return self._dependent

    @dependent.setter
    def dependent(self, val):
        if val in [ None , True , False ]:
            self._dependent = val
        else:
            self._dependent = None

    @property
    def rdependent(self):
        return self._rdependent

    @rdependent.setter
    def rdependent(self, val):
        if val in [ None , True , False ]:
            self._rdependent = val
        else:
            self._rdependent = None

    @property
    def distance(self):
        return self._distance

    @distance.setter
    def distance(self, dist):
        self._distance = dist

    @property
    def rdistance(self):
        return self._rdistance

    @rdistance.setter
    def rdistance(self, dist):
        self._rdistance = dist


    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, _direction):
        if _direction is None:
            self._direction = [['*'] * len(self.enclosing_loops)]
        elif type(_direction) is list:
            if len(_direction) > 0:
                if type(_direction[0]) is list:
                    self._direction = _direction
                else:
                    self._direction = [_direction]
            else:
                self._direction = [['*'] * len(self.enclosing_loops)]
        else:
            self._direction = [[_direction] * len(self.enclosing_loops)]


    @property
    def rdirection(self):
        return self._rdirection

    @rdirection.setter
    def rdirection(self, _direction):
        if _direction is None:
            self._rdirection = [['*'] * len(self.enclosing_loops)]
        elif type(_direction) is list:
            if len(_direction) > 0:
                if type(_direction[0]) is list:
                    self._rdirection = _direction
                else:
                    self._rdirection = [_direction]
            else:
                self._rdirection = [['*'] * len(self.enclosing_loops)]
        else:
            self._rdirection = [[_direction] * len(self.enclosing_loops)]

    @property
    def brk_action(self):
        return self._brk_action

    @brk_action.setter
    def brk_action(self, act):
        if getattr(self, "_brk_action", None) == None:
            self._brk_action = []

        if act == None:
            self._brk_action = []
            return

        _action, _val = act
        if _action in self._recurrance_brk.keys():
            self._brk_action = [(_action, _recurrance_brk[_action](_val))]


    @property
    def rbrk_action(self):
        return self._rbrk_action

    @rbrk_action.setter
    def rbrk_action(self, act):
        if getattr(self, "_rbrk_action", None) == None:
            self._rbrk_action = []

        if act == None:
            self._rbrk_action = []
            return

        _action, _val = act
        if _action in self._recurrance_brk.keys():
            self._rbrk_action = [(_action, _recurrance_brk[_action](_val))]


    def _expand(self, dep_list):
        return dep_vector_expand(self.enclosing_loops, dep_list)

    def break_loop(self, act):
        if type(act) is list:
            for _it in act:
                self.break_loop(_it)
        else:
            _action, _val = act
            if _action in self._recurrance_brk.keys():
                self.brk_action.append((_action, self._recurrance_brk[_action](_val)))


    def rbreak_loop(self, act):
        if type(act) is list:
            for _it in act:
                self.rbreak_loop(_it)
        else:
            _action, _val = act
            if _action in self._recurrance_brk.keys():
                self.rbrk_action.append((_action, self._recurrance_brk[_action](_val)))


    def clear_dir(self):
        self.direction = []

    def remove_dir(self, vdir):
        if type(vdir) is list:
            for _dir in vdir:
                self.direction.remove(_dir)

    def clear_rdir(self):
        self.rdirection = []

    def remove_rdir(self, vdir):
        if type(vdir) is list:
            for _dir in vdir:
                self.rdirection.remove(_dir)


class Dependency_Solver:
    def __init__(self, sub_pair_group, resolution_stage):
        self.s_pair_grp = sub_pair_group
        # the stage at which the solver can resolve the expression
        self.resolution_stage = resolution_stage
        self.dep = Dependency(self._curate_loops(self.s_pair_grp))

    def _prepare_loops(self, _grp):
        _lloops = _grp.pairs[0]().sub_a.subscripted_var.index_list
        _rloops = _grp.pairs[0]().sub_b.subscripted_var.index_list
        return prepare_loop_pairs(_lloops, _rloops)

    def _curate_loops(self, sgrp):
        _sub_loop_set = []
        _vloops, _div_idx = self._prepare_loops(sgrp)
        for _loop in sgrp.common_loops:
            #print("DJ Debug -- s-grp-common-loop =  {0}".format( _djlist(_loop)))
            _idx, _obj = dep_vector_idx(_vloops, _div_idx, _loop)
            if _idx is not None and _obj not in _sub_loop_set:
            #    print("   -- DJ Debug -- Chose s-grp-common-loop = {0}, chosen {1}".format( \
            #                _djlist(_loop), _djlist(_obj)))
                _sub_loop_set.append(_obj)
        return _sub_loop_set

    @property
    def s_pair_grp(self):
        return self._subscript_pair_grp()

    @s_pair_grp.setter
    def s_pair_grp(self, grp):
        self._subscript_pair_grp = weakref.ref(grp)

    @property
    def dep(self):
        return self._dep

    @dep.setter
    def dep(self, _dependency):
        if _dependency == None or type(_dependency) is Dependency:
            self._dep = _dependency
        else:
            self._dep = None

    @property
    def resolution_stage(self):
        return (self._rstage, phase.compilation_stages[self._rstage])

    @resolution_stage.setter
    def resolution_stage(self, _stage):
        if _stage in phase.compilation_stages.keys():
            self._rstage = _stage

    @property
    def div_idx(self):
        return self._div_idx

    @div_idx.setter
    def div_idx(self, val):
        self._div_idx = val

    def _find_idx(self, _var, _sub):
        _idx = []
        for _idx_var in _sub.keys():
            if _idx_var == '__Constant__':
                continue

            for _idx_obj in reversed(_var.index_list):
                if _idx_obj.ntype == 'For' and _idx_var in _idx_obj.record["for_targets"].target:
                    _idx.append((_idx_var, _idx_obj.record["for_targets"].target,
                                 _idx_obj.record["for_limits"]))
        return _idx

    # function assumes that loops have been normalized
    def _get_upper_bound(self, var, sub):
        idx_lst = self._find_idx(var, sub)
        _ub = [_upper_bound for (_idx, _idx_tgt, _upper_bound) in idx_lst]

        print(" > range list = {0}".format([_u.limits for _u in _ub]))
        return _ub


class ZIV(Dependency_Solver):
    def __init__(self, sub_pair, resolution_stage):
        super().__init__(sub_pair, resolution_stage)
        self._aot_setup()

    def _aot_setup(self):
        self.lhs_var = self.s_pair_grp.pairs[0]().sub_a.subscripted_var.var_ref["var"]
        self.lhs_sub = self.s_pair_grp.pairs[0]().sub_a.subscripted_var.var_ref["axes"][
            self.s_pair_grp.pairs[0]().sub_a.subscript_ref]
        self.rhs_var = self.s_pair_grp.pairs[0]().sub_b.subscripted_var.var_ref["var"]
        self.rhs_sub = self.s_pair_grp.pairs[0]().sub_b.subscripted_var.var_ref["axes"][
            self.s_pair_grp.pairs[0]().sub_b.subscript_ref]

    def test(self, runtime_args=None):
        if phase.get_curr_stg_sym() == "aot" :
            return self._aot_test()
        else :
            return self._runtime_test( runtime_args )
        
    def _runtime_test(self, runtime_args ):
        if self.dep.dependent != None :
            self.dep.rdependent = self.dep.dependent  # Already resolved at compile-time
            return self.dep.rdependent  # Already resolved at compile-time

        #presence of loop-invariant vars in subscript
        _lhs_const = self.lhs_sub['__Constant__']
        for _key in self.lhs_sub.keys() :
            if _key != '__Constant__' :
                _lhs_const += (getattr(runtime_args, _key, 0) * self.lhs_sub[_key])

        _rhs_const = self.rhs_sub['__Constant__']
        for _key in self.rhs_sub.keys() :
            if _key != '__Constant__' :
                _rhs_const += (getattr(runtime_args, _key, 0) * self.rhs_sub[_key])

        if _lhs_const != _rhs_const :
            self.dep.rdependent = False
        else :
            self.dep.rdependent = True
        return self.dep.rdependent

    def _aot_test(self):
        # check if any key mismatch is present
        if len(self.lhs_sub.keys()) != len(self.rhs_sub.keys()):
            self.resolution_stage = "runtime"
            return None

        _ziv_cmp = [
            key for key in self.lhs_sub.keys() if key != '__Constant__' and (
                key not in self.rhs_sub.keys() or self.lhs_sub[key] != self.rhs_sub[key])
        ]
        _ziv_cmp += [
            key for key in self.rhs_sub.keys()
            if key != '__Constant__' and key not in self.lhs_sub.keys()
        ]

        if len(_ziv_cmp) > 0:
            self.resolution_stage = "runtime"
            return None
        else:
            self.resolution_stage = "aot"
            if  self.lhs_sub['__Constant__'] != self.rhs_sub['__Constant__']:
                self.dep.clear_dir()
                self.dep.dependent = False
            else :
                self.dep.dependent = True

        return self.dep.dependent

    def display(self):
        print(
            " >> ZIV Sub-var = {0}[{1}] <--> {2}[{3} :: res_stage = {4} , direction = '{5}', distance = {6} , dependent = {7}, rdirection = {8} , rdistance = {9} , rdependent = {10}".
            format(self.lhs_var, self.lhs_sub, self.rhs_var, self.rhs_sub, self.resolution_stage,
                   self.dep.direction, self.dep.distance, self.dep.dependent, self.dep.rdirection, self.dep.rdistance, self.dep.rdependent))


class SIV(Dependency_Solver):
    def __init__(self, sub_pair, resolution_stage):
        super().__init__(sub_pair, resolution_stage)
        self._aot_setup()

    @property 
    def rsiv_test(self):
        return self._rsiv_test

    @rsiv_test.setter
    def rsiv_test(self, siv_function):
        self._rsiv_test = siv_function

    def _aot_setup(self):
        self.var_lhs = self.s_pair_grp.pairs[0]().sub_a.subscripted_var
        self.sub_lhs = self.var_lhs.var_ref["axes"][self.s_pair_grp.pairs[0]().sub_a.subscript_ref]
        self.var_rhs = self.s_pair_grp.pairs[0]().sub_b.subscripted_var
        self.sub_rhs = self.var_rhs.var_ref["axes"][self.s_pair_grp.pairs[0]().sub_b.subscript_ref]

    def _non_induction_presence(self, for_hdr_lhs, for_hdr_rhs):
        ret = False
        _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
        _lst = [key for key in self.sub_lhs.keys()] + [key for key in self.sub_rhs.keys()]
        for key in _lst:
            if key == '__Constant__':
                continue
            if key != _idx_lhs and self.sub_lhs[_idx_lhs] != 0:
                ret = True
                break
            if key != _idx_rhs and self.sub_rhs[_idx_rhs] != 0:
                ret = True
                break

        return ret

    def _non_induction_acc(self, for_hdr_lhs, for_hdr_rhs, runtime_args):
        _lhs_const =  _rhs_const = 0 
        _idx_lhs = _idx_rhs = None

        if for_hdr_lhs : 
            _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        if for_hdr_rhs : 
            _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs

        for _key in self.sub_lhs.keys():
            if _key == '__Constant__':
                _lhs_const += self.sub_lhs[_key]
            elif _key != None and _key != _idx_lhs :
                _lhs_const += (getattr(runtime_args,_key,0) * self.sub_lhs[_key])

        for _key in self.sub_rhs.keys():
            if _key == '__Constant__':
                _rhs_const += self.sub_rhs[_key]
            elif _key != None and _key != _idx_rhs :
                _rhs_const += (getattr(runtime_args,_key,0) * self.sub_rhs[_key])

        return (_lhs_const, _rhs_const)
    


    # Returns False if we are at runtime evaluation because
    # we expect the values to have been plugged into
    # the non-induction variables and deduced
    # If any variables in the list of bounds are to be lazily evaluated
    # then return True
    def _aot_assert_bounds(self, lbound):
        ret = False
        if phase.get_curr_stg() > phase.compilation_stages['aot']:
            return ret

        for _k in lbound.limits.keys():
            _usable, _var = lbound.limits[_k]
            if _usable == 'lazy':
                ret = True
                break
        return ret

    def _aot_strong_SIV(self, for_hdr_lhs, for_hdr_rhs):
        _ret = None
        #print("Calling strong SIV on ::::: ")
        #self.display()
        if self._non_induction_presence(for_hdr_lhs, for_hdr_rhs):
            self.resolution_stage = "runtime"
            return None

        _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
        #print("Index used in Srong SIV = {0} (obj={1}) limit={2} <<<>>>> {3}(obj = {4}, limit={5})".format(_idx_lhs , idx_obj_lhs , _ub_lhs ,  _idx_rhs , idx_obj_rhs, _ub_rhs ))

        if self._aot_assert_bounds(_ub_lhs) or self._aot_assert_bounds(_ub_rhs):
            self.resolution_stage = "runtime"
            return None

        _delta = (
            self.sub_lhs['__Constant__'] - self.sub_rhs['__Constant__']) // self.sub_lhs[_idx_lhs]
        _abs_delta = _delta
        if _abs_delta < 0:
            _abs_delta *= -1

        _rem = (
            self.sub_lhs['__Constant__'] - self.sub_rhs['__Constant__']) % self.sub_lhs[_idx_lhs]
        if _rem:
            self.dep.clear_dir()
            _ret = self.dep.dependent = False
            return _ret    # non-integral point cannot have a dependency within the iteration space

        # check whether distance of memory access lies within iteration space
        _lazy_end, _end = _ub_lhs.limits['end']
        _lazy_start, _start = _ub_lhs.limits['start']
        self.dep.distance = _delta
        if self.dep.distance < 0:
            self.dep.remove_dir([['<'], ['=']])
        elif self.dep.distance > 0:
            self.dep.remove_dir([['>'], ['=']])
        else:
            self.dep.remove_dir([['<'], ['>']])

        if _abs_delta < _end - _start:
            self.dep.dependent = _ret = True
        else:
            self.dep.dependent = _ret = False
            self.dep.clear_dir()

        return _ret

    def _aot_weak_crossing_SIV(self, for_hdr_lhs, for_hdr_rhs):
        #print("Calling weak-crossing-SIV on ::::: ")
        #self.display()
        if self._non_induction_presence(for_hdr_lhs, for_hdr_rhs):
            self.resolution_stage = "runtime"
            return None

        _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
        #print("Index used in Weak Crossing SIV = {0} (obj={1}) limit={2} <<<>>>> {3}(obj = {4}, limit={5})".format(_idx_lhs , idx_obj_lhs , _ub_lhs ,  _idx_rhs , idx_obj_rhs, _ub_rhs ))

        if self._aot_assert_bounds(_ub_lhs) or self._aot_assert_bounds(_ub_rhs):
            self.resolution_stage = "runtime"
            return None

        _denom = self.sub_lhs[_idx_lhs] - self.sub_rhs[_idx_rhs]
        _delta = self.sub_rhs['__Constant__'] - self.sub_lhs['__Constant__']

        # If the delta is zero, we might still have a dependence
        # at the zeroe'th memory location
        if _delta == 0:
            self.dep.remove_dir([['<'], ['>']])
            self.dep.break_loop(("peel_loop", _delta))
            self.dep.distance = _delta
            self.dep.dependent = True
            return self.dep.dependent

        # if the resulting intersection is at a -ve loop index - no dependence
        if (_delta > 0 and _denom < 0) or (_delta < 0 and _denom > 0):
            self.dep.dependent = False
            self.dep.clear_dir()
            return self.dep.dependent

        # the intersection is at a positive value.
        # 1. check if  cross-over is outside limits
        # 2. check if cross-over is at either upper or lower bounds
        # 3. if c2-c1 % a has no remainder, then c2-c1 / 2a will lie
        #    either on an integral point or mid-way through an integral point
        _crossover = _delta // _denom
        _lazy_strt, _strt = _ub_lhs.limits["start"]
        _lazy_end, _end = _ub_lhs.limits["end"]
        if _crossover < _strt or _crossover >= _end:
            self.dep.dependent = False
            self.dep.clear_dir()
            return self.dep.dependent

        if _crossover == _strt:
            self.dep.dependent = True
            self.dep.remove_dir([['<'], ['>']])
            self.dep.distance = 0
            self.dep.break_loop(("peel_loop", _crossover))
            return self.dep.dependent
        elif _crossover == (_end - 1):
            self.dep.dependent = True
            self.dep.remove_dir([['<'], ['>']])
            self.dep.distance = 0
            self.dep.break_loop(("peel_loop", _crossover))
            return self.dep.dependent

        # 3. if c2-c1 % a has no remainder, then c2-c1 / 2a will lie
        #    either on an integral point or mid-way through an integral point
        _coeff = self.sub_lhs[_idx_lhs]
        if _coeff < 0:
            _coeff *= -1

        if _delta % _coeff != 0:
            _ret = self.dep.dependent = False
            self.dep.clear_dir()
            return _ret
        else:
            _ret = self.dep.dependent = True
            if _delta % (2 * _coeff) != 0:    #
                self.dep.break_loop(("split_loop", _crossover + 1))
                self.dep.remove_dir([['=']])
            else:
                self.dep.break_loop([("split_loop", _crossover), ("peel_loop", _crossover)])

        self.dep.dependent = True
        return self.dep.dependent

    def _weak_zero_non_induction_presence(self, for_hdr_lhs, for_hdr_rhs):
        def _wz_check(for_hdr, idx_var):
            _ret = False
            _idx, idx_obj, _ub = for_hdr
            if idx_var != _idx:
                _ret = True
            return _ret

        _ret = False
        _lst = [key for key in self.sub_lhs.keys()] + [key for key in self.sub_rhs.keys()]
        for key in _lst:
            if key == '__Constant__':
                continue

            if for_hdr_lhs and not for_hdr_rhs:
                _ret = _wz_check(for_hdr_lhs, key)
                if _ret:
                    break
            elif not for_hdr_lhs and for_hdr_rhs:
                _ret = _wz_check(for_hdr_rhs, key)
                if _ret:
                    break
        return _ret

    # used to test subscript equations of the form
    # c1 + a*i = c2 ->  i = c2 - c1 / a
    # c1 = c2 + a*i ->  i = c1 - c2 / a
    # in either case :
    #   if i is non-integral -> no dependency
    #   if i not in loop-bounds -> no dependency
    #   if i = lower-bound  -> peel lower bound
    #       if type of weak zero is c1 + a*i = c2 -> dependency is [ < = ]
    #       if type of weak zero is c1 = c2 + a*i -> dependency is [ > = ]
    #   if i = upper-bound  -> peel upper bound
    #       if type of weak zero is c1 + a*i = c2 -> dependency is [ > = ]
    #       if type of weak zero is c1 = c2 + a*i -> dependency is [ < = ]
    #   any other case the dependence i '*'
    def _aot_weak_zero_SIV(self, for_hdr_lhs, for_hdr_rhs):
        def _weak_zero_priv(a_coeff, c1, c2, loop_bounds, loop_carried_dep, idx_position="lhs"):
            _idx, idx_obj, _ub = loop_bounds
            if self._aot_assert_bounds(_ub):
                self.resolution_stage = "runtime"
                return None

            _imm_start, _ub_start = _ub.limits["start"]
            _imm_end, _ub_end = _ub.limits["end"]

            if idx_position == 'rhs':
                _delta = c1 - c2
            else:
                _delta = c2 - c1

            abs_coeff = a_coeff
            if a_coeff < 0:
                abs_coeff *= -1

            # if delta = 0 ... then there is a dependence when i = 0
            if _delta == 0 and _ub_start == 0:
                self.dep.dependent = True
                self.dep.distance = 0
                self.dep.break_loop(("peel_loop", _ub_start))
                if not loop_carried_dep:
                    self.dep.remove_dir([['<'], ['>']])    # dependency is (=)
                elif idx_position == 'rhs':
                    self.dep.remove_dir([['<']])    # dependency is (= >)
                else:
                    self.dep.remove_dir([['>']])    # dependency is (= <)
                return self.dep.dependent

            if a_coeff < 0:
                _delta *= -1

            if _delta > (abs_coeff * (_ub_end - 1)):
                self.dep.clear_dir()
                self.dep.dependent = False
                return self.dep.dependent
            elif _delta == (abs_coeff * (_ub_end - 1)):
                self.dep.break_loop(("peel_loop", _ub_end))
                self.dep.dependent = True
                self.dep.distance = 0
                if not loop_carried_dep:
                    self.dep.remove_dir([['<'], ['>']])    # dependency is (=)
                elif idx_position == 'rhs':
                    self.dep.remove_dir([['>']])    # dependency is (= <)
                else:
                    self.dep.remove_dir([['<']])    # dependency is (= >)
                return self.dep.dependent

            if _delta < 0:
                self.dep.clear_dir()
                self.dep.dependent = False
                return self.dep.dependent

            if _delta > 0 and (_delta % a_coeff) != 0:
                self.dep.clear_dir()
                self.dep.dependent = False
            else:
                self.dep.dependent = True

            return self.dep.dependent

        #print("Calling weak Zero SIV on ::::: ")
        #self.display()
        _ret = None
        if self._weak_zero_non_induction_presence(for_hdr_lhs, for_hdr_rhs):
            self.resolution_stage = "runtime"
            return None

        # Check if the index var-binding corresponds to a shared loop or a private-loop
        # If a private-loop then there only exists a loop-independent dependence
        _loop_carried_dep = lambda _lim, _var_bind : \
                True if _lim in [ _rec.record["for_limits"] for _rec in _var_bind.index_list ]\
                else False

        if for_hdr_lhs:
            _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
            _ret = _weak_zero_priv(self.sub_lhs[_idx_lhs], self.sub_lhs["__Constant__"],
                                   self.sub_rhs["__Constant__"], for_hdr_lhs,
                                   _loop_carried_dep(_ub_lhs, self.var_rhs), "lhs")
        elif for_hdr_rhs:
            _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
            _ret = _weak_zero_priv(self.sub_rhs[_idx_rhs], self.sub_lhs["__Constant__"],
                                   self.sub_rhs["__Constant__"], for_hdr_rhs,
                                   _loop_carried_dep(_ub_rhs, self.var_lhs), "rhs")
        else:
            print("Parse Error : weak-zero SIV : both subscripts do not have relevant indices")

        return _ret

    #very simple gcd test for SIVs
    def _calc_gcd(self, a, b):
        if b == 0:
            return a
        return self._calc_gcd(b, a % b)

    def _aot_exact_SIV(self, for_hdr_lhs, for_hdr_rhs):
        # solve for exact SIV. Potentially run-time heavy
        #print("Calling exact SIV on ::::: ")
        #self.display()

        if self._non_induction_presence(for_hdr_lhs, for_hdr_rhs):
            self.resolution_stage = "runtime"
            return _ret

        _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
        #print("Index used in exact SIV = {0} (obj={1}) limit={2} <<<>>>> {3}(obj = {4}, limit={5})".format(_idx_lhs , idx_obj_lhs , _ub_lhs ,  _idx_rhs , idx_obj_rhs, _ub_rhs ))

        if self._aot_assert_bounds(_ub_lhs) or self._aot_assert_bounds(_ub_rhs):
            self.resolution_stage = "runtime"
            return None

        _delta = self.sub_rhs["__Constant__"] - self.sub_lhs["__Constant__"]

        _abs = lambda x: x if x > 0 else x * -1
        _abs_delta = _abs(_delta)
        _lhs_coeff = _abs(self.sub_rhs[_idx_lhs])
        _rhs_coeff = _abs(self.sub_lhs[_idx_rhs])

        _gcd = self._calc_gcd(_lhs_coeff, _rhs_coeff)
        _rem = _abs_delta % _gcd
        if _gcd < 1 or _rem > 0:
            self.dep.clear_dir()
            self.dep.dependent = False
            return self.dep.dependent
        else:
            self.dep.dependent = True

        return self.dep.dependent

    # Dependency test of form arr[a*i + c1] <--> arr[a*i + c2]
    # where i is the induction variable and c1,c2 & a are integers and
    def _aot_test(self):
        # check if both of them are constants
        #print(" > SIV Sub-var = {0}<{1}> <--> {2}[{3}]".format( self.var_lhs.var_ref["var"] , self.sub_lhs , self.var_rhs.var_ref["var"] , self.sub_rhs ))
        filter_idx = lambda _lst: _lst[0] if len(_lst) > 0 else None
        _idx1_lst = self._find_idx(self.var_lhs, self.sub_lhs)
        _idx1 = filter_idx(_idx1_lst)
        _idx2_lst = self._find_idx(self.var_rhs, self.sub_rhs)
        _idx2 = filter_idx(_idx2_lst)

        _siv_test, test_ty = self.select_SIV_test(_idx1, _idx2)
        if _siv_test == None:
            return None

        self.rsiv_test = getattr(self, "_runtime_" + test_ty, None)
        return _siv_test(_idx1, _idx2)

    def select_SIV_test(self, for_hdr_lhs, for_hdr_rhs):
        # This condition is never reached, because either _u1 -or _u2 exists for SIV
        if (for_hdr_lhs, for_hdr_rhs) == (None, None):
            self.resolution_stage = "runtime"
            print("Parsing Error, Abort")
            return None
        elif for_hdr_lhs != None and for_hdr_rhs != None:
            _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
            _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
            try:
                if self.sub_lhs[_idx_lhs] == self.sub_rhs[_idx_rhs]:
                    ret = (self._aot_strong_SIV, "strong_SIV")
                elif self.sub_lhs[_idx_lhs] == 0 or self.sub_rhs[_idx_rhs] == 0:
                    ret = (self._aot_weak_zero_SIV, "_weak_zero_SIV")
                elif self.sub_lhs[_idx_lhs] == -1 * self.sub_rhs[_idx_rhs]:
                    ret = (self._aot_weak_crossing_SIV, "weak_crossing_SIV")
                else:
                    ret = (self._aot_exact_SIV, "exact_SIV")
            except KeyError as _excpt:
                ret = (self._aot_weak_zero_SIV, "weak_zero_SIV")
        else:
            ret = (self._aot_weak_zero_SIV, "weak_zero_SIV")

        return ret


    # Dependency test of form arr[a*i + c1] <--> arr[a*i + c2]
    # where i is the induction variable and c1,c2 & a are integers and
    def _runtime_test(self, runtime_args):
        if self.dep.dependent != None :
            self.dep.rdependent = self.dep.dependent  # Already resolved at compile-time
            self.dep.rdirection = self.dep.direction  # Already resolved at compile-time
            self.dep.rdistance = self.dep.distance  # Already resolved at compile-time
            return self.dep.rdependent  # Already resolved at compile-time

        filter_idx = lambda _lst: _lst[0] if len(_lst) > 0 else None
        _idx1_lst = self._find_idx(self.var_lhs, self.sub_lhs)
        _idx1 = filter_idx(_idx1_lst)
        _idx2_lst = self._find_idx(self.var_rhs, self.sub_rhs)
        _idx2 = filter_idx(_idx2_lst)

        self.dep.rdependent = None
        self.dep.rdirection = None
        self.dep.rdirection = dep_vector_expand(self.dep.enclosing_loops, self.dep.rdirection)
        self.dep.rbrk_action = None
        return self.rsiv_test(_idx1, _idx2, runtime_args)

    def _runtime_strong_SIV(self, for_hdr_lhs, for_hdr_rhs, runtime_args):
        _ret = True
        _lhs_const, _rhs_const = self._non_induction_acc(for_hdr_lhs, for_hdr_rhs, runtime_args)
        _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
      

        _delta = ( _lhs_const - _rhs_const ) // self.sub_lhs[_idx_lhs]
        _abs_delta = _delta
        if _abs_delta < 0:
            _abs_delta *= -1

        _rem = ( _lhs_const - _rhs_const ) % self.sub_lhs[_idx_lhs]
        if _rem :
            self.dep.clear_rdir()
            self.dep.rdependent = False
            return self.dep.rdependent # non-integral point cannot have a dependency within the iteration space


        _lazy_end, _end = _ub_lhs.limits['end']
        _lazy_start, _start = _ub_lhs.limits['start']
        if _lazy_end == 'lazy': 
            _end = getattr( runtime_args , _end , None )

        if _lazy_start == 'lazy': 
            _start = getattr( runtime_args , _start , None )

        self.dep.rdistance = _delta
        if self.dep.rdistance < 0 :
            self.dep.remove_rdir([['<'], ['=']])
        elif self.dep.rdistance > 0:
            self.dep.remove_rdir([['>'], ['=']])
        else:
            self.dep.remove_rdir([['<'], ['>']])

        
        if _abs_delta < _end - _start:
            self.dep.rdependent = _ret = True
        else:
            self.dep.rdependent = _ret = False
            self.dep.clear_rdir()
        return _ret


    # used to test subscript equations of the form
    # c1 + a*i = c2 ->  i = c2 - c1 / a
    # c1 = c2 + a*i ->  i = c1 - c2 / a
    # in either case :
    #   if i is non-integral -> no dependency
    #   if i not in loop-bounds -> no dependency
    #   if i = lower-bound  -> peel lower bound
    #       if type of weak zero is c1 + a*i = c2 -> dependency is [ < = ]
    #       if type of weak zero is c1 = c2 + a*i -> dependency is [ > = ]
    #   if i = upper-bound  -> peel upper bound
    #       if type of weak zero is c1 + a*i = c2 -> dependency is [ > = ]
    #       if type of weak zero is c1 = c2 + a*i -> dependency is [ < = ]
    #   any other case the dependence i '*'
    def _runtime_weak_zero_SIV(self, for_hdr_lhs, for_hdr_rhs, runtime_args):
        # Closure to call with subscript containing relevant loop index
        def _weak_zero_priv(a_coeff, c1, c2, loop_bounds, loop_carried_dep, idx_position="lhs"):
            nonlocal runtime_args
            _idx, idx_obj, _ub = loop_bounds
            _lazy_end, _end = _ub.limits['end']
            _lazy_start, _start = _ub.limits['start']
            if _lazy_end == 'lazy':
                _end = getattr(runtime_args, _end, None)

            if _lazy_start == 'lazy':
                _start = getattr(runtime_args, _start, None)

            if idx_position == 'rhs':
                _delta = c1 - c2
            else:
                _delta = c2 - c1

            abs_coeff = a_coeff
            if a_coeff < 0:
                abs_coeff *= -1

            # if delta == 0 then there is a dependence when  i = 0
            if _delta == 0 and _start == 0:
                self.dep.rdependent = True
                self.dep.rdistance = 0
                self.dep.rbreak_loop(("peel_loop", _start))
                if not loop_carried_dep:
                    self.dep.remove_rdir([['<'], ['>']])    # dependency is (=)
                elif idx_position == "lhs":
                    self.dep.remove_rdir([['>']])    # dependency is (= < )
                else:
                    self.dep.remove_rdir([['<']])    # dependency is (= >)
                return self.dep.rdependent

            if a_coeff < 0:
                _delta *= -1

            if _delta > (abs_coeff * (_ub_end - 1)):
                self.dep.clear_rdir()
                self.dep.rdependent = False
                return self.dep.rdependent
            elif _delta == (abs_coeff * (_ub_end - 1)):
                self.dep.rbreak_loop(("peel_loop", _end - 1))
                self.dep.rdependent = True
                self.dep.rdistance = 0
                if not loop_carried_dep:
                    self.dep.remove_rdir([['<'], ['>']])    # dependency is (=)
                elif idx_position == "lhs":
                    self.dep.remove_rdir([['<']])    # dependency is (= > )
                else:
                    self.dep.remove_rdir([['>']])    # dependency is (= <)
                return self.dep.rdependent

            if _delta < 0:
                self.dep.clear_rdir()
                self.dep.rdependent = False
                return self.dep.rdependent

            if _delta > 0 and (_delta % a_coeff) != 0:
                self.dep.clear_rdir()
                self.dep.rdependent = False
            else:
                self.dep.rdependent = True

            return self.dep.rdependent

        # Accummulate loop-invariant expressions to pass into closure
        _lhs_const, _rhs_const = self._non_induction_acc(for_hdr_lhs, for_hdr_rhs, runtime_args)

        # Check if the index var-binding corresponds to a shared loop or a private-loop
        # If a private-loop then there only exists a loop-independent dependence
        _loop_carried_dep = lambda _lim, _var_bind : \
                True if _lim in [ _rec.record["for_limits"] for _rec in _var_bind.index_list ]\
                else False

        # Resolve which side of the expression the
        # const and varying memory access lines lies on
        if for_hdr_lhs:
            _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
            _ret = _weak_zero_priv(self.sub_lhs[_idx_lhs], _lhs_const, _rhs_const, for_hdr_lhs,
                                   _loop_carried_dep(_ub_lhs, self.var_rhs), "lhs")
        elif for_hdr_rhs:
            _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs
            _ret = _weak_zero_priv(self.sub_rhs[_idx_rhs], _lhs_const, _rhs_const, for_hdr_rhs,
                                   _loop_carried_dep(_ub_rhs, self.var_lhs), "rhs")
        return _ret

    def _runtime_weak_crossing_SIV(self, for_hdr_lhs, for_hdr_rhs, runtime_args):
        _ret = True
        # Accummulate loop-invariant expressions to pass into closure
        _lhs_const, _rhs_const = self._non_induction_acc(for_hdr_lhs, for_hdr_rhs, runtime_args)

        _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs

        _denom = self.sub_lhs[_idx_lhs] - self.sub_rhs[_idx_rhs]
        _delta = _rhs_const - _lhs_const

        # If the delta is zero, we might still have a dependence
        # at the zeroe'th memory location
        if _delta == 0:
            self.dep.remove_rdir([['<'], ['>']])
            self.dep.rbreak_loop(("peel_loop", _delta))
            self.dep.rdistance = _delta
            self.dep.rdependent = True
            return self.dep.rdependent

        # if the resulting intersection is at a -ve loop index - no dependence
        if (_delta > 0 and _denom < 0) or (_delta < 0 and _denom > 0):
            self.dep.rdependent = False
            self.dep.clear_rdir()
            return self.dep.rdependent

        # the intersection is at a positive value.
        # 1. check if  cross-over is outside limits
        # 2. check if cross-over is at either upper or lower bounds
        # 3. if c2-c1 % a has no remainder, then c2-c1 / 2a will lie
        #    either on an integral point or mid-way through an integral point
        _lazy_start, _start = _ub_lhs.limits["start"]
        if _lazy_start == 'lazy':
            _start = getattr( runtime_args , _start , None )

        _lazy_end, _end = _ub_lhs.limits["end"]
        if _lazy_end == 'lazy':
            _end = getattr( runtime_args , _end , None )

        _crossover = _delta // _denom
        if _crossover < _start or _crossover >= _end:
            self.dep.rdependent = False
            self.dep.clear_rdir()
            return self.dep.rdependent

        if _crossover == _start:
            self.dep.rdependent = True
            self.dep.remove_rdir([['<'], ['>']])
            self.dep.rdistance = 0
            self.dep.rbreak_loop(("peel_loop", _crossover))
            return self.dep.rdependent
        elif _crossover == (_end - 1):
            self.dep.rdependent = True
            self.dep.remove_rdir([['<'], ['>']])
            self.dep.rdistance = 0
            self.dep.rbreak_loop(("peel_loop", _crossover))
            return self.dep.rdependent


        # 3. if c2-c1 % a has no remainder, then c2-c1 / 2a will lie
        #    either on an integral point or mid-way through an integral point
        _coeff = self.sub_lhs[_idx_lhs]
        if _coeff < 0:
            _coeff *= -1

        if (_delta % _coeff) != 0:
            _ret = self.dep.rdependent = False
            self.dep.clear_rdir()
        else:
            _ret = self.dep.rdependent = True
            if _delta % (2 * _coeff) != 0:    #
                self.dep.rbreak_loop(("split_loop", _crossover + 1))
                self.dep.remove_rdir([['=']])
            else:
                self.dep.rbreak_loop([("split_loop", _crossover), ("peel_loop", _crossover)])

        return self.dep.rdependent

    def _runtime_exact_SIV(self, for_hdr_lhs, for_hdr_rhs, runtime_args):
        _lhs_const, _rhs_const = self._non_induction_acc(for_hdr_lhs, for_hdr_rhs, runtime_args)
        _idx_lhs, idx_obj_lhs, _ub_lhs = for_hdr_lhs
        _idx_rhs, idx_obj_rhs, _ub_rhs = for_hdr_rhs

        _delta = self.sub_rhs["__Constant__"] - self.sub_lhs["__Constant__"]

        _abs = lambda x: x if x > 0 else x * -1
        _abs_delta = _abs(_delta)

        _lhs_coeff = _abs(self.sub_rhs[_idx_lhs])
        _rhs_coeff = _abs(self.sub_lhs[_idx_rhs])

        _gcd = self._calc_gcd(_lhs_coeff, _rhs_coeff)
        _rem = _abs_delta % _gcd
        if _gcd < 1 or _rem > 0:
            self.dep.clear_rdir()
            self.dep.rdependent = False
        else:
            self.dep.rdependent = True
        return self.dep.rdependent

    def display(self):
        _act_display = lambda s, x: x.split_iter if s == "split_loop" else x.peel_iters

        print(
            " >> SIV Sub-var = {0}[{1}] <--> {2}[{3} :: res_stage = {4} , direction = '{5}', distance = {6} , dependent = {7}, rdirection = {8} , rdistance = {9} , rdependent = {10}".
            format(self.var_lhs, self.sub_lhs, self.var_rhs, self.sub_rhs, self.resolution_stage,
                   self.dep.direction, self.dep.distance, self.dep.dependent , self.dep.rdirection, self.dep.rdistance, self.dep.rdependent), end=" ")

        if self.dep.brk_action:
            print("aot-actions = {0}".format(
                [(_str, _act_display(_str, _act)) for (_str, _act) in self.dep.brk_action]))
        elif self.dep.rbrk_action:
            print("runtime-actions = {0}".format(
                [(_str, _act_display(_str, _act)) for (_str, _act) in self.dep.rbrk_action]))
        else:
            print("")


    def test(self, runtime_args=None):
        if phase.get_curr_stg_sym() == "aot" :
            return self._aot_test()
        else :
            return self._runtime_test( runtime_args )

# Class to test using RDIV solvers
# Should be callable from the
class RDIV(Dependency_Solver):
    def __init__(self, sub_pair, resolution_stage):
        super().__init__(sub_pair, resolution_stage)
        self._aot_setup()

    def _aot_setup(self):
        self.var_lhs = self.s_pair_grp.pairs[0]().sub_a.subscripted_var
        self.subs_lhs = [_p().sub_a.subscript_ref for _p in self.s_pair_grp.pairs]
        self.var_rhs = self.s_pair_grp.pairs[0]().sub_b.subscripted_var
        self.subs_rhs = [_p().sub_b.subscript_ref for _p in self.s_pair_grp.pairs]

    def _non_induction_presence(self):
        return True

    def display(self):
        print(
            " >> RDIV Sub-var = {0}[{1}] <--> {2}[{3} :: res_stage = {4} , direction = '{5}', distance = {6} , dependent = {7}]".
            format(self.var_lhs, self.subs_lhs, self.var_rhs, self.subs_rhs, self.resolution_stage,
                   self.dep.direction, self.dep.distance, self.dep.dependent))

    def test(self, runtime_args=None):
        if phase.get_curr_stg_sym() == "aot" :
            return self._aot_test()
        else :
            return self._runtime_test( runtime_args )


class ForHeader_List(flt_util.Variable_List):
    def __init__(self, items, compare=None):
        self.items = items
        self.compare = compare
        super().__init__(self.items, compare)

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, items):
        self._items = items

    def amalgamate(self, rf_hdr):
        return ForHeader_List( super()._union(rf_hdr).elements, self.compare)

    def cross_map(self, rf_hdr):
        return ForHeader_List(super()._cross_map(rf_hdr).elements, self.compare)

    def unique_pairs(self, rf_hdr):
        return ForHeader_List(super()._unique_pairs(rf_hdr).elements, self.compare)

    def intersection(self, rf_hdr):
        return ForHeader_List(super()._intersect(rf_hdr).elements, self.compare)


_djitr = lambda x : x.record['for_targets'].target[0]
_djnull = lambda x : _djitr(x) if x else None
_djlist = lambda x : _djitr(x) if type(x) is not tuple else  (_djnull(x[0]), _djnull(x[1]))

class MIV(Dependency_Solver):
    def __init__(self, sub_pair, resolution_stage):
        #print(" ---- initialising MIVs ------" )
        super().__init__(sub_pair, resolution_stage)
        #print(" ---- end of initialising MIVs ------\n\n\n" )
        self._aot_setup()
        self.extract_lim = lambda lim, runtime : lim[1] if lim[0] == 'imm' \
                                    else getattr(runtime, lim[1],None)
        self.minus = lambda x : -x if x < 0 else 0
        self.plus  = lambda x : x if x >= 0 else 0

    @property
    def minus(self):
        return self._minus

    @minus.setter
    def minus(self, func):
        if callable(func):
            self._minus = func
        else :
            self._minus = None   # to trigger an exception

    @property
    def plus(self):
        return self._plus

    @plus.setter
    def plus(self, func):
        if callable(func):
            self._plus = func
        else :
            self._plus = None   # to trigger an exception

    def _aot_setup(self):
        self.var_lhs = self.s_pair_grp.pairs[0]().sub_a.subscripted_var
        self.sub_lhs = self.var_lhs.var_ref["axes"][self.s_pair_grp.pairs[0]().sub_a.subscript_ref]
        self.var_rhs = self.s_pair_grp.pairs[0]().sub_b.subscripted_var
        self.sub_rhs = self.var_rhs.var_ref["axes"][self.s_pair_grp.pairs[0]().sub_b.subscript_ref]

    def _find_idx(self, _var, _sub):
        _idx = []
        for _idx_var in _sub.keys():
            if _idx_var == '__Constant__':
                continue

            for _idx_obj in reversed(_var.index_list):
                if _idx_obj.ntype == 'For' and _idx_var in _idx_obj.record["for_targets"].target:
                    _idx.append((_idx_var, _idx_obj.record["for_targets"].target,
                                 _idx_obj.record["for_limits"], _idx_obj))
        _idx.sort(key=lambda x : x[3].lvl)
        return _idx


    def _test_gcd(self , consts):
        def _gcd(a,b):
            while b!=0:
                a,b = (b, a % b )
            return a

        _dependence = None
        _const_diff = consts[1] - consts[0]
        _coeff_list = [ self.sub_lhs[_key]              \
                        for _key in self.sub_lhs.keys() \
                            if _key != '__Constant__' ] \
                    + [ self.sub_rhs[_key]              \
                        for _key in self.sub_rhs.keys() \
                            if _key != '__Constant__' ]
        _coeff_gcd = functools.reduce( _gcd, _coeff_list )
        if _const_diff % _coeff_gcd :
            if phase.get_curr_stg_sym() == "aot" :
                _dependence = self.dep.dependent = False
                self.dep.clear_dir()
            else :
               _dependence = self.dep.rdependent = False
               self.dep.clear_rdir()
        return _dependence

    def _non_induction_presence(self, forhdr_lhs, forhdr_rhs):
        ret = False
        _lst = [key for key in self.sub_lhs.keys()] + [key for key in self.sub_rhs.keys()]

        _extract_idx = lambda for_hdrs : [ _idx[0] for _idx in for_hdrs ]
        _idx_lhs = _extract_idx( forhdr_lhs )
        _idx_rhs = _extract_idx( forhdr_rhs )

        for key in _lst:
            if key == '__Constant__':
                continue
            if key not in _idx_lhs and key not in _idx_rhs :
                ret = True
                break
        return ret

    def _aot_assert_bounds(self, bounds):
        ret = False
        if phase.get_curr_stg() > phase.compilation_stages['aot']:
            return ret

        for _ldoms in bounds :
            for _lbound in _ldoms[2].limits.keys() :
                _usable, _var = _ldoms[2].limits[_lbound]
                if _usable == 'lazy':
                    ret = True
                    break
            if ret :
                break
        return ret

    def _non_induction_acc( self, forhdr_lhs, forhdr_rhs, runtime_args):
        _lhs_const = _rhs_const = 0
        _idx_lhs = [ _idx[0] for _idx in forhdr_lhs ]
        _idx_rhs = [ _idx[0] for _idx in forhdr_rhs ]
        _iterators = set()

        for _key in self.sublhs_norm.keys() :
            if _key == '__Constant__' :
                _lhs_const += self.sublhs_norm[_key]
            elif _key not in _idx_lhs :
                _lhs_const += (getattr(runtime_args, _key, 0) * self.sublhs_norm[_key])
            else :
                _iterators.add(_key)

        for _key in self.subrhs_norm.keys() :
            if _key == '__Constant__' :
                _rhs_const += self.subrhs_norm[_key]
            elif _key not in _idx_rhs :
                _rhs_const += (getattr(runtime_args, _key, 0) * self.subrhs_norm[_key])
            else :
                _iterators.add(_key)
        return (_lhs_const , _rhs_const, _iterators)

    # Helper function for banerjee to search for
    # index coefficients in the case of imperfect loops
    def _search_coeff(self, itr, lhs_rhs):
        try :
            if itr == None :
                return 0
            return self.sublhs_norm[itr] if lhs_rhs == 'lhs' else self.subrhs_norm[itr]
        except KeyError as kerr :
            return 0

    # Helper function for banerjee to search for
    # the correct loop to extract bounds from
    # Assumption - called only when the loop is called as a tuple
    def _search_divloop_bounds(self, loop ):
        _loop_lhs = _loop_rhs = None
        _exist = lambda _it , _sub : _it in _sub.keys()
        # If divergent loops - give both sets
        if loop[0] :
            _itr_lhs = loop[0].record['for_targets'].target[0]
            _loop_lhs = loop[0] if _exist(_itr_lhs, self.sublhs_norm) else None
        if loop[1] :
            _itr_lhs = loop[1].record['for_targets'].target[0]
            _loop_rhs = loop[1] if _exist(_itr_lhs, self.sublhs_norm) else None

        if _loop_lhs == None and _loop_rhs :
            _loop_lhs = _loop_rhs   # imperfect loop
        elif _loop_lhs and _loop_rhs == None :
            _loop_rhs = _loop_lhs   # imperfect loop

        return (_loop_lhs.record['for_limits'].limits , _loop_rhs.record['for_limits'].limits )

    def _loop_limits(self, loop_rec, runtime=None):
        _extract = lambda _tup : _tup[1] if _tup[0] == "imm" \
                                         else getattr(runtime, _tup[1],None)
        if type(loop_rec) is tuple : # imperfect loop-nest
            #assert loop_rec[0] != None and loop_rec[1] != None,\
            #     "Debug :: More work for imperfect loops to be done (lhs,rhs) = {0}--{1}".format(loop_rec[0],loop_rec[1])
            _loop_lhs, _loop_rhs = self._search_divloop_bounds(loop_rec)
            _li_x, _ui_x = _extract(_loop_lhs["start"]), (_extract(_loop_lhs["end"])-1)
            _li_y, _ui_y = _extract(_loop_rhs["start"]), (_extract(_loop_rhs["end"])-1)
        else : # perfect loop-nest case
            _loop = loop_rec.record["for_limits"].limits
            _li_x, _ui_x = _extract(_loop["start"]), (_extract(_loop["end"])-1)
            _li_y, _ui_y = _li_x, _ui_x
        return (_li_x,_li_y,_ui_x,_ui_y)

    def _banerjee_select(self, dep_type, ai, bi, loop, runtime=None):
        if dep_type == '*':
            return self._banerjee_all(ai, bi, loop, runtime)
        elif dep_type == '<':
            return self._banerjee_loopcarried_true(ai, bi, loop, runtime)
        elif dep_type == '>':
            return self._banerjee_loopcarried_anti(ai, bi, loop, runtime)
        else :
            return self._banerjee_loop_independent(ai, bi, loop, runtime)

    def _banerjee_all(self, ai, bi, loop, runtime=None ):
        _min = _max = 0
        _li_x, _li_y, _ui_x, _ui_y = self._loop_limits(loop, runtime)
        # Minimum calculation
        _min = -(self.minus(ai))*_ui_x + self.plus(ai)*_li_x \
                - self.plus(bi)*_ui_y + self.minus(bi)*_li_y
        # Maximum calculation
        _max = self.plus(ai)*_ui_x - self.minus(ai)*_li_x \
                + self.minus(bi)*_ui_y - self.plus(bi)*_li_y
        #print("* _min = {0}, max = {1}".format(_min, _max))
        # Alert !!! - check for divergent loops
        return (_min,_max)
    def _banerjee_loopcarried_true(self, ai, bi, loop, runtime=None ):
        _min = _max = 0
        _li,_li_y, _ui,_ui_y = self._loop_limits(loop, runtime)
        # Minimum calculation
        _min = ( -(self.plus(self.minus(ai)+bi)) * (_ui-1)) \
				+ (((self.minus(self.minus(ai)+bi)) + self.plus(ai)) * _li) - bi

        # Maximum calculation
        _max = ( self.plus(self.plus(ai)-bi) * (_ui-1)) \
				- (((self.minus(self.plus(ai)-bi)) + self.minus(ai)) * _li) - bi
        #print("< _min = {0}, max = {1}".format(_min, _max))
        return (_min,_max)
    def _banerjee_loop_independent(self, ai, bi, loop, runtime=None ):
        _min = _max = 0
        _li,_li_y, _ui,_ui_y = self._loop_limits(loop, runtime)
        # Minimum calculation
        _min = -self.minus(ai-bi)*_ui + self.plus(ai-bi)*_li
        # Maximum calculation
        _max = self.plus(ai-bi)*_ui - self.minus(ai-bi)*_li

        #print("< _min = {0}, max = {1}".format(_min, _max))
        return (_min,_max)

    def _banerjee_loopcarried_anti(self, ai, bi, loop, runtime=None ):
        _min = _max = 0
        _li,_li_y, _ui,_ui_y = self._loop_limits(loop, runtime)
        # Minimum calculation
        _min = ( -(self.minus(ai-self.plus(bi))) * (_ui-1)) \
                + (((self.plus(ai-self.plus(bi))) + self.minus(bi)) * _li) + ai

        # Maximum calculation
        _max = ( self.plus(ai+self.minus(bi)) * (_ui-1)) \
                - (((self.minus(ai+self.minus(bi))) + self.plus(bi)) * _li) + ai
        #print("> _min = {0}, max = {1}".format(_min, _max))
        return (_min,_max)

    # Main Banerjee Test functions
    # consts - tuple ( a0 , b0 )
    def _test_banerjee(self, dvec, consts ,  runtime_args=None):
        #print("Testing {0}".format(dvec))
        _ret = True
        _a0, _b0 = consts

        # accumulate min/max values for each index
        # corresponding to their direction
        _min_hi = _max_hi = 0

        #print("Enclosing loops = {0}\n -- {1}".format(self.dep.enclosing_loops\
        #        , [ _djlist(i) for i in self.dep.enclosing_loops ] ))
        _unpack = lambda rec : rec.record['for_targets'].target[0] if rec else None
        for _didx, _loop in enumerate(self.dep.enclosing_loops) :
            if type(_loop) is tuple :   # imperfect loop-nests
                _itr_lhs , _itr_rhs = _unpack(_loop[0]) , _unpack(_loop[1])
                _ai , _bi = (self._search_coeff(_itr_lhs,"lhs"), self._search_coeff(_itr_rhs,"rhs"))
            else :  # perfect loop-nesting for both sides of statement
                _itr = _loop.record['for_targets'].target[0]
                _ai , _bi = (self.sublhs_norm[_itr], self.subrhs_norm[_itr])

            _min, _max = self._banerjee_select(dvec[_didx], _ai, _bi, _loop, runtime_args)
            _min_hi += _min
            _max_hi += _max
        else :
            if _min_hi > (_b0 - _a0):
               _ret = False
            if _max_hi < (_b0 - _a0):
               _ret = False
        #print("Testing {0} , a0 = {1} , b0 = {2} ".format(dvec, _a0, _b0 ))
        return _ret


    def _banerjee(self, lvl, dvec, lvl_dep, num_loops, consts, runtime_args=None):
        # recursion termination
        if lvl >= num_loops:
            return None

        # specialize previous dependence vector from previous recursion level
        if lvl_dep != '*' :
            dvec[lvl] = lvl_dep

        _dirs = self.dep.direction if phase.get_curr_stg_sym() == "aot" else self.dep.rdirection

        if self._test_banerjee(dvec, consts, runtime_args):
            # In the case of divergent loops short circuit recursion
            if type(self.dep.enclosing_loops[lvl]) is tuple :
                _llvl = 0 if lvl < 0 else lvl
                if self.dep.enclosing_loops[_llvl][0] and self.dep.enclosing_loops[_llvl][1] :
                    _dirs.append( dvec[0:_llvl] + ["=" for _dep in range(num_loops-_llvl)])
                    return # No need to recurse any further

            # Recurse into dependence testing tree with specialisation
            for _dep in [ '<' , '=', '>' ]:
                self._banerjee( lvl+1, dvec.copy(), _dep \
                                , num_loops, consts, runtime_args)
            else :
                if lvl+1 == num_loops :
                    #self.dep.direction.append(dvec)
                    _dirs.append(dvec)

        #else:
        #    print("DEBUG ===== dvec = {0}> FALSE".format(dvec))

    def _exec_banerjee(self, consts, runtime_args=None):
        # reset expanded directions back to generic '*' for banerjee tests
        self.dep.direction.clear()
        _num_loops = len(self.dep.enclosing_loops)


        self._banerjee(-1, [ '*' for _dep in range(_num_loops) ], '*', _num_loops, consts, runtime_args)

        # After all benerjee tests are done, if list of  dependences
        # is empty, set the whole subgroup to be non-dependent
        _dirs = self.dep.direction if phase.get_curr_stg_sym() == "aot" \
                                   else self.dep.rdirection
        set_dep_flag = lambda deps : True if len(deps) else False
        if phase.get_curr_stg_sym() == "aot" :
            _dep = self.dep.dependent = set_dep_flag( _dirs )
        else :
            _dep = self.dep.rdependent = set_dep_flag( _dirs )

        #_num_dirs = len(self.dep.direction[0])
        #print("dependence = {0}\nnum-directions = {1}\nloop-order = {2}".format(_dirs, _num_dirs,\
        #             [ _rec for _rec in self.dep.enclosing_loops]))
        return _dep


    def _normalise_subs(self, forhdr_lhs, forhdr_rhs, unified_hdrs):
        _norm_idx = lambda _sub, _union : { _hdr[0] : 0  for _hdr in _union if _hdr[0] not in _sub }

        # normalise the lhs subscript
        self.sublhs_norm = self.sub_lhs.copy()
        self.sublhs_norm.update( _norm_idx(self.sub_lhs , unified_hdrs))

        # normalise the rhs subscript
        self.subrhs_norm = self.sub_rhs.copy()
        self.subrhs_norm.update( _norm_idx(self.sub_rhs,unified_hdrs))

    def _aot_test(self):
        def _norm_for_headers(lhs_idx, rhs_idx ):
            return ForHeader_List(lhs_idx, None).amalgamate( ForHeader_List(rhs_idx, None)).elements

        _forhdr_lhs = self._find_idx(self.var_lhs, self.sub_lhs)
        _forhdr_rhs = self._find_idx(self.var_rhs, self.sub_rhs)
        if self._non_induction_presence(_forhdr_lhs , _forhdr_rhs):
            #print("MIV-AOT - deferring to runtime - non-induction presence")
            self.resolution_stage = "runtime"
            return None

        # Check GCD - irrespective of aot or runtime loop bound limits
        if self._test_gcd(  (self.sub_lhs['__Constant__'],\
                             self.sub_rhs['__Constant__'])) == False :
            return False


        # Generate a union of all loop bounds relevant in
        #  both subscripts to apply the banerjee equations
        _unified_idx = _norm_for_headers( _forhdr_lhs , _forhdr_rhs )
        # assert bounds check ahead-of-time
        if self._aot_assert_bounds(_unified_idx):
            self.resolution_stage = "runtime"
            return None

        # normalize subscripts for use within Banerjee tests
        self._normalise_subs(_forhdr_lhs, _forhdr_rhs, _unified_idx)
        #print("normalised subscripts\nlhs = {0}\nrhs = {1}".format(self.sublhs_norm,self.subrhs_norm))
        return self._exec_banerjee((self.sub_lhs['__Constant__'],self.sub_rhs['__Constant__']))

    def _runtime_test(self, runtime_args):
        def _norm_for_headers(lhs_idx, rhs_idx ):
            return ForHeader_List(lhs_idx, None).amalgamate( ForHeader_List(rhs_idx, None)).elements


        if self.dep.dependent != None:
            # Already resolved at compile time
            self.dep.rdependent = self.dep.dependent
            self.dep.rdirection = self.dep.direction
            self.dep.rdistance = self.dep.distance
            return self.dep.rdependent

        # Reset for fresh execution
        self.dep.rdependent, self.dep.rdirection, self.dep.rdistance = None, [], 0
        self.dep.rdirection.clear() # banerjee test will expand correctly. 
        self.dep.rbrk_action = None 

        _forhdr_lhs = self._find_idx(self.var_lhs, self.sub_lhs)
        _forhdr_rhs = self._find_idx(self.var_rhs, self.sub_rhs)
        _unified_idx = _norm_for_headers( _forhdr_lhs , _forhdr_rhs )
        # normalize subscripts for use within Banerjee tests
        self._normalise_subs(_forhdr_lhs, _forhdr_rhs, _unified_idx)
        _consts = self._non_induction_acc(_forhdr_lhs, _forhdr_rhs, runtime_args)
        #print("Runtime normalised subscripts\nlhs = {0}\nrhs = {1}\nconstants = {2}".format(\
        #           self.sublhs_norm,self.subrhs_norm, _consts))
        
        # Check GCD - irrespective of aot or runtime loop bound limits
        if self._test_gcd(_consts) == False:
            return False

        return self._exec_banerjee((_consts[0], _consts[1]), runtime_args)
    

    def test(self, runtime_args=None):
        if phase.get_curr_stg_sym() == "aot" :
            return self._aot_test()
        else :
            return self._runtime_test( runtime_args )

    def display(self):
        print(
            " >> MIV Sub-var = {0}[{1}] <--> {2}[{3}] :: res_stage = {4} , direction = '{5}', distance = {6} , dependent = {7}, rdirection = {8} , rdistance = {9} , rdependent = {10}".
            format(self.var_lhs.var_ref['var'], self.sub_lhs, \
                   self.var_rhs.var_ref['var'], self.sub_rhs, \
                   self.resolution_stage, \
                   self.dep.direction, self.dep.distance, self.dep.dependent , \
                   self.dep.rdirection, self.dep.rdistance, self.dep.rdependent))


class Delta(Dependency_Solver):
    def __init__(self, sub_pair, resolution_stage):
        super().__init__(sub_pair, resolution_stage)
        self._aot_setup()

    def _aot_setup(self):
        self.var_lhs = self.s_pair_grp.pairs[0]().sub_a.subscripted_var
        self.subs_lhs = [_p().sub_a.subscript_ref for _p in self.s_pair_grp.pairs]
        self.var_rhs = self.s_pair_grp.pairs[0]().sub_b.subscripted_var
        self.subs_rhs = [_p().sub_b.subscript_ref for _p in self.s_pair_grp.pairs]
        #print("Delta Type lhs = {0}, {1}".format(type(self.subs_lhs), self.subs_lhs))
        #print("Delta Type rhs = {0}, {1}".format(type(self.subs_lhs), self.subs_lhs))

    def _non_induction_presence(self):
        return True

    def display(self):
        print(
            " >> Delta Sub-var = {0}[{1}] <--> {2}[{3} :: res_stage = {4} , direction = '{5}', distance = {6} , dependent = {7}]".
            format(self.var_lhs, self.subs_lhs, self.var_rhs, self.subs_rhs, self.resolution_stage,
                   self.dep.direction, self.dep.distance, self.dep.dependent))

    def test(self):
        #check for presence of non-induction variables
        #if self._non_induction_presence( for_hdr_lhs , for_hdr_rhs ):
        if self._non_induction_presence():
            self.resolution_stage = "runtime"
            return None

        return self.dep.dependent


class Loop_Set(flt_util.Variable_List):
    def __init__(self, _items, _compare=None):
        self.items = _items
        self.compare = _compare
        super().__init__(self.items, _compare)

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, _items):
        if type(_items) is list:
            self._items = _items.copy()
        else:
            self._items = [_items]

    def amalgamate(self, cb):
        _combined = super()._union(cb)
        return Loop_Set(_combined.elements)

    def cross_map(self, cb):
        _combined = super()._cross_map(cb)
        return Loop_Set(_combined.elements)

    def unique_pairs(self, _vlist):
        pair_list = super()._unique_pairs(_vlist)
        return Loop_Set(pair_list.elements)

    def intersection(self, cb):
        _combined = super()._intersect(cb)
        return Loop_Set(_combined.elements)


class Variable_Dependence:
    def __init__(self, var_pair):
        self.var_pair = var_pair
        self.common_loops = self._init_common_loops()
        self.dep_matrix = [['*'] * len(self.common_loops)]
        self.scalar_LI_Dep = var_pair

    @property
    def scalar_LI_Dep(self):
        return getattr(self, "_scalar_LI_Dep", None)

    # Scalar value - any loop independent dependence
    @scalar_LI_Dep.setter
    def scalar_LI_Dep(self, var_pair):
        if var_pair.is_scalar():
            va_skel = var_pair.var_a.skel_obj_node.payload
            vb_skel = var_pair.var_b.skel_obj_node.payload

            if va_skel.assign_stmt.lineno != vb_skel.assign_stmt.lineno :
                self._scalar_LI_Dep = True
            else:
                self._scalar_LI_Dep = False

    @property
    def var_pair(self):
        return self._var_pair()

    @var_pair.setter
    def var_pair(self, v_pair):
        self._var_pair = weakref.ref(v_pair)

    @property
    def dep_matrix(self):
        return self._dep_matrix

    @dep_matrix.setter
    def dep_matrix(self, dependence_matrix):
        if type(dependence_matrix) is list:
            self._dep_matrix = dependence_matrix
        else:
            self._dep_matrix = [dependence_matrix]

    @property
    def div_idx(self):
        return self._div_idx

    @div_idx.setter
    def div_idx(self, val):
        self._div_idx = val

    @property
    def dependence(self):
        return self._dependence

    @dependence.setter
    def dependence(self, val):
        # None signifies - defer to runtime
        if val in (True, False, None):
            self._dependence = val
        else:
            self._dependence = True

    @property
    def rdependence(self):
        return self._rdependence

    @rdependence.setter
    def rdependence(self, val):
        # None signifies - defer to runtime
        if val in (True, False):
            self._rdependence = val
        else:
            self._rdependence = True

    def _init_common_loops(self):
        _vla, _vlb, _dla, _dlb = classify_loop_scopes( self.var_pair.var_a.index_list, self.var_pair.var_b.index_list)
        _vl_set = Loop_Set(_vla, lambda x,y : x == y)
        _common_loops = _vl_set.cross_map(Loop_Set(_vlb)).items
        _common_loops.sort(key=lambda x: x.lvl)
        self.div_idx = len(_common_loops)
        return _common_loops

    def _get_dep_vector_idx(self, loop):
        _idx, _obj = dep_vector_idx(self.common_loops, self.div_idx, loop)
        return _idx

    def dependence_expand(self):
        return dep_vector_expand(self.common_loops, self.dep_matrix)

    def merge_vectors(self, sgrp_dep):
        loop_set = sgrp_dep.enclosing_loops
        direction_vectors = sgrp_dep.direction

        if not sgrp_dep.dependent:
            return

        num_vec = len(direction_vectors)
        _orig_vec_matrix_size = len(self.dep_matrix)

        # duplicate the overall direction vectors for each new vector direction set
        _matrix = [[_it for _it in _mat] for nvec in range(num_vec) for _mat in self.dep_matrix]

        # for each loop in the current group of subscripts
        # find the corresponding position in the duplicated
        # overall new matrix and back-patch the dependences
        # into each position for an overall picture
        for _nvec_idx, _n_vec in enumerate(direction_vectors):
            for _tvec_idx, _t_vec in enumerate(self.dep_matrix):
                for _idx_loop, _loop in enumerate(loop_set):
                    _idx = self._get_dep_vector_idx(_loop)
                    _matrix[_nvec_idx * _orig_vec_matrix_size + _tvec_idx][_idx] = _n_vec[_idx_loop]

        self.dep_matrix = _matrix

    def reset_rdep_matrix(self):
        self.rdep_matrix = [[_it for _it in _mat] for _mat in self.dep_matrix]

    def merge_rvectors(self, sgrp_dep):
        loop_set = sgrp_dep.enclosing_loops
        direction_vectors = sgrp_dep.rdirection

        if not sgrp_dep.rdependent:
            return

        num_vec = len(direction_vectors)
        _orig_vec_matrix_size = len(self.rdep_matrix)

        # duplicate the overall direction vectors for each new vector direction set
        _matrix = [[_it for _it in _mat] for nvec in range(num_vec) for _mat in self.rdep_matrix]

        # for each loop in the current group of subscripts
        # find the corresponding position in the duplicated
        # overall new matrix and back-patch the dependences
        # into each position for an overall picture
        for _nvec_idx, _n_vec in enumerate(direction_vectors):
            for _tvec_idx, _t_vec in enumerate(self.rdep_matrix):
                for _idx_loop, _loop in enumerate(loop_set):
                    _idx = self._get_dep_vector_idx(_loop)
                    _matrix[_nvec_idx * _orig_vec_matrix_size + _tvec_idx][_idx] = _n_vec[_idx_loop]

        self.rdep_matrix = _matrix

    def _infer_dependence(self):
        def check_dep(sub_grp_list):
            _dep = True
            if sub_grp_list:
                for _grp in sub_grp_list:
                    if _grp.dependence.dep.dependent is False:
                        return False
                    elif _grp.dependence.dep.dependent is None:
                        _dep = None    # We have the hope of getting a False at run-time
            return _dep

        _separable, _coupled = self.var_pair.coupling_list
        _dep_s = check_dep(_separable)
        if _dep_s == False:
            return _dep_s

        _dep_c = check_dep(_coupled)
        if _dep_c == False:
            return _dep_c

        if _dep_s == None or _dep_c == None:
            return None
        else:
            return True


    def _infer_rdependence(self):
        def check_rdep(sub_grp_list):
            if sub_grp_list:
                for _grp in sub_grp_list:
                    if _grp.dependence.dep.rdependent is False:
                        return False
            return True

        _separable, _coupled = self.var_pair.coupling_list
        _dep_s = check_rdep(_separable)
        if _dep_s == False:
            return _dep_s

        _dep_c = check_rdep(_coupled)
        if _dep_c == False:
            return _dep_c
        return True

    def infer_dependence(self):
        if phase.get_curr_stg_sym() == 'runtime':
            return self._infer_rdependence()
        return self._infer_dependence()

    def display_loops(self, dbg_lvl=0):
        if dbg_lvl > 2:
            print("var-A loops for variable {0} ::".format(self.var_pair.var_a))
            for _ir in self.var_pair.var_a.index_list:
                print("loop = for {0}  in  range ({1})".format(_ir.record["for_targets"].target,
                                                               _ir.record["for_limits"].limits))
            print("var-B loops for variable {0} ::".format(self.var_pair.var_b))
            for _ir in self.var_pair.var_b.index_list:
                print("loop = for {0}  in  range ({1})".format(_ir.record["for_targets"].target,
                                                               _ir.record["for_limits"].limits))

        if dbg_lvl > 1:
            print("var-pair common loops")
            for _idx, _ir in enumerate(self.common_loops):
                if type(_ir) is tuple:
                    _ir1, _ir2 = _ir
                    if not _ir1:
                        print("Loop(Tuple) LHS loop -> Empty")
                    else:
                        print("LHS loop = for {0}  in  range ({1})".format(
                            _ir1.record["for_targets"].target, _ir1.record["for_limits"].limits))
                        print("LHS Loops {0} =======\n{1}".format(_idx,
                                                                  astor.code_gen.to_source(
                                                                      _ir1.node)))
                    if not _ir2:
                        print("Loop(Tuple) RHS loop -> Empty")
                    else:
                        print("RHS loop = for {0}  in  range ({1})".format(
                            _ir2.record["for_targets"].target, _ir2.record["for_limits"].limits))
                        print("RHS Loops {0} =======\n{1}".format(_idx,
                                                                  astor.code_gen.to_source(
                                                                      _ir2.node)))
                else:
                    print("loop = for {0}  in  range ({1})".format(_ir.record["for_targets"].target,
                                                                   _ir.record["for_limits"].limits))
                    print("Loops {0} =======\n{1}".format(_idx, astor.code_gen.to_source(_ir.node)))

        if dbg_lvl > 0:
            if self.scalar_LI_Dep is None:
                print("Dependence = {0} , Calculated dependence matrix = {1}\n=================".
                      format(self.dependence, self.dep_matrix))
                if phase.get_curr_stg_sym() == 'runtime':
                    print("Runtime Dependence = {0} , Calculated runtime dependence matrix = {1}\n=================".
                                            format(self.rdependence, self.rdep_matrix))
            else:
                print("\nScalar variable identified. Dependence = {0}".format(self.dependence))
                print("Dependence carried by all common-loops. Loop independent dependence --> {0}".
                      format(self.scalar_LI_Dep))


# Gets a weakreference to the subscript
def test_selector(subscript_grp):
    select = {"ZIV": ZIV, "SIV": SIV, "MIV": MIV, "RDIV": RDIV, "Delta": Delta}

    if len(subscript_grp.pairs) == 1:
        return select[subscript_grp.pairs[0]().complexity](subscript_grp, "aot")
    else:
        return select["Delta"](subscript_grp, "aot")


def map_dependence_tests(s_grouplist, coupled_grp):
    print_mux = lambda _cp: "coupled" if _cp else "separable"
    #print(">>>>>>>>>>>>>>>> {0} Subscripts".format(print_mux(coupled_grp)))
    for _grp in s_grouplist:
        _grp.dependence = test_selector(_grp)
        _dependence = _grp.dependence.test()
        #_grp.dependence.display()
    #print("<<<<<<<<<<<<<<<< {0} Subscripts ".format(print_mux(coupled_grp)))


def generate_dependence_matrix(var_pairs, dbg_str="Cross"):
    for _idx_vp, _vp in enumerate(var_pairs):
        #print("{0} Var-pair-{1:02d} (a)= {2} <-> (b)={3}".format(
        #    dbg_str, _idx_vp, _vp.var_a.var_ref, _vp.var_b.var_ref))
        _separable, _coupled = _vp.coupling_list
        _vp.var_dep = Variable_Dependence(_vp)
        # if this is a scalar set of variables
        # mark it as such and create a summary set of loops
        if _separable:
            map_dependence_tests(_separable, coupled_grp=False)
            for _grp in _separable:
                _vp.var_dep.merge_vectors(_grp.dependence.dep)
        if _coupled:
            map_dependence_tests(_coupled, coupled_grp=True)
            for _grp in _coupled:
                _vp.var_dep.merge_vectors(_grp.dependence.dep)
        _vp.var_dep.dependence = _vp.var_dep.infer_dependence()


def dep_test(loop_nest):
    _xvp, _ovp = loop_nest.variable_pairs
    generate_dependence_matrix(_xvp)
    generate_dependence_matrix(_ovp, dbg_str="Output")


def runtime_dep_test(s_grouplist, arg_container, coupled_grp):
    print_mux = lambda _cp: "coupled" if _cp else "separable"
    #print(">>>>>>>>>>>>>>>> {0} Subscripts".format(print_mux(coupled_grp)))
    for _grp in s_grouplist:
        _dependence = _grp.dependence.test(arg_container)
        #_grp.dependence.display()
    #print("<<<<<<<<<<<<<<<< {0} Subscripts ".format(print_mux(coupled_grp)))

def rpatch_dependence_matrix(var_pairs, arg_container, dbg_str="Cross"):
    for _idx_vp, _vp in enumerate(var_pairs):
        #print("{0} Var-pair-{1:02d} (a)= {2} <-> (b)={3}, dep-matrix = {4}".format(
        #                dbg_str, _idx_vp, _vp.var_a.var_ref, _vp.var_b.var_ref, _vp.var_dep.dep_matrix ))
        _vp.var_dep.reset_rdep_matrix()   # reset for new instance
        _separable, _coupled = _vp.coupling_list
        if _separable :
            runtime_dep_test( _separable, arg_container, coupled_grp=False)
            for _grp in _separable :
                _vp.var_dep.merge_rvectors(_grp.dependence.dep)
        if _coupled : 
            runtime_dep_test( _coupled , arg_container, coupled_grp=True)
            for _grp in _coupled:
                _vp.var_dep.merge_rvectors(_grp.dependence.dep)
        _vp.var_dep.rdependence = _vp.var_dep.infer_dependence()

def runtime_exec_deptest(loop_nest, arg_container):
    _xvp, _ovp = loop_nest.variable_pairs
    rpatch_dependence_matrix(_xvp, arg_container)
    rpatch_dependence_matrix(_ovp, arg_container, dbg_str="Output")


# Method to extract valid nested loops for pairs of variables.
# Divergent loops are also returned where loop structures of the
# following type occur :
# for i in ....
#    for j in ......
#        ......
#    for j in ......
#        ......
# Assumption : we have atleast one common-loop
# This is true because we do the analysis starting
# only at loop-nest structures
def classify_loop_scopes(loops_lhs, loops_rhs):
    _valid_loops_lhs, _valid_loops_rhs = [], []
    _divergent_loops_lhs, _divergent_loops_rhs = [], []

    for _idx, (_loop_l, _loop_r) in enumerate(zip(loops_lhs, loops_rhs)):
        if _loop_l != _loop_r:
            _divergent_loops_lhs += loops_lhs[_idx:]
            _divergent_loops_rhs += loops_rhs[_idx:]
            break
        _valid_loops_lhs.append(_loop_l)
        _valid_loops_rhs.append(_loop_r)
    else:
        if _idx < (len(loops_lhs) - 1):
            _valid_loops_lhs += loops_lhs[_idx + 1:]
        if _idx < (len(loops_rhs) - 1):
            _valid_loops_rhs += loops_rhs[_idx + 1:]

    return (_valid_loops_lhs, _valid_loops_rhs, _divergent_loops_lhs, _divergent_loops_rhs)


def prepare_loop_pairs(lhs_loops, rhs_loops):
    _vla, _vlb, _dla, _dlb = classify_loop_scopes(lhs_loops, rhs_loops)
    #print("DJ Debug -- prep-loops --> lhs =  {0} --- rhs = {1}".format( \
    #           [ _djitr(i) for i in lhs_loops ], [ _djitr(i) for i in rhs_loops ]))
    #print("DJ Debug -- prep-loops --> vla = {0}---vlb = {1}\ndla= {2}\ndlb= {3}".format(
    #       [ _djitr(i) for i in _vla ], [ _djitr(i) for i in _vlb ],
    #       [ _djitr(i) for i in _dla ], [ _djitr(i) for i in _dlb ]))
    _vl_set = Loop_Set(_vla)
    _loop_set = _vl_set.amalgamate(Loop_Set(_vlb)).items
    _loop_set.sort(key=lambda x: x.lvl)

    _divergent_idx = len(_loop_set)
    _idx = 0
    for _idx, _dl_set in enumerate(zip(_dla, _dlb)):
        _loop_set.append(_dl_set)
    else:
        if _idx < (len(_dla) - 1):
            _loop_set += [(_dl, None) for _dl in _dla[_idx + 1:]]
        elif _idx < (len(_dlb) - 1):
            _loop_set += [(None, _dl) for _dl in _dlb[_idx + 1:]]
    #print("DJ Debug -- prep-loops --> lp_set = {0}".format(
    #       [ _djlist(i) for i in _loop_set] ))
    return _loop_set, _divergent_idx


# We may have unique common-loop bindings for both lhs and rhs
# or they may have diverged. Hence search for all loop conditions
def dep_vector_idx(loop_list, div_idx, trgt_loop):
    _idx, _loop_set = (None, None)
    for _idx, _loop_set in enumerate(loop_list):
        if _idx < div_idx:
            if _loop_set == trgt_loop:
                return (_idx, _loop_set)
        else:
            _lloop, _rloop = _loop_set
            if _lloop is not None and _lloop == trgt_loop:
                break
            elif _rloop is not None and _rloop == trgt_loop:
                break

    if _idx >= len(loop_list):
        return (None, None)
    else:
        return (_idx, _loop_set)
