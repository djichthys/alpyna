#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import weakref


class Graph_Edge:
    def __init__(self, name=None, payload=None):
        self.name = name
        self.payload = payload

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, val):
        self._name = val

    @property
    def payload(self):
        return self._payload()

    @payload.setter
    def payload(self, payload):
        self._payload = weakref.ref(payload)


class Shadow_Graph:
    # nodes - weak-references to actual graph nodes
    def __init__(self, nodes):
        self.shadow_nodes = nodes

    @property
    def shadow_nodes(self):
        return self._shadow_nodes

    @shadow_nodes.setter
    def shadow_nodes(self, node_list):
        if type(node_list) is list:
            self._shadow_nodes = self._create_shadow_nodes(node_list)
        elif type(node_list) is Graph_Node:
            self._shadow_nodes = self._create_shadow_nodes([node_list])
        else:
            self._shadow_nodes = []

    def _create_shadow_edge(self, orig_edge, orig_node_list, new_node_list):
        _new_lbl = None
        _lbl, _succ = orig_edge
        if _lbl is not None and not isinstance(_lbl, str):
            _new_lbl = weakref.ref(_lbl)
        return (_new_lbl, new_node_list[orig_node_list.index(_succ)])

    def _create_shadow_nodes(self, node_list):
        _shadow_list = [Graph_Node(node.name, weakref.ref(node.payload)) for node in node_list]
        for _sh_node, _node in zip(_shadow_list, node_list):
            for _succ in _node.successors:
                _sh_node.successors.append(self._create_shadow_edge(_succ, node_list, _shadow_list))
        return _shadow_list


#Graph Node contains
#  id : name of the node as a string
#  edges : list of nodes that this node connects to - representative of outgoing edges
class Graph_Node():
    def __init__(self, name, payload=None, edges=None):
        self.name = name
        self.successors = edges
        self.payload = payload

    @property
    def successors(self):
        return self._successors

    @successors.setter
    def successors(self, succ):
        format_edge = lambda _succ: _succ if type(_succ[0]) == tuple else [(None, _it) for _it in succ]
        if type(succ) is list:
            if succ:
                self._successors = format_edge(succ)
            else:
                self._successors = succ
        elif type(succ) is tuple:
            self._successors = [succ]
        elif succ is None:
            self._successors = []
        else:
            self._successors = [(None, succ)]

    @property
    def payload(self):
        if callable(self._payload):
            return self._payload()
        else : 
            return self._payload

    @payload.setter
    def payload(self, payload):
        self._payload = payload


# Connected Component Class holds a list of Graph-Nodes that are strongly connected
#   name : name of the SCC as a string
#   edges : list of other SCCs  that this SCC connects to - representative of outgoing edges
#   node_list : list of Graph Nodes contained within this SCC
class Connected_Component():
    def __init__(self, node_list, name=None, edges=None):
        self.name = name
        self.node_list = node_list
        self.edges = edges

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, val):
        self._name = val

    @property
    def edges(self):
        return self._edges

    @edges.setter
    def edges(self, edge):
        if edge is None or type(edge) is list:
            self._edges = edge
        elif type(edge) is Connected_Component:
            self._edges.append(edge)
        else:
            print("type before type-error = {0}".format(type(edge)))
            raise TypeError

    def create_id(self, new_name=None):
        if (new_name == None):    # add random name generator later on
            generated_id = "__anonymous"
        elif type(new_name) is str:
            generated_id = new_name
        elif (type(new_name) is int or type(new_name) is float):
            generated_id = str(new_name)
        elif type(new_name) is list:
            iterator = iter(new_name)
            if type(new_name[0]) is str:
                generated_id = iterator.__next__()
                while (True):
                    try:
                        nxt = iterator.__next__()
                        generated_id += "_" + nxt
                    except StopIteration:
                        break
            elif (type(new_name[0]) is int or type(new_name[0]) is float):
                generated_id = str(iterator.__next__())
                while (True):
                    try:
                        nxt = str(iterator.__next__())
                        generated_id += "_" + nxt
                    except StopIteration:
                        break
            else:    #type is Graph_Node
                generated_id = iterator.__next__().name
                while (True):
                    try:
                        nxt = iterator.__next__().name
                        generated_id += "_" + nxt
                    except StopIteration:
                        break
        else:
            try:
                generated_id = new_name.name
            except AttributeError:
                raise AttributeError(
                    "attr - name does not exist. Only custom class type allowed is type Graph_Node")
        return generated_id


# Worker Class to help with connecting strongly connected regions
class Node_to_SCC_Map():
    def __init__(self, scc):
        self.scc = {}
        if type(scc) is list:
            for supernode in scc:
                for node in supernode.node_list:
                    self.scc[node] = supernode
        elif type(scc) is Connected_Component:
            for node in scc.node_list:
                self.scc[node] = scc


# Function to connect between strongly connected regions
def connect_super_nodes(super_node_list):
    reverse_map = Node_to_SCC_Map(super_node_list)

    for s_node in super_node_list:
        for node in s_node.node_list:
            for _lbl, dest in node.successors:
                _dest_scc = reverse_map.scc[dest]
                if (_dest_scc != s_node):
                    if s_node.edges == None:
                        s_node.edges = [_dest_scc]
                    elif _dest_scc not in s_node.edges:
                        s_node.edges.append(_dest_scc)

        #Some SCCs may have no outgoing edges 
        #and either have only incoming or no incoming edges 
        if s_node.edges == None:
            s_node.edges = []

    return super_node_list


# Tarjan's algorithm : input should be the complete list of Nodes
# with outgoing edges already encoded
def strongly_connected_components(graph):
    index = 0
    stack = []
    component_graph = []    #should contain list of Connected_Components()

    # node : node at which to start further searh.
    # Note that this function will be called recursively
    def strong_connect(node):
        nonlocal index
        node.index = index
        node.lowest_link = index    #initialise to current index - might change later
        index += 1
        stack.append(node)
        node.on_stack = True

        for edge_lbl, successor in node.successors:
            if type(successor) != Graph_Node:
                continue

            if not (hasattr(successor, "index")):
                #successor has not yet been visited - recurse on this node
                strong_connect(successor)

                # either this node is the root of an SCC or
                # this node is on the only path from the root node or
                # the successor node has an edge to a node closer to the root node
                # than the current node
                node.lowest_link = min(node.lowest_link, successor.lowest_link)
            elif (hasattr(successor, "on_stack") and successor.on_stack == True):
                #Successor is already on stack => already visited
                node.lowest_link = min(node.lowest_link, successor.index)

        #if node is a root node, pop stack and generate an Strongly connected component
        if node.lowest_link == node.index:
            scc_nodes = []
            while True:
                component = stack.pop()
                component.on_stack = False
                scc_nodes.append(component)
                if component == node:
                    break
            # compute edges to new set
            super_node = Connected_Component(scc_nodes)
            super_node.name = super_node.create_id(scc_nodes)
            component_graph.append(super_node)

    for node in graph:
        if not (hasattr(node, "index")):
            strong_connect(node)

    def destruct_scratch_var(obj, var_name):
        if hasattr(obj, var_name):
            delattr(node, var_name)

    # Clean-up scratch variables, because we might be re-using the same graph nodes again
    for node in graph:
        for _var_name in ["on_stack", "index", "lowest_link"]:
            destruct_scratch_var(node, _var_name)

    return component_graph



# To be used along with  Tarjan's algorithm
# input should be a list of nodes belonging to a 
# strongly connected component
def is_cyclic( graph_scc ) : 
	_cyclic = False 
	_len_scc_list = len(graph_scc.node_list)
	if (_len_scc_list > 1) :
		_cyclic = True 
	else : 
		# Single node within SCC, with potential self-loop, and/or 
		# potentially multiple outgoing edges to other SCC nodes 
		for _edge , _node in graph_scc.node_list[0].successors:
			if _node == graph_scc.node_list[0] : 
				_cyclic = True 
				break 
	return _cyclic 




def display_graph( nodes ):
    print("================================")
    print("Graph connections :") 
    print("================================")
    format_name = lambda _lbl, _name: (_lbl.name, _name.name) if _name != None else (_lbl.name, _name)
    for node in nodes:
        edges = [format_name(_j_lbl, j) for _j_lbl, j in node.successors]
        print("name = {0}, edges-to {1}".format(node.name, edges))

def display_scc_graph( super_nodes):
    print("================================")
    print("Strongly connected components")
    print("================================")
    for idx_i, s_node in enumerate(super_nodes):
        print("supernode[{0}] = {1} -- cyclic = {2}".format(idx_i, s_node.name, is_cyclic(s_node)))
        for _node in s_node.node_list:
            print("components = {0}".format(_node.name))

        for idx_j, _sn_edge in enumerate(s_node.edges):
            print("supernode-connections[{0}] = {1}".format(idx_j, _sn_edge.name))

        print("================================")


def render_graph( nodes , f_prefix="loop_dag"):
	import graphviz as gv 
	disambiguate = lambda arg : arg() if callable(arg) else arg 
	_gv_graph = gv.Digraph(name=f_prefix + " Loop Statements", format="pdf" )
	for node in nodes : 
		_gv_graph.node(node.name)
		for _edge, _dest in node.successors : 
			_gv_graph.edge(node.name, _dest.name, label=disambiguate(_edge).name)
		_gv_graph.render("img/orig_"+ f_prefix)
	
	
	

def render_scc_graph( nodes , super_nodes, f_prefix="scc_loop_dag" ) : 
	import graphviz as gv 
	_gv_graph = gv.Digraph(name=f_prefix + " Loop Statements", format="pdf" )
	for _idx , supernode in enumerate(super_nodes): 
		cluster_name = "cluster_" + supernode.name
		with _gv_graph.subgraph(name=cluster_name) as subg : 
			subg.attr(style="filled")
			subg.attr(color="lightgrey")
			subg.node_attr.update(style="filled", color="white")
			subg.attr(label="scc_{0}_".format(_idx) + supernode.name)
			for node in supernode.node_list : 
				subg.node(node.name) 

	disambiguate = lambda arg : arg() if callable(arg) else arg 
	for node in nodes : 
		for _edge , _dest in node.successors : 
			_gv_graph.edge(node.name , _dest.name , label=disambiguate(_edge).name)

	_gv_graph.render("img/scc_"+f_prefix)



if __name__ == "__main__":
    #unit-test
    #also a primer on how to use the classes and the function
    init_list = []
    for i in range(0, 10, 1):
        init_list.append(Graph_Node("{0:02d}".format(i)))

    init_list[0].successors = [(Graph_Edge("edj_lbl_00_01"), init_list[1]), 
                               (Graph_Edge("edj_lbl_00_00"), init_list[0])]
    init_list[1].successors = [(Graph_Edge("edj_lbl_01_02"), init_list[2])]
    init_list[2].successors = [(Graph_Edge("edj_lbl_02_00"), init_list[0])]
    init_list[3].successors = [(Graph_Edge("edj_lbl_03_01"), init_list[1]), 
                               (Graph_Edge("edj_lbl_03_02"), init_list[2]),
                               (Graph_Edge("edj_lbl_03_04"), init_list[4])]
    init_list[4].successors = [(Graph_Edge("edj_lbl_04_03"), init_list[3]), 
                               (Graph_Edge("edj_lbl_04_05"), init_list[5])]
    init_list[5].successors = [(Graph_Edge("edj_lbl_05_02"), init_list[2]), 
                               (Graph_Edge("edj_lbl_05_06"), init_list[6])]
    init_list[6].successors = [(Graph_Edge("edj_lbl_06_05"), init_list[5])]
    init_list[7].successors = [(Graph_Edge("edj_lbl_07_04"), init_list[4]), 
                               (Graph_Edge("edj_lbl_07_06"), init_list[6]),
                               (Graph_Edge("edj_lbl_07_07"), init_list[7]), 
                               (Graph_Edge("edj_lbl_07_08"), init_list[8])]
    init_list[8].successors = [(Graph_Edge("edj_lbl_08_09"), init_list[9])]
    init_list[8].successors.append((Graph_Edge("dupe_edj_lbl_08_09"), init_list[9]))

    display_graph( init_list )

    scc_list = strongly_connected_components(init_list)
    for i in range(0, len(init_list), 1):
        print("node[{0}] @ {1}".format(i, init_list[i]))
    for _idx, _scc in enumerate(scc_list):
        print("scc[{0}] ({1}) --> {2}".format(_idx, _scc.name, _scc.node_list))

    super_nodes = connect_super_nodes(scc_list)

    display_scc_graph( super_nodes )


    #show diagrammatically
    render_graph( init_list , f_prefix="myfunc_")
    render_scc_graph( init_list , reversed(super_nodes), f_prefix="myfunc_" ) 
