#!/home/dejice/work/python-tutorial/ast-/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2019>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


#import Runtime_Components as rtc
import astor
import Dependency_Solver as solve
import Generate_DAG as dag
import Hardware.hw_base as hwb
import Utils as util


class Closure_Arg_Container:
    def __init__(self):
        pass


def _ufunc_mux(xfer_container, mod_hndl, base_name, runtime_opts):
    #print("_ufunc_mux() ::: calling function {0}\nruntime-opts = {1}".format(base_name, runtime_opts))

    # If timer is passed from outside loop-nest scope
    # use it -- otherwise create our own
    _timer = runtime_opts.timer if runtime_opts else util.Timer()

    # Check if we are doing static setup profiling for this architecture
    assert runtime_opts,"Static profile-options not found"
    if runtime_opts.profile_type in ['framework_init_cki_cg', 'framework_init_ci_cc' ]:
        _perf_obj = Static_Profile( xfer_container, mod_hndl, base_name, runtime_opts)
        _perf_obj.static_profile()
        return xfer_container

    # Ensure that configuration options can be read
    runtime_opts.read_config()

    _drv_sym = _exec_func(xfer_container, mod_hndl, base_name, _timer, runtime_opts)
    #_drv_sym = _exec_func(xfer_container, mod_hndl, base_name, _timer)
    _drv = getattr(mod_hndl, _drv_sym, None)
    if _drv:
        _drv(xfer_container, _timer)
    else:
        print("BIG BIG BIG Error ::: Could not find loop-nest driver to execute")
    #print("GPU/VM Timer analysis-time [GPU] = {0}, compile-time = {1} , xfer-time-to-dev = {2}, exec-time = {3}, xfer-time-to-host = {4}, total = {5} ".format( _timer.analysis_time, _timer.compile_time, _timer.xfer_to_dev_time , _timer.exec_time , _timer.xfer_to_host_time, _timer.total_time()))
    return xfer_container


def insert_codeobj(module_obj, txt):
    _co = compile(txt, '<anyscale-jit-exec>', 'exec')
    exec(_co, module_obj.__dict__)


def _resolve_scevref_type(ln_scevref, s_arg, runtime_args):
    _dtype = type(getattr(runtime_args, s_arg.arg_name(False), None))
    if _dtype in hwb.Numpy_Supported_types:
        s_arg.dtype = hwb.HW_supported_types[_dtype]
    elif _dtype in hwb.Native_types:
        s_arg.dtype = hwb.HW_supported_types[hwb.Native_Numpy_Map[_dtype]]

    ln_scevref.insert_scev_ref(s_arg)
    return


#def _exec_func(xfer_container, mod_hndl, base_name, timer):
def _exec_func(xfer_container, mod_hndl, base_name, timer, opts):
    # perf counters
    timer.start_analysis()

    # runtime analysis 
    scevref_map = _runtime_analysis_exec( xfer_container\
              , mod_hndl, base_name )
    _patch_datatypes( xfer_container, mod_hndl\
         , base_name, scevref_map )

    # perf counters
    timer.end_analysis()

    if opts != None :
        _ln = mod_hndl._runtime_anchor.ln_map[base_name]
        opts.cost_estimate = dag._lnest_cost(_ln.code_gen.exec_order, mod_hndl.mod_opts.read_config())
        opts.xfer_cost = hwb.data_xfer_cost( _ln.code_gen.xfer_cost, mod_hndl.mod_opts.read_config(), xfer_container)
   
    # compile and insert into runtime execution  
    _jit_kernel(mod_hndl, base_name, timer, opts)
    assert opts and opts.profile_dev, "Profiling Device information not provided"
    if opts and opts.profile_dev:
        _dev = 'vm' if opts.profile_dev == 'interpreter' else opts.profile_dev
        return "_{0}_".format(_dev) + base_name

    return "_gpu_" + base_name


def _jit_kernel(mod_hndl, base_name, timer, opts):
    if opts and opts.profile_dev:
        _devices = [ opts.profile_dev ]
    else:
        _devices = ["interpreter", "cpu", "gpu"]

    # perf counter
    timer.start_compile()

    _ln = mod_hndl._runtime_anchor.ln_map[base_name]
    if "gpu" in _devices :
        for _stmt in _ln.code_gen.kernels.keys():
            _kern_co = _ln.code_gen.kernels[_stmt]["gpu"]
            #print("compiling and inserting stmt {0} --> kernel-body = \n{1}".format(
            #        astor.codegen.to_source(_stmt.assign_stmt.node), _kern_co.runtime_kernel))
            insert_codeobj(mod_hndl, _kern_co.runtime_kernel)

    #for _idx, _drv in enumerate(["interpreter", "cpu", "gpu"]):
    for _idx, _drv in enumerate(_devices):
        if _ln.code_gen.compiled_drv[_drv] :
            _drv_code = _ln.code_gen.compiled_drv[_drv]
        else :
            _drv_code = _ln.code_gen.drivers[_drv]
        #print("driver[{0}] --> \n{1}".format(_idx, _drv_code ))
        insert_codeobj(mod_hndl, _drv_code)

    # perf counter
    timer.end_compile()


def _patch_datatypes(xfer_container, mod_hndl, base_name, scevref_map):
    _ln = mod_hndl._runtime_anchor.ln_map[base_name]

    # Patch the type for each argument
    _ln.code_gen.runtime_patch_types(scevref_map, xfer_container)
    _ln.code_gen.runtime_patch_driver_args(scevref_map, trgt_drv=["cpu"])

    for _stmt in _ln.code_gen.kernels.keys():
        #print("============Finalizing Runtime Type-patching for runtime Statement=============")
        #print("{0}".format(astor.codegen.to_source(_stmt.assign_stmt.node).rstrip("\n ")))
        #print("============Collected Runtime Type for runtime Statement=============")

        for dev in dag.Device_Types.keys():
            _kern = _ln.code_gen.kernels[_stmt][dev]
            _type_str = _kern.patch_argtypes(scevref_map, xfer_container)
            # After compilation clear the runtime scev arrays
            _kern.runtime_scevref_kernel_args = None

    def patch_rparse( lpcont ):
        for _obj in lpcont :
            if type(_obj) is dag.Vec_LoopContainer:
                patch_rparse( _obj.children )
            elif type(_obj) is dag.Vec_StmtContainer:
                for _dev in dag.Device_Types:
                    _obj.par_threads[_dev].patch_types( _obj.graph_node.payload, scevref_map, xfer_container )

    patch_rparse( _ln.code_gen.exec_order )
    #assert False, " code-gen -= {0}".format(_ln.code_gen.exec_order)





# If the dependence graph was parallelised statically,
#   a) check for any scalar-reference write type kernels. If these are present,
#      build arrays of scalars according to its types
def _runtime_analysis_exec(xfer_container, mod_hndl, base_name):
    _ln = mod_hndl._runtime_anchor.ln_map[base_name]
    ln_scevref = hwb.SCEV_Ref()

    # Check if any dependences need backpatching
    # If so backpatch and regenerate the graph
    # This will include any scev-ref reduce() type kernels as well
    if _ln.graph_builder.codegen_stage == "runtime":
        solve.runtime_exec_deptest(_ln, xfer_container)
        dag.runtime_analyse(_ln, xfer_container)

        for _scev_ref in _ln.code_gen.scev_ref:
            _resolve_scevref_type(ln_scevref, _scev_ref, xfer_container)

        # At runtime generate drivers after scev-ref placement has occured
        _ln.code_gen.runtime_gen_drv(xfer_container, ln_scevref)
        # Debug - print out the loop structure
        #dag._display_vec_struct( _ln.code_gen.exec_order, mod_hndl.mod_opts.read_config())
        #print("\n\n\n>>>>>>>>\nCOST -- runtime = {0}\n".format(\
        #        dag._lnest_cost(_ln.code_gen.exec_order, mod_hndl.mod_opts.read_config())))

        for _stmt in _ln.code_gen.kernels.keys():
            #print("============Finalize Runtime Analysis for runtime Statement=============")
            #print("{0}".format(astor.codegen.to_source(_stmt.assign_stmt.node).rstrip("\n ")))
            #print("============Collecting Runtime Analysis-Ref for runtime Statement=============")
            for dev in dag.Device_Types.keys():
                _kern = _ln.code_gen.kernels[_stmt][dev]
                if _kern.scevref_kernel:
                    _kern.patch_scevref_kernel(ln_scevref, xfer_container)

        for _dev in ["cpu", "gpu"]:
            _code = _ln.code_gen.runtime_finalize_drv(_dev, xfer_container, ln_scevref)

    # Even if all the dependences were known at compile-time,
    # There might still have been scevref reduce() type dependences
    # Finalize such kernels at runtime.
    if _ln.graph_builder.codegen_stage == "aot":
        _regen_drv = False
        for _scev_ref in _ln.code_gen.scev_ref:
            _regen_drv = True
            _resolve_scevref_type(ln_scevref, _scev_ref, xfer_container)

        # If there was a scalar variable write  dependence type
        # in loop-nest we have to patch the relevant kernel
        # and regenerate the whole driver
        if _regen_drv:
            for _stmt in _ln.code_gen.kernels.keys():
                #print("============Collecting SCEV-Ref for runtime Statement=============")
                #print("{0}".format(astor.codegen.to_source(_stmt.assign_stmt.node).rstrip("\n ")))
                #print("============Collecting SCEV-Ref for runtime Statement=============")
                for dev in dag.Device_Types.keys():
                    _kern = _ln.code_gen.kernels[_stmt][dev]
                    if _kern.scevref_kernel:
                        _kern.patch_scevref_kernel(ln_scevref, xfer_container)
                        _kern.runtime_gen_drv(ln_scevref, xfer_container)
            for _dev in ["cpu", "gpu"]:
                _code = _ln.code_gen.runtime_finalize_drv(_dev, xfer_container, ln_scevref)
        # Debug - print out the loop structure
        #dag._display_vec_struct( _ln.code_gen.exec_order)
        #print("\n\n\n>>>>>>>>\nCOST -- runtime-aot = {0}\n".format(\
        #           dag._lnest_cost(_ln.code_gen.exec_order, mod_hndl.mod_opts.read_config() )))

    return ln_scevref



class Static_Profile:
	def __init__(self, runtime_container, alpyna_module_obj, kern_name, prog_opts):
		self.xfer_container = runtime_container
		self.mod_hndl = alpyna_module_obj
		self.base_name = kern_name
		self.opts = prog_opts

	def _profile_kern_analysis(self):
		# perf counters
		self.opts.timer.start_analysis()

		# runtime analysis
		scevref_map = _runtime_analysis_exec( self.xfer_container , self.mod_hndl, self.base_name )
		_patch_datatypes( self.xfer_container, self.mod_hndl , self.base_name, scevref_map )

		# perf counters
		self.opts.timer.end_analysis()

		# compile and insert into runtime execution
		_jit_kernel(self.mod_hndl, self.base_name, self.opts.timer, self.opts)

	# profile execution to find  Cki/Ci_gpu ratio
	# Cki is the estimated cost of invoking a kernel on the accelerator
	# The host interpreter (through the driver)
	# invokes the kernel on the accelerator
	def _acc_kernel_invocation_profile(self):
		# Do all runtime dependence analysis and jit kernel into alpyna module
		self._profile_kern_analysis()

		for _dev in [ '_gpu_' ]:
			_drv = getattr(self.mod_hndl, _dev + self.base_name, None)
			assert _drv, "Could not find --{0}-- variant, kernel-symbol = {1}".format(_dev, _dev+self.base_name)
			_drv(self.xfer_container, self.opts.timer)
			#print("Timer analysis-time [{0}] = {1}, compile-time = {2} , xfer-time-to-dev = {3}, exec-time = {4}, xfer-time-to-host = {5}, total = {6} ".format( _dev, self.opts.timer.analysis_time, self.opts.timer.compile_time, self.opts.timer.xfer_to_dev_time , self.opts.timer.exec_time , self.opts.timer.xfer_to_host_time, self.opts.timer.total_time()))


	# profile to obtain static Cost_int/Cost_cpu ratio
	def _acc_vm_cpujit_profile(self):
		# Do all runtime dependence analysis and jit kernel into alpyna module
		self._profile_kern_analysis()
		_permitted_devices = ["interpreter", "cpu"]
		assert self.opts.profile_dev in _permitted_devices, "Profile-dev not in {0}".format(_permitted_devices)
		_dev = "_vm_" if self.opts.profile_dev == "interpreter" else "_cpu_"
		_drv = getattr(self.mod_hndl, _dev + self.base_name, None)
		_drv(self.xfer_container, self.opts.timer)
		assert _drv, "Could not find --{0}-- variant, kernel-symbol = {1}".format(_dev, _dev+self.base_name)
		print("Timer analysis-time [{0}] = {1}, compile-time = {2} , xfer-time-to-dev = {3}, exec-time = {4}, xfer-time-to-host = {5}, total = {6} ".format( _dev, self.opts.timer.analysis_time, self.opts.timer.compile_time, self.opts.timer.xfer_to_dev_time , self.opts.timer.exec_time , self.opts.timer.xfer_to_host_time, self.opts.timer.total_time()))
		


	def static_profile(self):
		self.opts.timer.reset()
		if self.opts.profile_type == 'framework_init_cki_cg':
			self._acc_kernel_invocation_profile()
		elif self.opts.profile_type == 'framework_init_ci_cc': 
			self._acc_vm_cpujit_profile()

