#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import weakref
import astor
import Utils as flt_util
import Tarjan as trj
import Dependency_Solver as solve


import numpy as np
import Hardware.cpu as cpu
import Hardware.cuda_gpu as gpu
import Hardware.hw_base as hwb

max_loop_limit = 100   ## assume no one will write a 100-nested loop

Device_Types = { "gpu" : gpu.Vec_CUDA_GPU \
                ,"cpu" : cpu.Vec_Host_CPU \
                ,"interpreter" : cpu.Host_VM }

Device_Txfr_Types = {     "gpu" : gpu.Vec_CUDA_Txfr \
                        , "cpu" : cpu.Host_CPU_Txfr \
                        , "interpreter" : cpu.Host_VM_Txfr }


def gen( prefix, init_val):
    initial = init_val
    while True :
        yield prefix + "_" + str(init_val)
        init_val += 1
    return None

class Label:
    def __init__(self , prefix , init_val):
        self.prefix = prefix
        self.init_val = init_val


    def anon_name(self):
        while True :
            yield self.prefix + "_" + str(self.init_val)
            self.init_val += 1

    def reset(self, val=0):
        self.init_val


class Assign_Node_Container :
	def __init__(self, stmt , loop_list=None):
		self.assign_stmt = stmt
		self.enclosing_loops = loop_list


	@property
	def assign_stmt(self):
		return self._assign_stmt()
	@assign_stmt.setter
	def assign_stmt(self , stmt):
		self._assign_stmt = weakref.ref(stmt)

	@property
	def enclosing_loops(self) :
		return self._enclosing_loops

	@enclosing_loops.setter
	def enclosing_loops(self, loops) :
		if type(loops) is list :
			if loops :
				self._enclosing_loops = [ weakref.ref(_loop) for _loop in loops ]
			else :
				self._enclosing_loops = []
		elif loops is None :
			self._enclosing_loops = []
		else :
			self._enclosing_loops = [ loops ]


class Dependence_Edge:
	def __init__(self, var_pair , loop, loop_list , dep_type , dep_reverse=False):
		self._loop_list = loop_list   # Disposable after init
		self.carry_loop = loop
		del(self._loop_list)
		self.var_pair = var_pair
		self.dep_type = dep_type
		self.dep_reverse= dep_reverse

	@property
	def carry_loop(self):
		if self._loop is None :
			return (self._loop_idx , self._loop)
		else :
			return (self._loop_idx , self._loop())

	@carry_loop.setter
	def carry_loop(self, val ):
		if type(val) is tuple :
			self._loop_idx , _loop_ref = val
		else :
			_llist = getattr(self,"_loop_list",None)
			if _llist :
				self._loop_idx , _loop_ref = _llist.index(val), val
			else :
				self._loop_idx , _loop_ref = None , val

		if _loop_ref is None :
			self._loop = _loop_ref
		else :
			self._loop = weakref.ref(_loop_ref)

	@property
	def dep_type(self):
		return self._dep_type

	@dep_type.setter
	def dep_type(self, dtype ):
		if dtype in [ 'true' , 'anti' , 'output' , 'scalar'] :
			self._dep_type = dtype
		else :
			self._dep_type = None


	@property
	def var_pair(self):
		return self._var_pair()

	@var_pair.setter
	def var_pair(self, _pair):
		self._var_pair = weakref.ref(_pair)

	def dep_string(self):
		_dep_translate = { 'true' : 't' , 'anti': '^' , 'output':'o' , 'scalar':'s'}
		_loop_idx , _loop_item = self.carry_loop
		carrying_loop = lambda x : str(x) if x < max_loop_limit else "inf"
		return "D{0}_{1}".format( _dep_translate[self.dep_type], carrying_loop(_loop_idx))



class DAG_Builder(flt_util.Record_Parser):
	def __init__(self , loop_nest , func_name="_anon"):
		self.loop_nest = loop_nest
		super().__init__(self.loop_nest.skel_obj.skeleton \
						, self.loop_nest.boundary.start \
						, self.loop_nest.boundary.end)
		self.vfunc_name = func_name
		self.stmt_list = []
		self.codegen_stage = "aot"
		self.edges = []

	@property
	def loop_nest(self):
		return self._loop_nest()

	@loop_nest.setter
	def loop_nest(self, nest ):
		self._loop_nest = weakref.ref( nest)

	@property
	def vfunc_name(self):
		return self._vfunc_name

	@vfunc_name.setter
	def vfunc_name(self, _name ):
		self._vfunc_name = _name


	@property
	def edges(self):
		return self._edges

	@edges.setter
	def edges(self, edge_list):
		if type(edge_list) is list :
			self._edges = edge_list
		elif type(edge_list) is trj.Class_Edge :
			self._edges = [ edge_list ]
		elif edge_list is None :
			self._edges = []
		else :
			self.label = Label( "_anon_statement" , 0 )
			self.name_gen = self.label.anon_name()
			self._edges = [ trj.Graph_Edge( next(self.name_gen), edge_list ) ]



	@property
	def codegen_stage(self):
		return self._codegen_stage

	@codegen_stage.setter
	def codegen_stage(self , val ):
		if val in solve.phase.compilation_stages.keys():
			self._codegen_stage = val
		else :
			self._codegen_stage = 'runtime'


	def parse_stmts(self):
		_tok = self.consume()
		while _tok :
			if _tok.ntype == 'Assign' :
				_node_name = self.vfunc_name + "_" + str(_tok.lineno)
				self.stmt_list.append(trj.Graph_Node(_node_name,  payload= Assign_Node_Container(_tok)))

			_tok = self.consume()
		return None


	def map_node(self, skel_rec ):
		for _graph_node in self.stmt_list:
			if skel_rec == _graph_node.payload.assign_stmt:
				_node = _graph_node
				break
		else :
			_node = None
		return _node


	def _determine_dep_edge(self, vec , var_pair, output_dep):
		dep_type_sym = lambda x : x if not output_dep else 'output'
		_node_s1 , _node_s2 = var_pair.var_a.skel_obj_node, var_pair.var_b.skel_obj_node
		_common_loops = var_pair.var_dep.common_loops
		_eval_dep = None
		for _idx, (_dep, _loop) in enumerate(zip(vec, _common_loops)) :
			if _dep == '=' :
				continue
			if _dep == '<':
				# true/output dependence carried by loop _idx
				_eval_dep = Dependence_Edge(var_pair , (_idx, _loop),
												_common_loops , dep_type_sym("true"))
				_edge = trj.Graph_Edge( _eval_dep.dep_string(), _eval_dep)
				self.edges.append((_eval_dep , _edge))
				_node_s1.successors.append(( _edge , _node_s2 ))
			else :
				# anti/output dependence carried by loop _idx
				_eval_dep = Dependence_Edge(var_pair , (_idx, _loop),
												_common_loops, dep_type_sym("anti"), True)
				_edge = trj.Graph_Edge( _eval_dep.dep_string(), _eval_dep)
				self.edges.append((_eval_dep , _edge))
				_node_s2.successors.append(( _edge , _node_s1 ))
			break  # we do not need to continue with the loop
		else :
			# output dependence lhs -> rhs  carried by infinity
			# true dependence lhs -> rhs  carried by infinity
			# anti dependence rhs -> lhs  carried by infinity
			if output_dep :
				if var_pair.var_a != var_pair.var_b :
					_eval_dep = Dependence_Edge(var_pair , (max_loop_limit, None), _common_loops ,"output")
					_edge = trj.Graph_Edge( _eval_dep.dep_string(), _eval_dep)
					self.edges.append((_eval_dep , _edge))
					_node_s1.successors.append(( _edge , _node_s2 ))
			else :
				if _node_s1.payload.assign_stmt.lineno < _node_s2.payload.assign_stmt.lineno :
					_eval_dep = Dependence_Edge(var_pair , (max_loop_limit, None),
													_common_loops , dep_type_sym("true"))
					_edge = trj.Graph_Edge( _eval_dep.dep_string(), _eval_dep)
					self.edges.append((_eval_dep , _edge))
					_node_s1.successors.append(( _edge , _node_s2 ))
				elif _node_s1.payload.assign_stmt.lineno > _node_s2.payload.assign_stmt.lineno :
					_eval_dep = Dependence_Edge(var_pair , (max_loop_limit, None),
													_common_loops , dep_type_sym("anti"), True)
					_edge = trj.Graph_Edge( _eval_dep.dep_string(), _eval_dep)
					self.edges.append((_eval_dep , _edge))
					_node_s2.successors.append(( _edge , _node_s1 ))

	def _scalar_dep_edge(self, var_pair, output_dep):
		_node_s1 , _node_s2 = var_pair.var_a.skel_obj_node, var_pair.var_b.skel_obj_node
		_common_loops = var_pair.var_dep.common_loops
		_eval_dep = None
		for _idx, _loop in enumerate(_common_loops):
			_eval_dep = Dependence_Edge(var_pair , (_idx, _loop),
											_common_loops , "scalar")
			_edge = trj.Graph_Edge( _eval_dep.dep_string(), _eval_dep)
			self.edges.append((_eval_dep , _edge))
			_node_s1.successors.append(( _edge , _node_s2 ))

			#If there is a loop independent dependence, it leads to a cycle
			# capture this with a reverse edge
			if output_dep :
				continue

			_eval_dep_rev = Dependence_Edge(var_pair , (_idx, _loop),
											_common_loops , "scalar", True)
			_edge_rev = trj.Graph_Edge( _eval_dep_rev.dep_string(), _eval_dep_rev)
			self.edges.append((_eval_dep_rev , _edge_rev))
			_node_s2.successors.append(( _edge_rev , _node_s1 ))
		else :
			if var_pair.var_dep.scalar_LI_Dep == True :
				_eval_dep = Dependence_Edge(var_pair , (max_loop_limit, None),
												_common_loops , "scalar")
				_edge = trj.Graph_Edge( _eval_dep.dep_string(), _eval_dep)
				self.edges.append((_eval_dep , _edge))

				if _node_s1.payload.assign_stmt.lineno <= _node_s2.payload.assign_stmt.lineno :
					_node_s1.successors.append(( _edge , _node_s2 ))
				else:
					_node_s2.successors.append(( _edge , _node_s1 ))



	def _connect_nodes(self, var_pair, output_dep=False):
		# Optimise for scalar variables to reduce memory
		if var_pair.var_dep.scalar_LI_Dep is not None :
			self._scalar_dep_edge(var_pair , output_dep)
			return

		# Determine if we are at static or runtime compilation
		resolve_dep_matrix = lambda var_dep : var_dep.dep_matrix \
											if solve.phase.get_curr_stg_sym() == 'aot' \
											else var_dep.rdep_matrix
		_vdep = [[ _dir for _dir in _vec ] for _vec in resolve_dep_matrix(var_pair.var_dep)]
		_vdep = solve.dep_vector_expand( var_pair.var_dep.common_loops  , _vdep )
		for _vec in _vdep :
			self._determine_dep_edge(_vec , var_pair , output_dep)


	# Returns the compile-eval stage that is required to prove all
	# dependences within the Loop-Nest DAG
	def _aot_build(self):
		_ret = self.codegen_stage  # Last known stage for evaluation
		xvp, opvp = self.loop_nest.variable_pairs
		for _idx, _vp in enumerate(xvp) :
			if _vp.var_dep.dependence is None :
				return solve.phase.get_next_stg_sym()
			elif _vp.var_dep.dependence is False :
				continue # No edge to generate
			#Dependence found between pair.
			self._connect_nodes( _vp , False)


		for _idx, _vp in enumerate(opvp) :
			if _vp.var_dep.dependence is None :
				return solve.phase.get_next_stg_sym()
			elif _vp.var_dep.dependence is False :
				continue # No edge to generate
			#Dependence found between pair.
			self._connect_nodes( _vp , True)
		return _ret

	def _aot_deconstruct(self):
		self.edges.clear()
		for _node in self.stmt_list :
			_node.successors.clear()
		return True

	def _runtime_deconstruct(self):
		self.edges.clear()
		for _node in self.stmt_list :
			_node.successors.clear()
		assert False, "when are we calling this ???" 
		return True


	# Returns the compile-eval stage that is required to prove all
	# dependences within the Loop-Nest DAG
	def _runtime_build(self):
		xvp, opvp = self.loop_nest.variable_pairs
		for _idx, _vp in enumerate(xvp) :
			if _vp.var_dep.rdependence is False :
				continue # No edge to generate
			#Dependence found between pair.
			self._connect_nodes( _vp , False)

		for _idx, _vp in enumerate(opvp) :
			if _vp.var_dep.rdependence is False :
				continue # No edge to generate
			#Dependence found between pair.
			self._connect_nodes( _vp , True)
		return solve.phase.get_curr_stg_sym()

	# Returns the stage at which loop-nest can be evaluated
	# Eg.   Curr-stage   | Resulting Decision |  Actions
	#       1.  AOT      |       AOT          |   Run - solver
	#       2.  AOT      |       Run-time     |   Clean-up DAG edge-creation
	#       3.  Run-time |       AOT          |   Use already solved DAG
	#       1.  Run-time |       Run-time     |   Run all deferred dependence checkers
	def build(self):
		_curr_stage = solve.phase.get_curr_stg_sym()
		_curr_stage_sym = "_" + _curr_stage + "_build"
		_build = getattr(self, _curr_stage_sym   , None)
		if _build is None :
			return _build

		_ret = _build()
		# Move the clean-up condition into a generic function - for cleaner code
		if solve.phase.compilation_stages[_ret] > solve.phase.get_curr_stg() :
			return self.defer_eval_stage()

		return _curr_stage

	# Method used to postpone evaluation of dependences to the next stage.
	# This occurs when array dependences cannot be evalutated at run-time
	# due to e.g. unknown values of loop invariant variables
	def defer_eval_stage(self):
		return self.teardown()


	def teardown(self):
		# prepare to defer to the next stage and leave maximum useful partial eval information
		_curr_stage = solve.phase.get_curr_stg_sym()
		_cleanup_sym = "_" + _curr_stage + "_deconstruct"

		_clean_up = getattr(self, _cleanup_sym   , None)
		if _clean_up :
			_clean_up()

		_next_stage = solve.phase.get_next_stg_sym()
		self.codegen_stage = _next_stage
		return _next_stage


	def display(self):
		xvp, opvp = self.loop_nest.variable_pairs
		print("=================\nCross - Variable List ----- DAG\n========================")
		for _idx, _vp in enumerate(xvp) :
			print("{0} <----> {1}".format(self.vfunc_name + "_" + str( _vp.var_a.skel_obj_node.payload.assign_stmt.lineno), self.vfunc_name + "_" + str( _vp.var_b.skel_obj_node.payload.assign_stmt.lineno) ))
			print("Var-pair {0} <----> {1}".format( _vp.var_a.var_ref , _vp.var_b.var_ref))

		print("=================\nOutput - Variable List ----- DAG\n=========================")
		for _idx, _vp in enumerate(opvp) :
			print("{0} <----> {1}".format(self.vfunc_name + "_" + str( _vp.var_a.skel_obj_node.payload.assign_stmt.lineno), self.vfunc_name + "_" + str( _vp.var_b.skel_obj_node.payload.assign_stmt.lineno) ))
			print("Var-pair {0} <----> {1}".format( _vp.var_a.var_ref , _vp.var_b.var_ref))

	def display_node_graph(self) :
		trj.display_graph( self.stmt_list )


	def display_scc_graph(self, super_nodes ):
		trj.display_scc_graph( super_nodes )


	def display_scc_img_graph(self, super_nodes, nodes=None, filename_prefix="ldag"):
		get_nodes = lambda node_list : node_list if  node_list else self.stmt_list
		trj.render_graph( get_nodes(nodes) , filename_prefix )
		trj.render_scc_graph( get_nodes(nodes) , super_nodes , filename_prefix )



class Vec_LoopContainer :
	def __init__(self, loop_skel, nest_lvl , children=None):
		self.loop_skel = loop_skel
		self.nest_lvl = nest_lvl
		self.children = children
		self.dom_size = None

	@property
	def loop_skel(self):
		if self._loop_skel is None :
			return None
		return self._loop_skel()

	@loop_skel.setter
	def loop_skel(self, skel_obj):
		if callable( skel_obj):
			skel_obj = skel_obj()

		if skel_obj.ntype == "For" :
			self._loop_skel = weakref.ref(skel_obj)
		else :
			self._loop_skel = None

	@property
	def children(self):
		return self._children

	@children.setter
	def children(self , children):
		# list of child objects - could be for-loop skel container objects or
		# Graph-nodes from the shadow graph
		if children is None :
			self._children = []
		else :
			self._children = children

	@property
	def dom_size(self):
		return self._dom

	@dom_size.setter
	def dom_size(self, domain):
		self._dom = domain

	def _str_loop_hdr(self) :
		_trgt = astor.code_gen.to_source(self.loop_skel.record["for_targets"].node)
		_lim  = astor.code_gen.to_source(self.loop_skel.record["for_limits"].node)
		return "for {0} in {1} :".format( _trgt.rstrip("\n "), _lim.rstrip("\n "))


	def _extract_domsize(self, runtime_args):
		_lim = self.loop_skel.record['for_limits'].limits['end']
		if _lim[0] == 'imm' :
			_dom_end =  _lim[1]
		elif solve.phase.get_curr_stg_sym() == 'runtime':
			_dom_end = getattr(runtime_args, _lim[1], None)
		else :
			_dom_end = None
		return _dom_end

	def display(self, dev, kdict, drv_dict , lvl=0) :
		print("{0}{1}".format(2*lvl*" ", self._str_loop_hdr()))
		for _child in self.children :
			_child.display(dev, kdict, drv_dict , lvl+1)


	def gen_kernel(self, _kdict, scev_ref, lvl=1):
		for _child in self.children :
			_child.gen_kernel(_kdict, scev_ref ,lvl+1)

	def gen_drv(self, _kdict, scev_ref, lvl=1 ):
		self.dom_size = self._extract_domsize(None)
		for _child in self.children :
			_child.gen_drv(_kdict, scev_ref, lvl+1)

	def runtime_gen_kernel(self, _kdict, scev_ref, runtime_args, lvl=1):
		for _child in self.children :
			_child.runtime_gen_kernel(_kdict, scev_ref, runtime_args, lvl+1)

	def runtime_gen_drv(self, _kdict, scevref_map, runtime_args, lvl=1 ):
		self.dom_size = self._extract_domsize(runtime_args)
		for _child in self.children :
			_child.runtime_gen_drv(_kdict, scevref_map, runtime_args, lvl+1)


	def gen_host_loop_drv(self, lvl, kdict, dev_type):
		_sanitise_empty = lambda co_txt : "" if co_txt == "" else co_txt + "\n"
		_append = lambda head, body : body if body == "" else head + body

		_drv_body = "" 
		for _child in self.children :
			_drv_body += _sanitise_empty( _child.gen_host_loop_drv(lvl+1,kdict,dev_type))

		return  _append("{0}{1}\n".format(2*lvl*" ", self._str_loop_hdr()),  _drv_body)

class Vec_Function_Argument:
	def __init__(self, name, dim_str, dtype=None, vtype=None ):
		self.name = name
		self.dim_str = dim_str
		self.dtype = dtype
		self.vtype = vtype

	@property
	def dtype(self):
		return self._dtype

	@dtype.setter
	def dtype(self, dtype):
		self._dtype = dtype

	@property
	def vtype(self):
		return (self._vtype, self._rec)

	@vtype.setter
	def vtype(self, vtype):
		if vtype is None :
			self._vtype , self._rec = None, None 
			return 

		if type(vtype) is tuple :
			_vtype, self._rec = vtype 
			if not callable(self._rec):
				self._rec = weakref.ref(self._rec())
		else : 
			_vtype = vtype 
			self._rec = None

		if _vtype in [ "vec", "iterator", "iterdom_max" , "scev_trgt", "scev_src"\
						, "scev_array" , "looplim_iv", "constant" ]:
			self._vtype = _vtype
		else : 
			self._vtype = None
	

	@property
	def dim_str(self):
		return self._dim_str

	@dim_str.setter
	def dim_str(self, dim_string ):
		self._dim_str = dim_string



	def arg_name(self, caller=True):
		_stringize = lambda obj : obj if type(obj) is str else str(obj)

		_vt, _rec = self.vtype 
		if _vt is None : 
			return self.name

		if _vt == "vec" : 
			if caller == True :
				return "_cvec_" + self.name
			return self.name 
		elif _vt in [ "scev_src",  "scev_trgt" , "looplim_iv", "constant", "iterator"]:
			return self.name
		elif _vt == "iterdom_max" : 
			if caller == True :
				return _stringize( _rec().record["for_limits"].limits["end"][1] )
			return self.name 
		elif _vt == "scev_array" : 
			return "scevref_arr_{0}".format(self.dtype)


class Vec_StmtContainer :
	def __init__(self, graph_node , parallel_dims ):
		self.graph_node = graph_node
		self.parallel_dims = parallel_dims
		self.par_threads = { _key : None for _key in Device_Types.keys() }

	@property
	def graph_node(self):
		return self._graph_node()

	@graph_node.setter
	def graph_node(self, node):
		self._graph_node = weakref.ref(node)

	@property
	def driver(self):
		return self._driver

	@driver.setter
	def driver(self,val):
		mk_list = lambda it : it if type(it) is list else [it]
		self._driver = mk_list(val)

	@property
	def par_threads(self):
		return self._par_threads

	@par_threads.setter
	def par_threads(self, dimensions):
		self._par_threads = dimensions


	#def create_kernel(self , dev_type ):
	#	_kern = Device_Types[dev_type](self.graph_node , self.parallel_dims)
	#	return _kern.create_kernel()

	#def create_driver(self , dev_type ):
	#	_driver = Device_Types[dev_type](self.graph_node , self.parallel_dims)
	#	#return _driver.create_driver()

	def display(self, dev, kdict, drv_dict ,lvl=0):
		if dev == 'gpu':
			if kdict[self.graph_node.payload][dev].scevref_kernel :
				_kern_str = kdict[self.graph_node.payload][dev].scevref_kernel_core
			else : 
				_kern_str = kdict[self.graph_node.payload][dev].raw_kernel
		else : 
			_kern_str = kdict[self.graph_node.payload][dev].raw_kernel
		
		print("=========================")
		print("Created Kernel [ {0} ]>>>>\n{1}\nCreated Driver [ {0} ]>>>>\n{2}".format( dev , _kern_str, drv_dict[dev]))
		print("=========================")

	def gen_kernel(self, _kdict, scev_ref, lvl=1 ):
		dev_kernel_dict = {}
		for _key in Device_Types.keys():
			_device = Device_Types[_key](self.graph_node, self.parallel_dims)
			_device.gen_kernel(scev_ref, lvl) # do not increment nesting level for the call to the device kernel
			dev_kernel_dict[_key] = _device

		_kdict[self.graph_node.payload] = dev_kernel_dict



	def gen_drv(self, kdict, scev_ref, lvl=1 ):
		_stmt = kdict[self.graph_node.payload]
		for _device in Device_Types.keys():
			_dev = _stmt[_device]
			# do not increment nesting level
			# for the call to the device kernel
			self.par_threads[_device]  = _dev.gen_drv(scev_ref, lvl)


	def runtime_gen_kernel(self, _kdict, scev_ref, runtime_args, lvl=1 ):
		dev_kernel_dict = { }
		#for _key in [ _key for _key in Device_Types.keys() if _key != 'interpreter']:
		for _key in Device_Types.keys() :
			_device = Device_Types[_key](self.graph_node, self.parallel_dims)
			_device.runtime_gen_kernel(scev_ref, runtime_args,lvl) # do not increment nesting level for the call to the device kernel
			dev_kernel_dict[_key] = _device

		_kdict[self.graph_node.payload] = dev_kernel_dict


	def runtime_gen_drv(self, kdict, scevref_map, runtime_args, lvl=1 ):
		_stmt = kdict[self.graph_node.payload]
		#for _device in [ _key for _key in Device_Types.keys() if _key != 'interpreter']:
		for _device in Device_Types.keys() :
			_dev = _stmt[_device]
			# do not increment nesting level for
			# the call to the device kernel
			self.par_threads[_device]  = _dev.runtime_gen_drv(scevref_map, runtime_args, lvl)

	def gen_host_loop_drv(self, lvl, kdict, dev_type):
		_sanitise_null = lambda co_txt : "" if co_txt is None else co_txt
		_append = lambda head, body : body if body == "" else head + body
		_kernels = kdict[self.graph_node.payload]
		_drv = _kernels[dev_type]
		return  _append("{0}".format(2*lvl*" ") , _sanitise_null( _drv.raw_drv))



class Loop_Vectorize :
	def __init__(self, shadow_graph, graph_builder ):
		self.shadow_graph = shadow_graph
		self.graph_builder = graph_builder
		self.exec_order = None

		# List of scalar vars passed by ref.
		# This is done because the values have to be returned back to host for
		# for further reading
		self.scev_ref = self._init_scev_ref_list()
		self.vec_ref = self._init_vec_ref_list()
		self.dyn_lim, _loop_idx  = self._init_dyn_looplim()
		self.vec_ref = self._add_const_params( self.vec_ref , _loop_idx)
		self.kernels = {}
		self.drivers = {} 
		self.compiled_drv = {}
		self.xfer_cost = {}

	@property
	def graph_builder(self):
		_ret = getattr(self,"_graph_builder",None)
		if _ret :
			return _ret()
		return None

	@graph_builder.setter
	def graph_builder(self, build):
		if type(build) is DAG_Builder :
			self._graph_builder = weakref.ref(build)
		else :
			self._graph_builder = None


	@property
	def exec_order(self ):
		return self._exec_order

	@exec_order.setter
	def exec_order(self, order ):
		if type(order) is list :
			if order :
				self._exec_order = order
			else :
				self._exec_order = []
		elif order is None :
			self._exec_order = []
		else :
			self._exec_order = [ order ]


	@property
	def scev_ref(self):
		return self._scev_ref

	@scev_ref.setter
	def scev_ref(self, scalars):
		if type(scalars) is list :
			if scalars :
				self._scev_ref = scalars
			else :
				self._scev_ref = []
		elif scalars is None :
			self._scev_ref = []
		else :
			self._scev_ref = [ scalars ]


	@property
	def kernels(self):
		return self._kernels

	#Dictionary key: loop-nest function , value = [ kernel-accelerator's ]
	@kernels.setter
	def kernels(self, klist):
		self._kernels = klist

	@property
	def drivers(self):
		return self._drivers

	#Dictionary key: loop-nest function , value = [ kernel-accelerator's ]
	@drivers.setter
	def drivers(self, drv_map):
		self._drivers = drv_map


	def _init_vec_ref_list(self):
		_vec_list = uniq_var_txfr_list( self.graph_builder.loop_nest.target_vars \
									, None \
									, lambda var_bind : (True,"vec") if len(var_bind.var_ref["axes"]) > 0 \
																	else (False,None))

		def add_source_vars( var_bind, scev_refs ):
			if len(var_bind.var_ref["axes"]) > 0 :
				return (True,"vec")
			_loop_tgts = { _idx for _loop in var_bind.index_list for _idx in _loop.record["for_targets"].target }
			if var_bind.var_ref["var"] in _loop_tgts :
				return (False, None)

			if var_bind.var_ref["var"] in scev_refs :
				return (False, None)
			return (True, "scev_src")

		_vec_list = uniq_var_txfr_list( self.graph_builder.loop_nest.source_vars
								, _vec_list\
								, lambda var_bind : add_source_vars(var_bind ,\
										[ arg.arg_name(True) for arg in self.scev_ref]))
		return _vec_list

	def _init_scev_ref_list(self):
		_scev_list = uniq_var_txfr_list( self.graph_builder.loop_nest.target_vars \
								, None \
								, lambda var_bind : (True,"scev_trgt") if len(var_bind.var_ref["axes"]) == 0 \
														else (False,None))
		return _scev_list

	def _add_const_params(self, filter_list , loop_indices ):
		dim_str = lambda ty,dim_list : (ty,"") if len(dim_list) <= 0 else (ty, "[" + ":," * (len(dim_list)-1) + ":]")
		_init_filter_list = lambda var_list : [] if var_list is None else var_list
		_var_list  = _init_filter_list(filter_list)

		for _varbind in self.graph_builder.loop_nest.variable_list :
			for _sub in _varbind.var_ref["axes"]:
				for _idx_bind in [ _key for _key in _sub.keys() if _key != '__Constant__'] :
					for _fvar in _var_list :
						if _fvar.name == _idx_bind or _idx_bind in loop_indices  :
							break
					else : 
						_ty, _dim_str =  dim_str("_EvalType", [])
						_var_list.append(Vec_Function_Argument(_idx_bind , _dim_str, _ty, "constant"))
		
		return _var_list
					
	def _llbind_ref( self, _limit_vars ):
		# if any reference in a var-axes is not a loop-target
		# add to dynamic-ref list
		for _varbind in self.graph_builder.loop_nest.variable_list :
			_loop_tgt = [ _tgt for loopbind in _varbind.index_list \
								for _tgt in loopbind.record["for_targets"].target ]
			for _sub in  _varbind.var_ref['axes']:
				for _idx_key in _sub.keys() :
					if _idx_key == '__Constant__':
						continue
					elif _idx_key not in _loop_tgt and _idx_key not in _limit_vars :
						_limit_vars += [ _idx_key ]
		return _limit_vars

	def _init_dyn_looplim( self ):
		rec_parser = self.graph_builder
		rec_parser.rewind()
		_tok = rec_parser.consume()

		_lim_vars = []
		_loop_idx_tgt = [] 
		while _tok :
			if _tok.ntype == 'For' :
				# Add any targets to the loop indices
				for _tgt in _tok.record['for_targets'].target :
					if _tgt not in _loop_idx_tgt :
						_loop_idx_tgt += [ _tgt ] 
					
				# Add any non-constant evaluated limits 
				for _key in ["start", "end" , "step"]:
					if _tok.record['for_limits'].limits[_key][0] == "lazy" :
						_ll_sym = _tok.record['for_limits'].limits[_key][1]
						if _ll_sym not in _lim_vars :
							_lim_vars.append( _tok.record['for_limits'].limits[_key][1] )
			_tok = rec_parser.consume()

		_lim_vars = self._llbind_ref( _lim_vars )
		_dyn_limit_var = self._add_uniq_looplimits( self.scev_ref + self.vec_ref, _lim_vars )
		rec_parser.rewind()
		return _dyn_limit_var, _loop_idx_tgt

	def _add_uniq_looplimits(self, orig_list, search_list):
		_uniq_lims = []
		dim_str = lambda ty,dim_list : (ty,"") if len(dim_list) <= 0 else (ty, "[" + ":," * (len(dim_list)-1) + ":]")
		for _lim_var in search_list :
			for _arg_var in  orig_list :
				if _arg_var.name == _lim_var :
					break
			else :
				_ty, _dim_str = dim_str("_EvalType",[])
				_uniq_lims.append( Vec_Function_Argument(_lim_var, _dim_str, _ty , "looplim_iv"))
		return _uniq_lims

	def _resolve_dev_txfr(self,dev_type):
		_data_txfr_obj = Device_Txfr_Types[dev_type]( self.graph_builder.vfunc_name\
									, self.vec_ref, self.scev_ref, self.dyn_lim)
		return _data_txfr_obj


	def generate_loop_skeleton(self):
		#self.graph_builder.display_node_graph()
		self.exec_order +=  self._generate( self.shadow_graph.shadow_nodes , 0 \
											, "{0}_ldag_lvl_".format(self.graph_builder.vfunc_name) , 0)
		#self.graph_builder.display_node_graph()
		#self.graph_builder.display_scc_graph(reversed(super_nodes))
		#self.graph_builder.display_scc_img_graph(reversed(super_nodes))

	#Class strucuture to hold meta-data about the looping structure
	#Parent = [  obj where each obj is type For-Type  or Node-type ]
	# obj-type :| For-Type -> Topo-order of either
	#                         :| For-Type itself (for nested loops )
	#                         :| Node-Type
	# obj-type :| Node-Type -> list of dimensions we can parallelize

	#Remove edges mid-processing of code-generate algorithm
	# All edges at the specified loop-level are deleted
	# along with all outgoing
	def _remove_dep_edges( self, scc , lvl ):
		for _node in scc.node_list:
			# copy the edge list to ensure consistency of list iterator while looping
			# Ensure old list is replaced with pruned edge-list
			_dep_edges = [ _edge for _edge in _node.successors ]
			for _edge in _node.successors :
				_lbl , _succ = _edge
				if _succ not in scc.node_list :
					_dep_edges.remove(_edge)
					continue

				_loop_idx , _loop = _lbl().payload.carry_loop
				if lvl == _loop_idx :
					_dep_edges.remove(_edge)
			else :
				_node.successors = _dep_edges


	def _generate(self, sgraph, loop_lvl , prefix , scc_n ):
		lvl_carry_loop = lambda nodes , lvl : nodes[0].payload.enclosing_loops[lvl]

		_this_lvl_exec_order = []
		scc_list = trj.strongly_connected_components( sgraph )
		#super_nodes = trj.connect_super_nodes(scc_list)    # this may only be required for debugging and rendering
		_rprefix = prefix + "{0:02d}-{1:02d}_".format( loop_lvl , scc_n )
		#self.graph_builder.display_scc_img_graph(super_nodes, self.shadow_graph.shadow_nodes, filename_prefix= _rprefix )
		for _idx , _scc in enumerate(reversed(scc_list)):
			if trj.is_cyclic(_scc) :
				self._remove_dep_edges( _scc, loop_lvl )
				_sub_graph_actions = self._generate( _scc.node_list, loop_lvl + 1, _rprefix , _idx )
				#print("Lvl-{0}, node-list = {1}".format( loop_lvl , [ _ty().ntype for _ty in _scc.node_list[0].payload.enclosing_loops ]))
				_generated_loop = Vec_LoopContainer( lvl_carry_loop(_scc.node_list,loop_lvl) , loop_lvl , _sub_graph_actions )
				_this_lvl_exec_order += [ _generated_loop ] # append actions to the list of actions
			else :
				_shadow_node = _scc.node_list[0]
				_rem_dim = len(_shadow_node.payload.enclosing_loops) - loop_lvl
				_this_lvl_exec_order.append( Vec_StmtContainer( _shadow_node , _rem_dim ))
		return _this_lvl_exec_order


	def display(self):
		print("==========================\nGenerated Loop nest")
		for _idx, _obj in enumerate(self.exec_order):
			_obj.display("gpu", self.kernels , self.drivers  , 1)
		print("End of Generated Loop Nest\n==========================")

	# Create kernels for each of the statements
	def gen_kernels(self):
		for _obj in self.exec_order :
			_obj.gen_kernel( self.kernels, self.scev_ref )

	def gen_drv(self):
		for _obj in self.exec_order :
			_obj.gen_drv( self.kernels, self.scev_ref )

	# Create kernels at runtime for each of the statements
	def runtime_gen_kernels(self, runtime_args):
		for _obj in self.exec_order :
			_obj.runtime_gen_kernel( self.kernels, self.scev_ref , runtime_args)

	def runtime_gen_drv(self, runtime_args, scevref_map):
		for _obj in self.exec_order :
			_obj.runtime_gen_drv( self.kernels, scevref_map , runtime_args)


	def finalize_drv(self, dev_type, synchronize=False):
		if synchronize :
			_indent = 1 
		else :
			_indent = 2 

		# For any type-dependent accelerator
		# if any kernel is a scevref kernel - do not generate
		# the finalized driver
		if solve.phase.get_curr_stg_sym() == 'aot' \
					and dev_type not in ['interpreter']:
			for _stmt in self.kernels.keys():	
				if self.kernels[_stmt][dev_type].scevref_kernel : 
					return None

		# For each device, obtain Data transfer class and populate with relevant code
		_data_txfr_obj = self._resolve_dev_txfr(dev_type) 
		_stmt, _xfer_todev = _data_txfr_obj.prologue(1,"stream", self.graph_builder)
		for _obj in self.exec_order :
			_stmt += _obj.gen_host_loop_drv(_indent, self.kernels, dev_type) + "\n"

		_epilogue, _xfer_tohost = _data_txfr_obj.epilogue(1,"stream", self.graph_builder)
		_stmt += _epilogue
		self.xfer_cost[dev_type] = _xfer_todev + _xfer_tohost
	
		self.drivers[dev_type] = _stmt
		return _stmt 


	def runtime_finalize_drv(self, dev_type, runtime_args, scev_map, synchronize=False):
		if synchronize :
			_indent = 1 
		else :
			_indent = 2 

		_data_txfr_obj = self._resolve_dev_txfr(dev_type) 
		_stmt, _xfer_todev = _data_txfr_obj.runtime_prologue(1,"stream", self.graph_builder, scev_map, runtime_args)
		for _obj in self.exec_order :
			_stmt += _obj.gen_host_loop_drv(_indent, self.kernels, dev_type) + "\n"

		_epilogue, _xfer_tohost = _data_txfr_obj.runtime_epilogue(1,"stream", self.graph_builder, scev_map, runtime_args)
		_stmt += _epilogue
		self.xfer_cost[dev_type] = _xfer_todev + _xfer_tohost
	
		self.drivers[dev_type] = _stmt
		return _stmt 

	# Re-init every class member except the classification 
	# of arguments and variables at run-time 
	# are determined at run-time
	def runtime_reinit(self, shadow_graph, graph_builder ):
		self.shadow_graph = shadow_graph
		self.graph_builder = graph_builder
		self.exec_order = None

		self.kernels = {} 
		self.drivers = { "interpreter" : self.drivers["interpreter"] }
		self.xfer_cost = {}


	# This function will be called only after all the scevref kernels and runtime kernel 
	# generation is called.
	def runtime_patch_types(self, scevref_map, runtime_args):
		_resolve_type = lambda _arr : "{0}".format(_arr.dtype) if type(_arr) is np.ndarray \
										else hwb.HW_supported_types[type(_var)]
		for _arg in self.vec_ref : 
			_var = getattr( runtime_args , _arg.arg_name(False), None)
			_arg.dtype = _resolve_type(_var)
		for _arg in self.scev_ref : 
			_var = getattr( runtime_args , _arg.arg_name(True), None)
			_arg.dtype = _resolve_type( _var)
		for _arg in self.dyn_lim : 
			_var = getattr( runtime_args , _arg.arg_name(True), None)
			_arg.dtype = _resolve_type( _var)


	# Patch driver with run-time type arguments
	# This method is called only after types
	# have been patched into the loop-nest arguments
	# with runtime_patch_types()
	def runtime_patch_driver_args(self, scevref_map, trgt_drv=["cpu"]):
		self.compiled_drv = { _trgt : None for _trgt in Device_Txfr_Types.keys() }
		for _trgt in trgt_drv :
			_data_txfr_obj = self._resolve_dev_txfr(_trgt)
			_patch_drv_types = getattr( _data_txfr_obj, "runtime_drv_signature", None)
			if not _patch_drv_types or not callable( _patch_drv_types ):
					self.compiled_drv[_trgt] = None
					continue
			self.compiled_drv[_trgt] = _patch_drv_types(0, scevref_map) + "\n" + self.drivers[_trgt]

def uniq_var_txfr_list(orig_list, filter_list , filt ):
	dim_str = lambda ty,dim_list : (ty,"") if len(dim_list) <= 0 else (ty, "[" + ":," * (len(dim_list)-1) + ":]")
	_init_filter_list = lambda var_list : [] if var_list is None else var_list 
	_var_list = _init_filter_list( filter_list )

	for _stmt_var in orig_list : 
		for _tvar in _var_list : 
			if _tvar.name == _stmt_var.var_ref["var"]:
				break 
		else :
			_valid, _k_arg_type = filt( _stmt_var )
			if _valid :
				_ty, _dim_str =  dim_str("_EvalType", _stmt_var.var_ref["axes"])
				_var_list.append(Vec_Function_Argument(_stmt_var.var_ref["var"] \
														, _dim_str, _ty, _k_arg_type))
	return _var_list




def aot_graph( loop_agg  ):
	#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	#print("DAG-Builder")
	#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	for _idx_func, _func in enumerate(loop_agg.functions):
		for _idx_loop, _loop_nest in enumerate(_func.loop_truss):
			#print("======================================")
			#print(" Function {0} :: Loop list {1} , type(_func) = {2}".format(_idx_func, _idx_loop , _loop_nest ))
			#_loop_nest.graph_builder.display()
			_analysis_phase = _loop_nest.graph_builder.build()
			if solve.phase.compilation_stages[_analysis_phase] > solve.phase.get_curr_stg() :
				#print("Analysis of Function[{0}]:Loop[{1}] deferred to stage {2}".format(_idx_func , _idx_loop , _analysis_phase))
				_scratch_graph = trj.Shadow_Graph(_loop_nest.graph_builder.stmt_list)
				_loop_nest.code_gen = Loop_Vectorize( _scratch_graph , _loop_nest.graph_builder)
				_loop_nest.code_gen.gen_drv()
				_loop_nest.code_gen.finalize_drv('interpreter')
				continue

			#for _didx , (_eval_dep , edge)  in enumerate(_loop_nest.graph_builder.edges ):
			#	print("eval-dep[{0}] = {1}".format(_didx , _eval_dep ))
			#	_dep_edge  = edge.payload
			#	print("edge [{0}] = {1} , _lbl = {2}".format(_didx ,edge.name  , edge.payload.dep_type ) )

			_scratch_graph = trj.Shadow_Graph(_loop_nest.graph_builder.stmt_list)

			#for _didx , node  in enumerate(_scratch_graph.shadow_nodes) :
			#	print("scratch-graph[{0}] --> {1}".format(_didx , node))
			#	for _eidx, edge in enumerate(node.successors) :
			#		_lbl , _succ = edge
			#		print(" edge[{0}] ----  {1} ---- > {2}".format(_eidx , _lbl , _succ ))

			_loop_nest.code_gen = Loop_Vectorize( _scratch_graph , _loop_nest.graph_builder)
			_loop_nest.code_gen.generate_loop_skeleton()
			#_loop_nest.code_gen.display()
			_loop_nest.code_gen.gen_kernels()
			_loop_nest.code_gen.gen_drv()
			_drivers = {} 

			for _dev in [ "interpreter" , "cpu", "gpu" ] :
				_drivers[_dev] = _loop_nest.code_gen.finalize_drv(_dev)

			for _stmt in _loop_nest.code_gen.kernels.keys():
				print("============Statement=============")
				print("{0}".format(astor.codegen.to_source(_stmt.assign_stmt.node).rstrip("\n ")))
				print("============Statement=============")

				for dev in _loop_nest.code_gen.kernels[_stmt].keys():
					_kern = lambda kern : kern.scevref_kernel_core if kern.scevref_kernel else kern.raw_kernel
					print("kernel--{0} = \n{1}==============".format(dev, _kern(_loop_nest.code_gen.kernels[_stmt][dev])))
					print("drv--{0} = {1}\n==============".format(dev, _loop_nest.code_gen.kernels[_stmt][dev].raw_drv))

			print("=============Loop-Nest  {0}===============".format(_loop_nest.graph_builder.vfunc_name))
			for _dev in [ "interpreter" , "cpu", "gpu" ] :
				print(">>> Driver for {0} = >>\n{1}".format( _dev,_drivers[_dev]))

	return None





def _display_vec_struct(_lpcont, cost_metric_opts, indent=0):
    for _idx, _obj in enumerate(_lpcont):
        if type(_obj) is Vec_LoopContainer:
            print("1.{0}Record-{1}, Lvl-{2}, Children- {3}".format("--" * indent, _obj.dom_size,
                                                                   _obj.nest_lvl, _obj.children))
            _display_vec_struct(_obj.children, cost_metric_opts, indent + 1)
        elif type(_obj) is Vec_StmtContainer:
            print("{0}{1} -- par-threads => {2}=".format("--" * indent, _obj, cost_metric_opts))
            print("{0}{1} -- par-threads-cpu = {2}, cost = {3}, par-product = {4}".format(
                "--" * (indent + 1), _obj, _obj.par_threads['cpu'].seq_loop_dom,
                _obj.par_threads['cpu'].cost(cost_metric_opts), _obj.par_threads['cpu'].parallel_dom_size ))
            print("{0}{1} -- par-threads-vm = {2}, cost = {3}".format(
                "--" * (indent + 1), _obj, _obj.par_threads['interpreter'].seq_loop_dom,
                _obj.par_threads['interpreter'].cost(cost_metric_opts)))
            print(
                "{0}{1} -- par-threads-gpu = seq = {2} , hwpar = {3} (loops = {4}), kpar = {5} , cost = {6} ".
                format("--" * (indent + 1), _obj, _obj.par_threads['gpu'].seq_loop_dom,
                       _obj.par_threads['gpu'].hw_par_threads, _obj.par_threads['gpu'].hw_par_loops,
                       _obj.par_threads['gpu'].kernel_loops,
                       _obj.par_threads['gpu'].cost(cost_metric_opts)))
        else:
            raise TypeError


def _lnest_cost(_lpcont, cost_metric_opts):
    _cost = {_dev: 0.0 for _dev in Device_Types}
    for _obj in _lpcont:
        if type(_obj) is Vec_LoopContainer:
            _lcost = _lnest_cost(_obj.children, cost_metric_opts)
            for _dev in Device_Types:
                _cost[_dev] += _lcost[_dev]
        elif type(_obj) is Vec_StmtContainer:
            for _dev in Device_Types:
                _cost[_dev] += _obj.par_threads[_dev].cost(cost_metric_opts)
    return _cost




def runtime_analyse( loop_nest , runtime_args):
	#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	#print("Runtime DAG-Builder")
	#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	_analysis_phase = loop_nest.graph_builder.build()
	#for _didx , (_eval_dep , edge)  in enumerate(loop_nest.graph_builder.edges ):
	#	print("eval-dep[{0}] = edge {1} to node {2}".format(_didx , _eval_dep , edge))
	#	_dep_edge  = edge.payload
	#	print("edge [{0}] = {1} , _lbl = {2} , \n---- payload = {3}".format(_didx ,edge.name  , edge.payload.dep_type , _dep_edge))

	#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	_scratch_graph = trj.Shadow_Graph(loop_nest.graph_builder.stmt_list)
	#for _didx , node  in enumerate(_scratch_graph.shadow_nodes) :
	#	for _eidx, edge in enumerate(node.successors) :
	#		_lbl , _succ = edge
	#		print("scratch graph  edge[{0}] ----  Label= {1}  , Label-name = {2} , Label-payload = {3} ----- > {4}".format(_eidx , _lbl, _lbl().name , _lbl().payload , _succ ))
	loop_nest.code_gen.runtime_reinit( _scratch_graph, loop_nest.graph_builder)
	loop_nest.code_gen.generate_loop_skeleton()

	# Generate all kernels for devices
	# Some of these kernels could potentially be SCEV-Ref kernels
	loop_nest.code_gen.runtime_gen_kernels(runtime_args)
	#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
	#print("Finish DAG-Builder")
	#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")


#	for _idx_func, _func in enumerate(loop_agg.functions):
#		for _idx_loop, _loop_nest in enumerate(_func.loop_truss):
#			print("======================================")
#			print(" Function {0} :: Loop list {1} , type(_func) = {2}".format(_idx_func, _idx_loop , _loop_nest ))
#			_loop_nest.graph_builder.display()
#			_analysis_phase = _loop_nest.graph_builder.build()
#			if solve.phase.compilation_stages[_analysis_phase] > solve.phase.get_curr_stg() :
#				print("Analysis of Function[{0}]:Loop[{1}] deferred to stage {2}".format(_idx_func , _idx_loop , _analysis_phase))
#				_scratch_graph = trj.Shadow_Graph(_loop_nest.graph_builder.stmt_list)
#				_loop_nest.code_gen = Loop_Vectorize( _scratch_graph , _loop_nest.graph_builder)
#				_loop_nest.code_gen.gen_drv()
#				_loop_nest.code_gen.finalize_drv('interpreter')
#				continue
#
#			#for _didx , (_eval_dep , edge)  in enumerate(_loop_nest.graph_builder.edges ):
#			#	print("eval-dep[{0}] = {1}".format(_didx , _eval_dep ))
#			#	_dep_edge  = edge.payload
#			#	print("edge [{0}] = {1} , _lbl = {2}".format(_didx ,edge.name  , edge.payload.dep_type ) )
#
#			_scratch_graph = trj.Shadow_Graph(_loop_nest.graph_builder.stmt_list)
#
#			#for _didx , node  in enumerate(_scratch_graph.shadow_nodes) :
#			#	print("scratch-graph[{0}] --> {1}".format(_didx , node))
#			#	for _eidx, edge in enumerate(node.successors) :
#			#		_lbl , _succ = edge
#			#		print(" edge[{0}] ----  {1} ---- > {2}".format(_eidx , _lbl , _succ ))
#
#			_loop_nest.code_gen = Loop_Vectorize( _scratch_graph , _loop_nest.graph_builder)
#			_loop_nest.code_gen.generate_loop_skeleton()
#			#_loop_nest.code_gen.display()
#			_loop_nest.code_gen.gen_kernels()
#			_loop_nest.code_gen.gen_drv()
#			_drivers = {} 
#
#			for _dev in [ "interpreter" , "cpu", "gpu" ] :
#				_drivers[_dev] = _loop_nest.code_gen.finalize_drv(_dev)
#
#			for _stmt in _loop_nest.code_gen.kernels.keys():
#				print("============Statement=============")
#				print("{0}".format(astor.codegen.to_source(_stmt.assign_stmt.node).rstrip("\n ")))
#				print("============Statement=============")
#
#				for dev in _loop_nest.code_gen.kernels[_stmt].keys():
#					_kern = lambda kern : kern.scevref_kernel_core if kern.scevref_kernel else kern.raw_kernel
#					print("kernel--{0} = \n{1}==============".format(dev, _kern(_loop_nest.code_gen.kernels[_stmt][dev])))
#					print("drv--{0} = {1}\n==============".format(dev, _loop_nest.code_gen.kernels[_stmt][dev].raw_drv))
#
#			print("=============Loop-Nest  {0}===============".format(_loop_nest.graph_builder.vfunc_name))
#			for _dev in [ "interpreter" , "cpu", "gpu" ] :
#				print(">>> Driver for {0} = >>\n{1}".format( _dev,_drivers[_dev]))
#
#	return None
