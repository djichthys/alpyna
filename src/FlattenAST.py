#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import IndexExtract
import Extract_Memory_Dependency as Mem_Dep
import For_Metadata as For_Meta
import Func_Hdr as Func_Hdr
import Utils as flt_util
import weakref
import Flat_Record as flt_rec

def iter_node(node):
    fields = getattr(node, "_fields", None)
    if fields is not None:
        for name in fields:
            val = getattr(node, name, None)
            if val is not None:
                yield name, val
    elif isinstance(node, list):
        for value in node:
            yield "list-item", val


class Accumulator(ast.NodeTransformer):
    def __init__(self, node=None):
        self.visit_stack = []    # every visited node
        self.poi_stack = []    # nearest interesting parent node
        self.nest_level = 0
        self.skeleton = []
        self._func_list = []    # this acts as a list of functions

        # should hold all predicated conditions
        # used to enable analysis of variables in
        self.predicate_stack = []

    @property
    def func_list(self):
        return self._func_list

    @func_list.setter
    def func_list(self, f_list):
        self._func_list = f_list


    # nest level, statement-no , type  ,
    #          |  { "for_targets" : for_targets, "for_limits" : for_limits }
    #          |  { "targets" : [] , "value" = { "var_name" : "" , "axes" : [ ]  } Assign / AugAssign
    #          |  {  "name" : ... , "args" : { arg : annotation } } FunctionDef

    def generic_visit(self, node, nest_level=-1, push=True):
        # push - Boolean
        #      - True - generic visit should push onto stack
        #        False : specialised function would have already pushed onto stack
        #        in both cases the generic visit will pop() off stack
        if push == True:
            self.visit_stack.append(weakref.ref(node))

        if nest_level > -1:
            self.nest_level = nest_level

        node = super().generic_visit(node)

        if push == True:
            self.visit_stack.pop()

        return node

    def visit_If(self, node):
        self.predicate_stack.append(node)
        node = self.generic_visit(node, self.nest_level, False)
        self.predicate_stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.func_list.append(node)

        self.poi_stack.append(weakref.ref(node))
        self.visit_stack.append(weakref.ref(node))

        record = flt_rec.Flat_Record(self.nest_level, node)
        node = record.gen_ast_record(node)
        self.skeleton.append(record)

        self.nest_level += 1
        node = self.generic_visit(node, self.nest_level, False)
        self.nest_level -= 1
        self.poi_stack.pop()
        return node

    def visit_AugAssign(self, node):
        return self.generic_visit(node, self.nest_level, False)

    def visit_Assign(self, node):
        self.visit_stack.append(weakref.ref(node))
        node = self.generic_visit(node, -1, False)
        self.visit_stack.pop()
        #node, record = self.gen_ast_parse_record_Assign(self.nest_level, node)
        #self.skeleton.append(record)

        _cond_predicate = None
        if len(self.predicate_stack) :
            _cond_predicate = self.predicate_stack[-1].test
        record = flt_rec.Flat_Record(self.nest_level, node, _cond_predicate)
        node = record.gen_ast_record(node)
        #flt_util.display_record(record)
        self.skeleton.append(record)

        return node

    def visit_For(self, node):
        self.poi_stack.append(weakref.ref(node))
        self.visit_stack.append(weakref.ref(node))

        record = flt_rec.Flat_Record(self.nest_level, node)
        node = record.gen_ast_record(node)
        self.skeleton.append(record)

        self.nest_level += 1
        node = self.generic_visit(node, -1, False)
        self.nest_level -= 1
        self.poi_stack.pop()
        return node

    def display_func_list(self):
        for idx, node in enumerate(self.func_list):
            print("function[{0:d}] = {1:s} , line={2} , col = {3} ".format(
                idx, node.name, node.lineno, node.col_offset))

    def display_records(self):
        flt_util.display_record(self.skeleton)

    def rcopy(self):
        _skeleton = []
        for _rec in self.skeleton:
            _skeleton += [_rec.rcopy()]
        return _skeleton

if __name__ == '__main__':
    import astor
    import Normalize_Loop
    import Subscript_Partition as sp
    with open("test.py", mode="r") as fd:
        code = fd.read()
        tree = ast.parse(code)

    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print(ast.dump(tree,True, True))
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    _ast = Normalize_Loop.normalize_pass(tree)
    _ast_src = astor.code_gen.to_source(_ast)
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print("Transformed Code")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print("{0}".format(_ast_src))
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    tree = ast.parse(_ast_src)
    acc = Accumulator()
    acc.visit(tree)
    acc.display_records()
    sp.partition(acc)
