#!/home/dejice/work/python-tutorial/ast-venv/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import astor
import sys
import weakref
import For_Metadata as metadata
import Extract_Memory_Dependency as Mem_Dep


class For_Meta_Container:
    def __init__(self, ast_node, loop_nest):
        self.loop_nest = loop_nest
        self.node = ast_node
        self.record = dict()
        self.record['for_limits'] = metadata.For_Limits(getattr(self.node, "iter", None))
        self.record['for_targets'] = metadata.For_Targets(getattr(self.node, "target", None))

    @property
    def node(self):
        return self._node()

    @node.setter
    def node(self, for_node):
        self._node = weakref.ref(for_node)

    @property
    def loop_nest(self):
        return self._loop_nest()

    @node.setter
    def loop_nest(self, nest):
        _ref = lambda obj: obj() if callable(obj) else obj
        self._loop_nest = weakref.ref(_ref(nest))


class Assignment_Container:
    def __init__(self, ast_node, record, containing_loop):
        self.stmt_node = ast_node
        self.record = record
        self.loop_nest = containing_loop

        self.limit_stmt = False
        self.hoistable = True

    @property
    def stmt_node(self):
        return self._stmt_node()

    @stmt_node.setter
    def stmt_node(self, stmt_node):
        self._stmt_node = weakref.ref(stmt_node)

    @property
    def record(self):
        return self._record

    @record.setter
    def record(self, record):
        self._record = record

    @property
    def loop_nest(self):
        return self._loop_nest

    @loop_nest.setter
    def loop_nest(self, nest):
        _ref = lambda obj: obj() if callable(obj) else obj
        self._loop_nest = weakref.ref(_ref(nest))


class LoopNest_Extract(ast.NodeTransformer):
    def __init__(self):
        self.loop_stack = []
        self.loop_nest = []
        self.loop_meta = dict()    # key loop-nest : value = list of sub-for-loop metadata
        self.assign_stmt = dict()
        self.pnode_stack = []

    @property
    def assign_stmt(self):
        return self._assign_stmt

    @assign_stmt.setter
    def assign_stmt(self, stmt_list):
        self._assign_stmt = stmt_list

    @property
    def loop_stack(self):
        return self._loop_stack

    @loop_stack.setter
    def loop_stack(self, stack_init):
        self._loop_stack = stack_init

    @property
    def pnode_stack(self):
        return self._pnode_stack

    @pnode_stack.setter
    def pnode_stack(self, stack_init):
        self._pnode_stack = stack_init

    @property
    def loop_nest(self):
        return self._loop_nest

    @loop_nest.setter
    def loop_nest(self, init_list):
        self._loop_nest = init_list

    @property
    def loop_meta(self):
        return self._loop_meta

    @loop_meta.setter
    def loop_meta(self, meta_init):
        self._loop_meta = meta_init

    @property
    def limit_stmts(self):
        return self._limit_stmts

    @limit_stmts.setter
    def limit_stmts(self, initial_stmts):
        self._limit_stmts = initial_stmts

    def visit_For(self, node):
        if len(self.loop_stack) == 0:
            self.loop_nest.append(weakref.ref(node))

        self.loop_stack.append(node)

        if self.loop_stack[0] not in self.loop_meta:
            self.loop_meta[self.loop_stack[0]] = []
        self.loop_meta[self.loop_stack[0]].append(For_Meta_Container(node, self.loop_nest[-1]))

        node = self.generic_visit(node)
        self.loop_stack.pop()
        return node

    def visit_Assign(self, node):
        if len(self.loop_stack) == 0:
            return node

        _record = {"src": [], "trgt": []}
        _src = Mem_Dep.Assign_MemAccess(node.value)
        node.value = _src.visit(node.value)
        _record["src"] = _src

        _tgt_list = []
        for trgt_idx, _trgt in enumerate(node.targets):
            _xtrct_trgt = Mem_Dep.Assign_MemAccess(_trgt)
            node.targets[trgt_idx] = _xtrct_trgt.visit(_trgt)
            _tgt_list.append(_xtrct_trgt)

        for _tgt in _tgt_list:
            for _dep in _tgt.depends:
                _record["trgt"].append(_dep)

        _new_node = ast.Assign(node.targets, node.value)
        _new_node = ast.fix_missing_locations(node)

        if self.loop_nest[-1]() not in self.assign_stmt:
            self.assign_stmt[self.loop_nest[-1]()] = []

        self.assign_stmt[self.loop_nest[-1]()].append( \
                 Assignment_Container( _new_node , _record \
                      , self.loop_nest[-1]))

        return _new_node

    def visit_FunctionDef(self, node):
        self.pnode_stack.append(node)
        node = self.generic_visit(node)
        self.pnode_stack.pop()
        return node

    def visit_Module(self, node):
        self.pnode_stack.append(node)
        node = self.generic_visit(node)
        self.pnode_stack.pop()
        return node


class Hoist_Limits:
    def __init__(self, lnest_container, looplim_vars):
        self.lnest_container = lnest_container
        self.looplimit_vars = looplim_vars

    @property
    def lnest_container(self):
        return self._lnest_container()

    @lnest_container.setter
    def lnest_container(self, lnest_container):
        _ref = lambda obj: obj() if callable(obj) else obj
        self._lnest_container = weakref.ref(_ref(lnest_container))

    @property
    def looplimit_vars(self):
        return self._looplim_vars

    @looplimit_vars.setter
    def looplimit_vars(self, lim_vars):
        self._looplim_vars = lim_vars

    def _mark_LoopLimit_Stmts(self, _stmt, lnest):
        short_circuit = False
        for _loop in self.lnest_container.loop_meta[lnest]:
            if _loop.node == lnest:
                continue

            for _var in _stmt.record['trgt']:
                for _lim_var in ['start', 'end', 'step']:
                    if _loop.record['for_limits'].limits[_lim_var][0] == 'lazy' \
                      and _var['var']  ==  _loop.record['for_limits'].limits[_lim_var][1] :
                        _stmt.limit_stmt = True
                        short_circuit = True
                        break

                if short_circuit:
                    break

                # Auxilliary check added to cope with only the 'end' of
                # range limits being replaced with a temporary variable 
                if _var['var'] in self.looplimit_vars:
                    _stmt.limit_stmt = True
                    break

            if short_circuit:
                break

    def identify_LoopLimit_Stmts(self):
        for _idx, _node in enumerate(self.lnest_container.loop_nest):
            for _stmt in self.lnest_container.assign_stmt[_node()]:
                self._mark_LoopLimit_Stmts(_stmt, _node())

    def display(self):
        for _idx, _node in enumerate(self.lnest_container.loop_nest):
            print("loop-nest = {0}".format(_node()))
            for _stmt in self.lnest_container.assign_stmt[_node()]:
                print("stmt trgt = {0}--lim-stmt-flag = {1}, hoistable = {2}".format(\
                   _stmt.record['trgt'], _stmt.limit_stmt, _stmt.hoistable ))

    def _check_expr_var_write(self, hoist_tgt, lnest):
        _mem_access = False
        _short_circuit = False
        for _stmt in self.lnest_container.assign_stmt[lnest]:
            if hoist_tgt == _stmt:    # Dont check stmt against itself
                continue

            for _hvar in hoist_tgt.record['src'].depends:
                for _var in _stmt.record['trgt']:
                    if _hvar['var'] == _var['var'] and not _stmt.hoistable:
                        _mem_access = _short_circuit = True
                        break
                if _short_circuit:
                    break

            if _short_circuit:
                break

        return _mem_access

    def _check_expr_loop_idx(self, hoist_tgt, lnest):
        _idx_access = False
        _short_circuit = False
        _cmp_list = [
            _idx for _lp in self.lnest_container.loop_meta[lnest]
            for _idx in _lp.record['for_targets'].target
        ]
        for _hvar in hoist_tgt.record['src'].depends:
            if _hvar['var'] in _cmp_list:
                _idx_access = True
                break

            for _sub in _hvar['axes']:
                for _idx in _cmp_list:
                    if _idx in _sub:
                        _idx_access = _short_circuit = True
                        break
                if _short_circuit:
                    break

            if _short_circuit:
                break
        return _idx_access

    def identify_hoistable(self):
        for _idx, _lnest in enumerate(self.lnest_container.loop_nest):
            for _stmt in self.lnest_container.assign_stmt[_lnest()]:
                if _stmt.limit_stmt == False:
                    continue
                if self._check_expr_var_write(_stmt, _lnest()):
                    _stmt.hoistable = False
                    continue
                if self._check_expr_loop_idx(_stmt, _lnest()):
                    _stmt.hoistable = False
                    continue


class AST_Hoist_Limits(ast.NodeTransformer):
    def __init__(self, lnest_container):
        self.lnest_container = lnest_container
        self.insertion_loc = {}    # key : loop-nest to hoist above , value = parent-list
        self.hoistable_stmts = self._acc_hoistable()
        self.gc_barrier = []

    @property
    def lnest_container(self):
        return self._lnest_container()

    @lnest_container.setter
    def lnest_container(self, lnest_container):
        _ref = lambda obj: obj() if callable(obj) else obj
        self._lnest_container = weakref.ref(_ref(lnest_container))

    @property
    def hoistable_stmts(self):
        return self._hoistable_stmts

    @hoistable_stmts.setter
    def hoistable_stmts(self, hoistables):
        self._hoistable_stmts = hoistables

    @property
    def gc_barrier(self):
        return self._gc_barrier

    @gc_barrier.setter
    def gc_barrier(self, preserve_list):
        self._gc_barrier = preserve_list

    def _acc_hoistable(self):
        _node_list = []
        for _lnest in self.lnest_container.loop_nest:
            _node_list += [ _assgmt.stmt_node \
                 for _assgmt in self.lnest_container.assign_stmt[_lnest()] \
                 if _assgmt.hoistable and _assgmt.limit_stmt ]
        return _node_list

    def _acc_insertion_loc(self, parent_list):
        for _lnest in self.lnest_container.loop_nest:
            if _lnest() in parent_list:
                self.insertion_loc[_lnest()] = parent_list

    def visit_Module(self, node):
        node = self.generic_visit(node)
        self._acc_insertion_loc(node.body)
        return node

    def visit_FunctionDef(self, node):
        node = self.generic_visit(node)
        self._acc_insertion_loc(node.body)
        return node

    def visit_If(self, node):
        node = self.generic_visit(node)

        self._acc_insertion_loc(node.body)
        self._acc_insertion_loc(node.orelse)

        for _child in self.hoistable_stmts:
            if _child in node.body:
                self._delete_node(_child, node.body)
            elif _child in node.orelse:
                self._delete_node(_child, node.orelse)
        return node

    def visit_For(self, node):
        node = self.generic_visit(node)
        for _child in self.hoistable_stmts:
            if _child in node.body:
                self._delete_node(_child, node.body)
            elif _child in node.orelse:
                self._delete_node(_child, node.orelse)

        return node

    def _delete_node(self, node, parent_body):
        parent_body.remove(node)
        self.gc_barrier.append(node)

    def post_process(self):
        for _lnest in self.lnest_container.loop_nest:
            _parent_list = self.insertion_loc[_lnest()]
            for _assgmt in self.lnest_container.assign_stmt[_lnest()]:
                if _assgmt.hoistable and _assgmt.limit_stmt:
                    _idx = _parent_list.index(_lnest())
                    _parent_list.insert(_idx, _assgmt.stmt_node)


def hoist(txt, loop_lim_vars):
    orig_tree = ast.parse(txt)
    _lnest_parser = LoopNest_Extract()
    _lnest_parser.visit(orig_tree)

    _id_hoistable = Hoist_Limits(_lnest_parser, loop_lim_vars)
    _id_hoistable.identify_LoopLimit_Stmts()
    _id_hoistable.identify_hoistable()
    #_id_hoistable.display()

    _hoist = AST_Hoist_Limits(_lnest_parser)
    _tree = _hoist.visit(orig_tree)
    _hoist.post_process()
    return astor.codegen.to_source(_tree)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("usage : provide file-name")
    else:
        with open(sys.argv[1], 'r') as fd:
            hoist(fd.read())
