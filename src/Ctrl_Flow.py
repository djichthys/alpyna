#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2019>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import weakref
import astor
import sys
import For_Metadata as For_Meta


def condvar_gen(prepend):
    start = 0
    while True:
        yield "{0}_{1:03d}".format(prepend, start)
        start += 1


class Collect_VarNames(ast.NodeVisitor):
    def __init__(self):
        self.variables = []
        self.stack = 0
        self.store_array_name = False

    def visit_Name(self, node):
        if self.stack == 0 or self.store_array_name == True:
            self.variables.append(node.id)
            self.store_array_name = False
            return

    def visit_Subscript(self, node):
        if type(node.value) is ast.Name:
            self.store_array_name = True
            self.stack += 1
            self.generic_visit(node)
            self.stack -= 1
        else:
            self.stack += 1
            self.generic_visit(node)
            self.stack -= 1
        return


class Guard_Presence_Pass(ast.NodeVisitor):
    def __init__(self, lg_varlist):
        self.guard_vars = lg_varlist
        self.present = False
        self.assign_tgt_presence = False

        self.assign_stack = []

    def visit_Assign(self, node):
        _tgt_vars = []
        for _tgt in node.targets:
            _parse_varname = Collect_VarNames()
            _parse_varname.visit(node)
            _tgt_vars += _parse_varname.variables
        #self.assign_stack.append( [ _tgt.id for _tgt in node.targets ] )
        self.assign_stack.append(_tgt_vars)
        self.generic_visit(node)
        self.assign_stack.pop()
        return

    def visit_Name(self, node):
        if node.id in self.guard_vars:
            self.present = True

        #print(" Presence Check ----- {0} :: guard_vars = {1} -- present-flag = {2}, ass-tgt flag = {3}".format(ast.dump(node), self.guard_vars, self.present , self.assign_tgt_presence))
        if self.assign_stack and self.assign_stack[-1] \
          and self.present and node.id in self.assign_stack[-1] :
            #print("assign-stack = {0}, top = {1}".format(self.assign_stack , self.assign_stack[-1]))
            self.assign_tgt_presence = True


class Exit_Predicate_Xform(ast.NodeTransformer):
    def __init__(self, p_node, lg, lg_varmap):
        self.lguard = lg
        self.lg_var_list = self._get_lguard_list(p_node, lg_varmap)
        self.stack = []
        self.guard_stack = []

        self.lift_guard = None

    def generic_visit(self, node):
        self.stack.append(weakref.ref(node))
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_If(self, node):
        _check_lg_presence = Guard_Presence_Pass(self.lg_var_list)
        _check_lg_presence.visit(node.test)
        if _check_lg_presence.present == True:
            self.guard_stack.append(weakref.ref(node))

        node = self.generic_visit(node)

        if _check_lg_presence.present == True:
            self.guard_stack.pop()
        return node

    def visit_Assign(self, node):
        node = self.generic_visit(node)
        _guard = self._gen_loop_guard_predicate(node)
        if _guard == None:
            #print("If test condition NOT required = {0}\n\n\n".format( astor.codegen.to_source(node) ))
            return node

        _new_node = ast.If(test=_guard, body=[node], orelse=[])
        #print("If dump = {0}\n\n\n".format( ast.dump(_new_node) ))
        #print("If test condition = {0}\n\n\n".format( astor.codegen.to_source(_new_node) ))
        return ast.If(test=_guard, body=[node], orelse=[])

    def _get_lguard_list(self, p_node, var_map):
        _lg_vars = None
        for _func in var_map.keys():
            _loop_list, _lg_list = var_map[_func]
            if p_node in _loop_list:
                _lg_vars = _lg_list
                break

        assert _func != None, "loop {0} NOT in function--> ( loop-list, loop-guard-list ) map".format(
            p_node)
        return [_node.id for _node in _lg_vars]

    def _gen_loop_guard_predicate(self, node):
        if len(self.guard_stack) == 0:
            _check_lg_presence = Guard_Presence_Pass([_pred.id for _pred in self.lguard])
            _check_lg_presence.visit(node)
            if _check_lg_presence.assign_tgt_presence:
                return None
            elif len(self.lguard) > 1:
                return ast.BoolOp(op=ast.And(), values=self.lguard)
            else:
                assert len(self.lguard), "L-Guards = {0}".format(self.lguard)
                return self.lguard[0]
        else:
            _guard = [_pred for _pred in self.lguard]
            for _pred in self.guard_stack:
                _guard.append(_pred().test)
            _test_predicate = ast.BoolOp(op=ast.And(), values=_guard)

            # Replace statement guard with new statement guard and return Node
            self.guard_stack[-1]().test = _test_predicate
            return None    # Return the Assign node as is after replacement of guard statement


class PostProcess_Container:
    def __init__(self, action, parent_list, anchor_node, xform_node, ps_flag):
        self.action = action
        self.parent_list = parent_list
        self.anchor_node = anchor_node
        self.xform_node = xform_node
        self.ps_flag = ps_flag

        self.lguard_scope_map = None

    @property
    def action(self):
        return self._action

    @action.setter
    def action(self, action):
        if action in ["insert", "replace_attr" \
      , "delete_if", "delete_tail" \
      , "delete_node", "convert_exit_predicate"] :
            self._action = action
        else:
            self._action = None

    def display(self):
        print_ps = lambda ps: "before" if ps else "after"
        print("{0} in {1} :: {2} {3} anchor-node = {4} ".format(self.action, self.parent_list,
                                                                self.xform_node,
                                                                print_ps(self.ps_flag),
                                                                self.anchor_node))

    def delete_tail(self):
        _idx = self.parent_list.index(self.anchor_node)
        for _null_stmt in self.parent_list[_idx + 1:]:
            self.parent_list.pop()
        else:
            self.parent_list.pop(_idx)

    def insert(self):
        assert self.anchor_node in self.parent_list \
          , "Anchor node {0} not indexed in parent-list {1}".format( self.anchor_node , self.parent_list)

        _idx = self.parent_list.index(self.anchor_node)
        if self.ps_flag:
            self.parent_list.insert(_idx, self.xform_node)
        else:
            self.parent_list.insert(_idx + 1, self.xform_node)

    def replace_attr(self):
        setattr(self.parent_list, self.anchor_node, self.xform_node)

    #Assumption is that node to delete is an ast.If node
    def delete_if(self):
        _anchor_idx = self.parent_list.index(self.anchor_node)
        _idx = -1
        for _idx, _lift_node in enumerate(self.xform_node.orelse):
            self.parent_list.insert(_anchor_idx + _idx, _lift_node)
        else:
            _idx += 1

        self.parent_list.pop(_anchor_idx + _idx)

    def delete_node(self):
        self.parent_list.remove(self.anchor_node)

    def convert_exit_predicate(self):
        for _idx, _node in enumerate(self.anchor_node.body):
            _add_pred_pass = Exit_Predicate_Xform( self.anchor_node \
             ,  self.xform_node \
             , self.lguard_scope_map )
            _new_node = _add_pred_pass.visit(_node)
            #print("Convert-Exit-Predicate  Debug :: source = {0}".format( _new_node ))
            self.anchor_node.body[_idx] = _new_node

    def execute(self):
        if self.action == None:
            return

        _exec_func = getattr(self, self.action, None)
        assert _exec_func, "Could not obtain post-process function -- {0}()".format(self.action)
        if _exec_func:
            _exec_func()


class Isolate_Exit(ast.NodeTransformer):
    def __init__(self, root, predicate_generator=None):
        self.root = root
        self.stack = []
        self.branch_stack = []

        if predicate_generator:
            self.gen_predicate = predicate_generator
        else:
            self.gen_predicate = condvar_gen("_condvar")

        self.actions = []
        self.generated_condvars = []

        self.container_stack = []
        self.acc_lguard = {}    # key is module / function node

    @property
    def root(self):
        return self._root()

    @root.setter
    def root(self, node):
        if callable(node):
            self._root = node
        else:
            self._root = weakref.ref(node)

    def generic_visit(self, node):
        self.stack.append(weakref.ref(node))
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        self.acc_lguard[node] = ([], [])
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        self.acc_lguard[node] = ([], [])
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_If(self, node):
        node = self.generic_visit(node)
        node = self.xform_if_exit_branch(node)
        return node

    # ps_flag  - insert before or after the anchor node
    #            True - precede , False - succeed
    def _gen_postprocess_insert(self, anchor_node, xform_node, ps_flag):
        _reserve = self.stack.pop()
        if isinstance(_reserve(), (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container(\
                   "insert", _reserve().body \
                   , anchor_node, xform_node, ps_flag))
        elif isinstance(_reserve(), (ast.For, ast.If)):
            if anchor_node in _reserve().body:
                self.actions.append( PostProcess_Container(\
                       "insert", _reserve().body \
                       , anchor_node, xform_node, ps_flag))
            else:
                self.actions.append( PostProcess_Container(\
                       "insert", _reserve().orelse \
                       , anchor_node, xform_node, ps_flag))
        else:
            assert False, "We failed"

        self.stack.append(_reserve)
        return

    # only call this if parent node is an
    # if : else condition
    # This is called by a visit to an If condition
    def xform_if_exit_branch(self, node):
        _gen_isolated_exit = lambda cond, orelse, orelse_body :\
              ast.If( test = cond, body= [ ast.Break() ], orelse=orelse_body) \
              if orelse == False else \
              ast.If( test = ast.UnaryOp(op=ast.Not(), operand=cond) \
                , body= [ ast.Break() ], orelse = [] )

        #First check the if-condition for break statements
        _condvar = _pred = _orelse_anchor = None
        for _idx, _stmt in enumerate(node.body):
            if type(_stmt) is ast.Break:
                # for single break statement - dont do anything
                if len(node.body) == 1:
                    break
                # 1. create a temp-var and predicate condition (_temp_var = _p)
                #    and store for later use
                _condvar = next(self.gen_predicate)
                self.generated_condvars.append(_condvar)
                _pred = ast.Assign(
                    targets=[ast.Name(id=_condvar, ctx=ast.Store())], value=node.test)
                # 2. change test condition to check for truth of newly-created temp condition
                node.test = ast.Name(id=_condvar, ctx=ast.Load())
                # 3. generate new if condition with single break stmt
                #    copy over the else condition to the new node
                _isolated_exit = _gen_isolated_exit( \
                     ast.Name(id= _condvar , ctx=ast.Load()), False, node.orelse )
                # retain node to insert else-body xformation
                # in the correct relative location
                _orelse_anchor = _isolated_exit

                # 4. delete break stmt and everything else after it
                self.actions.append( PostProcess_Container(\
                       "delete_tail", node.body \
                       , _stmt, None, False))

                # 5. insert conditional predicate and new if condition
                self._gen_postprocess_insert(node, _pred, True)
                self._gen_postprocess_insert(node, _isolated_exit, False)
                # 6. Delete orelse from original node now that we have relocated the break
                self.actions.append( PostProcess_Container(\
                       "replace_attr", node , "orelse" , [], False))
                break

        for _stmt in node.orelse:
            if type(_stmt) is ast.Break:
                # If conditional predicate was already generated
                # then re-use it. If not generate it and insert into if header
                if _pred == None:
                    # 1. create a temp-var and predicate condition (_temp_var = _p)
                    #    and retain for further processing
                    _condvar = next(self.gen_predicate)
                    self.generated_condvars.append(_condvar)
                    _pred = ast.Assign(
                        targets=[ast.Name(id=_condvar, ctx=ast.Store())], value=node.test)
                    # 2. change test condition to check for truth of newly-created temp condition
                    node.test = ast.Name(id=_condvar, ctx=ast.Load())
                    # 3. insert conditional statement above If condition
                    self._gen_postprocess_insert(node, _pred, True)

                # 4. generate new if condition with single break stmt
                #    copy over the else condition to the new node
                _isolated_exit = _gen_isolated_exit( \
                     ast.Name(id= _condvar , ctx=ast.Load()), True, None )
                # 5. delete break stmt and everything else after it
                self.actions.append( PostProcess_Container(\
                       "delete_tail", node.orelse \
                       , _stmt, None, False))
                # 6. insert conditional predicate and new if condition
                if _orelse_anchor == None:
                    _orelse_anchor = node
                self._gen_postprocess_insert(_orelse_anchor, _isolated_exit, False)
        return node

    def display_post_process(self):
        for _act in self.actions:
            _act.display()

    def post_process(self):
        for _act in self.actions:
            _act.execute()


# Exit branch isolation into separate
# separate statements which we can feed
# into exit branch relocation
def isolate_exit_branches(ast_tree):
    _isolate = Isolate_Exit(ast_tree)
    _new_tree = _isolate.visit(ast_tree)
    #_isolate.display_post_process()
    _isolate.post_process()

    _lift_else = Lift_OrElse( _isolate.gen_predicate \
                            , _isolate.generated_condvars , _isolate.acc_lguard )

    # Lift out any else branches and put them into an else condition
    _new_tree = _lift_else.visit(_new_tree)
    #_lift_else.display_post_process()
    _lift_else.post_process()
    return (_new_tree, _lift_else)


def gen_predicated_loops(ast_tree
       , private_vars=None \
       , predicate_gen=None \
       , lguard_map=None):

    # Both the isolate and Predication classes are initialised with the same
    # lguard-maps and Predication-generators. Hence any variables generated
    # come from the same generator and any generated conditional variables are
    # reflected in both classes

    _isolate = Isolate_Conditional_Loop(predicate_gen, private_vars, lguard_map)
    _predicate_loop = Predicate_LoopBody(predicate_gen, private_vars, lguard_map)

    _idx = 1
    _exec_split = _exec_pred = True

    #Continue both passes until there is no change
    while True:
        while _exec_split:
            _isolate.transition_reset()
            #print("\n\nisolate conditional branch - pass {0}\n=====================\n".format( _idx))
            _idx += 1
            ast_tree = _isolate.visit(ast_tree)
            _isolate.post_process()
            #print("isolated For xformed  source = \n{0}".format( astor.codegen.to_source(ast_tree)))
            #_isolate.display_post_process()
            if len(_isolate.tainted):
                _exec_pred = True
            else:
                _exec_split = False

        while _exec_pred:
            _predicate_loop.transition_reset()
            #print("\n\nPredicate isolated conditional loops - pass {0}\n=====================\n".format( _idx))
            _idx += 1
            ast_tree = _predicate_loop.visit(ast_tree)
            _predicate_loop.post_process()
            #print("Predicated For body xformed  source = \n{0}".format( astor.codegen.to_source(ast_tree)))
            #_predicate_loop.display_post_process()
            if len(_predicate_loop.tainted):
                _exec_split = True
            else:
                _exec_pred = False
        if not _exec_split and not _exec_pred:
            break
    return (ast_tree, _isolate)


# Isolate any loops inside an if-condition
# Assumption is that All else conditions have been
# deleted. If not execute a Lift_OrElse Pass over the AST


class Isolate_Conditional_Loop(ast.NodeTransformer):
    def __init__(self, predicate_generator, priv_condvars, lguard_map):
        self.stack = []
        self.actions = []

        if predicate_generator:
            self.gen_predicate = predicate_generator
        else:
            self.gen_predicate = condvar_gen("_condvar")

        self.generated_condvars = priv_condvars
        self.container_stack = []
        self.lguard_map = lguard_map
        self.tainted = []

    @property
    def lguard_map(self):
        return self._lguards

    @lguard_map.setter
    def lguard_map(self, lguards):
        if type(lguards) is dict:
            self._lguards = lguards
        else:
            self._lguards = {}

    @property
    def generated_condvars(self):
        return self._generated_condvars

    @generated_condvars.setter
    def generated_condvars(self, condvars):
        if type(condvars) is list:
            self._generated_condvars = condvars
        elif condvars == None:
            self._generated_condvars = []
        else:
            self._generated_condvars = [condvars]

    def transition_reset(self):
        self.tainted.clear()
        self.container_stack.clear()
        self.actions.clear()
        self.stack.clear()

    # Override the function in the base-class to store
    def generic_visit(self, node):
        self.stack.append(weakref.ref(node))
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    # Caution : Only the if-body is checked to keep code simple
    # Code should be run through a Lift-or-Else pass before this pass
    def visit_If(self, node):
        node = self.generic_visit(node)
        # Check if the current node-body has any node that is in the tainted list
        # if so do not do anything otherwise, it will lead to catastrophic re-ordering of code
        _check_reorder_race = lambda obj: obj in self.tainted
        if len( [ _node for _node in filter( _check_reorder_race , node.body ) ]) \
          or len( [ _node for _node in filter( _check_reorder_race, node.orelse)] ):
            return node

        # Check if either If body or else body have a Loop in it
        _if_loops = [ _node for _node in \
              filter( lambda obj : \
                 True if isinstance(obj, ast.For) \
                 else False , node.body) ]
        _else_loops = [ _node for _node in \
              filter( lambda obj : \
                 True if isinstance(obj, ast.For) \
                 else False , node.orelse) ]

        if len(_if_loops) == 0 and len(_else_loops) == 0:
            return node

        # Check if we have already lifted the If-test condition
        # If yes, then do not lift the test-predicate
        _condvar = _pred = None

        _var_checklist = self.generated_condvars.copy()
        _var_checklist += self._get_lguard_list(self.container_stack[-1](), self.lguard_map)
        _check_lg_presence = Guard_Presence_Pass(_var_checklist)
        _check_lg_presence.visit(node.test)
        if not _check_lg_presence.present:
            # 1. create a temp-var and predicate condition (_temp_var = _p)
            #    and store for later use
            _condvar = next(self.gen_predicate)
            self.generated_condvars.append(_condvar)
            _pred = ast.Assign(targets=[ast.Name(id=_condvar, ctx=ast.Store())], value=node.test)
            # 2. change test condition to check for truth of newly-created temp condition
            node.test = ast.Name(id=_condvar, ctx=ast.Load())
            # 3. insert conditional predicate before current If
            self._gen_postprocess_insert(node, _pred, True)
            self._mark_tainted(node)
        else:
            _pred = node.test

        # Split the body of the If-condition to isolate any for loops
        self._split_body(node, node.body, _pred)
        return node

    # Will schedule the splitting up and isolation of
    # For-nodes within a parent body and isolating it
    def _split_body(self, anchor_node, parent_body, condition):
        not_loops = []
        for _sub in reversed(parent_body):

            # 1. Accumulate all non-Loops to lift
            if type(_sub) is not ast.For:
                not_loops.append(_sub)
                continue

            # 2. Found a for-loop
            # 2.1. if other types of nodes are present, lift it to parent first
            if len(not_loops):
                _to_lift = ast.If( test = condition \
                      , body = [ _node for _node in reversed(not_loops)],  orelse = [] )
                self._gen_postprocess_insert(anchor_node, _to_lift, False)
                for _node in not_loops:
                    self.actions.append( PostProcess_Container(\
                          "delete_node", parent_body, _node, None, True))
                not_loops.clear()    # clear the list to start the next lift operation
                self._mark_tainted(anchor_node)

            # 2.2 if there are no other loop/non-loop
            #   statements before the current one, do not do anything
            if parent_body.index(_sub) == 0:
                continue    # This will terminate loop

            # 2.3 since there are other loop/non-loop statements
            #   lift current for-node out into parent body and
            #   schedule deletion from current list
            _to_lift = ast.If( test = condition \
                   , body = [ _sub ] ,  orelse = [] )
            self._gen_postprocess_insert(anchor_node, _to_lift, False)
            self.actions.append( PostProcess_Container( "delete_node" \
                   , parent_body, _sub , None, True))
            self._mark_tainted(anchor_node)

    # ps_flag  - insert before or after the anchor node
    #            True - precede , False - succeed
    def _gen_postprocess_insert(self, anchor_node, xform_node, ps_flag):
        _reserve = self.stack.pop()
        if isinstance(_reserve(), (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container(\
                  "insert", _reserve().body \
                  , anchor_node, xform_node, ps_flag))
        elif isinstance(_reserve(), (ast.For, ast.If)):
            if anchor_node in _reserve().body:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().body \
                      , anchor_node, xform_node, ps_flag))
            else:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().orelse \
                      , anchor_node, xform_node, ps_flag))
        else:
            assert False, "We failed"

        self.stack.append(_reserve)
        return

    def _get_lguard_list(self, func, var_map):
        assert func != None, "func {0} NOT in function--> ( loop-list, loop-guard-list ) map".format(
            func)
        _loop_list, _lg_list = var_map[func]
        return [_node.id for _node in _lg_list]

    def _mark_tainted(self, node):
        if node not in self.tainted:
            self.tainted.append(node)

    def post_process(self):
        for _act in self.actions:
            _act.execute()

    def display_post_process(self):
        for _act in self.actions:
            _act.display()


# If we find a loop inside a conditional statement
# raise loop body while predicating entire loop-body.
# If we do this only for conditions consisting solely
# of variables we have introduced.
class Predicate_LoopBody(ast.NodeTransformer):
    def __init__(self, predicate_generator, priv_condvars, lguard_map):
        self.stack = []
        self.actions = []

        if predicate_generator:
            self.gen_predicate = predicate_generator
        else:
            self.gen_predicate = condvar_gen("_condvar")

        self.generated_condvars = priv_condvars
        self.container_stack = []
        self.lguard_map = lguard_map
        self.tainted = []

    @property
    def lguard_map(self):
        return self._lguards

    @lguard_map.setter
    def lguard_map(self, lguards):
        if type(lguards) is dict:
            self._lguards = lguards
        else:
            self._lguards = {}

    @property
    def generated_condvars(self):
        return self._generated_condvars

    @generated_condvars.setter
    def generated_condvars(self, condvars):
        if type(condvars) is list:
            self._generated_condvars = condvars
        elif condvars == None:
            self._generated_condvars = []
        else:
            self._generated_condvars = [condvars]

    def transition_reset(self):
        self.tainted.clear()
        self.container_stack.clear()
        self.actions.clear()
        self.stack.clear()

    # Override the function in the base-class to store
    def generic_visit(self, node):
        self.stack.append(weakref.ref(node))
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    # Caution : Only the if-body is checked to keep code simple
    # Code should be run through a Lift-or-Else pass before this pass
    def visit_If(self, node):
        node = self.generic_visit(node)
        # Check if the current node-body has any node that is in the tainted list
        # if so do not do anything otherwise, it will lead to catastrophic re-ordering of code
        _check_reorder_race = lambda obj: obj in self.tainted
        if len( [ _node for _node in filter( _check_reorder_race , node.body ) ]) \
          or len( [ _node for _node in filter( _check_reorder_race, node.orelse)] ):
            #print("TAINTED NODE = \n{0}".format(astor.codegen.to_source(node)))
            return node

        # Check if we have already lifted the If-test condition
        # If yes, then do not lift the test-predicate
        _condvar = _pred = None

        _var_checklist = self.generated_condvars.copy()
        _var_checklist += self._get_lguard_list(self.container_stack[-1](), self.lguard_map)
        _check_lg_presence = Guard_Presence_Pass(_var_checklist)
        _check_lg_presence.visit(node.test)
        if not _check_lg_presence.present:
            # 1. create a temp-var and predicate condition (_temp_var = _p)
            #    and store for later use
            _condvar = next(self.gen_predicate)
            self.generated_condvars.append(_condvar)
            _pred = ast.Assign(targets=[ast.Name(id=_condvar, ctx=ast.Store())], value=node.test)
            # 2. change test condition to check for truth of newly-created temp condition
            node.test = ast.Name(id=_condvar, ctx=ast.Load())
            # 3. insert conditional predicate before current If
            self._gen_postprocess_insert(node, _pred, True)
            self._mark_tainted(node)
        else:
            _pred = node.test

        self._predicate_loop(node, _pred)
        return node

    # Will schedule the splitting up and isolation of
    # For-nodes within a parent body and isolating it
    def _predicate_loop(self, anchor_node, condition):
        # The If condition should be a guarding a single for-loop
        if len(anchor_node.body) != 1 or type(anchor_node.body[0]) is not ast.For:
            return

        replace_orelse = lambda pbody , condition : \
             pbody if len(pbody) == 0 \
             else ast.If( test = condition, body = pbody, orelse = [] )
        _orig_loop = anchor_node.body[0]
        _new_loop = ast.For( target = _orig_loop.target \
             , iter = _orig_loop.iter   \
             , body = [ ast.If( test = condition, body = _orig_loop.body, orelse = [] ) ]
             , orelse = replace_orelse( _orig_loop.orelse , condition ))
        self._gen_postprocess_insert(anchor_node, _new_loop, False)
        self.actions.append( PostProcess_Container( "delete_node" \
                , self.stack[-1]().body, anchor_node, None, True))
        self._mark_tainted(anchor_node)

    # ps_flag  - insert before or after the anchor node
    #            True - precede , False - succeed
    def _gen_postprocess_insert(self, anchor_node, xform_node, ps_flag):
        _reserve = self.stack.pop()
        if isinstance(_reserve(), (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container(\
                  "insert", _reserve().body \
                  , anchor_node, xform_node, ps_flag))
        elif isinstance(_reserve(), (ast.For, ast.If)):
            if anchor_node in _reserve().body:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().body \
                      , anchor_node, xform_node, ps_flag))
            else:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().orelse \
                      , anchor_node, xform_node, ps_flag))
        else:
            assert False, "We failed"

        self.stack.append(_reserve)
        return

    def _get_lguard_list(self, func, var_map):
        assert func != None, "func {0} NOT in function--> ( loop-list, loop-guard-list ) map".format(
            func)
        _loop_list, _lg_list = var_map[func]
        return [_node.id for _node in _lg_list]

    def _mark_tainted(self, node):
        if node not in self.tainted:
            self.tainted.append(node)

    def post_process(self):
        for _act in self.actions:
            _act.execute()

    def display_post_process(self):
        for _act in self.actions:
            _act.display()


# Implementation of algorithm in Allen and Kennedy book
# "Optimizing Compilers for modern architectures"
class Exit_Branch_Relocation(ast.NodeTransformer):
    def __init__(self):
        self.stack = []
        self.loop_stack = []
        self.container_stack = []

        self.actions = []
        self.gen_lg_condvar = condvar_gen("_lexit")
        self.gen_condvar = condvar_gen("_mbool")

        self.lguard = {}
        # key is module / function node :
        # value is list-of-all loop-guard-vars within the function
        #    or if directly under the module,
        self.acc_lguard = {}    # key is module / function node

    # Override the function in the base-class to store
    def generic_visit(self, node):
        self.stack.append(weakref.ref(node))
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        self.acc_lguard[node] = ([], [])
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        self.acc_lguard[node] = ([], [])
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_For(self, node):
        self.loop_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        # do the branch relocation at this for-loop nesting level
        # accumulate map of  func/module-node : ( [ for-loop nodes ] , [ individual-loop-guards ] )
        _loop_nodes, _lg_vars = self.acc_lguard[self.container_stack[-1]()]
        _loop_nodes.append(node)

        _curr_lg = self.lguard.get(node, [])
        _lg_vars += _curr_lg

        # Schedule conversion of all relevant statements
        if len(_curr_lg) > 0:
            self.actions.append( PostProcess_Container(\
                   "convert_exit_predicate", None \
                   , node , _curr_lg , True))
        self.loop_stack.pop()
        return node

    def visit_If(self, node):
        node = self.generic_visit(node)
        if self._is_exit(node):
            self._reloc_exit(node)
        return node

    def _reloc_exit(self, node):
        # check that there is a containing loop
        num_loops = len(self.loop_stack)
        if num_loops == 0:
            return

        _curr_lg = None
        # 1. generate conditional-var and insert above immediate loop
        _lg_condvar = next(self.gen_lg_condvar)
        _lg_assign = ast.Assign( targets=[ast.Name(id=_lg_condvar, ctx=ast.Store())]\
              , value=ast.NameConstant(value=True))
        self._gen_lg_insert(_lg_assign)
        # 2. replace exit branch with two new statements
        _condvar = next(self.gen_condvar)
        # 3. Accumulate loop-guard condition for application in all statements
        self._accumulate_lguard(_lg_condvar)
        # 4. Create loop-guard statements and schedule insertion
        _lg_conj1 = ast.Assign( targets = [ast.Name( id= _condvar, ctx=ast.Store()) ] \
              , value = ast.UnaryOp(op = ast.Not(), operand =  node.test ))
        _lg_conj2 = ast.Assign(targets=[ ast.Name(id = _lg_condvar, ctx = ast.Store())] \
              , value = ast.BoolOp(op = ast.And() \
                  , values=[ ast.Name(id=_lg_condvar, ctx= ast.Load()) \
                     , ast.Name(id=_condvar, ctx= ast.Load()) ]))
        self._gen_postprocess_insert(node, _lg_conj1, True)
        self._gen_postprocess_insert(node, _lg_conj2, True)

        # Schedule deletion of original exit branch
        self._sched_postprocess_del(node, node, True)

    # original lguard has been set at the beginning of the loop nesting level
    def _accumulate_lguard(self, lg_var):
        # Keep track of which loop exit guard predication should apply to
        try:
            self.lguard[self.loop_stack[-1]()].append(ast.Name(id=lg_var, ctx=ast.Load()))
        except KeyError as ke:
            self.lguard[self.loop_stack[-1]()] = [ast.Name(id=lg_var, ctx=ast.Load())]

    def _is_exit(self, node):
        assert type(node) is ast.If, "Only pass in ast-If nodes into Branch-Reloc check!!!"
        if len(node.body) == 1 and type(node.body[0]) is ast.Break:
            return True
        return False

    def _gen_lg_insert(self, xform_node):
        _loop = self.loop_stack.pop()
        self._gen_postprocess_insert(_loop(), xform_node, True, search_node=_loop())
        self.loop_stack.append(_loop)
        return

# ps_flag  - insert before or after the anchor node
#            True - precede , False - succeed

    def _sched_postprocess_del(self, anchor_node, xform_node, ps_flag):
        _reserve = self.stack[-1]

        if isinstance(_reserve(), (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container(\
                  "delete_if", _reserve().body \
                  , anchor_node , xform_node, ps_flag))
        elif isinstance(_reserve(), (ast.For, ast.If)):
            if anchor_node in _reserve().body:
                self.actions.append( PostProcess_Container(\
                      "delete_if", _reserve().body \
                      , anchor_node, xform_node, ps_flag))
            else:
                self.actions.append( PostProcess_Container(\
                      "delete_if", _reserve().orelse \
                      , anchor_node, xform_node, ps_flag))
        else:
            assert False, "We failed"

# ps_flag  - insert before or after the anchor node
#            True - precede , False - succeed

    def _gen_postprocess_insert(self, anchor_node, xform_node, ps_flag, search_node=None):
        def search_weakrefs(haystack, needle):
            for _idx, _item in enumerate(haystack):
                if _item() == needle:
                    return _idx
            raise ValueError("{0} is not in list {1}".format(needle, haystack))

        # parent AST node of immediate containing for-loop
        if search_node:
            _reserve_idx = search_weakrefs(self.stack, search_node)
            _reserve = self.stack[_reserve_idx - 1]
        else:
            _reserve = self.stack[-1]

        if isinstance(_reserve(), (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container(\
                  "insert", _reserve().body \
                  , anchor_node , xform_node, ps_flag))
        elif isinstance(_reserve(), (ast.For, ast.If)):
            if anchor_node in _reserve().body:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().body \
                      , anchor_node, xform_node, ps_flag))
            else:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().orelse \
                      , anchor_node, xform_node, ps_flag))
        else:
            assert False, "We failed"

    def post_process(self):
        for _act in self.actions:
            _act.lguard_scope_map = self.acc_lguard
            _act.execute()

    def display_post_process(self):
        for _act in self.actions:
            _act.display()


# Relocate exit branches and convert to forward branch
def reloc_exit_branches(ast_tree):
    _reloc_exit = Exit_Branch_Relocation()
    _xformed_ast = _reloc_exit.visit(ast_tree)
    #_reloc_exit.display_post_process()
    _reloc_exit.post_process()
    return (_xformed_ast, _reloc_exit)


class Lift_OrElse(ast.NodeTransformer):
    def __init__(self, predicate_generator, priv_condvars, lguard_map):
        self.stack = []
        self.actions = []

        if predicate_generator:
            self.gen_predicate = predicate_generator
        else:
            self.gen_predicate = condvar_gen("_condvar")

        self.generated_condvars = priv_condvars
        self.container_stack = []
        self.lguard_map = lguard_map

    @property
    def lguard_map(self):
        return self._lguards

    @lguard_map.setter
    def lguard_map(self, lguards):
        if type(lguards) is dict:
            self._lguards = lguards
        else:
            self._lguards = {}

    @property
    def generated_condvars(self):
        return self._generated_condvars

    @generated_condvars.setter
    def generated_condvars(self, condvars):
        if type(condvars) is list:
            self._generated_condvars = condvars
        elif condvars == None:
            self._generated_condvars = []
        else:
            self._generated_condvars = [condvars]

    def _get_lguard_list(self, func, var_map):
        assert func != None, "func {0} NOT in function--> ( loop-list, loop-guard-list ) map".format(
            func)
        _loop_list, _lg_list = var_map[func]
        return [_node.id for _node in _lg_list]

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    # Override the function in the base-class to store
    def generic_visit(self, node):
        self.stack.append(weakref.ref(node))
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_If(self, node):
        node = self.generic_visit(node)

        # Check if we have already lifted the If-test condition
        # If yes, then do not lift the test-predicate
        _condvar = _pred = None

        _var_checklist = self.generated_condvars.copy()
        _var_checklist += self._get_lguard_list(self.container_stack[-1](), self.lguard_map)
        _check_lg_presence = Guard_Presence_Pass(_var_checklist)
        _check_lg_presence.visit(node.test)
        if not _check_lg_presence.present:
            # 1. create a temp-var and predicate condition (_temp_var = _p)
            #    and store for later use
            _condvar = next(self.gen_predicate)
            self.generated_condvars.append(_condvar)
            _pred = ast.Assign(targets=[ast.Name(id=_condvar, ctx=ast.Store())], value=node.test)
            # 2. change test condition to check for truth of newly-created temp condition
            node.test = ast.Name(id=_condvar, ctx=ast.Load())
            # 3. insert conditional predicate before current If
            self._gen_postprocess_insert(node, _pred, True)

        # Only if the else condition does exist - Raise it to the parent body of the If-Node
        if len(node.orelse) == 0:
            return node

        # 4. Replace Else condition with corresponding If condition
        #    with inverted predicate condition
        if _condvar:
            _lifted_orelse = ast.If( test = ast.UnaryOp(op=ast.Not() \
                   , operand=ast.Name(id=_condvar, ctx=ast.Load())) \
                   , body= node.orelse,  orelse = [] )
        else:
            _lifted_orelse = ast.If( test = ast.UnaryOp(op=ast.Not(), operand=node.test ) \
                   , body= node.orelse,  orelse = [] )

        # 5. Add lifted else condition after current if condition
        #    into the parent body of the If-condition
        self._gen_postprocess_insert(node, _lifted_orelse, False)

        # 6. Delete orelse from original node now that we have relocated the break
        self.actions.append( PostProcess_Container(\
               "replace_attr", node , "orelse" , [], False))
        return node

    # ps_flag  - insert before or after the anchor node
    #            True - precede , False - succeed
    def _gen_postprocess_insert(self, anchor_node, xform_node, ps_flag):
        _reserve = self.stack.pop()
        if isinstance(_reserve(), (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container(\
                  "insert", _reserve().body \
                  , anchor_node, xform_node, ps_flag))
        elif isinstance(_reserve(), (ast.For, ast.If)):
            if anchor_node in _reserve().body:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().body \
                      , anchor_node, xform_node, ps_flag))
            else:
                self.actions.append( PostProcess_Container(\
                      "insert", _reserve().orelse \
                      , anchor_node, xform_node, ps_flag))
        else:
            assert False, "We failed"

        self.stack.append(_reserve)
        return

    def post_process(self):
        for _act in self.actions:
            _act.execute()

    def display_post_process(self):
        for _act in self.actions:
            _act.display()


class Lex_Sort(ast.NodeVisitor):
    def __init__(self, search_list):
        self.search_list = search_list
        self.variables = []
        self.stack = 0
        self.store_array_name = False

    def visit_Name(self, node):
        if self.stack == 0 or self.store_array_name == True:
            if node.id in self.search_list \
              and node.id not in self.variables :
                self.variables.append(node.id)

            self.store_array_name = False
            return

    def visit_Subscript(self, node):
        if type(node.value) is ast.Name:
            self.store_array_name = True
            self.stack += 1
            self.generic_visit(node)
            self.stack -= 1
        else:
            self.stack += 1
            self.generic_visit(node)
            self.stack -= 1
        return


class Fwd_Branch_Predication(ast.NodeTransformer):
    def __init__(self, predicate_generator, private_vars, lguard_map):
        if predicate_generator:
            self.gen_predicate = predicate_generator
        else:
            self.gen_predicate = condvar_gen("_condvar")

        self.generated_condvars = private_vars
        self.lguard_map = lguard_map

        self.stack = []
        self.loop_stack = []
        self.container_stack = []
        self.predicate_stack = []
        self._privvars = []

        self.insert_actions = []
        self.del_actions = []

    @property
    def generated_condvars(self):
        return self._generated_condvars

    @generated_condvars.setter
    def generated_condvars(self, condvars):
        if type(condvars) is list:
            self._generated_condvars = condvars
        elif condvars == None:
            self._generated_condvars = []
        else:
            self._generated_condvars = [condvars]

    @property
    def lguard_map(self):
        return self._lguard_map

    @lguard_map.setter
    def lguard_map(self, lguard_map):
        if type(lguard_map) is dict:
            self._lguard_map = lguard_map
        elif type(lguard_map) is tuple:
            self._lguard_map = {lguard_map[0]: lguard_map[1]}
        elif lguard_map == None:
            self._lguard_map = {}
        else:
            self._lguard_map = {lguard_map: ([], [])}

    # Override the function in the base-class to store
    def generic_visit(self, node):
        self.stack.append(node)
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_Module(self, node):
        self.container_stack.append(node)
        _ref_privvars = self._privvars = self._introduced_vars(node)

        node = self.generic_visit(node)

        self._privvars = _ref_privvars
        self.container_stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(node)
        _ref_privvars = self._privvars = self._introduced_vars(node)

        node = self.generic_visit(node)

        self._privvars = _ref_privvars
        self.container_stack.pop()
        return node

    def visit_For(self, node):
        self.loop_stack.append(node)
        node = self.generic_visit(node)
        self.loop_stack.pop()
        return node

    def visit_If(self, node):
        self.predicate_stack.append(node)
        node = self.generic_visit(node)
        self.predicate_stack.pop()
        return node

    def visit_Assign(self, node):
        node = self.generic_visit(node)
        # If there is no recurrance leave the statement as it is
        if len(self.loop_stack) == 0:
            return node

        self._gen_predicated_Assign(node)
        return node

    def _gen_predicated_Assign(self, node):
        _valid_predicates = self._prune_cond_predicates()
        if _valid_predicates == None:    # or len(_valid_predicates) == 1 :
            return
            #if _valid_predicates == None  :
            #    print("No valid predicates.", end= " ")
            #else :
            #    print("Only one valid predicate.", end= " ")
            #print("Dont do anything")
            #return

        _predicates = self._sort_predicates(node, _valid_predicates.copy())
        _new_cond = ast.If( test = ast.BoolOp( op=ast.And()  \
                , values=[ _pred.test for _pred in _predicates ])\
             , body = [ node ] , orelse = [] )

        # Find the location of the outermost If-condition within the body of the loop
        # and replace Assign statement there
        self.insert_actions.append( PostProcess_Container(\
               "insert", self.loop_stack[-1].body \
               , _valid_predicates[0] , _new_cond, True))

        # We will be deleting the current if-condition.
        self.del_actions.append( PostProcess_Container(\
               "delete_if", self.loop_stack[-1].body \
               , _valid_predicates[0] , _valid_predicates[0] , True))

        return

    def _sort_predicates(self, node, predicate_list):
        def min_index(obj):
            _min = self._predicate_var_global_idx(obj, self._privvars)
            return _min

        predicate_list.sort(key=min_index)
        return predicate_list

    def _predicate_var_global_idx(self, predicate, priv_vars):
        _predicate_vars = self._lex_sort_privvars(predicate, priv_vars)
        if len(_predicate_vars):
            # since variables in IF-condition predicate is already lexically
            # sorted according, just search for the index of the first on
            return priv_vars.index(_predicate_vars[0])
        return sys.maxint

    def _prune_cond_predicates(self):
        # Find the point on the general stack where the loop node is
        _loop_anchor = self.stack.index(self.loop_stack[-1])

        # Check whether there is any need for predication
        _node = None
        for _node in self.stack[_loop_anchor + 1:]:
            if type(_node) is ast.If:
                break
        if _node == None:
            return None

        return self.predicate_stack[self.predicate_stack.index(_node):]

    def _get_lguard_list(self, func, var_map):
        assert func != None, "func {0} NOT in function--> ( loop-list, loop-guard-list ) map".format(
            func)
        _loop_list, _lg_list = var_map[func]
        return [_node.id for _node in _lg_list]

    def _lex_sort_privvars(self, node, varlist):
        _lex_sort = Lex_Sort(varlist)
        _lex_sort.visit(node)
        return _lex_sort.variables

    def _introduced_vars(self, node):
        _varlist = self.generated_condvars.copy()
        _varlist += self._get_lguard_list(node, self.lguard_map)
        return self._lex_sort_privvars(node, _varlist)

    def _prune_duplicate_actions(self, actions):
        _seen = []
        _delete = []
        for _act in actions:
            if _act.anchor_node in _seen:
                _delete.append(_act)
            else:
                _seen.append(_act.anchor_node)

        for _act in _delete:
            actions.remove(_act)

    def post_process(self):
        for _act in self.insert_actions:
            _act.execute()

        self._prune_duplicate_actions(self.del_actions)
        for _act in self.del_actions:
            _act.execute()

    def display_post_process(self):
        for _act in self.insert_actions:
            _act.display()
            print("")
        for _act in self.del_actions:
            _act.display()
            print("")


# Do Forward branch simplification
def eliminate_fwd_branches( ast_tree \
       , private_vars=None \
       , predicate_gen=None \
       , lguard_map=None):
    _lift_alt_branch = Lift_OrElse(predicate_gen, private_vars, lguard_map)
    ast_tree = _lift_alt_branch.visit(ast_tree)
    #_lift_alt_branch.display_post_process()
    _lift_alt_branch.post_process()
    #print("Lift-orelse  xformed-source = {0}".format( astor.codegen.to_source(ast_tree )))

    _fwd_branch_eliminate = Fwd_Branch_Predication( predicate_gen  \
            , _lift_alt_branch.generated_condvars , lguard_map)

    ast_tree = _fwd_branch_eliminate.visit(ast_tree)
    #_fwd_branch_eliminate.display_post_process()
    _fwd_branch_eliminate.post_process()
    return (ast_tree, _fwd_branch_eliminate)


# Tag all Forward Branch predication variables for expansion
# Collect all loops enveloping the forward branch
class Vector_CondVar_Expansion_Tag(ast.NodeVisitor):
    def __init__(self, private_vars):
        self.generated_condvars = private_vars

        self.loop_stack = []
        self.assign_stack = []

        self.expanded_condvars = {}
        self._ref = lambda obj: obj() if callable(obj) else obj

    @property
    def generated_condvars(self):
        return self._generated_condvars

    @generated_condvars.setter
    def generated_condvars(self, condvars):
        if type(condvars) is list:
            self._generated_condvars = condvars
        elif condvars == None:
            self._generated_condvars = []
        else:
            self._generated_condvars = [condvars]

    def visit_For(self, node):
        self.loop_stack.append(weakref.ref(node))
        self.generic_visit(node)
        self.loop_stack.pop()

    def visit_Assign(self, node):
        self.assign_stack.append(weakref.ref(node))
        self.generic_visit(node)
        self.assign_stack.pop()

    def visit_Name(self, node):
        self.generic_visit(node)
        if node.id in self.generated_condvars:
            self._condvar_tag(node)

    # Method is called regardless of where the conditional variable is
    # Process the variable only if this is in the target of an assign stmt
    # There should only be one assign stmt for a conditional variable
    def _condvar_tag(self, node):
        if len(self.assign_stack) == 0 or \
         node.id not in [ _nd.id for _nd in self._ref(self.assign_stack[-1]).targets ] :
            return

        assert node.id not in self.expanded_condvars, \
          "If-Predication :Same Fwd conditional-variable <{0}> being assigned to twice. dict = {1}".format(node.id, self.expanded_condvars )

        _check_trgt = [ _var_node.id  \
            for _var_node in self._ref(self.assign_stack[-1]).targets \
             if type(_var_node) is ast.Name ]
        if node.id in _check_trgt:
            # Map all loops enveloping the assignment to the Forward condition variable write
            self.expanded_condvars[node.id] = self.loop_stack.copy()

    def display(self):
        for _key in self.expanded_condvars:
            print(" variable = {0} , expand to {0}{1}".format( _key\
              , [ self._ref(_loop).target.id  for _loop in self.expanded_condvars[ _key ]] ))


# Expand all Forward branch predication variables with
# binding of loop-nest variables
class Predicate_CondVar_Expansion(ast.NodeTransformer):
    def __init__(self, expanded_condvars):
        self.condvars = expanded_condvars
        self._ref = lambda obj: obj() if callable(obj) else obj
        self.touched_nodes = []

        self.lnest_stack = []
        self.stack = []
        self.initialised_vars = []
        self.actions = []

    @property
    def condvars(self):
        return self._condvars

    @condvars.setter
    def condvars(self, condvars):
        if type(condvars) is dict:
            self._condvars = condvars
        else:
            self._condvars = {}

    def generic_visit(self, node):
        # Keep track of all nodes
        self.stack.append(node)
        if node not in self.touched_nodes:
            node = super().generic_visit(node)

        self.stack.pop()
        return node

    def visit_For(self, node):
        self.lnest_stack.append(node)
        node = self.generic_visit(node)
        self.lnest_stack.pop()
        return node

    def visit_Name(self, node):
        node = self.generic_visit(node)
        # if the variable is a fwd compiler introduced conditional variable
        # return the expanded scalar variable
        if node.id in self.condvars:
            _node = self._expand_condvar(node, self.condvars[node.id])
            self.touched_nodes.append(_node)
            self._initialise_cond_vector(node.id)
            return _node
        return node

    # Helper function .... only a singular Name node
    # will ever be passed in. These will be the names
    # of the variables introduced by the compiler for
    # predicated execution
    def _expand_condvar(self, orig_node, loop_dom):
        _num_loops = len(loop_dom)
        _curr = orig_node

        # if number of accumulated loops is greater than zero,
        # expand variables to include all the loops
        if _num_loops > 0:
            _extract_idx = lambda obj: ast.Name(id=self._ref(obj).target.id, ctx=ast.Load())
            _tup_idx = [_extract_idx(_itr) for _itr in loop_dom]
            _curr = ast.Subscript(value = ast.Name(id=orig_node.id, ctx=ast.Load()) ,\
                                  slice = ast.Index( value = ast.Tuple( elts = _tup_idx ,\
                                                                        ctx = ast.Load())) ,\
                                  ctx = orig_node.ctx)
        return _curr

    # Helper function - to add post-process actions
    # to initialise all the vetor variants of conditional variables
    def _initialise_cond_vector(self, var):
        if var in self.initialised_vars or len(self.condvars[var]) == 0:
            return

        _lnest_idx = self.stack.index(self._ref(self.lnest_stack[0]))
        _reserve = self.stack[_lnest_idx - 1]
        if isinstance(_reserve, (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container( \
                    "insert", _reserve.body \
                    , self._ref(self.lnest_stack[0]), self._gen_condvar(var), True))
        elif isinstance(_reserve, (ast.For, ast.If)):
            if self._ref(self.lnest_stack[0]) in _reserve.body:
                self.actions.append( PostProcess_Container( \
                        "insert", _reserve.body \
                       , self._ref(self.lnest_stack[0]), self._gen_condvar(var), True))
            else:
                self.actions.append( PostProcess_Container( \
                        "insert", _reserve.orelse \
                       , self._ref(self.lnest_stack[0]), self._gen_condvar(var), True))
        else:
            assert False, "Cannot find parent of loop for conditional-var = {0}".format(var)

        self.initialised_vars.append(var)
        return

    def _gen_condvar(self, var):
        _limit_astgen = lambda lim : ast.Num(n= lim['end'][1]) \
                                    if lim['end'][0] == 'imm' \
                                    else \
                                    ast.Name(id = lim['end'][1], ctx=ast.Load())
        _limit_tgt = lambda _loop : \
                        _limit_astgen( For_Meta.For_Limits( \
                                            getattr(self._ref(_loop)\
                                            , "iter", None)).limits)
        _init_numpy_node = \
            ast.Call( func = ast.Attribute(
                                    value = ast.Name(id='np', ctx=ast.Load()) \
                                    , attr='full', ctx = ast.Load())          \
                    , args = [ ast.Tuple( elts = [ _limit_tgt(_loop) for _loop in self.condvars[var]] \
                                            , ctx=ast.Load()) \
                                , ast.Num(n=1) ]
                                , keywords = [])

        _node = ast.Assign(targets=[ast.Name(id=var, ctx=ast.Store())], value=_init_numpy_node)
        return _node

    def display(self):
        print("\n\n\nConditional-vars = {0}".format(self.condvars))

    def display_post_process(self):
        for _act in self.actions:
            _act.display()

    def post_process(self):
        for _act in self.actions:
            _act.execute()


# Helper class used to re-parse and regenerate
# mapping between exit-branch variables + loop-bodies
# and  their enveloping function/module bodies
class LGuard_Regeneration(ast.NodeVisitor):
    def __init__(self, lguards):
        self.lguards = lguards
        self.lguard_map = {}

        self.container_stack = []
        self.loop_stack = []
        self._ref = lambda obj: obj() if callable(obj) else obj

    def visit_Module(self, node):
        self.container_stack.append(node)
        self.lguard_map[node] = ([], set())
        self.generic_visit(node)
        self.container_stack.pop()

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        self.lguard_map[node] = ([], set())
        self.generic_visit(node)
        self.container_stack.pop()

    def visit_For(self, node):
        self.lguard_map[self._ref(self.container_stack[-1])][0].append(weakref.ref(node))
        self.generic_visit(node)

    def visit_Name(self, node):
        self.generic_visit(node)
        if node.id in self.lguards:
            self.lguard_map[self._ref(self.container_stack[-1])][1].add(node.id)


# Tag and collect all Exit branch predication variables and
# their enveloping loops for further split scalar expansion passes
class Vector_LGuard_Expansion_Tag(ast.NodeVisitor):
    def __init__(self, lguard_map):
        self.lguard_map = lguard_map

        self.container_stack = []
        self.loop_stack = []
        self._ref = lambda obj: obj() if callable(obj) else obj

        self.var_loop_map = {}

    @property
    def lguard_map(self):
        return self._lguard_map

    @lguard_map.setter
    def lguard_map(self, lguard_map):
        if type(lguard_map) is dict:
            self._lguard_map = lguard_map
        elif type(lguard_map) is tuple:
            self._lguard_map = {lguard_map[0]: lguard_map[1]}
        elif lguard_map == None:
            self._lguard_map = {}
        else:
            self._lguard_map = {lguard_map: ([], [])}

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        self.generic_visit(node)
        self.container_stack.pop()

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        self.generic_visit(node)
        self.container_stack.pop()

    def visit_For(self, node):
        self.loop_stack.append(weakref.ref(node))
        self.generic_visit(node)
        self.loop_stack.pop()

    def visit_Assign(self, node):
        self.generic_visit(node)
        for _trgt_var in node.targets:
            _lg_detect = Lex_Sort(self._lguard_varlist())
            _lg_detect.visit(_trgt_var)
            if len(_lg_detect.variables):
                self._acc_lgvar_loops(_trgt_var)

    def _acc_lgvar_loops(self, node):
        _container = self._ref(self.container_stack[-1])
        if _container not in self.var_loop_map:
            self.var_loop_map[_container] = {node.id: []}

        _var_loop_map = self.var_loop_map[_container]
        if node.id not in _var_loop_map:
            _var_loop_map[node.id] = []

        _loop_list = [self._ref(_loop) for _loop in _var_loop_map[node.id]]
        for _loop in self.loop_stack:
            if self._ref(_loop) not in _loop_list:
                _var_loop_map[node.id].append(_loop)

    def _lguard_varlist(self):
        if len(self.container_stack) == 0:
            return set()
        return self.lguard_map[self._ref(self.container_stack[-1])][1]

    def display(self):
        for _cidx, _container in enumerate(self.var_loop_map):
            print("--------------------------------------")
            print("Container-[{0}] -> node-{1}  --> {2} )".format(_cidx, _container,
                                                                  self.var_loop_map[_container]))
            for _vidx, _lgvar in enumerate(self.var_loop_map[_container]):
                print(">>>>>>>>>>>>>\nContainer-[{0}] -> var-{1} [{2}])".format(
                    _cidx, _vidx, _lgvar))
                for _lidx, _loop in enumerate(self.var_loop_map[_container][_lgvar]):
                    print("Container-[{0}] -> var-{1} [{2}] --loop = {3}".format(\
                      _cidx, _vidx, _lgvar, self._ref(_loop)  ))


# This class is only called from within the context of
# an assign statement within a loop. Done this way to
# isolate Loop-Guard Vars within an assign statement
class LGuard_Assign_Expansion_Helper(ast.NodeTransformer):
    def __init__(self,
                 container,
                 loop_stack,
                 lg_var_map,
                 lg_var_track,
                 touched_nodes,
                 initialised_vars,
                 parent_node,
                 parent_stack,
                 parent_postprocessor,
                 lhs=True):
        self.container = container
        self.lg_var_map = lg_var_map
        self.lg_var_track = lg_var_track
        self.touched_nodes = touched_nodes
        self.lhs = lhs

        self.loop_stack = loop_stack
        self.parent_node = parent_node
        self.stack = parent_stack
        self.initialised_vars = initialised_vars
        self.actions = parent_postprocessor

        self._ref = lambda obj: obj() if callable(obj) else obj

    def generic_visit(self, node):
        if node not in self.touched_nodes:
            node = super().generic_visit(node)
        return node

    def visit_Name(self, node):
        node = self.generic_visit(node)

        # If this  is a Loop-guard variable
        # then expand depending on where the variable
        # is in the body of the code
        _loops = self._lguard_varlist(node.id)
        if _loops:
            # Optimisation for space : expand only in innermost
            # loop. Since we know that any expansion will still
            # carry a loop-carried dependence on the inner-most loop
            # we might as well save the space for multi-dimensional expansion
            node = self._lg_expand(node, [_loops[-1]])
            self.touched_nodes.append(node)
        return node

    def _lguard_varlist(self, name):
        try:
            return self.lg_var_map[self.container][name]
        except KeyError as ke:
            return None

    # Given a Name-node, return the correct Binding Ref to the caller
    # The values are either :
    # -1 - indicating this is the first time we are seeing this variable
    # 0  - indicating that this is the second time we are seeing this variable
    # lineno - This is uses later to counter the effect of BFS search of AST node  tree
    #          giving wrong results
    def _get_var_tracking(self, node):
        try:
            return self.lg_var_track[self.container][node.id]
        except KeyError as ke:
            return None
        return None

    # This function is called from an expression that is
    # an Assign statement.
    def _lg_expand(self, node, loops):
        _var_tracking = self._get_var_tracking(node)
        assert _var_tracking != None, "Could not find var-tracking for variable {0}".format(node.id)

        _var_name = node.id
        _node_lineno = node.lineno
        if _var_tracking > 0:
            node = self._expand_var(node, loops, True, False)
        elif _var_tracking == 0:
            if self.lhs:
                node = self._expand_var(node, loops, True, False)
                self._incr_var_tracking(_var_name, _node_lineno)
            else:
                node = self._expand_var(node, loops, False, False)
        else:
            node = self._expand_var(node, loops, False, True)
            self._incr_var_tracking(_var_name, _node_lineno)
            self._initialise_cond_vector(_var_name, loops)

        return node

    def _incr_var_tracking(self, var_name, lineno):
        if self.lg_var_track[self.container][var_name] == -1:
            self.lg_var_track[self.container][var_name] += 1
        else:
            self.lg_var_track[self.container][var_name] = lineno

    # Helper function .... only a singular Name node
    # will ever be passed in. These will be the names
    # of the variables introduced by the compiler for
    # predicated execution
    def _expand_var(self, orig_node, loop_dom, next_loop, init_var):
        _idx_num = lambda obj , curr_loop, total_loops: \
           ast.Num(n=0) \
           if init_var and curr_loop == total_loops-1 \
           else \
           ast.Name(id=self._ref(obj).target.id ,ctx = ast.Load())

        _extract_idx = lambda obj , curr_loop , total_loops : \
            ast.BinOp( left = _idx_num(obj, curr_loop, total_loops) \
                       , op=ast.Add(), right=ast.Num(n=1)) \
            if next_loop and curr_loop == total_loops-1 \
            else        \
            _idx_num(obj, curr_loop, total_loops)

        _num_loops = len(loop_dom)
        _curr = orig_node

        # if number of accumulated loops is greater than zero,
        # expand variables to include all the loops
        if _num_loops > 0:
            _tup_idx = [_extract_idx(_itr, _ln, _num_loops) for _ln, _itr in enumerate(loop_dom)]
            _curr = ast.Subscript(value = ast.Name(id=orig_node.id, ctx=ast.Load()) ,\
                                  slice = ast.Index( value = ast.Tuple( elts = _tup_idx ,\
                                                                        ctx = ast.Load())) ,\
                                  ctx = orig_node.ctx)
        return _curr

    # Helper function - to add post-process actions
    # to initialise all the vetor variants of conditional variables
    def _initialise_cond_vector(self, var, loops):
        if var in self.initialised_vars or len(loops) == 0:
            return

        if len(self.loop_stack) > 0:
            _lnest_idx = self.stack.index(self._ref(self.loop_stack[0]))
            _reserve = self.stack[_lnest_idx - 1]
            _anchor_node = self.stack[_lnest_idx]
        else:
            _reserve = self.stack[-1]
            _anchor_node = self.parent_node

        if isinstance(_reserve, (ast.Module, ast.FunctionDef)):
            self.actions.append( PostProcess_Container( \
                    "insert", _reserve.body \
                    , _anchor_node, self._gen_condvar(var,loops), True))
        elif isinstance(_reserve, (ast.For, ast.If)):
            if self._ref(loops[0]) in _reserve.body:
                self.actions.append( PostProcess_Container( \
                        "insert", _reserve.body \
                       , _anchor_node, self._gen_condvar(var,loops), True))
            else:
                self.actions.append( PostProcess_Container( \
                        "insert", _reserve.orelse \
                       , _anchor_node, self._gen_condvar(var,loops), True))
        else:
            assert False, "Cannot find parent of loop for conditional-var = {0}".format(var)

        self.initialised_vars.append(var)
        return

    def _gen_condvar(self, var, loops):
        def _limit_astgen(lim, idx, num_loops):
            if idx == (num_loops - 1):
                if lim['end'][0] == 'imm':
                    return ast.Num(n=lim['end'][1] + 1)
                else:
                    return ast.BinOp(
                        left=ast.Name(id=lim['end'][1], ctx=ast.Load()),
                        op=ast.Add(),
                        right=ast.Num(n=1))
            else:
                if lim['end'][0] == 'imm':
                    return ast.Num(n=lim['end'][1])
                else:
                    return ast.Name(id=lim['end'][1], ctx=ast.Load())

        _num_of_loops = len(loops)
        _limit_tgt = lambda _loop ,_idx : \
                        _limit_astgen( For_Meta.For_Limits( \
                                            getattr(self._ref(_loop)\
                                            , "iter", None)).limits, _idx , _num_of_loops)
        _init_numpy_node = \
            ast.Call( func = ast.Attribute(
                                    value = ast.Name(id='np', ctx=ast.Load()) \
                                    , attr='full', ctx = ast.Load())          \
                    , args = [ ast.Tuple( elts = [ _limit_tgt(_loop, _idx) for _idx, _loop in enumerate(loops)] \
                                            , ctx=ast.Load()) \
                                , ast.NameConstant(value=True) ]
                                , keywords = [])
        _node = ast.Assign(targets=[ast.Name(id=var, ctx=ast.Store())], value=_init_numpy_node)
        return _node

    def display(self):
        print("\n\n\nConditional-vars = {0}".format(self.condvars))

    def display_post_process(self):
        for _act in self.actions:
            _act.display()

    def post_process(self):
        for _act in self.actions:
            _act.execute()


# Stage-1 Expansion of Exit-branch predication variables
# used to expand only those variables within
# Assign statements used to evaluate the Exit-branch condition.
class LGuard_Assign_Expansion(ast.NodeTransformer):
    def __init__(self, var_loop_map):
        self.lg_loop_map = var_loop_map
        self._ref = lambda obj: obj() if callable(obj) else obj
        self.track_lg_vars = { _container : \
              { _var:-1 for _var in self.lg_loop_map[_container]} \
              for _container in self.lg_loop_map }
        self.touched_nodes = []

        self.loop_stack = []
        self.container_stack = []
        self.stack = []
        self.initialised_vars = []
        self.actions = []    # Post-processor actions

    def generic_visit(self, node):
        self.stack.append(node)
        if node not in self.touched_nodes:
            node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_For(self, node):
        self.loop_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.loop_stack.pop()
        return node

    def visit_Assign(self, node):
        node = self.generic_visit(node)

        _lg_replace = LGuard_Assign_Expansion_Helper( \
            self._ref(self.container_stack[-1]), \
            self.loop_stack, self.lg_loop_map,   \
            self.track_lg_vars, self.touched_nodes, \
            self.initialised_vars, node, self.stack , self.actions, False)
        node.value = _lg_replace.visit(node.value)
        for _idx, _lhs in enumerate(node.targets):
            _lg_replace = LGuard_Assign_Expansion_Helper( \
                self._ref(self.container_stack[-1]), \
                self.loop_stack, self.lg_loop_map,   \
                self.track_lg_vars, self.touched_nodes,\
                self.initialised_vars, node,  self.stack , self.actions , True)
            node.targets[_idx] = _lg_replace.visit(_lhs)
        return node

    def display(self):
        print("\nLoop-guard-vars = {0}".format(self.lg_loop_map))
        for _cidx, _container in enumerate(self.lg_loop_map):
            print("--------------------------------------")
            print("Container-[{0}] -> node-{1}  --> {2} )".format( _cidx \
                , _container ,self.lg_loop_map[_container] ))
            for _vidx, _lgvar in enumerate(self.lg_loop_map[_container]):
                print(">>>>>>>>>>>>>\n* Container-[{0}] -> var-{1} [{2}])".format(
                    _cidx, _vidx, _lgvar))
                for _lidx, _loop in enumerate(self.lg_loop_map[_container][_lgvar]):
                    print("*** Container-[{0}] -> var-{1} [{2}] --loop = {3}".format(\
                      _cidx, _vidx, _lgvar, self._ref(_loop)  ))
        print("\n\n")

    def display_post_process(self):
        for _act in self.actions:
            _act.display()

    def post_process(self):
        for _act in self.actions:
            _act.execute()


# Expand all Loop-guard variables within each predicate.
# Exclude statemtents where the loop-guard variables are
# being assigned to. This should have been already done
# in an earlier pass.
# Exit-branch predication was split into two passes
# due to the Breadth-First-Search method of the AST library
class Predicate_LGuard_Expansion(ast.NodeTransformer):
    def __init__(self, lg_var_map, lg_var_track, touched_nodes):
        self.lg_loop_map = lg_var_map
        self.touched_nodes = touched_nodes
        self.track_lg_vars = lg_var_track

        self._ref = lambda obj: obj() if callable(obj) else obj
        self.container_stack = []

    def generic_visit(self, node):
        if node not in self.touched_nodes:
            node = super().generic_visit(node)
        return node

    def visit_Module(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_FunctionDef(self, node):
        self.container_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.container_stack.pop()
        return node

    def visit_Name(self, node):
        _loops = self._lguard_varlist(node.id)
        if _loops:
            # Optimisation for space : expand only in innermost
            # loop. Since we know that any expansion will still
            # carry a loop-carried dependence on the inner-most loop
            # we might as well save the space for multi-dimensional expansion
            node = self._lg_expand(node, [_loops[-1]])
            self.touched_nodes.append(node)
        return node

    def _lguard_varlist(self, name):
        if len(self.container_stack) == 0:
            return None

        try:
            return self.lg_loop_map[self._ref(self.container_stack[-1])][name]
        except KeyError as ke:
            return None

    # This function is called from an expression that is not
    # an Assign statement.
    def _lg_expand(self, node, loops):
        _var_tracking = self._get_var_tracking(node)
        assert _var_tracking != None, "Could not find var-tracking for variable {0}".format(node.id)
        assert _var_tracking > 0, "Variable Tracking for {0} not updated in LGuard Assign-Var expansion".format(
            node.id)

        if node.lineno < _var_tracking:
            node = self._expand_var(node, loops, False)
        else:
            node = self._expand_var(node, loops, True)
        return node

    # Helper function .... only a singular Name node
    # will ever be passed in. These will be the names
    # of the variables introduced by the compiler for
    # predicated execution
    def _expand_var(self, orig_node, loop_dom, next_loop):
        _extract_idx = lambda obj , curr_loop , total_loops : \
            ast.BinOp( left = ast.Name( id = self._ref(obj).target.id, ctx = ast.Load())\
                 , op=ast.Add(), right=ast.Num(n=1)) \
            if next_loop and curr_loop == total_loops-1 \
            else        \
            ast.Name(id=self._ref(obj).target.id ,ctx = ast.Load())
        _num_loops = len(loop_dom)
        _curr = orig_node

        # if number of accumulated loops is greater than zero,
        # expand variables to include all the loops
        if _num_loops > 0:
            _tup_idx = [_extract_idx(_itr, _ln, _num_loops) for _ln, _itr in enumerate(loop_dom)]
            _curr = ast.Subscript(value = ast.Name(id=orig_node.id, ctx=ast.Load()) ,\
                                  slice = ast.Index( value = ast.Tuple( elts = _tup_idx ,\
                                                                        ctx = ast.Load())) ,\
                                  ctx = orig_node.ctx)
        return _curr

    def _get_var_tracking(self, node):
        try:
            return self.track_lg_vars[self._ref(self.container_stack[-1])][node.id]
        except KeyError as ke:
            return None
        return None


# Expand all compiler introduced
# conditional variables
# This is done by converting all control-flow dependences
# into data-dependences.
def vectorise_private_vars(ast_tree, private_vars=None, lguard_map=None):
    # Sanitise AST tree with line-no's and recreate AST tree
    ast_tree = ast.parse(astor.codegen.to_source(ast_tree))

    # Recreate Lguard-variable into a searchable list
    lguard_set = set()
    for _key in lguard_map:
        _loops, _varlist = lguard_map[_key]
        for _node in _varlist:
            lguard_set.add(_node.id)

    # Tag each fwd-conditional variable with loops
    _condvar_tag = Vector_CondVar_Expansion_Tag(private_vars)
    _condvar_tag.visit(ast_tree)
    #_condvar_tag.display()

    # Scalar expansion of all forward-branch variables
    _vectorise_condvars = Predicate_CondVar_Expansion(_condvar_tag.expanded_condvars)
    ast_tree = _vectorise_condvars.visit(ast_tree)
    #_vectorise_condvars.display()

    # Since we have re-parsed AST tree to fill in missing
    # line-no's after all the previous passes
    # rebuild Loop-Guard Map with keys for current AST node bindings
    _regen_lguard = LGuard_Regeneration(lguard_set)
    _regen_lguard.visit(ast_tree)

    # Expand all Loop-Guard Variables.
    # Done in two stages due to BFS traversal of AST tree.

    # 1. Collect All loops enveloping each exit-branch predication variable
    _lgvar_tag = Vector_LGuard_Expansion_Tag(_regen_lguard.lguard_map)
    _lgvar_tag.visit(ast_tree)
    #_lgvar_tag.display()

    # 2. Expand all exit branch Loop-guard predication variables within
    #    assignment statements. Also collect loop-nest locations of
    #    exit-branch predication evaluation. This will be used in
    #    stage-3.
    _vec_lgvar_assign = LGuard_Assign_Expansion(_lgvar_tag.var_loop_map)
    _vec_lgvar_assign.visit(ast_tree)
    #_vec_lgvar_assign.display()

    # 3. Expand remaining locations of the exit branch loop-guard variables.
    _vec_lgvar_predicate_expand = Predicate_LGuard_Expansion(\
            _lgvar_tag.var_loop_map \
            , _vec_lgvar_assign.track_lg_vars \
            , _vec_lgvar_assign.touched_nodes )
    ast_tree = _vec_lgvar_predicate_expand.visit(ast_tree)

    # Execute all post-processing events on AST
    # Essentially expand and initialise all new conditional predicates
    _vectorise_condvars.post_process()
    _vec_lgvar_assign.post_process()

    return ast_tree, (private_vars, _regen_lguard.lguard_map)


# Driver to if-convert Control-flow divergence within loop-nests
def xform(code_str):
    if type(code_str) is str:
        _ast_tree = ast.parse(code_str)
    else:
        _ast_tree = code_str

    # Isolate exit branches
    _xformed_ast, _isolate_obj = isolate_exit_branches(_ast_tree)
    #print("isolated exit branch xformed  source = \n{0}".format(
    #    astor.codegen.to_source(_xformed_ast)))

    _xformed_ast, _isolate_obj = gen_predicated_loops(_xformed_ast
          , private_vars = _isolate_obj.generated_condvars  \
          , predicate_gen = _isolate_obj.gen_predicate \
          , lguard_map = _isolate_obj.lguard_map )
    #print("isolated For xformed  source = \n{0}".format(
    #    astor.codegen.to_source(_xformed_ast)))

    #Relocate Exit branch
    _xformed_ast, _reloc_exit_obj = reloc_exit_branches(_xformed_ast)
    #print("relocated exit branch xformed  source = \n{0}".format(
    #    astor.codegen.to_source(_xformed_ast)))

    _xformed_ast, _fwd_eliminate = eliminate_fwd_branches(_xformed_ast   \
          , private_vars = _isolate_obj.generated_condvars  \
          , predicate_gen = _isolate_obj.gen_predicate \
          , lguard_map = _reloc_exit_obj.acc_lguard )

    #print("Predicated execution xformed  source = \n{0}".format(
    #     astor.codegen.to_source(_xformed_ast)))

    _xformed_ast, expanded_syms = vectorise_private_vars( \
                                      _xformed_ast\
                                      , _isolate_obj.generated_condvars\
                                      , _reloc_exit_obj.acc_lguard)
    return (astor.codegen.to_source(_xformed_ast), expanded_syms)


if __name__ == '__main__':
    import ctrl_test as ut
    print("ast-tree = {0}".format(ut.code_str))
    #print("ast-tree = {0}".format(ast.dump(ast.parse(ut.code_str), True, True)))

    _new_src, exported_syms = xform(ut.code_str)
    print("Predicated execution xformed  source = \n{0}\n\nexported-symbols = {1}".format(
        _new_src, exported_syms))
