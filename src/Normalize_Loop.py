#!/home/dejice/work/python-tutorial/ast-venv/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.

import ast
import astor
import weakref
import copy
import IndexExtract
import sys


class For_Limits:
    def __init__(self, for_iter_node, prepend=""):
        self.node = for_iter_node
        self.limits = {"start": None, "end": None, "step": None}
        self.temp_assign = {"start": None, "end": None, "step": None}
        self._unique_start = self._unique_name("tfsta", pre=prepend)
        self._unique_end = self._unique_name("tfend", pre=prepend)
        self._unique_step = self._unique_name("tfstep", pre=prepend)

    @property
    def node(self):
        if self._node is None:
            return None
        else:
            return self._node()

    @node.setter
    def node(self, val):
        self._node = weakref.ref(val)

    def _unique_name(self, name, pre='', limit=2048):
        idx = 0
        if pre != '':
            pre = pre + '_'
        while idx < limit:
            varname = "_" + pre + name + "_{0:02x}".format(idx) + "_"
            yield varname
            idx += 1

    #WARNING : do not use with _range_limit on same object
    def _range_limit_xform(self, limits):
        def gen_assign_node(expr_node, _lim, _varlist):
            # _lim is the key that is written into the dictionary
            # _varlist is a list of variables in the parent body
            _tmp = next(_lim)
            while _tmp in _varlist:
                _tmp = next(_lim)
                pass

            if isinstance(expr_node, ast.Name):
                _assign = expr_node
            else:
                _assign = ast.Assign([ast.Name(_tmp, ast.Store())], expr_node)
            return _assign

        if len(limits) == 1:
            self.limits["start"] = 0
            self.limits["step"] = 1
            try:
                self.limits["end"] = ast.literal_eval(limits[0])
            except ValueError as excpt:
                self.limits["end"] = gen_assign_node(limits[0], getattr(self, "_unique_end", None),
                                                     [])
        else:
            self.limits["step"] = 1
            for key, _arg in zip(["start", "end", "step"], limits):
                try:
                    eval_arg = ast.literal_eval(_arg)
                    self.limits[key] = eval_arg
                except ValueError as excpt:
                    self.limits[key] = gen_assign_node(_arg, getattr(self, "_unique_" + key, None),
                                                       [])
        #if any of the limits were expressions, the
        #the limit would have become an assign statement of form _tmp_var = expr
        #replace the limit with just the variable name
        for key in self.limits.keys():
            if isinstance(self.limits[key], (int, ast.Name)):
                continue

            # the key-val within limits dict is an ast Assign() node with a single temp var
            # replace the assign node with the variable
            self.temp_assign[key] = self.limits[key]
            self.limits[key] = self.limits[key].targets[0]

        self.modified_loop_iter = self.limits

    @property
    def modified_loop_iter(self):
        return self._modified_loop_iter

    @modified_loop_iter.setter
    def modified_loop_iter(self, limits):
        ast_node_convert = lambda val: ast.Num(val) if type(val) is int else val
        # AST API change between 3.4 and 3.5
        _api_compat = lambda :  \
                        ast.Call(
                            ast.Name('range', ast.Load()), [                            \
                                ast_node_convert(self.limits["start"]),                 \
                                ast_node_convert(self.limits["end"]),                   \
                                ast_node_convert(self.limits["step"])], [], None, None) \
                        if sys.version_info.minor <= 4 else                             \
                            ast.Call(                                                   \
                                ast.Name('range', ast.Load()), [                        \
                                    ast_node_convert(self.limits["start"]),             \
                                    ast_node_convert(self.limits["end"]),               \
                                    ast_node_convert(self.limits["step"])], [])

        self._modified_loop_iter = _api_compat()

    def lower_loop_hdr(self):
        if type(self.node) is ast.Call:
            if 'func' in self.node._fields:
                if self.node.func.id == 'range':
                    self._range_limit_xform(self.node.args)

    def display_limits(self, limits=None):
        def _display(limits):
            for key in ["start", "end", "step"]:
                if type(limits[key]) is int:
                    d_arg = limits[key]
                elif type(limits[key]) is ast.Num:
                    d_arg = limits[key].n
                elif type(limits[key]) is ast.Name:
                    d_arg = limits[key].id
                else:
                    d_arg = limits[key]
                print("limit {0} -> {1}".format(key, d_arg))

        if limits is None:
            _display(self.limits)
        else:
            _display(limits)

    def extract_wo_xform(self):
        if type(self.node) is ast.Call:
            if 'func' in self.node._fields:
                if self.node.func.id == 'range':
                    return self._range_limit(self.node.args)
        return None

    #WARNING : do not use with _range_limit_xform on same object
    def _range_limit(self, range_args):
        limits = {"start": None, "end": None, "step": None}
        if len(range_args) == 1:
            limits["start"] = 0
            limits["step"] = 1
            limits["end"] = range_args[0]
        else:
            limits["step"] = 1
            for key, _arg in zip(["start", "end", "step"], range_args):
                limits[key] = _arg

        return limits


class For_Simplify(ast.NodeTransformer):
    def __init__(self):
        self.poi_stack = []
        # ( body_list [] , index to insert, node to insert )
        self.actions = []

    def formatted_src(self, _orig_map):
        _map = {}
        for key, val in _orig_map.items():
            if isinstance(val, int):
                continue
            if val is None:
                continue

            _map[key] = astor.code_gen.to_source(val)
        return _map

    def _poi_stack_op(self, node):
        self.poi_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.poi_stack.pop()
        return node

    def visit_Module(self, node):
        return self._poi_stack_op(node)

    def visit_FunctionDef(self, node):
        return self._poi_stack_op(node)

    def visit_If(self, node):
        return self._poi_stack_op(node)

    def visit_For(self, node):
        self.poi_stack.append(weakref.ref(node))
        node = self.generic_visit(node)

        for_limit = For_Limits(getattr(node, "iter", None), node.target.id)
        for_limit.lower_loop_hdr()

        self.lift_loop_limit(for_limit)
        node.iter = for_limit.modified_loop_iter
        self.poi_stack.pop()
        return node

    def lift_loop_limit(self, for_limits):
        def insert_actions(body, _wref_node, for_limits):
            limit_map = {}
            if _wref_node() not in body:
                raise ValueError

            for key in ["start", "step", "end"]:
                if type(for_limits.temp_assign[key]) is ast.Assign:
                    limit_map[key] = for_limits.temp_assign[key]
            if len(limit_map) > 0:
                self.actions.append((body, _wref_node, limit_map))

        reserve = self.poi_stack.pop()
        for idx, poi_node in enumerate(reversed(self.poi_stack)):
            #if type(poi_node()) is ast.Module:
            if isinstance(poi_node(), (ast.Module, ast.FunctionDef)):
                stmt_list = poi_node().body
                insert_actions(stmt_list, reserve, for_limits)
                break
            elif isinstance(poi_node(), (ast.For, ast.If)):
                stmt_list = poi_node().body
                alt_stmt_list = poi_node().orelse
                try:
                    insert_actions(stmt_list, reserve, for_limits)
                except ValueError as v_err:
                    insert_actions(alt_stmt_list, reserve, for_limits)
                break
        self.poi_stack.append(reserve)
        return

    def postprocess_looplimits(self, sym_export):
        for parent, wref_for, _new_limits in self.actions:
            _pos = parent.index(wref_for())
            idx = 0
            for key in ["start", "end", "step"]:
                _insert_node = _new_limits.get(key, None)
                if _insert_node:
                    sym_export += [ _node.id                          \
                                    for _node in _insert_node.targets \
                                        if _node not in sym_export ]
                    parent.insert(_pos + idx, _insert_node)
                    idx += 1
        return sym_export


def ast_normal_loop_hdr(limits, tgt):
    ast_node_convert = lambda val: ast.Num(val) if type(val) is int else val

    _old_start = limits["start"]
    _old_end = limits["end"]
    _old_step = limits["step"]

    limits["start"] = 0
    limits["step"] = 1

    _old_end = ast_node_convert(_old_end)
    _old_step = ast_node_convert(_old_step)
    _old_start = ast_node_convert(_old_start)

    # new_end = ((upper - lower ) + (step-1)) // step     - for +ve  direction
    # new_end = ((upper - lower ) - (step+1)) // step     - for -ve  direction

    step_minus_1 = ast.BinOp(_old_step, ast.Sub(), ast.Num(1))
    upper_minus_lower = ast.BinOp(_old_end, ast.Sub(), _old_start)
    numerator = ast.BinOp(upper_minus_lower, ast.Add(), step_minus_1)
    new_end = ast.BinOp(numerator, ast.FloorDiv(), _old_step)

    idx_mult_step = ast.BinOp(tgt, ast.Mult(), _old_step)
    # If original start is known to be zero
    # do not unnecessarily add a zero constant to it
    if type(_old_start) is ast.Num and _old_start.n == 0 :
        new_tgt = idx_mult_step
    else :
        new_tgt = ast.BinOp(_old_start, ast.Add(), idx_mult_step)

    canon = IndexExtract.Canonical_LinearForm()
    xform = IndexExtract.Xform()

    new_tgt = canon.visit(new_tgt)
    new_tgt = xform.visit(new_tgt)

    new_end = canon.visit(new_end)
    new_end = xform.visit(new_end)
    return new_tgt, new_end


class ForTgtReplace(ast.NodeTransformer):
    def __init__(self, src_root, ast_idx, new_ast_idx):
        self._var = ast_idx
        self._replace_var = new_ast_idx

        self.abort = False
        self.orig_node = copy.deepcopy(src_root)
        self.src_root = weakref.ref(src_root)
        self.for_stack = []
        self.for_stack_xcpt = None
        # if any assignment contains our variable abort
        # if within any  for-loop target

    def generic_visit(self, node):
        if self.abort == False:
            node = super().generic_visit(node)
        return node

    def visit_Name(self, node):
        if self.for_stack_xcpt is not None and node == self.for_stack_xcpt():
            return node

        if node.id == self._var.id:
            node = self._replace_var
        return node

    def visit_Assign(self, node):
        for tgt in node.targets:
            if type(tgt) is ast.Name and tgt.id == self._var.id:
                self.abort = True
                break
        else:
            node = self.generic_visit(node)
        return node

    def visit_AugAssign(self, node):
        if type(node.target) is ast.Name and node.target.id == self._var.id:
            self.abort = True
        else:
            node = self.generic_visit(node)
        return node

    def visit_For(self, node):
        self.for_stack.append(weakref.ref(node))

        # do-not-care condition if we are the root
        # we do not care that we are being assigned in the root for-loop node
        if node == self.for_stack[0]():
            self.for_stack_xcpt = weakref.ref(node.target)
            node = self.generic_visit(node)
        elif type(node.target) is ast.Name and node.target.id == self._var.id:
            self.abort = True
        else:
            node = self.generic_visit(node)

        self.for_stack.pop()
        if self.abort == True and len(self.for_stack) == 0:
            node = self.orig_node

        return node


class ForNormalize(ast.NodeTransformer):
    def __init__(self):
        self.poi_stack = []

    def _poi_stack_op(self, node):
        self.poi_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        self.poi_stack.pop()
        return node

    def visit_For(self, node):
        # AST API change between 3.4 and 3.5
        _create_new_range = lambda _end:                                                    \
                                ast.Call( ast.Name('range', ast.Load()),                    \
                                            [ast.Num(0), _end, ast.Num(1)], [], None, None) \
                                if sys.version_info.minor <= 4 else                         \
                                ast.Call( ast.Name('range', ast.Load()),                    \
                                            [ast.Num(0), _end, ast.Num(1)], [])

        self.poi_stack.append(weakref.ref(node))
        node = self.generic_visit(node)
        for_limit = For_Limits(getattr(node, "iter", None), node.target.id)
        curr_lim = for_limit.extract_wo_xform()
        new_tgt, new_lim = ast_normal_loop_hdr(curr_lim, node.target)
        tgt_replace = ForTgtReplace(node, node.target, new_tgt)
        node = tgt_replace.visit(node)
        if tgt_replace.abort == False:
            setattr(node, "iter", _create_new_range(new_lim))

        self.poi_stack.pop()
        return node


def normalize_pass(_ast, dbg=False):
    def debug(dbg, log_str):
        if dbg:
            print(log_str)

    ins = For_Simplify()
    _ast = ins.visit(_ast)
    _exported_sym = ins.postprocess_looplimits([])
    debug(dbg, "Intermediate-1  Code Body :::\n{0}".format(astor.code_gen.to_source(_ast)))
    debug(dbg, "=================================\n")
    f_normal = ForNormalize()
    _ast = f_normal.visit(_ast)
    debug(dbg, "Intermediate-2  Code Body :::\n{0}".format(astor.code_gen.to_source(_ast)))
    debug(dbg, "=================================\n")
    ins = For_Simplify()
    _ast = ins.visit(_ast)
    _exported_sym = ins.postprocess_looplimits(_exported_sym)
    return (_ast, _exported_sym)


if __name__ == '__main__':
    mycode = """
n=42
k = 0

def func( k , n ) :
    k += 10
    n = n / k
    for idx_k in range(200, delta(3) , 4 ) :
        t = gamma(34+ idx_k) + 43
        for idx_i in range(13,200, 2 ) :
            beta = gamma(15) + 4
    return k

for i in range(k + 7 * 2 , n + 4 / obj.func(x+3), n+1  ):
    k += i
    if k < 10 : 
        d=3
    else :
        for j in range(i+1 ,n* 4 ,2*j):
            j += k + i
    print('i = {0}'.format(i))
"""
    _ast = ast.parse(mycode)
    print("Body :::\n{0}".format(ast.dump(_ast)))
    print("=================================\n")
    print("Code :::\n{0}".format(mycode))
    print("=================================\n")

    # Test for-loop normailzation pass
    _ast = normalize_pass(_ast, True)

    print("Transformed Code Body :::\n{0}".format(astor.code_gen.to_source(_ast)))
    print("=================================\n")
