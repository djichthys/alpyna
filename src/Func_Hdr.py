#!/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import weakref


class Func_Boundary(ast.NodeTransformer):
    def __init__(self):
        self.name = "Unknown"
        self.arguments = {}


    @property
    def node(self):
        return self._node()

    @node.setter 
    def node(self, node):
        self._node = weakref.ref(node)

    #def finalize(self, node):
    #    if type(node) is weakref:
    #        self._node = node
    #    else:
    #        self._node = weakref.ref(node)

    def visit_arguments(self, node):
        for item in node.args:
            self.arguments[item.arg] = item.annotation

        for item in node.kwonlyargs: 
            self.arguments[item.arg] = item.annotation

        _new_node = self.generic_visit(node)
        return _new_node

    def visit_FunctionDef(self, node):
        _new_node = self.generic_visit(node)
        _new_node = ast.copy_location(_new_node, node)
        self.node = _new_node
        self.name = _new_node.name
        return _new_node

    def display(self):
        switch = { None : lambda x : None , \
                   getattr(ast,'Str') : lambda x : x.s ,\
                   getattr(ast,'Num') : lambda x : x.n ,\
                   getattr(ast,'Name') : lambda x : x.id }

        def mux(key):
            ret = self.arguments.get(key, None)
            if ret:
                return type(ret)
            else:
                return None

        print("function = {0} --- arguments = {1}".format(
            self.name, {
                key: switch[mux(key)](self.arguments[key])
                if key is not None else 'None'
                for key in self.arguments.keys()
            }))




if __name__ == '__main__':

    class Dummy(ast.NodeTransformer):
        def __init__(self):
            self.functions = []

        def visit_FunctionDef(self, node):
            _func = Func_Boundary()
            node = _func.visit(node)
            _func.finalize(node)
            self.functions.append(
                _func)  ## use skeleton in main region.Accumulator (region.py)
            return self.generic_visit(node)

        def display(self):
            for func_obj in self.functions:
                func_obj.display()

    with open("test.py", mode="r") as fd:
        code = fd.read()
        tree = ast.parse(code)
        print("{0}".format(ast.dump(tree)))

    dummy = Dummy()
    tree = dummy.visit(tree)
    dummy.display()
