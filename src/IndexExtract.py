#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
# /usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.

import ast
import astor


class Canonical_LinearForm(ast.NodeTransformer):
    def __init__(self):
        self.stack = -1

    def visit_Name(self, node):
        if self.stack < 0:
            node = ast.BinOp(ast.Num(1), ast.Mult(), node)
        return node

    def visit_UnaryOp(self, node):
        self.stack += 1
        node = self.generic_visit(node)
        if isinstance(node.op, ast.USub):
            _left, _right = (node.operand, ast.Num(-1))
            _op = ast.Mult()
            node = ast.BinOp(_left, _op, _right)
        elif isinstance(node.op, ast.UAdd):
            node = node.operand

        self.stack -= 1
        return node

    def visit_BinOp(self, node):
        self.stack += 1
        node = self.generic_visit(node)
        if isinstance(node.op, ast.Sub):
            old_node = node
            _right = ast.BinOp(ast.Num(-1), ast.Mult(), node.right)
            node = ast.BinOp(node.left, ast.Add(), _right)

        self.stack -= 1
        return node


class Xform(ast.NodeTransformer):
    def __init__(self):
        self.indent = ""

    def __eval_simple_Binop_Num__(self, op, left, right):
        ret = None
        if type(left) is not int and type(right) is not int:
            return None

        if isinstance(op, ast.Add):
            return left + right
        elif isinstance(op, ast.Sub):
            return left - right
        elif isinstance(op, ast.Mult):
            return left * right
        elif isinstance(op, ast.Div):
            return left / right
        elif isinstance(op, ast.FloorDiv):
            return left // right
        else:
            return None

    def _expand(self, exp_node):
        retval = exp_node
        if isinstance(exp_node, ast.Num):
            retval = exp_node.n
            return retval

        if isinstance(exp_node, ast.NameConstant):
            retval = exp_node.n
            return retval

        if isinstance(exp_node,ast.UnaryOp) and \
                         isinstance(exp_node.op,(ast.UAdd, ast.USub, ast.Not , ast.Invert)) and \
                         isinstance(exp_node.operand,(ast.Num, ast.UnaryOp, ast.BinOp)):
            _operand = self._expand(exp_node.operand)
            if isinstance(_operand, (int, float)):
                if isinstance(exp_node.op, ast.UAdd):
                    retval = +_operand
                elif isinstance(exp_node.op, ast.USub):
                    retval = -_operand
                elif isinstance(exp_node.op, ast.UNot):
                    retval = not _operand
                elif isinstance(exp_node.op, ast.UInvert):
                    retval = ~_operand
            return retval
        return self._binop_expand( exp_node) 

    def _binop_expand(self, exp_node):
        retval = exp_node
        if isinstance(exp_node,ast.BinOp) and \
                         isinstance(exp_node.op,(ast.Sub, ast.Add, ast.Mult , ast.Div,ast.FloorDiv)) and \
                         isinstance(exp_node.right,(ast.Num, ast.UnaryOp, ast.BinOp)) and \
                         isinstance(exp_node.left,(ast.Num, ast.UnaryOp, ast.BinOp)) :
            _right = self._expand(exp_node.right)
            _left = self._expand(exp_node.left)
            if (isinstance(_right, (int, float))
                    and isinstance(_left, (int, float))):
                if isinstance(exp_node.op, ast.Sub):
                    retval = _left - _right
                elif isinstance(exp_node.op, ast.Add):
                    retval = _left + _right
                elif isinstance(exp_node.op, ast.Mult):
                    retval = _left * _right
                elif isinstance(exp_node.op, ast.Div):
                    retval = _left / _right
                elif isinstance(exp_node.op, ast.FloorDiv):
                    retval = _left // _right
            elif (isinstance(_right, (int, float))):
                if _right == 0:
                    if isinstance(exp_node.op, (ast.Sub, ast.Add)):
                        retval = _left
                    elif isinstance(exp_node.op, ast.Mult):
                        retval = 0
                elif _right == 1:
                    if isinstance(exp_node.op,
                                  (ast.Mult, ast.Div, ast.FloorDiv)):
                        retval = _left
            elif (isinstance(_left, (int, float))):
                if _left == 0:
                    if isinstance(exp_node.op, ast.Sub):
                        retval = self._arithmetic_negate(_right)
                    elif isinstance(exp_node.op, ast.Add):
                        retval = _right
                    elif isinstance(exp_node.op,
                                    (ast.Mult, ast.Div, ast.FloorDiv)):
                        retval = 0
                elif _left == 1:
                    if isinstance(exp_node.op, ast.Mult):
                        retval = _right
            return retval
        return retval

    def _arithmetic_negate(self, node):
        if isinstance(node, (int, float)):
            node = node * -1
        elif type(node) is ast.Num:
            node = ast.Num(node.n * -1)
        elif type(node) is ast.Name:
            node = ast.UnaryOp(ast.USub, node)
        elif type(node) is ast.UnaryOp:
            if type(node.op) is ast.USub:
                node = node.operand
            elif type(node.op) is ast.UAdd:
                node = ast.UnaryOp(ast.USub, node.operand)
        return node

    #elif type(exp_node.op) is ast.Mult  and type(_left) is ast.BinOp and isinstance(_left.op (ast.Add,ast.Sub)):
    #    # special distributive case raised by loop normalisation
    #    print("Exit (1) : node = [{0}]".format(astor.to_source(exp_node)))
    #    _dist_left = self._expand( ast.BinOp( _left.left , ast.Mult() , ast.Num(_right)))
    #    if isinstance(_dist_left, (int,float)):
    #        _dist_left = ast.Num(_dist_left)
    #    _dist_right = self._expand( ast.BinOp( _left.right , ast.Mult() , ast.Num(_right)))
    #    if isinstance(_dist_right, (int,float)):
    #        _dist_right = ast.Num(_dist_right)
    #
    #   retval = self.expand( ast.BinOp( _dist_left , exp_node.op , _dist_right))
    #elif type(exp_node.op) is ast.Mult  and type(_right) is ast.BinOp and isinstance(_right.op (ast.Add,ast.Sub)):
    #    print("Exit (20) : node = [{0}]".format(astor.to_source(exp_node)))
    #    _dist_left = self._expand( ast.BinOp( ast.Num(_left), ast.Mult() , _right.left))
    #    if isinstance(_dist_left, (int,float)):
    #        _dist_left = ast.Num(_dist_left)
    #    _dist_right = self._expand( ast.BinOp( ast.Num(_left), ast.Mult() , _right.right))
    #    if isinstance(_dist_right, (int,float)):
    #        _dist_right = ast.Num(_dist_right)
    #
    #    retval = self.expand( ast.BinOp( _dist_left , exp_node.op , _dist_right))

    # This function is added to cater for the effect of loop normalisation
    # creating a complex non-linear expression
    # used to distribute  _const * ( _expr +/- _expr)
    def _mult_distribute(self, node ):
        # Check this is a binary-op node
        if type(node) is not ast.BinOp : 
            return node , False 
    
        # check that we have the form (_const -op ( _expr1 -op _expr2 ))
        if type(node.left) is ast.Num and type(node.right) is ast.BinOp :
            const = node.left
            complex_node = node.right 
            complex_loc = 'right' 
        elif type(node.right) is ast.Num and type(node.left) is ast.BinOp :
            const = node.right
            complex_node = node.left 
            complex_loc = 'left'
        else : 
            return node , False 

        # check that we have the form ( _const * (_expr1 +/- _expr2 ))
        #check the outer binary operation is a multiplication 
        if type(node.op) is not ast.Mult :
            return node , False 
        outer_op = node.op 

        #check the inner binary operation is a add / subtract  
        if not isinstance(complex_node.op , (ast.Add,ast.Sub)) :
            return node , False 
        
        if complex_loc == 'left' : 
            _left = ast.BinOp(complex_node.left , ast.Mult(), ast.Num(const.n))
            _right = ast.BinOp(complex_node.right , ast.Mult(), ast.Num(const.n))
        else : 
            _left = ast.BinOp(ast.Num(const.n), ast.Mult(), complex_node.left)
            _right = ast.BinOp( ast.Num(const.n), ast.Mult(), complex_node.right)

        node = ast.BinOp( _left ,  complex_node.op   , _right) 
        return node , True 


    def _eval_associative(self,
                          node,
                          outer_op,
                          const,
                          complex_node,
                          complex_loc="left"):
        """This function is only to be used to reduce expressions
		   using associativity and should only be used when either 
		   one of left or right operands are known to be 1) constant
		   or a 2) Name. eg simplifying (i * 2) * 3 , 3 + (i-7) 
		   complex_loc specifies whether the known complex node is on the left or right"""
        #print("Entry :: node = [{0}] , outer_op = [{1}], const = {2} , complex_node = {3} , complex_loc = {4}".format(astor.to_source(node), outer_op, const , astor.to_source(complex_node), complex_loc))
        if not type(const) in (int, float):
            #print("Exit (1) : node = [{0}]".format(astor.to_source(node)))
            return node

        if not isinstance(outer_op, (ast.Mult, ast.Add, ast.Sub)):
            #print("Exit (2) : node = [{0}]".format(astor.to_source(node)))
            return node

        if complex_loc not in ("left", "right"):
            #print("Exit (3) : node = [{0}]".format(astor.to_source(node)))
            return node

        # We do associative reduction only if there are two integers to evaluate
        if type(const) is not int or \
            type(complex_node) is not ast.BinOp or \
            ( type(complex_node.left) is not ast.Num and type(complex_node.right) is not ast.Num ) :
            #print("Exit (4) : node = [{0}]".format(astor.to_source(node)))
            return node

        # if the complex binary-op is on the right for a subtraction
        # then negate the arguments if possible (because associativity
        # is not always correct for subtraction and division)
        # We are not considering division - for the moment!!!
        if type(outer_op) is ast.Sub:
            if complex_loc == "right":
                cn_left = self._arithmetic_negate(complex_node.left)
                cn_right = self._arithmetic_negate(complex_node.right)
                if id(cn_left) != id(complex_node.left) \
                   or id(cn_right) != id(complex_node.right) :
                    complex_node.left = cn_left
                    complex_node.right = cn_right
                    outer_op = ast.Add()
                else:  #no change - could not reduce - give up here
                    #print("Exit (5) : node = [{0}]".format(astor.to_source(node)))
                    return node
            elif complex_loc == "left":
                const = self._arithmetic_negate(const)
                outer_op = ast.Add()


        _precedence = { getattr(ast,'Add') : 0 \
                             , getattr(ast,'Sub') : 0 \
                             , getattr(ast,'Mult') : 1 }
        if _precedence[type(outer_op)] != _precedence[type(complex_node.op)]:
            #print("Exit (6) : node = [{0}]".format(astor.to_source(node)))
            return node



        _complex_const, _complex_const_loc = None, None
        if isinstance(complex_node.left, ast.Num):
            _complex_const, _complex_const_loc = complex_node.left.n, "left"
        elif isinstance(complex_node.right, ast.Num):
            _complex_const, _complex_const_loc = complex_node.right.n, "right"
		

        """case inner-binop 
		   *) _complex_const = _complex_const * const 
		   +) _complex_const = _complex_const + const 
		   -) case _complex_const_loc 
		       right) _complex_const  = _complex_const -  const 
		       left) _complex_const = _complex_const + const"""
        _simplify = { getattr(ast,'Mult') : lambda loc, const_i, const_o : const_i * const_o ,\
                      getattr(ast,'Add')  : lambda loc, const_i, const_o : const_i + const_o ,\
                      getattr(ast,'Sub')  : lambda loc, const_i, const_o : \
                                                    const_i - const_o if loc == "right"    \
                                                    else const_i + const_o}

        _complex_const = _simplify[type(complex_node.op)](
            _complex_const_loc, _complex_const, const)
        if _complex_const_loc == "left":
            complex_node.left.n = _complex_const
        elif _complex_const_loc == "right":
            complex_node.right.n = _complex_const

        node = complex_node
        #print("Exit (7) : node = [{0}]".format(astor.to_source(node)))
        return node

    #def visit_Expr(self, node) :
    #	""" Remove this after debugging use"""
    #	expr_eval = self._expand(node.value)
    #	if isinstance(expr_eval,ast.BinOp) :
    #		print("Expression evaluation = --{0}--".format(ast.dump(expr_eval)))
    #	elif type(expr_eval) is int :
    #		print("Expression evaluation = --{0}--".format(expr_eval) )
    #
    #	return self.generic_visit(node)

    def visit_BinOp(self, node):
        node = self.generic_visit(node)
        # check whether distribution is performed over a multiply operator
        # if so, then recurse again to ensure that we attempt simplification
        node, _immediate_recurse = self._mult_distribute(node)
        if _immediate_recurse :
            node = self.generic_visit(node)

        _partial_left = self._expand(node.left)
        _partial_right = self._expand(node.right)

        if type(_partial_left) is int and type(_partial_right) is int:
            _eval_n = self.__eval_simple_Binop_Num__(node.op, _partial_left,
                                                     _partial_right)
            if _eval_n is not None:
                node = ast.Num(_eval_n)
        elif type(_partial_left) is int:
            node.left = ast.Num(_partial_left)
            node = self._eval_associative(node, node.op, _partial_left,
                                          _partial_right, "right")
        elif type(_partial_right) is int:
            node.right = ast.Num(_partial_right)
            node = self._eval_associative(node, node.op, _partial_right,
                                          _partial_left, "left")

        return node


class Linear_Extract(ast.NodeVisitor):
    def __init__(self):
        self.terms = {"__Constant__" : 0 }
        self.stack = -1
        self._aggregate = lambda key, val: self.terms[key] + val if key in self.terms else val

    def visit_Num(self, node):
        if self.stack < 0:
            self.terms["__Constant__"] = node.n

    def add_mult_terms(self, node):
        if isinstance(node.left, ast.Name) and \
           isinstance(node.right, ast.Num) :
            self.terms[node.left.id] = self._aggregate(node.left.id,
                                                       node.right.n)
        elif isinstance(node.right, ast.Name) and \
             isinstance(node.left, ast.Num) :
            self.terms[node.right.id] = self._aggregate(
                node.right.id, node.left.n)

    def visit_BinOp(self, node):
        self.stack += 1
        self.generic_visit(node)

        if isinstance(node.op, ast.Mult):
            self.add_mult_terms(node)
        elif isinstance(node.op, ast.Add):
            if isinstance(node.left, ast.Num):
                self.terms["__Constant__"] = self._aggregate(
                    "__Constant__", node.left.n)
            elif isinstance(node.left, ast.Name):
                self.terms[node.left.id] = self._aggregate(node.left.id, 1)

            if isinstance(node.right, ast.Num):
                self.terms["__Constant__"] = self._aggregate(
                    "__Constant__", node.right.n)
            elif isinstance(node.right, ast.Name):
                self.terms[node.right.id] = self._aggregate(node.right.id, 1)

        self.stack -= 1


def extract_index(node):
    if type(node) is str:
        node = ast.parse(node)

    #Pass to convert to canonical linear format
    canonical_xform = Canonical_LinearForm()
    canonical_ast = canonical_xform.visit(node)

    #arithematic reduction to simplified format
    xform = Xform()
    _new_ast = xform.visit(canonical_ast)
    _new_ast = ast.fix_missing_locations(_new_ast)

    #extract components of Linear expression
    xtract_obj = Linear_Extract()
    xtract_obj.visit(_new_ast)

    return _new_ast, xtract_obj.terms


if __name__ == '__main__':
    #expr_str = """10 + (2 + -3) * -jj  - (3 * 5  + 0 ) *1  + -12 + 3 *-(-i + 0) * -3 + 17 + 2 *(-1*-(-j)) + l +3 * 4 + 7 - k  + 4 * (start + idx * 10) + 3*k - 7 * ll - -(-i)"""
    expr_str = """n"""
    #expr_str = """3 * (10 + idx * 4)""" 
    #expr_str = """-(-i) * -3 + 17 + 2 *(-1*-(-j)) + l"""
    orig_ast = ast.parse(expr_str)
    print("==============original string====================")
    print("{0}".format(expr_str))
    print("==============original ast====================")
    print("{0}".format(ast.dump(orig_ast)))
    print("==============original ast====================")

    canon = Canonical_LinearForm()
    canonical_ast = canon.visit(orig_ast)

    print("=================canonical ast======================")
    print("{0}".format(ast.dump(canonical_ast)))
    #print(" canonical code : \n {0}\n ".format(astor.code_gen.to_source(canonical_ast)))
    print("=================canonical ast======================")
    xform = Xform()
    _new_ast = xform.visit(canonical_ast)
    _new_ast = ast.fix_missing_locations(_new_ast)

    xtract_obj = Linear_Extract()
    xtract_obj.visit(_new_ast)

    print("=================new ast======================")
    print("{0}".format(ast.dump(_new_ast)))
    print(" retransformed code : \n {0}\n ".format(
        astor.code_gen.to_source(_new_ast)))
    print(" extraction : \n {0}\n ".format(xtract_obj.terms))
    print("=================new ast======================")

    print("======================================")
    print("Call function :::: ")
    print("======================================")
    xform_ast, linear_terms = extract_index(expr_str)
