import math


#def fbcorr( imgs, filters, output ):
#  n_imgs, n_rows, n_cols, n_channels = imgs.shape
#  n_filters, height, width, n_ch2 = filters.shape
#  for h in range(height):
#   for w in range(width):
#    for j in range(n_channels):
#     for i in range(n_imgs):
#      for r in range(n_rows - height + 1):
#       for c in range(n_cols - width + 1):
#        for f in range(n_filters):
#          output[i,f,r,c] = output[i,f,r,c] + imgs[i, r+h, c+w, j] * filters[f, h, w, j]

#def vector_add(c , a , b ):
#  for i in range(c.size):
#    c[i] = a[i] + b[i]
     

#def conv2d( y , x , h ):
#  s = h.shape[0] // 2
#  for k in range( -s, s+1 ):
#    for l in range( -s, s+1 ):
#      for i in range( s, x.shape[0] - s ):
#        for j in range( s, x.shape[1] - s ):
#          y[i-s,j-s] = y[i-s,j-s] + x[i+k,j+l] + h[k+s,l+s]
#
#
#def syr2k( alpha , beta, c, a, b):
#  for i in range( c.shape[0] ):
#    for j in range( c.shape[1] ):
#      c[i,j] = c[i,j] * beta
#
#  for k in range( c.shape[1] ):
#    for i in range( c.shape[0] ):
#      for j in range( c.shape[1] ):
#        c[i,j] = c[i,j] + (alpha * a[j][k]*b[i][k]) + (alpha*b[j,k]*a[i,k])


#def atax( A, x, y, tmp ):
#  for i in range( A.shape[1] ):
#    for j in range( A.shape[0] ):
#      tmp[i] = tmp[i] + A[i,j] * x[j]
#    for j in range( A.shape[0] ):
#      y[j] = y[j] + A[i,j] * tmp[i]



#def gemver( alpha,beta,A,u1,u2,v1,v2,w,x,y,z ):
#  for i in range( A.shape[0] ):
#    for j in range( A.shape[1] ):
#      A[i,j] = A[i,j] + u1[i]*v1[j] + u2[i]*v2[j]
#
#  for j in range( A.shape[1] ):
#    for i in range( A.shape[0] ):
#      x[i] = x[i] + beta * A[j,i] * y[j]
#
#  for i in range( x.shape[0] ):
#    x[i] = x[i] + z[i]
#
#  for j in range( A.shape[1] ):
#    for i in range( A.shape[0] ):
#      w[i] = w[i] + alpha * A[i,j] * x[j]
#
#
#def black_scholes( call, put, s, x, t, r, v , max_iter):
#  _sqrt   = np.zeros(s.size)
#  _d1     = np.zeros(s.size)
#  _d2     = np.zeros(s.size)
#  _k      = np.zeros(s.size)
#  _exp_rt = np.zeros(s.size)
#  _cnd_d1 = np.zeros(s.size)
#  _cnd_d2 = np.zeros(s.size)
#
#  _a1 = 0.31938153
#  _a2 = -0.356563782
#  _a3 = 1.781477937
#  _a4 = -1.821255978
#  _a5 = 1.330274429
#  _rsqrt2pi = 0.39894228040143267793994605993438
#
#
#  for _itr in range(max_iter):
#    for i in range(len(s)):
#      _sqrt[i] = math.sqrt(t[i])
#
#      # calculate d1 and cumulative d1 
#      _d1[i] = (math.log(s[i]/x[i]) + (r + 0.5*v*v)*t[i]) / v * _sqrt[i]
#      _k[i]  = 1.0 / (1.0 + 0.2316419 * math.fabs(_d1[i]))
#      _cnd_d1[i] = ((_rsqrt2pi * math.exp(-0.5 * _d1[i] * _d1[i])) * \
#                    (_k[i] * (_a1 + _k[i] * (_a2 + _k[i] * (_a3 + _k[i] * (_a4 + _k[i] * _a5))))))
#      if _cnd_d1[i] > 0:
#        _cnd_d1[i] = 1.0 - _cnd_d1[i]
#
#      # calculate d2 and cumulative d2 
#      _d2[i] = _d1[i] - (v * _sqrt[i])
#      _k[i]  = 1.0 / (1.0 + 0.2316419 * math.fabs(_d2[i]))
#      _cnd_d2[i] = ((_rsqrt2pi * math.exp(-0.5 * _d2[i] * _d2[i])) * \
#                    (_k[i] * (_a1 + _k[i] * (_a2 + _k[i] * (_a3 + _k[i] * (_a4 + _k[i] * _a5))))))
#      if _cnd_d2[i] > 0:
#        _cnd_d2[i] = 1.0 - _cnd_d2[i]
#
#      _exp_rt[i] = math.exp((-1.0 * r) * t[i])
#      call[i] = (s[i] * _cnd_d1[i]) - (x[i] * _exp_rt[i] * _cnd_d2[i])
#      put[i] = (x[i] * _exp_rt[i] * (1.0 - _cnd_d2[i])) - (s[i] * (1.0 - _cnd_d1[i]))

#
#def ceiling( arr_a , cvar ):
# for idx_i in range(len(arr_a)):
#   if cvar < 10 :
#     if arr_a[idx_i] < 100 : 
#         arr_a[idx_i] = math.ceil(arr_a[idx_i])
#def mandelbrot( image, c, iter_lim, max_magnitude ):
# _z = np.array([[complex(0,0) for y in range(image.shape[1]) ] for x in range(image.shape[0])])
#
# for _iter in range( iter_lim ):
#   for x in range( image.shape[0] ):
#     for y in range( image.shape[1] ):
#       _z[x,y] = _z[x,y]**2 + c[x,y]
#       if abs(_z[x,y]) < max_magnitude :
#         image[y,x] = _iter
#       else : 
#         image[y,x] = 255 



#def hilbert_matrix( matrix_size ):
#  h_matrix = np.zeros((matrix_size,matrix_size),dtype='float32')
#  for i in range(matrix_size):
#    for j in range(matrix_size):
#      h_matrix[i,j] = 1.0 / ((i+1) + (j+1) - 1.0)
#  return h_matrix


#def vector_add(c , a , b ):
#  for i in range(c.size):
#    c[i] = a[i] + b[i]
     
#def read_conditional(arg_a , arg_b, myc ):
#  _mstart = 2
#  _nend = 200
#  for i in range(2, 20):
#    arg_a[i+1] = arg_a[i] + 10
#    for j in range(0,200,1):
#      arg_b[i+10][j] = arg_a[i] + 10
#
#  for i in range(2, 20):
#    arg_a[2] = 10
#    for j in range(0,200,1):
#      arg_b[i+10][j] = arg_a[i] + 10

#def caledonia( arg_a, arg_b, limits) : 
# i_max, j_max, k_max, m_max = limits 
# for i in range(1,i_max+1,1):
#   for j in range(0,j_max+1,1):
#     for k in range(0,k_max,1):
#       for m in range(0,m_max,1):
#         arg_a[i+1][j][k][m] = arg_a[i][j+1][k][m] + 4 + arg_b[i]
#         arg_a[i-1][j][k][m] = arg_a[i][j][k][m] + 43 

#def caledonia( arg_a, arg_b, limits) : 
# i_max, j_max, k_max, m_max = limits 
# for i in range(0,10,1):
#   for j in range(0,20,1):
#     for k in range(0,40,1):
#       for m in range(0,50,1):
#         arg_a[i][j+10][k][m] = arg_a[i][j][k][m] + 4 + arg_b[i]
#         arg_a[i+1][j][k][m] = arg_a[i][j][k][m] + 43 


# for i in range(0,10,1):
#  for j in range(2*n,n+9,2):
#   arg_b[i+1][j-5] = arg_b[i][5-j] + arg_a[i][j]
# return arg_b 

# kdelta = arg_a[0] + 10 
# for i in range(0,20,1):
#   for j in range(0,30,1):
#     arg_a[i+1][j+1] = arg_a[i][j] + 4
#     arg_b[i+25][j] = arg_b[i][j] * 3 + kdelta


#def george(arg_a , arg_b , n ): 
# t = 0 
# d = 0.13
# for i in range(0,10,1):
#  for j in range(2*n,n+9,2):
#   t = t + arg_b[i][j+1] + arg_a[i][j]
#   d = d + arg_b[i][j+1] + arg_a[i][j]
#
# for i in range(0,10,1):
#  for j in range(2*n,n+9,2):
#   arg_b[i+1][j-5] = arg_b[i][5-j] + arg_a[i][j]
#
# return 10 

#def george( arg_a ):
#    i_max, j_max = np.shape(arg_a)
#    for idx_i in range(i_max):
#        for idx_j in range(j_max):
#            arg_a[idx_i][idx_j] = arg_a[idx_i][idx_j] + 1 
#    return arg_a 
#

def matmul( mat_a , mat_b , mat_c ):
    ma_rmax, ma_cmax = np.shape( mat_a )
    b_rmax, mb_cmax = np.shape( mat_b )
    for k in range(ma_cmax):
        for i in range(ma_rmax):
            for j in range(mb_cmax):
                #mat_c[i][j] = mat_c[i][j] + mat_a[i][k] * mat_b[k][j]
                mat_c[i,j] = mat_c[i,j] + mat_a[i,k] * mat_b[k,j]

#def saxpy( arr_y , arr_x , constval ):
#    for idx_i in range(len(arr_y)):
#        arr_y[idx_i] = constval * arr_x[idx_i] + arr_y[idx_i] 
#
#
#
#def jacobi_relax_core( next_x , curr_x , err):
#    i_max , j_max = np.shape( curr_x)
#    for i in range(1,i_max-1):
#        for j in range(1,j_max-1):
#            next_x[i,j] = 0.25 * ( curr_x[i,j+1] + curr_x[i,j-1] + curr_x[i-1,j] + curr_x[i+1,j])
#            err[i,j] = next_x[i,j] - curr_x[i,j]
#


#def conway( curr , nxt , size ):
#    for i in range(1,size-1):
#        for j in range(1,size-1):
#            # first count number of live neighbours (between 0 and 8)
#            nxt[i,j] = curr[i-1,j-1] + curr[i-1,j] + curr[i-1,j+1] + \
#                        curr[i,j-1]  +               curr[i,j+1] +   \
#                        curr[i+1,j-1] + curr[i+1,j] + curr[i+1,j+1]
#            # nxt cell is live if curr is dead but has 3 live neighbours, 
#            # or if curr is alive and has 2 or 3 live neighbours
#            nxt[i,j] = ((~(-curr[i,j]) & nxt[i,j]) | ((curr[i,j]*nxt[i,j])|curr[i,j]))==3
#

#def conv1d( y , x , h ) :
#  for j in range( len(h) ):
#    for i in range( len(x) ):
#      if (i-j) >= 0 :
#        y[i] = y[i] + x[i-j] * h[j]
#
#
#def conv2d( y , x , h ):
#  s = h.shape[0] // 2
#  for k in range( -s, s+1 ):
#    for l in range( -s, s+1 ):
#      for i in range( s, x.shape[0] - s ):
#        for j in range( s, x.shape[1] - s ):
#          y[i-s,j-s] = y[i-s,j-s] + x[i+k,j+l] + h[k+s,l+s]




#def nbody_calc_acceleration( pos, acc ):
# _pos_x = pos.x 
# _pos_y = pos.y 
# _pos_z = pos.z 
#
# _acc_x = acc.x
# _acc_y = acc.y
# _acc_z = acc.z
# # assumption all accelerations have been zero'ed out
# for j in range(len(bodies)):
#   # Compute accelerations
#   for i in range(len(bodies)):
#     if j != i :
#       _acc_x[i] = _acc_x[i] + (((_pos_x[j]-_pos_x[i])*G*mass[j]) / (math.pow(\
#                                          math.sqrt(math.pow(_pos_x[i] - _pos_x[j],2) \
#                                        + math.pow(_pos_y[i] - _pos_y[j],2) \
#                                        + math.pow(_pos_z[i] - _pos_z[j],2)), 3)))
#       _acc_y[i] = _acc_y[i] + (((_pos_y[j]-_pos_y[i])*G*mass[j]) / (math.pow(\
#                                          math.sqrt(math.pow(_pos_x[i] - _pos_x[j],2) \
#                                        + math.pow(_pos_y[i] - _pos_y[j],2) \
#                                        + math.pow(_pos_z[i] - _pos_z[j],2)), 3)))
#       _acc_z[i] = _acc_z[i] + (((_pos_z[j]-_pos_z[i])*G*mass[j]) / (math.pow(\
#                                          math.sqrt(math.pow(_pos_x[i] - _pos_x[j],2) \
#                                        + math.pow(_pos_y[i] - _pos_y[j],2) \
#                                        + math.pow(_pos_z[i] - _pos_z[j],2)), 3)))
#
#
#def nbody_update( pos, acc ):
# _pos_x = pos.x 
# _pos_y = pos.y 
# _pos_z = pos.z 
#
# _acc_x = acc.x
# _acc_y = acc.y
# _acc_z = acc.z
#
