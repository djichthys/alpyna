#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import Interface_Rewrite as api_xform
import Dependency_Solver as solve


class Runtime_Anchor:
    def __init__(self, canon_ast, loop_agg):
        self.loop_agg = loop_agg
        self.gc_barrier = None    # initialize first
        self.ast_root = canon_ast
        self.ln_map = self._init_loopnest_map()

    @property
    def loop_agg(self):
        return self._loop_agg

    @loop_agg.setter
    def loop_agg(self, loop_agg):
        self._loop_agg = loop_agg

    @property
    def gc_barrier(self):
        return self._gc_barrier

    @gc_barrier.setter
    def gc_barrier(self, gc_barrier):
        if getattr(self, '_gc_barrier', None):
            self._gc_barrier = gc_barrier
        else:
            self._gc_barrier = []

    @property
    def ast_root(self):
        return self._ast_root

    @ast_root.setter
    def ast_root(self, root):
        self._ast_root = root

    @property
    def ln_map(self):
        return self._loopnest_map

    @ln_map.setter
    def ln_map(self, loopnest_map):
        self._loopnest_map = loopnest_map

    def _init_loopnest_map(self):
        return { _ln.graph_builder.vfunc_name : _ln \
           for _func in self.loop_agg.functions \
            for _ln in _func.loop_truss }


def runtime_setup(loop_agg, canon_ast, runtime_opts):
    global runtime
    runtime = Runtime_Anchor(canon_ast, loop_agg)
    _anyscale = api_xform.static_code_rewrite(runtime, runtime_opts)
    setattr(_anyscale, "_runtime_anchor", runtime)

    for _idx_func, _func in enumerate(runtime.loop_agg.functions):
        for _idx_loop, _loop_nest in enumerate(_func.loop_truss):
            _code_gen = getattr(_loop_nest, 'code_gen', None)
            if _code_gen == None:
                continue

            _anyscale.rt.insert_codeobj(_anyscale, _code_gen.drivers['interpreter'])

    solve.phase.transition_phase()
    return _anyscale
