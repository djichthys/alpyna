#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

import types
import sys

import ast
import weakref
import astor
import Generate_DAG as dag

module_init_str = """
import AnyScale_Runtime as rt
import numpy as np
from numba import cuda, jit

mod_self = None
mod_opts = None
"""


class UserFunction_Xform(ast.NodeTransformer):
    def __init__(self, rt_env, func_rec):
        self.rt_env = rt_env
        self.func_rec = func_rec
        self.closures = []

    @property
    def rt_env(self):
        return self._rt_env()

    @rt_env.setter
    def rt_env(self, env):
        if callable(env):
            self._rt_env = env
        else:
            self._rt_env = weakref.ref(env)

    @property
    def func_node(self):
        return self._func_node()

    @func_node.setter
    def func_node(self, func_node):
        if callable(func_node):
            self._func_node = func_node
        else:
            self._func_node = weakref.ref(func_node)

    def _search_loop_truss(self, node):
        _loop_nest = None
        for _lnest in self.func_rec.loop_truss:
            if _lnest.boundary.start.node == node:
                _loop_nest = _lnest
                break
        return _loop_nest

    def _loop_meta_bindings(self, loop_nest):
        _looplim_bind = []
        _looptgt_bind = []
        loop_nest.rewind()
        _rec = loop_nest.consume()
        while _rec:
            if _rec.ntype == 'For':
                _for_meta = _rec.record
                _looptgt_bind += _for_meta['for_targets'].target

                _for_meta_lim = _for_meta['for_limits'].limits
                _looplim_bind += [ _for_meta_lim[_key][1] \
                     for _key in ["start", "end", "step"] \
                      if _for_meta_lim[_key][0] == "lazy" ]
            _rec = loop_nest.consume()
        return (_looptgt_bind, _looplim_bind)

    def _hoisted_vars(self, loop_nest):
        return [_arg.arg_name(True) for _arg in loop_nest.code_gen.dyn_lim]

    def _gen_closure_xfer(self, loop_nest):
        _loop_tgts, _loop_limits = self._loop_meta_bindings(loop_nest)
        arg_xfer = dag.uniq_var_txfr_list(
            loop_nest.source_vars, None,
            lambda varbind: (True, None) if len(varbind.var_ref["axes"]) > 0 or varbind.var_ref["var"] not in _loop_tgts else (False, None)
        )

        arg_xfer = dag.uniq_var_txfr_list( loop_nest.target_vars \
                 , arg_xfer , lambda varbind : (True, None))
        _decl, _pack, _unpack = ("", "", "")

        for _arg in arg_xfer:
            _decl += "  nonlocal {0}\n".format(_arg.arg_name(False))
            _pack += "  xfer_container.{0} = {0}\n".format(_arg.arg_name(False))
            _unpack += "  {0} = xfer_container.{0}\n".format(_arg.arg_name(False))

        # If there were any variables hoisted outside the loop
        # add these to declarations to transsfer through closure
        for _llbind in self._hoisted_vars(loop_nest):
            _decl += "  nonlocal {0}\n".format(_llbind)
            _pack += "  xfer_container.{0} = {0}\n".format(_llbind)
            _unpack += "  {0} = xfer_container.{0}\n".format(_llbind)

        return (_decl, _pack, _unpack)

    def _gen_closure(self, loop_nest):
        _decl, _pack, _unpack = self._gen_closure_xfer(loop_nest)
        _clsr = "def {0}():\n".format(loop_nest.graph_builder.vfunc_name)
        _clsr += _decl + "  xfer_container = rt.Closure_Arg_Container()\n"
        _clsr += _pack
        _clsr += '  rt._ufunc_mux( xfer_container, mod_self, "{0}", mod_opts)\n'.format(
            loop_nest.graph_builder.vfunc_name)
        _clsr += _unpack
        return _clsr

    def visit_For(self, node):
        node = self.generic_visit(node)
        _loop_nest = self._search_loop_truss(node)
        if _loop_nest == None:
            return node

        _call_closure = ast.parse("{0}()".format(_loop_nest.graph_builder.vfunc_name))
        self.rt_env.gc_barrier.append(node)
        self.closures.append(self._gen_closure(_loop_nest))
        return _call_closure.body[0]


class Module_Xform(ast.NodeTransformer):
    def __init__(self, rt_base_env):
        self.rt_env = rt_base_env

    @property
    def rt_env(self):
        return self._rt_env()

    @rt_env.setter
    def rt_env(self, env):
        if callable(env):
            self._rt_env = env
        else:
            self._rt_env = weakref.ref(env)

    def _search_function(self, node):
        _func_record = None
        for _func in self.rt_env.loop_agg.functions:
            if node == _func.start.node:
                _func_record = _func
                break
        return _func_record

    def visit_FunctionDef(self, node):
        node = self.generic_visit(node)
        function_rec = self._search_function(node)
        if function_rec == None:
            return node

        func_xform = UserFunction_Xform(self.rt_env, function_rec)
        node = func_xform.visit(node)
        for _clsr in reversed(func_xform.closures):
            node.body.insert(0, ast.parse(_clsr).body[0])
        return node


def create_module(txt, module_name, opts, desc="Anyscale Parallelisation Engine"):
    _ns = types.ModuleType(module_name, desc)
    _ns.__file__ = "{0}.pyc".format(module_name)
    _co = compile(txt, "{0}.py".format(module_name), 'exec')
    exec(_co, _ns.__dict__)
    sys.modules[module_name] = _ns
    _ns.mod_self = _ns
    _ns.mod_opts = opts
    return _ns


# arg txt : could be the function string or
#           the ast code object from parsing the ast
def insert_skeleton_ufunc(module_obj, txt):
    _co = compile(txt, "<no-file>", 'exec')
    exec(_co, module_obj.__dict__)



def static_code_rewrite(rt_base, runtime_options):
    _gen_closure = Module_Xform(rt_base)
    kernel_xform = _gen_closure.visit(rt_base.ast_root)

    #print("================================================")
    #print("displaying re-written AST")
    #print("================================================")
    #print("{0}".format(astor.codegen.to_source(kernel_xform)))

    _init_module_str = module_init_str + "\n\n" + astor.codegen.to_source(kernel_xform)
    #print("================================================")
    #print("displaying initialised module")
    #print("================================================")
    #print("{0}".format(_init_module_str))

    # Create initial User-facing interface for module
    _anyscale = create_module(_init_module_str \
                             ,  "AnyScale" \
                             , runtime_options \
                             , desc="Anyscale Loop Parallelisation Engine")
    return _anyscale
