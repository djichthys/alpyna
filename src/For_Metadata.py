#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import weakref

#import Extract_Memory_Dependency as Mem_Dep
#import Utils as flt_util


class For_Limits(ast.NodeVisitor):
    def __init__(self, for_iter_node):
        self.node = for_iter_node
        self.std_iterator = None
        self.std_iterator_ctx = None
        self.limits = {"start": ("lazy", None), "end": ("lazy", None), "step": ("lazy", None)}
        self._init_extract()

    @property
    def node(self):
        if self._node is None:
            return None
        else:
            return self._node()

    @node.setter
    def node(self, variable):
        self._node = weakref.ref(variable)

    def _init_range_limit_args(self, range_lim_args):
        if type(range_lim_args) is list:
            if len(range_lim_args) == 1:
                self.limits["start"] = ("imm", 0)
                self.limits["step"] = ("imm", 1)
                if type(range_lim_args[0]) is weakref:
                    print(">>>>>>>>>>>>>>> are we ever here 1 ")
                    _lim_args = range_lim_args[0]()
                else:
                    _lim_args = range_lim_args[0]

                try:
                    eval_arg = ast.literal_eval(_lim_args)
                    self.limits["end"] = ("imm", eval_arg)
                except ValueError as excpt:
                    if type(_lim_args) is ast.Name:
                        self.limits["end"] = ("lazy", _lim_args.id)
                    else:
                        self.limits["end"] = ("lazy", _lim_args)

            else:
                for key, arg in zip(["start", "end", "step"], range_lim_args):
                    #print("entering __init_range_limit_arg else for type = {0}".format((type(arg))))
                    if type(arg) is weakref:
                        _arg = arg()
                    else:
                        _arg = arg

                    try:
                        eval_arg = ast.literal_eval(_arg)
                        self.limits[key] = ("imm", eval_arg)
                    except ValueError as excpt:
                        if type(_arg) is ast.Name:
                            self.limits[key] = ("lazy", _arg.id)
                        else:
                            self.limits[key] = ("lazy", _arg)

    def _init_extract(self):
        if type(self.node) is ast.Call:
            if 'func' in self.node._fields:
                if self.node.func.id == 'range':
                    self.std_iterator = self.node.func.id
                    self.std_iterator_ctx = self.node.func.ctx
                    self._init_range_limit_args(self.node.args)
        else:
            print("Something went wrong")

    def _rcopy_limits(self):
        _dict = self.limits
        return {_key: (_dict[_key][0], _dict[_key][1]) for _key in _dict.keys()}

    def rcopy(self):
        _obj = For_Limits(self.node)
        _obj.limits = self._rcopy_limits()
        return _obj


class For_Targets(ast.NodeVisitor):
    def __init__(self, node):
        self.node = node
        self.target = []
        self._rexpand(node)

    @property
    def node(self):
        return self._node()

    @node.setter
    def node(self, node):
        self._node = weakref.ref(node)

    def visit_Name(self, node):
        self.target.append(node.id)

    def _rexpand(self, node):
        if type(node) is ast.Name:
            self.target.append(node.id)
        else:
            self.generic_visit(node)

    def _rcopy_target(self):
        return [_tgt for _tgt in self.target]

    def rcopy(self):
        _obj = For_Targets(self.node)
        _obj.target = self._rcopy_target()
        return _obj
