#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import functools
import json

import Static_Analysis_Driver as alpyna
import Utils as util
import Hardware.cuda_gpu as gpu

static_profile_kernel = """
def _static_profile_kernel(arg_a, i_dom):
    par_size = arg_a.shape[0]
    for _i in range(i_dom):
        for _j in range(par_size):
            arg_a[_j] = arg_a[_j] + 1
    return arg_a


def _cpujit_profile_kernel(arg_a, i_dom):
    par_size = arg_a.shape[0]
    for _i in range(i_dom):
        for _j in range(par_size):
            arg_a[_j] = arg_a[_j] + 1
    return arg_a
"""


class Architecture_Profile:
    def __init__(self, opts=None):
        self.profiling_kernels = static_profile_kernel
        self.opts = opts

        # cki-profile limits
        self.cki_profile_params = (0, 0, 0, 0)

        #internal-flags for use with profiler
        self._alpyna_obj = None

    @property
    def profiling_kernels(self):
        return self._ki_kernel

    @profiling_kernels.setter
    def profiling_kernels(self, txt):
        self._ki_kernel = txt if txt else static_profile_kernel

    @property
    def opts(self):
        return self._opts

    @opts.setter
    def opts(self, options):
        self._opts = options if options else util.ALPyNA_Options(prof_type="framework_init_cki_cg")

    @property
    def cki_profile_params(self):
        return (self._start, self._exp, self._deviation, self._max_iter)

    @cki_profile_params.setter
    def cki_profile_params(self, params):
        self._start = params[0]
        self._exp = params[1]
        self._deviation = params[2]    # encoded as a percentage
        self._max_iter = params[3]

    def set_cki_profile_bounds(self, start, exp, deviation, max_iter):
        self.cki_profile_params = (start, exp, deviation, max_iter)

    def _cki_bounds_generator(self, timer, scratch_pad):
        _start, _exp, _deviation, _max_iter = self.cki_profile_params

        # Start at the minimal point and keep goin
        _itr = 0
        _inflection = False
        _curr_avg = scratch_pad[0][1]
        _dom_itr = _start
        while _itr < _max_iter and not _inflection:
            _dom_itr = _start * (_exp**_itr)
            yield _dom_itr
            _curr_avg = functools.reduce(lambda x, y: x + y,
                                         [_x[1] for _x in scratch_pad]) / len(scratch_pad)
            if timer.exec_time > (_curr_avg * (1 + _deviation / 100)):
                _inflection = True
                break
            else:
                scratch_pad.append((_dom_itr, timer.exec_time))
            _itr += 1

        # Stop checking if we could not find an inflection
        # even after checking upto maximum number of iterations
        if not _inflection:
            return

        _min_itr, _max_itr = scratch_pad[-1][0], _dom_itr
        while _inflection and (_min_itr + _start) <= _max_itr:
            _dom_itr = (_min_itr + _max_itr) // 2
            yield _dom_itr
            if timer.exec_time > (_curr_avg * (1 + _deviation / 100)):
                _max_itr = _dom_itr
            else:
                _min_itr = _dom_itr

    def _relative_cki_cost(self, seq_itr, par_itr):
        # Create a dummy thread-hierarchy object to get the partition
        _gpu_threads = gpu.Vec_CUDA_Domain(None)

        # get the divided thread hierarchy
        _thl = _gpu_threads._partition(([1], [par_itr]))
        _th = (tuple(_thl[0]), tuple(_thl[1]))
        # Create a dummy loop-bound set to calculate parallel cost
        print(" _th = {0}".format(_th))
        calc_Cki_Cg = gpu.GPU_Exec_Cost([seq_itr], (_th, [par_itr]), [])
        return calc_Cki_Cg.parallel_cost(None, 1.0)

    def profile_kern_invocation(self, idom=100):
        self.opts.profile_type = "framework_init_cki_cg"
        _start, _exp, _deviation, _max_iter = self.cki_profile_params

        if not self._alpyna_obj:
            self._alpyna_obj = alpyna.static_analyse(self.profiling_kernels, self.opts)

        # One round just to warm up ---
        arr_a = np.zeros((_start, ), dtype='uint32')
        arr_b = self._alpyna_obj._static_profile_kernel(arr_a, idom)

        arr_a = np.zeros((_start, ), dtype='uint32')
        arr_b = self._alpyna_obj._static_profile_kernel(arr_a, idom)

        # Start with the average value for a kernel calling the minimum number of threads
        _scratch_pad = [(_start, self.opts.timer.exec_time)]

        _itr_val = _start
        for _i,_itr_val in enumerate( self._cki_bounds_generator( \
                                            self.opts.timer, _scratch_pad)):
            arr_a = np.zeros((_itr_val, ), dtype='uint32')
            arr_b = self._alpyna_obj._static_profile_kernel(arr_a, idom)
            #print("==============================+")
            #print("_i = {0}, itr_val = {1}, exec_time = {2}".format(_i, _itr_val, self.opts.timer.exec_time))
            #print("scratch_pad = {0}, avg = {1}, len = {2}".format( _scratch_pad \
            #        , functools.reduce(lambda x,y : x+y, \
            #            [ _x[1] for _x in _scratch_pad])/len(_scratch_pad) \
            #        , len( _scratch_pad)))
            #print("exec-{0} : output = \narr_a = {1}\narr_b = {2}, arr-shape = {3} -- {4}".format(_i,arr_a, arr_b, arr_a.shape, arr_b.shape))

        self.opts.config["gpu"]["Cki_Cg_ratio"] = self._relative_cki_cost(idom, _itr_val)
        print(" lambda-X = {0}".format(self.opts.config["gpu"]["Cki_Cg_ratio"]))

    def profile_interpreter_cost(self, idom=100):
        self.opts.profile_type = "framework_init_ci_cc"
        if not self._alpyna_obj:
            self._alpyna_obj = alpyna.static_analyse(self.profiling_kernels, self.opts)

        # One round just to warm up ---
        _start = 1024 * 128
        self.opts.profile_dev = "interpreter"
        arr_a = np.zeros((_start, ), dtype='uint32')
        arr_b = self._alpyna_obj._cpujit_profile_kernel(arr_a, idom)

        arr_a = np.zeros((_start, ), dtype='uint32')
        arr_b = self._alpyna_obj._cpujit_profile_kernel(arr_a, idom)

        # Temporarily hold the time taken in the interpreter
        _itime = util.Timer()
        _itime.analysis_time = self.opts.timer.analysis_time
        _itime.compile_time = self.opts.timer.compile_time
        _itime.exec_time = self.opts.timer.exec_time
        _itime.xfer_to_dev_time = self.opts.timer.xfer_to_dev_time
        _itime.xfer_to_host_time = self.opts.timer.xfer_to_host_time
        ci_units_per_sec = _start / _itime.exec_time

        # One round just to warm up cpu---
        self.opts.profile_dev = "cpu"
        arr_a = np.zeros((_start, ), dtype='uint32')
        arr_b = self._alpyna_obj._cpujit_profile_kernel(arr_a, idom)

        arr_a = np.zeros((_start, ), dtype='uint32')
        arr_b = self._alpyna_obj._cpujit_profile_kernel(arr_a, idom)

        self.opts.config["cpu"]["Cint_Ccpu_ratio"] = _itime.exec_time / self.opts.timer.exec_time
        #print("==============================+")
        #print("interpreter-time = {0}".format(_itime))
        #print("cpujit-time = {0}".format(self.opts.timer))
        #print("CI_Cc ratio = {0}".format(self.opts.config["cpu"]["Cint_Ccpu_ratio"]))
        return ci_units_per_sec


def _init_profile(cpu, gpu):
    _arch_profile = Architecture_Profile()
    _arch_profile.opts.config = {
        "gpu": {
            "mhz": gpu.freq,
            "ll_cache": gpu.ll_cache_size,
            "Cki_Cg_ratio": None,
            "Cint_Cgpu_ratio": None,
            "bw": None
        },
        "cpu": {
            "mhz": cpu.freq,
            "ll_cache": cpu.ll_cache_size,
            "Cint_Ccpu_ratio": None
        }
    }

    # Start Cki/Cg profile
    _arch_profile.set_cki_profile_bounds(32, 2, 10, 256)
    _arch_profile.profile_kern_invocation(100)

    # Start CI/Ccpu profile
    _ci_per_sec = _arch_profile.profile_interpreter_cost(256)

    # Estimate CI/Cgpu cost
    _json_hndl = _arch_profile.opts.config
    _json_hndl["gpu"]["Cint_Cgpu_ratio"] = \
        _json_hndl["cpu"]["Cint_Ccpu_ratio"] * gpu.freq \
        * gpu.ll_cache_size / ( cpu.freq * cpu.ll_cache_size * gpu.cache_ratio)
    _json_hndl["gpu"]["bw"] = gpu.bw * (2**10 * 2**10) / _ci_per_sec

    print("Profiled constants = Cki/Cg = {0} , CI/Ccpu = {1}, CI/Cgpu = {2}, BW-per-sec = {3}".format( \
            _arch_profile.opts.config["gpu"]["Cki_Cg_ratio"] ,    \
            _arch_profile.opts.config["cpu"]["Cint_Ccpu_ratio"] , \
            _arch_profile.opts.config["gpu"]["Cint_Cgpu_ratio"] , \
            _arch_profile.opts.config["gpu"]["bw"] ))

    _arch_profile.opts.write_config(_json_hndl)


class CPU_Param:
    def __init__(self, freq, llc):
        self.freq = freq
        self.ll_cache_size = llc


class CUDA_GPU_Param:
    def __init__(self, freq, llc, smlvl_llc_share_ratio, xfer_speed):
        self.freq = freq
        self.ll_cache_size = llc
        self.cache_ratio = smlvl_llc_share_ratio
        self.bw = xfer_speed    # bw - MiBps


if __name__ == '__main__':
    #cpu_param = CPU_Param(3900, 8192)
    cpu_param = CPU_Param(800, 8192)
    gpu_param = CUDA_GPU_Param(1500, 1536, 9 * 2, 8192)
    _init_profile(cpu_param, gpu_param)
