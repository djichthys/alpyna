#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import weakref
import ast
import IndexExtract
import astor


class MemAccess(ast.NodeTransformer):
    def __init__(self):
        self.axis = []
        self.name = None
        self.stack = 0
        self.store_index = False
        self.store_array_name = False
        self.finalized = False

    def visit_Name(self, node):
        if self.stack == 0 or self.store_array_name is True:
            self.name = node.id
            self.store_array_name = False
        return node

    def visit_Index(self, node):
        if type(node.value) is ast.Tuple :
            _st_idx = 0
            for _st_idx, _index in enumerate(node.value.elts):
                _elt_node, _dict = IndexExtract.extract_index(_index)
                node.value.elts[_st_idx] = _elt_node
                self.axis[self.stack - _st_idx -1] = _dict
            return node

        node, self.axis[self.stack - 1] = IndexExtract.extract_index(node)
        return node

    def visit_Subscript(self, node):
        if type(node.slice) is ast.Index and type(node.slice.value) is ast.Tuple :
            _incr = len(node.slice.value.elts)
            self.axis += [ {} for i in range( _incr ) ]
        else :
            self.axis.append({})
            _incr = 1

        if type(node.value) is ast.Subscript:
            self.stack += _incr
            node = self.generic_visit(node)
            self.stack -= _incr
        elif type(node.value) is ast.Name:
            self.store_array_name = True
            self.stack += _incr
            node = self.generic_visit(node)
            self.stack -= _incr
        else:
            self.stack += _incr
            node = self.generic_visit(node)
            self.stack -= _incr
        return node

    def canonical_mem_access(self):
        if not self.finalized:
            self.axis.reverse()
        #print(" Canon Mem-access : -< name = {0} ,  axes = {1}  >-".format(self.name,self.axis))
        return self.name, self.axis


class Assign_MemAccess(ast.NodeTransformer):
    def __init__(self, node):
        self.node = node

        self.stack = []
        self.ignore_stack = []
        self.store = 0
        self.depends = []
        self.name = None

    @property
    def node(self):
        return self._node()

    @node.setter
    def node(self, init_node):
        _ref = lambda obj: obj() if callable(obj) else obj
        self._node = weakref.ref(_ref(init_node))

    def generic_visit(self, node):
        self.stack.append(node)
        node = super().generic_visit(node)
        self.stack.pop()
        return node

    def visit_Call(self, node):
        self.ignore_stack.append(node.func)
        node = self.generic_visit(node)
        self.ignore_stack.pop()
        return node

    def visit_Name(self, node):
        # If during recursion, the branch is to be ignored or
        # Only if this node is visited as a visit to a scalar then store
        # if len(self.ignore_stack) > 0 or len(self.store) > 0 :
        self.stack.append(node)
        _ignore_nodes = [ _node for _node in \
                           filter( lambda _nd : True if _nd in self.ignore_stack else False , self.stack )]
        self.stack.pop()
        if len(_ignore_nodes) > 0:
            return node
        if self.store > 0:
            return node

        mem_access = MemAccess()
        node = mem_access.visit(node)
        name, axes = mem_access.canonical_mem_access()
        self.depends.append({"var": name, "axes": axes})
        return node

    def visit_Subscript(self, node):
        _ignore_nodes = [
            _node for _node in filter(lambda _nd: _nd in self.ignore_stack, self.stack)
        ]
        if len(_ignore_nodes) > 0:
            return node

        if self.store == 0:
            self.store += 1
            mem_access = MemAccess()
            node = mem_access.visit(node)
            self.store -= 1
            name, axes = mem_access.canonical_mem_access()
            self.depends.append({"var": name, "axes": axes})

        return node

    def rcopy(self):
        _obj = Assign_MemAccess(self.node)
        _obj.depends = assign_rcopy(self.depends)
        return _obj


def assign_rcopy(varlist):
    rcopy_axis = lambda axis: {_key: axis[_key] for _key in axis.keys()}

    _new_varlist = []
    for _var in varlist:
        _new_varlist += [{
            "var": _var['var'],
            "axes": [rcopy_axis(_axis) for _axis in _var['axes']]
        }]
    return _new_varlist


if __name__ == '__main__':

    class Dummy(ast.NodeTransformer):
        def __init__(self):
            self.p_src = []
            pass

        #def visit_If(self, node) :
        #    _pred = Assign_MemAccess(node)
        #    node.test = _pred.visit(node.test)
        #    self.p_src = _pred.depends
        #    return node

        def visit_Assign(self, node):
            source = Assign_MemAccess(node)
            node.value = source.visit(node.value)
            node = ast.Assign(node.targets, node.value)
            self.p_src = source.depends
            return node

    print("\n")
    #expr = """k = a[32] + b[2 + -(-i) * 3 * -2  + 3 -2][j-2]"""
    #+ b[2 + -(-i) * 3 * -2  + 3 -2][j-2]"""
    #expr = """k = np.abs(np.sqrt(a[i+32][j]), t[i+1] - vec ) + b[2 + -(-i) * 3 * -2  + 3 -2, j-2] / np.power(delta[i+1],echo[j+3])"""
    expr = """k = a[i+1,j+3][p][k+10,l] + newvar"""
    #expr = """k = a + b"""
    orig_ast = ast.parse(expr)
    dummy_obj = Dummy()
    xform_ast = dummy_obj.visit(orig_ast)
    print("original string = -< {0} >-\n{1}".format(expr, ast.dump(orig_ast)))
    print("=================================================")
    print("xform string = -< {0} >-".format(astor.code_gen.to_source(xform_ast)))
    print("=================================================")
    print("sources  -< {0} >-".format(dummy_obj.p_src))
