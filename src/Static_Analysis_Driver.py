#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import ast
import astor
import Normalize_Loop
import Hoist as hst
import Ctrl_Flow as ctrl
import FlattenAST as flat_ast
import Subscript_Partition as sp
import sys
import Dependency_Solver as solve
import Generate_DAG as dag
import Runtime as run
import Utils as util

def static_analyse(mod_txt, opts):
    tree = ast.parse(mod_txt)

    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print(ast.dump(tree,True, True))
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    _ast, exported_syms = Normalize_Loop.normalize_pass(tree)
    _ast_src = astor.code_gen.to_source(_ast)
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("Normalized Code")
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("{0}".format(_ast_src))
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    _ast_src = hst.hoist(_ast_src, exported_syms)
    _ast_src, export_condvars = ctrl.xform( _ast_src )
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("Transformed Code")
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("{0}".format(_ast_src))
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    tree = ast.parse(_ast_src)
    acc = flat_ast.Accumulator()

    canon_ast = acc.visit(tree)
    canon_ast_src = astor.code_gen.to_source(canon_ast)
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("Canonicalized Subscripts")
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("{0}".format(canon_ast_src))
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #acc.display_records()

    # Parse and group paritions in all loops
    #classified_loops = sp.partition(acc)
    classified_functions = sp.partition(acc)

    # Solve dependency for each of these loops
    for _func in classified_functions.functions:
        for _loop_nest in _func.loop_truss:
            solve.dep_test(_loop_nest)

    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    #print("LSE list")
    #for _idx_func, _func in enumerate(classified_functions.functions):
    #    for _idx_loop, _loop_nest in enumerate(_func.loop_truss):
    #        print("======================================")
    #        print(" Function {0} :: Loop list {1} -> ".format(_idx_func, _idx_loop))
    #        xvp, opvp = _loop_nest.variable_pairs
    #        print("Cross - Variable List\n{0}".format(
    #            [(vp.var_a.var_ref['var'], vp.var_b.var_ref['var']) for vp in xvp]))
    #        for _vp in xvp:
    #            _vp.var_dep.display_loops(1)
    #        print("Output - Variable List\n{0}".format(
    #            [(vp.var_a.var_ref['var'], vp.var_b.var_ref['var']) for vp in opvp]))
    #        for _vp in opvp:
    #            _vp.var_dep.display_loops(1)
    dag.aot_graph(classified_functions)
    _anyscale_module = run.runtime_setup(classified_functions, canon_ast, opts )
    return _anyscale_module


if __name__ == '__main__':
    if len(sys.argv) == 1:
        debug()
    else:
        debug(sys.argv[1])
