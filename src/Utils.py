#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.

import ast
import weakref
from timeit import default_timer as time
import os
import json


def display_record(rec):
    _pred_ast = lambda obj: obj.depends if obj else '<Null>'

    def display_for_record(rec):
        print(
            "\n-->> [For] : <level = {0:02d}>, <line_{1:02d}, <type-{2}>, <target={3}>, <limits={4}>, <predicate={5}>".
            format(rec.lvl, rec.lineno, rec.ntype, rec.record["for_targets"].target,
                   rec.record["for_limits"].limits, _pred_ast(rec.predicate_rec)))

    def display_assign_record(rec):
        print(
            "\n-->> [Assign] : <level = {0:02d}>, <line_{1:02d}, <type-{2}>, <targets={3}>, <source={4}>,\n<predicate={5}>".
            format(rec.lvl, rec.lineno, rec.ntype, rec.record["trgt"], rec.record["src"].depends,
                   _pred_ast(rec.predicate_rec)))

    def display_func_def_record(rec):
        stringify = lambda x: x.s if type(x) is ast.Str else x
        print(
            "\n-->> [FunctionDef] : <level = {0:02d}>, <line_{1:02d}, <type-{2}>, <name={3}>, <arguments={4}>,<predicate={5}>".
            format(
                rec.lvl, rec.lineno, rec.ntype, rec.record.name,
                {key: stringify(rec.record.arguments[key])
                 for key in rec.record.arguments.keys()}, _pred_ast(rec.predicate_rec)))

    def display_record_list(rec_list, disp_func_map):
        for item in rec_list:
            display_func = display_func_map.get(item.ntype, None)
            if display_func:
                display_func(item)

    switcher = {"For": "for", "Assign": "assign", "FunctionDef": "func_def"}
    func_list = locals()
    display_func_map = {
        key: func_list.get("display_" + switcher[key] + "_record", None)
        for key in switcher.keys()
    }

    # if a list of flat_ast records are passed in, then
    # directly call the record display list function
    # else create a list with a single item in it and
    # call display record list
    if type(rec) is list:
        display_record_list(rec, display_func_map)
    else:
        display_record_list([rec], display_func_map)


class Record_Parser:
    def __init__(self, list_to_parse, custom_start=None, custom_end=None):
        self.rec_list = list_to_parse

        if custom_start is not None:
            self.start = custom_start
        else:
            self.start = 0

        self.current = self.start
        self.list_len = len(self.rec_list)

        if custom_end is not None:
            self.end = custom_end
        else:
            self.end = self.list_len

    @property
    def rec_list(self):
        return self._rec_list

    @rec_list.setter
    def rec_list(self, set_list_val):
        self._rec_list = set_list_val

    @property
    def current(self):
        return self._current

    @current.setter
    def current(self, val):
        self._current = val

    @property
    def list_len(self):
        return self._list_len

    @list_len.setter
    def list_len(self, val):
        if type(val) is list:
            self._list_len = len(val)
        else:
            self._list_len = val

    @property
    def start(self):
        return self._start_idx

    @start.setter
    def start(self, _arg):
        self._start_idx = 0
        if type(_arg) is int:
            self._start_idx = 0
        else:
            try:
                self._start_idx = self.rec_list.index(_arg)
            except ValueError as ve:
                print("Could not find index for custom_start within list {0}".format(self.rec_list))
                print("exception {0} --- setting start value = 0".format(str(ve)))
                self._start_idx = 0

    @property
    def end(self):
        return self._end_idx

    @end.setter
    def end(self, _arg):
        self._end_idx = self.list_len
        if type(_arg) is int:
            self._end_idx = min(self.list_len, _arg)
        else:
            try:
                self._end_idx = min(self.list_len, self.rec_list.index(_arg) + 1)
            except ValueError as ve:
                print("Could not find index for custom_end within list {0}".format(self.rec_list))
                print("exception {0} --- setting end value = {1} ".format(str(ve), self.list_len))
                self._end_idx = self.list_len

    def look_ahead(self):
        if self.current >= self.end:
            return None
        return self.rec_list[self.current]

    def consume(self):
        retval = None
        if self.current < self.end:
            retval = self.rec_list[self.current]
            self.current += 1

        return retval

    def rewind(self):
        self.current = self.start


class Variable_List:
    def __init__(self, _items, compare_func):
        self.elements = _items
        self.compare_func = compare_func

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, _elements):
        if type(_elements) is list:
            self._elements = _elements
        else:
            self._elements = [_elements]

    def _unique_pairs(self, _dlist, _compare_override=None):
        if len(_dlist.elements) > len(self.elements):
            _larger_set, _smaller_set = _dlist.elements, self.elements
        else:
            _larger_set, _smaller_set = self.elements, _dlist.elements

        if _compare_override != None:
            _compare_func = _compare_override
        else:
            _compare_func = self.compare_func

        _map = []
        if _compare_func == None:
            return _map

        for _item_a in _smaller_set:
            for _item_b in _larger_set:
                if _compare_func(_item_a, _item_b):
                    if not hasattr(_item_a, 'observed') or _item_a.observed == 'partial':
                        _item_a.observed = 'partial'
                    if not hasattr(_item_b, 'observed'):
                        _item_b.observed = 'partial'

                    if _item_a.observed == 'full' or _item_b.observed == 'full':
                        continue
                    _map.append((_item_a, _item_b))
            _item_a.observed = 'full'

        # remove the observed flag, in-case we need to re-use for something else
        for _item_a in _smaller_set:
            if hasattr(_item_a, 'observed'):
                del _item_a.observed
        for _item_b in _larger_set:
            if hasattr(_item_b, 'observed'):
                del _item_b.observed

        return Variable_List(_map, _compare_func)

    # Same as intersection, but ensures uniqueness if
    # the smaller set contains a duplicate
    # Note : compare function SHOULD be provided
    #        either at class instantiation or as override
    def _cross_map(self, _dlist, _compare_override=None):
        if len(_dlist.elements) > len(self.elements):
            _larger_set, _smaller_set = _dlist.elements, self.elements
        else:
            _larger_set, _smaller_set = self.elements, _dlist.elements

        if _compare_override != None:
            _compare_func = _compare_override
        else:
            _compare_func = self.compare_func

        _map = []
        if _compare_func == None:
            return _map
        for _item_a in _smaller_set:
            for _item_b in _larger_set:
                if _compare_func(_item_a, _item_b):
                    if not hasattr(_item_a, 'observed') or _item_a.observed == False:
                        _map.append(_item_a)
                        _item_a.observed = True
                    if not hasattr(_item_b, 'observed') or _item_b.observed == False:
                        _map.append(_item_b)
                        _item_b.observed = True

        # remove the observed flag, in-case we need to re-use for something else
        for _item_a in _smaller_set:
            if hasattr(_item_a, 'observed'):
                del (_item_a.observed)
        for _item_b in _larger_set:
            if hasattr(_item_b, 'observed'):
                del (_item_b.observed)

        return Variable_List(_map, _compare_func)

    def _union(self, set_b, compare_override=None):
        _compare_func = None
        if compare_override != None:
            _compare_func = compare_override
        elif self.compare_func != None:
            _compare_func = self.compare_func

        if len(set_b.elements) > len(self.elements):
            _larger_set, _smaller_set = set_b.elements.copy(), self.elements.copy()
        else:
            _larger_set, _smaller_set = self.elements.copy(), set_b.elements.copy()

        amalgamated = []
        for _item_a in _larger_set:
            if _compare_func is not None:
                for _item_b in _smaller_set:
                    if _compare_func(_item_a, _item_b):
                        _smaller_set.remove(_item_b)
            else:
                if _item_a in _smaller_set:
                    _smaller_set.remove(_item_a)

            amalgamated.append(_item_a)

        # Now that everything in the larger set has been added
        # if there is anything left in the smaller set, it is
        # unique and we can just append these to the end of the list
        for _item_b in _smaller_set:
            amalgamated.append(_item_b)

        return Variable_List(amalgamated, _compare_func)

    # compare function has signature
    # - def func( a , b )
    # - returns true
    def _intersect(self, set_b, compare_override=None):
        if len(set_b.elements) > len(self.elements):
            _larger_set, _smaller_set = set_b.elements, self.elements
        else:
            _larger_set, _smaller_set = self.elements, set_b.elements

        _compare_func = None
        if compare_override != None:
            _compare_func = compare_override
        elif self.compare_func != None:
            _compare_func = self.compare_func

        _intersected = []

        # iterate over smaller set , because we can only have
        # elements from this set in the final set
        for _item_a in _smaller_set:
            if _compare_func is not None:
                for _item_b in _larger_set:
                    if _compare_func(_item_a, _item_b):
                        _intersected.append(_item_a)
            else:
                if _item_a in _larger_set:
                    _intersected.append(_item_a)

        return Variable_List(_intersected, _compare_func)


class CA:
    def __init__(self, val):
        self.val = val


class CA_Container(Variable_List):
    def __init__(self, ca, _comp=None):
        self.ca = ca    # ca is list of type CA
        self.ica = None
        if isinstance(_comp, (list, tuple, int)):
            super().__init__(self.ca, None)
        else:
            super().__init__(self.ca, lambda a, b: True if a.val == b.val else False)

    def cross_map(self, cb):
        self.ica = super()._cross_map(cb)
        return CA_Container(self.ica.elements)

    def union(self, cb):
        self.ica = super()._union(cb)
        return CA_Container(self.ica.elements)

    def intersection(self, cb):
        self.ica = super()._intersect(cb)
        return CA_Container(self.ica.elements)

    def unique_pairs(self, cb):
        self.ica = super()._unique_pairs(cb)
        return CA_Container(self.ica.elements)


if __name__ == '__main__':
    #ml1 = [ CA(i) for i in range(10) ]
    #ml2 = [ CA(i) for i in range(0,10,2) ]
    ml1 = [CA(i) for i in 'dejicejacoj']
    ml2 = [CA(i) for i in 'rubydejicejcob']
    _container1 = CA_Container(ml1)
    _container2 = CA_Container(ml2)
    intersect_result = _container1.cross_map(_container2)
    #u_pairs = _container1.unique_pairs(_container2)

    print("=====================\nList-1")
    for _idx, _it in enumerate(_container1.elements):
        print(" [{0}] --> ca1.val({1} -- id = {2} )".format(_idx, _it.val, id(_it)))

    print("=====================\nList-2")
    for _idx, _it in enumerate(_container2.elements):
        print(" [{0}] --> ca2.val({1} -- id = {2} )".format(_idx, _it.val, id(_it)))

    print("=====================\nCrossMap Result")
    for _idx, _it in enumerate(intersect_result.ca):
        print(" [{0}] --> result.val({1} -- id = {2} )".format(_idx, _it.val, id(_it)))
    u_pairs = _container1.unique_pairs(_container2)
    print("=====================\nU-Pair Result")
    for _idx, _it in enumerate(u_pairs.ca):
        _it1, _it2 = _it
        print(" [{0}] --> result.val({1},{2}) -- id = {3}--{4})".format(
            _idx, _it1.val, _it2.val, id(_it1), id(_it2)))
    print("=====================\nAmalgamate Result")
    _union_result = _container1.union(_container2)
    for _idx, _it in enumerate(_union_result.ca):
        print(" [{0}] --> result.val({1} -- id = {2} )".format(_idx, _it.val, id(_it)))

    print("=====================\nIntersection Result")
    _container3 = CA_Container(ml1, 1)
    _container4 = CA_Container([ml1[it] for it in range(2, len(ml1), 3)], 1)
    _intersect_result = _container3.intersection(_container4)
    for _idx, _it in enumerate(_intersect_result.ca):
        print(" [{0}] --> result.val({1} -- id = {2} )".format(_idx, _it.val, id(_it)))


class Timer:
    def __init__(self):
        self.analysis_start = 0
        self.analysis_time = 0

        self.compile_start = 0
        self.compile_time = 0

        self.exec_start = 0
        self.exec_time = 0

        self.xfer_to_dev_start = 0
        self.xfer_to_dev_time = 0

        self.xfer_to_host_start = 0
        self.xfer_to_host_time = 0

    def __str__(self):
        return "analysis_time = {0}".format(self.analysis_time) +\
               ", compile_time = {0}".format(self.compile_time) +\
               ", exec_time = {0}".format(self.exec_time) +\
               ", xfer_to_dev_time = {0}".format(self.xfer_to_dev_time) +\
               ", xfer_to_host_time = {0}".format(self.xfer_to_host_time)

    def start_analysis(self):
        self.analysis_start = time()

    def end_analysis(self):
        self.analysis_time = time() - self.analysis_start
        return self.analysis_time

    def start_compile(self):
        self.compile_start = time()

    def end_compile(self):
        self.compile_time = time() - self.compile_start
        return self.compile_time

    def start_exec(self):
        self.exec_start = time()

    def end_exec(self):
        self.exec_time = time() - self.exec_start
        return self.exec_time

    def start_xfer_to_dev(self):
        self.xfer_to_dev_start = time()

    def end_xfer_to_dev(self):
        self.xfer_to_dev_time = time() - self.xfer_to_dev_start
        return self.xfer_to_dev_time

    def start_xfer_to_host(self):
        self.xfer_to_host_start = time()

    def end_xfer_to_host(self):
        self.xfer_to_host_time = time() - self.xfer_to_host_start
        return self.xfer_to_host_time

    def total_time(self):
        return self.analysis_time + self.compile_time \
                + self.xfer_to_dev_time + self.xfer_to_host_time \
                + self.exec_time

    def reset(self):
        self.analysis_start = 0
        self.analysis_time = 0
        self.compile_start = 0
        self.compile_time = 0
        self.exec_start = 0
        self.exec_time = 0
        self.xfer_to_dev_start = 0
        self.xfer_to_dev_time = 0
        self.xfer_to_host_start = 0
        self.xfer_to_host_time = 0


# command-line options to be provided or defaulted
class ALPyNA_Options:
    def __init__(self, prof_type=None, prof_loc=None, prof_timer=None, dev=None):
        self.profile_type = prof_type
        self.profile_dev = dev
        self.profile_loc = prof_loc
        self.timer = prof_timer
        self.config = None

    @property
    def profile_type(self):
        return self._profile_type

    @profile_type.setter
    def profile_type(self, prof_opt):
        if prof_opt in ["framework_init_cki_cg", "framework_init_ci_cc", "exec_profile"]:
            self._profile_type = prof_opt
        else:
            self._profile_type = None

    @property
    def profile_dev(self):
        return self._profile_dev

    @profile_dev.setter
    def profile_dev(self, prof_opt):
        if prof_opt in ["interpreter", "cpu", "gpu"]:
            self._profile_dev = prof_opt
        else:
            self._profile_dev = None

    @property
    def profile_loc(self):
        return self._profile_location

    @profile_loc.setter
    def profile_loc(self, loc):
        self._profile_location = os.getcwd() + "/.alpyna_profile.json" if not loc else loc

    @property
    def timer(self):
        return self._timer

    @timer.setter
    def timer(self, utimer):
        self._timer = utimer if utimer else Timer()

    def __str__(self):
        return "profile_type = {0}".format(self.profile_type)  + \
               ", profile_loc = {0}".format(self.profile_loc) + \
               ", timer = {0}".format(self.timer)

    def write_config(self, config):
        with open(self.profile_loc, "w") as fd:
            json.dump(config, fd)

    def read_config(self, flush=False):
        if flush or not self.config:
            self.config = None
            with open(self.profile_loc, "r") as fd:
                self.config = json.load(fd)
                return self.config
        assert self.config, "Error reading ALPyNA profile {0}. Use Static_Profile_Setup.py".format(
            self.opts.profile_loc)
        return self.config
