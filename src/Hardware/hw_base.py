#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import re
import functools

Numpy_Supported_types = [
    np.bool, np.byte, np.ubyte, np.short, np.ushort, np.intc, np.uintc, np.int_, np.uint,
    np.longlong, np.ulonglong, np.half, np.float16, np.single, np.double, np.longdouble, np.csingle,
    np.cdouble, np.clongdouble
]

Native_types = [ bool, int, float, complex ]
Native_Numpy_Map = {
 bool    : np.bool    , 
 int     : np.int_    , 
 float   : np.single  , 
 complex : np.csingle
}

def type_basename( ty_list , append_dict=None):
	if append_dict == None : 
		_tmap = {} 
	else : 
		_tmap = append_dict

	_check = re.compile('(<class \'numpy.)([a-z]+[0-9]*)\'>')
	for _ty in ty_list :
		_mobj = _check.match(str(_ty))
		if _mobj :
			_tmap[ _ty ] = _mobj.group(2)
			continue 

		if _ty in Native_types :
			_native_map_obj = _check.match( str(Native_Numpy_Map[_ty]) )
			if _native_map_obj :
				_tmap[ _ty ] = _native_map_obj.group(2)
			else : 
				_tmap[np.bool] = 'bool'
	return _tmap 

HW_supported_types = type_basename( Numpy_Supported_types )
HW_supported_types = type_basename( Native_types, HW_supported_types )
HW_supported_typesizes = { HW_supported_types[key] : key(0).nbytes  for key in Numpy_Supported_types[1:]}


class SCEV_Ref:
    def __init__(self):
        self.scev_array = []    ## list of lists
        self.type_map = {}
        self.num_types = 0

    def reset(self):
        self.scev_array.clear()
        self.type_map.clear()
        self.num_types = 0

    @property
    def scev_array(self):
        return self._scev_arr

    @scev_array.setter
    def scev_array(self, arg):
        self._scev_arr = arg

    @property
    def type_map(self):
        return self._tmap

    @type_map.setter
    def type_map(self, arg):
        self._tmap = arg

    def insert_scev_ref(self, scev_ref):
        try:
            _arr_idx = self.type_map[scev_ref.dtype]
            self.scev_array[_arr_idx].append(scev_ref)
        except KeyError as _dict_miss:
            self.type_map[scev_ref.dtype] = self.num_types
            self.num_types += 1
            self.scev_array += [[scev_ref]]

    def locate_var(self, var_arg):
        _scev_arr_idx = self.type_map[var_arg.dtype]
        for _arg_idx, arg in enumerate(self.scev_array[_scev_arr_idx]):
            if var_arg.name == arg.name:
                break
        else:
            _arg_idx = None
        return (_scev_arr_idx, _arg_idx)

    def display(self):
        for _ty in self.type_map.keys():
            print("Type = {0} ..... idx = {1}".format(_ty, self.type_map[_ty]))
            for arg in self.scev_array[self.type_map[_ty]]:
                print("arg :: name = {0} , dim-str = <{1}> , dtype = {2}, index = {3}".format(
                    arg.name, arg.dim_str, arg.dtype , self.scev_array[self.type_map[_ty]].index(arg)))




class Host_Base_Txfr:
	def __init__(self, func_name , vec_list , scev_list, dynarg_list):
		self.vfunc_name = func_name
		self.vec_list = vec_list 
		self.scev_list = scev_list 
		self.dynarg_list = dynarg_list


	@property
	def vfunc_name(self):
		return self._vfunc_name
		
	@vfunc_name.setter
	def vfunc_name(self, name):
		self._vfunc_name = name

	@property 
	def vec_list(self):
		return self._vec_list

	@vec_list.setter
	def vec_list(self, args):
		self._vec_list = args

	@property 
	def scev_list(self):
		return self._scev_list

	@scev_list.setter
	def scev_list(self, args):
		self._scev_list = args


	@property 
	def dynarg_list(self):
		return self._dynarg_list

	@dynarg_list.setter
	def dynarg_list(self, args):
		self._dynarg_list = args


class Execution_Cost:
	def __init__(self, seq_loops):
		self.seq_loop_dom = seq_loops

	@property 
	def seq_loop_dom(self):
		return self._seq_loop_dom

	@seq_loop_dom.setter
	def seq_loop_dom(self, seq_loops):
		self._seq_loop_dom = seq_loops

	def seq_cost(self):
		pass

	def _extract_bounds(self,loop):
		_deref = lambda x : x() if callable(x) else x
		_deref(loop)


def data_xfer_cost(xfer_cost, hw_prof_const, xfer_container):
    # for now only consider numpy ndarrays -- expand later
    _arr_size = lambda _arr: _arr.nbytes if type(_arr) is np.ndarray else 0
    _xfer_cost_norm = {}
    for _dev in xfer_cost.keys():
        _norm_k = 1
        if _dev in hw_prof_const and "bw" in hw_prof_const[_dev]:
            _norm_k = hw_prof_const[_dev]["bw"]
        _xfer_cost_norm[_dev] = functools.reduce(lambda x, y: x + y, [0] + [
            _arr_size(getattr(xfer_container, _arg.arg_name(False), None))
            for _arg in xfer_cost[_dev]
        ])
        _xfer_cost_norm[_dev] /= _norm_k

    return _xfer_cost_norm
