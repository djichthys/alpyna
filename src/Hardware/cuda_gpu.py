#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import weakref
import astor
import numpy as np
import functools 

import Generate_DAG as dag
import Dependency_Solver as solve
from Hardware import hw_base as hwb


min_tpb = 32
max_tpb = 1024
max_hw_dimensions = 3
hw_dim_axes = [ 'x' , 'y' , 'z' ]

# Hardware description of nVidia Cards 
# map with key = model. value = map of keys
# SM (streaming multiprocessors)
# Freq (frequency in MegaHertz)
gpu_model = 'gtx-1060'
hw_parm = { 
'gtx-1060': { 
              "sm"  : 9 ,
              "ws"  : 32 ,
              "wsched" : 4
            },
'titan-xp': { 
              "sm"  : 30 ,
              "ws"  : 32 ,
              "wsched" : 4
            }
}



class Vec_CUDA_GPU:
	def __init__(self, graph_node , parallel_dims ):
		self.graph_node = graph_node
		self.parallel_dims = parallel_dims
		self.func_args = self._init_kernel_args()
		self.raw_kernel = None

		self.scevref_kernel_core = None
		self.scevref_args =  None
		self.scevref_kernel = False

		self.raw_drv = None
		self.generate_phase = solve.phase.get_curr_stg_sym()
		self.runtime_kernel = None
		self.runtime_scevref_kernel_args = None

	@property
	def graph_node(self):
		return self._graph_node()

	@graph_node.setter
	def graph_node(self, node):
		self._graph_node = weakref.ref(node)

	@property
	def func_args(self):
		return (self._arg_dict , self._arg_list)

	@func_args.setter
	def func_args(self, args ):
		if type(args) is tuple :
			_first, _second = args
			if type(_first) is dict :
				self._arg_dict = _first
			elif type(_second) is dict :
				self._arg_dict = _second

			if type(_first) is list :
				self._arg_list = _first
			elif type(_second) is list :
				self._arg_list = _second
	@property
	def scevref_kernel_core(self):
		return self._scevref_kernel_core

	@scevref_kernel_core.setter
	def scevref_kernel_core(self,kern_str):
		self._scevref_kernel_core = kern_str


	@property
	def scevref_args(self):
		return self._scevref_args

	@scevref_args.setter
	def scevref_args(self,arg):
		if type(arg) is list :
			self._scevref_args = arg 
		elif arg == None :
			self._scevref_args = [] 
		else : 
			self._scevref_args = [ arg ] 

	@property
	def raw_kernel(self):
		return self._raw_kernel

	@raw_kernel.setter
	def raw_kernel(self,kern_str):
		self._raw_kernel = kern_str

	@property
	def raw_drv(self):
		return self._raw_drv

	@raw_drv.setter
	def raw_drv(self,drv):
		self._raw_drv = drv

	@property
	def runtime_scevref_kernel_args(self):
		return self._runtime_scevref_kernel_args

	@runtime_scevref_kernel_args.setter
	def runtime_scevref_kernel_args(self, scev_arg):
		if type(scev_arg) is list :
			self._runtime_scevref_kernel_args = scev_arg
		elif scev_arg == None : 
			self._runtime_scevref_kernel_args = []
		else : 
			self._runtime_scevref_kernel_args = [ scev_arg ]
			

	def _resolve_lims(self, lim, runtime_args=None):
		# This function is a place-holder for when I need to query the loop limit end
		# at run-time
		# for the moment assert
		if solve.phase.get_curr_stg_sym() == 'runtime':
			if lim[0] == 'lazy': 
				return getattr(runtime_args, lim[1], None)
			else :
				return lim[1]
		else : 
			if lim[0] == 'imm': 
				return lim[1]
		assert False , "Run-time for-loop end limit - cannot create kernel"

	def _vec_stmt(self, indent):
		_pred = self.graph_node.payload.assign_stmt.predicate_ast
		_obj = self.graph_node.payload.assign_stmt.node
		if _pred :
			_code = [ "{0}if {1}:".format(indent, astor.codegen.to_source(_pred).rstrip("\n")) ]
			_code.append("{0}  {1}".format(indent, astor.codegen.to_source(_obj).rstrip("\n")))
		else :
			_code = [ "{0}{1}".format(indent, astor.codegen.to_source(_obj).rstrip("\n")) ]
			
		return _code


	def _gen_kern_str(self, stmt_lst):
		_code_txt = ""
		for cu_stmt in stmt_lst :
			_code_txt += cu_stmt + "\n"
		return _code_txt


	# Hoist to base class
	def _check_scalar_ref(self,scev_ref):
		if len(scev_ref) > 0 :
			# search for targets within scev_ref
			_assign_trgts = self.graph_node.payload.assign_stmt.record["trgt"]
			for _arg in _assign_trgts :
				for _scev in scev_ref :
					if _arg['var'] == _scev.name :
						self.generate_phase = solve.phase.get_next_stg_sym()
						self.scevref_kernel = True
						self.scevref_args += [ _scev ] 
		return  # Break out of all loops



	#lvl is not used in GPU kernel code, but kept
	#because interface is similar to other devices
	def gen_kernel(self, scev_ref, lvl=1):
		assert solve.phase.get_curr_stg_sym() == "aot", "exception at stage {0}".format(solve.phase.get_curr_stg_sym() )
		self._check_scalar_ref(scev_ref)
		if self.scevref_kernel == True :
			self.scevref_kernel_core = self._gen_kern_str(self._scevref_kernel_body_core())
		else :
			self.raw_kernel = self._gen_kern_str(self._cuda_kernel_hdr() + self._cuda_kernel_body())


	#lvl is not used in GPU kernel code, but kept
	#because interface is similar to other devices
	def runtime_gen_kernel(self, scev_ref, runtime_args, lvl=1):
		self._check_scalar_ref(scev_ref)
		if self.scevref_kernel == True :
			self.scevref_kernel_core = self._finalise_scevkernel( \
										self._scevref_kernel_body_core())
		else :
			self.raw_kernel = self._gen_kern_str(self._cuda_kernel_hdr() \
								 + self._cuda_kernel_body(runtime_args ))

	def _resolve_func_arg_type(self, var_bind):
		#check if it is a vector
		if len(var_bind["axes"]) > 0 :
			return "vec"

		_rec = self.graph_node.payload.assign_stmt.record
		#check if scalar is in the targets
		if var_bind["var"] in [ _var['var'] for _var in _rec["trgt"]]:
			return "scev_trgt"
		else :
			return "scev_src"



	def _init_kernel_args(self):
		dim_str = lambda ty,dim_list : (ty,"") if len(dim_list) <= 0 else (ty, "[" + ":," * (len(dim_list)-1) + ":]")
		_id_serial_loops = lambda orig_loops , num_par_dims : orig_loops[ : len(orig_loops) - num_par_dims ]
		_id_par_loops = lambda orig_loops , num_par_dims : orig_loops[ len(orig_loops) - num_par_dims : ]
		_ref = lambda obj : obj() if callable(obj) else obj

		_arg_dict = {}  ## map of argument index within list
		_arg_list = []
		_rec = self.graph_node.payload.assign_stmt.record
		#print("source arguments = {0}".format( _rec["src"].depends ))
		#print("target arguments = {0}".format(  _rec["trgt"] ))


		# Add all unique array names within the sources of each assign statement
		# except those found as loop index variables. Index variables
		# will be added later as iterator value of sequential loop -OR-
		# they will be defined internally as a function of thread-id
		_idx_var_excl = { _trgt \
							for _loop_var in self.graph_node.payload.enclosing_loops \
								for _trgt in _ref(_loop_var).record["for_targets"].target }
		for _var in _rec["src"].depends :
			# if indices are read directly in assignment statement - skip
			if _var["var"] in _idx_var_excl :
				continue

			if _var["var"] not in _arg_dict.keys():
				_ty , _dim_str = dim_str("_EvalType", _var["axes"])
				_idx = len(_arg_list)
				_arg_list.append(dag.Vec_Function_Argument(_var["var"] \
												, _dim_str, _ty \
												, self._resolve_func_arg_type(_var)))
				_arg_dict[_var["var"]] = _idx

		# Add all unique array names within the predicates of each assign statement
		for _var in _rec["pred"]:
			if _var["var"] not in _arg_dict.keys():
				_ty , _dim_str = dim_str("_EvalType", _var["axes"])
				_idx = len(_arg_list)
				_arg_list.append(dag.Vec_Function_Argument(_var["var"] \
												, _dim_str, _ty \
												, self._resolve_func_arg_type(_var)))
				_arg_dict[_var["var"]] = _idx


		# Add all unique array names within the target of each assign statement
		for _var in _rec["trgt"]:
			if _var["var"] not in _arg_dict.keys():
				_ty , _dim_str = dim_str("_EvalType", _var["axes"])
				_idx = len(_arg_list)
				_arg_list.append(dag.Vec_Function_Argument(_var["var"] \
												, _dim_str, _ty \
												, self._resolve_func_arg_type(_var)))
				_arg_dict[_var["var"]] = _idx


		# Add all unique constants within subscripts to the array 
		_loop_idx_excl = [ _trgt \
							for _loop_var in self.graph_node.payload.enclosing_loops \
								for _trgt in _ref(_loop_var).record["for_targets"].target ] + ['__Constant__']
		for _var in _rec["src"].depends + _rec["pred"] + _rec["trgt"]:
			for _subscript in _var["axes"]:
				for _idx_var in _subscript.keys():
					if _idx_var not in _loop_idx_excl and _idx_var not in _arg_dict.keys():
						_ty , _dim_str = dim_str("_EvalType", [])
						_idx = len(_arg_list)
						_arg_list.append(dag.Vec_Function_Argument(_idx_var , _dim_str, _ty \
														, "constant"))
						_arg_dict[_idx_var] = _idx

		# Add the iteration value of loops that are executed sequentially within loop-nest
		# so that we can calculate memory array offsets correctly
		_serial_loops =  _id_serial_loops( self.graph_node.payload.enclosing_loops, self.parallel_dims )
		for _loop_var in _serial_loops :
			for _trgt in _ref(_loop_var).record["for_targets"].target :
				if _trgt not in _arg_dict.keys():
					_ty , _dim_str = dim_str("int32", [])
					_idx = len(_arg_list)
					_arg_list.append( dag.Vec_Function_Argument(_trgt, _dim_str, _ty, \
																("iterator", _loop_var)) )
					_arg_dict[_trgt] = _idx


		# To counter iteration domain thread-pool padding, we need to pass in
		# actual maximums of iteration domains for all loops run in parallel
		_par_loops = _id_par_loops( self.graph_node.payload.enclosing_loops, self.parallel_dims)
		for _loop_var in _par_loops :
			for _trgt in _ref(_loop_var).record["for_targets"].target :
				_iterdom_max = _trgt + "_iterdom_max"
				if _iterdom_max not in _arg_dict.keys():
					_ty , _dim_str = dim_str("int32", [])
					_idx = len(_arg_list)
					_arg_list.append( dag.Vec_Function_Argument(_iterdom_max, \
																_dim_str, _ty,\
																( "iterdom_max", _loop_var)))
					_arg_dict[_iterdom_max] = _idx

		return (_arg_dict , _arg_list)


	# This function should only ever be called at runtime whether the kernel
	# is generated ahead of time or at runtime
	def patch_argtypes(self, scevref_map , runtime_args):
		_resolve_type = lambda _arr : "{0}".format(_arr.dtype) if type(_arr) is np.ndarray else hwb.HW_supported_types[type(_var)]

		_arg_dict , _arg_list = self.func_args
		# runtime patch the type of argument into the argument-container
		# scev-ref kernel and their types have already been patched when
		# the runtime kernel was generated
		for _arg in _arg_list :
			_vt, _rec = _arg.vtype
			if _vt in [ "iterator", "iterdom_max" ]:
				continue

			_var = getattr( runtime_args, _arg.arg_name(False), None)
			_arg.dtype = _resolve_type( _var )

		## Append scev-refs with types
		_combined_args = [ _arg for _arg in _arg_list if _arg.vtype[0] != "scev_trgt" ]
		_combined_args += [ _arg for _arg in self.runtime_scevref_kernel_args ]

		num_args = len(_combined_args)
		_decorator = "@cuda.jit"
		if num_args :
			_decorator += "('"

		for _a_idx, _arg in enumerate(_combined_args):
			_decorator += _arg.dtype + _arg.dim_str
			if _a_idx < (num_args-1):
				_decorator += ","
		else :
			if num_args :
				_decorator += "')"

		self.runtime_kernel = _decorator + "\n" + self.raw_kernel
		return [ _decorator ]


	def _cuda_arg_str(self, caller):
		_arg_dict , _arg_list = self.func_args
		num_args = len(_arg_list)

		_arg_str = ""
		for _a_idx, _arg in enumerate(_arg_list):
			_vt, _rec = _arg.vtype 
			if _vt == "scev_trgt" : 
				continue

			_arg_str +=  _arg.arg_name(caller)
			if _a_idx < (num_args-1):
				_arg_str += ", "
		return _arg_str
		
	def _cuda_kernel_hdr(self):
		_arg_dict , _arg_list = self.func_args
		num_args = len(_arg_list)
		_kernel_hdr =  "def _gpu_{0}({1}):".format(self.graph_node.name,self._cuda_arg_str(False))

		return [ _kernel_hdr ]



	# The only reason this gets called is because we have a
	# write to a scalar value within the kernel
	# Separate function for this special case
	def _scevref_kernel_body_core(self):
		_cuda_dim_strings = [ "  tid_x = cuda.threadIdx.x"]
		_dom_pad_guard = [ "tid_x > 0" ]

		return _cuda_dim_strings \
				+ self._gen_iterdom_guard(_dom_pad_guard) + ["  "] \
				+ self._vec_stmt("  ")



	def _gen_iterdom_guard(self, guard_list):
		if len(guard_list) == 0 :
			return []

		_idx = None
		_guard_str = "  if"
		for _idx , _guard in enumerate(guard_list[:-1]) :
			_guard_str += " " + _guard + " or"
		else :
			_guard_str += " " + guard_list[-1] + " :"
		return [ _guard_str, "    return" ]


	def _cuda_kernel_single_thread(self):
		_cuda_dim_strings = [ "  tid_x = cuda.threadIdx.x"]

		_dom_pad_guard =    [ "  if tid_x > 0:" ]
		_dom_pad_guard +=   [ "    return" ]

		return _cuda_dim_strings \
				+ _dom_pad_guard + ["  "] \
				+ self._vec_stmt("  ")

	def _cuda_kernel_body(self, runtime_args=None):
		# Get list of loops enclosing this statement that can be parallelized
		# as a slice - i.e. new array. This is so that we can sort the lists
		# to choose which ones we should use to parallelize on the GPU
		_id_par_loops = lambda orig_loops , num_par_dims : orig_loops[ len(orig_loops) - num_par_dims : ]
		_par_loops = _id_par_loops( self.graph_node.payload.enclosing_loops, self.parallel_dims)

		#_iter_end = lambda _loop_end : _loop_end[1] if _loop_end[0] == 'imm' else self.place_holder(runtime_args)
		_iter_end = lambda _loop_end : self._resolve_lims(_loop_end, runtime_args)
		_ref = lambda obj : obj() if callable(obj) else obj
		_par_loops.sort(key= lambda obj : _iter_end(_ref(obj).record["for_limits"].limits["end"]), reverse=True)

		# If we have no loops running in parallel, then generate a kernel with
		# minimum threads to run statement in series
		if len(_par_loops) == 0 :
			return self._cuda_kernel_single_thread()

		# Start with defining the loops we will use for hardware parallelization
		_cuda_dim_strings = []
		_cuda_loop_itr_calc = []
		_cuda_serial_loops = []
		for _dim , (_axis , _hw_tid_axis) in enumerate(zip( hw_dim_axes , _par_loops)) :
			 _cuda_dim_strings += [ "  bsize_{0} = cuda.blockDim.{0}".format(_axis)]
			 _cuda_dim_strings += [ "  bid_{0} = cuda.blockIdx.{0}".format(_axis)]
			 _cuda_dim_strings += [ "  tid_{0} = cuda.threadIdx.{0}".format(_axis)]


		# Calculate thread-id for all the axes that are parallel
		# Also generate guard condition against parallel domain iteration space padding
		_dom_pad_guard = []
		_dim = None
		for _dim , (_axis , _hw_tid_axis) in enumerate(zip( hw_dim_axes , _par_loops)) :
			_loop_trgt_var = _ref(_hw_tid_axis).record["for_targets"].target[0]
			_cuda_loop_itr_calc += [ "  {0} = bid_{1} * bsize_{1} + tid_{1}".format(_loop_trgt_var, _axis)]
			_dom_pad_guard += [ "{0} >= {0}_iterdom_max".format(_loop_trgt_var) ]
		else :
			if _dim is not None and (_dim+1) < len(_par_loops) :
				for _nest , _for_stmt in enumerate( _par_loops[_dim+1:]):
					_trgt = astor.code_gen.to_source(_ref(_for_stmt).record["for_targets"].node)
					if _ref(_for_stmt).record["for_limits"].limits["end"][0] == 'lazy' :
						_lim  = "range(0, {0}_iterdom_max, 1)".format(_trgt.rstrip("\n "))
					else : 
						_lim  = astor.code_gen.to_source(_ref(_for_stmt).record["for_limits"].node)
					_cuda_serial_loops += [ "  {0}for {1} in {2}:".format(_nest*2*" ", _trgt.rstrip("\n "), _lim.rstrip("\n ")) ]

		return _cuda_dim_strings + ["  "] \
				+ _cuda_loop_itr_calc + ["  "] \
				+ self._gen_iterdom_guard(_dom_pad_guard) + ["  "] \
				+ _cuda_serial_loops \
				+ self._vec_stmt("  {0}".format(2 * len(_cuda_serial_loops) * " "))



	def _aot_cuda_kernel_driver(self, lvl):
		_dep_carrying_loops = lambda orig_loops , num_par_dims : orig_loops[ 0 : len(orig_loops) - num_par_dims ]
		_id_par_loops = lambda orig_loops , num_par_dims : orig_loops[ len(orig_loops) - num_par_dims : ]
		_iter_end = lambda _loop_end : self._resolve_lims(_loop_end)
		_ref = lambda obj : obj() if callable(obj) else obj

		# map which loops are to be executed in parallel 
		# sort them for maximum parallelisation 
		_par_loops = _id_par_loops( self.graph_node.payload.enclosing_loops, self.parallel_dims)
		_par_loops.sort(key= lambda obj : _iter_end(_ref(obj).record["for_limits"].limits["end"]), reverse=True)

		# Map parallelisable loops onto GPU hardware thread axes
		# and partition thread hierarchy according to device characteristics
		_hw_par_axes = [ _ref(_hw_tid_axis) for (_axis , _hw_tid_axis) in zip( hw_dim_axes , _par_loops) ]
		_iter_dom = Vec_CUDA_Domain( [ _hw_tid_axis for (_axis , _hw_tid_axis) in zip( hw_dim_axes , _par_loops) ] )
		_thread_partition = _iter_dom.partition()

		# Map outer loops carrying dependence for this statement and
		# loops which have to be executed sequentially within a kernel
		# due to lack of parallel hardware axes
		_seq_loop_dom = [ _iter_end( _ref(obj).record["for_limits"].limits["end"] ) \
			for obj in _dep_carrying_loops( self.graph_node.payload.enclosing_loops, self.parallel_dims) ]

		_kernel_loops =  [ _iter_end( _ref(_loop).record["for_limits"].limits["end"] ) \
								for _loop in _par_loops if _ref(_loop) not in _hw_par_axes ]
		_gpu_cost = GPU_Exec_Cost( _seq_loop_dom , \
						( _thread_partition, \
							[ _iter_end(_ref(_loop).record["for_limits"].limits["end"]) for _loop in _hw_par_axes] \
						), \
						_kernel_loops)
		return _gpu_cost
			

	def gen_drv(self, scev_ref, lvl=1):
		_thread_partition = self._aot_cuda_kernel_driver(lvl)
		self.raw_drv =  "{0}[{1}, {2}, stream]({3})".format("_gpu_" + self.graph_node.name \
							, _thread_partition.hw_par_threads[0] \
							, _thread_partition.hw_par_threads[1] \
							, self._cuda_arg_str(True))
		return _thread_partition



	def _runtime_cuda_kernel_driver(self, runtime_args, lvl):
		_dep_carrying_loops = lambda orig_loops , num_par_dims : orig_loops[ 0 : len(orig_loops) - num_par_dims ]
		_id_par_loops = lambda orig_loops , num_par_dims : orig_loops[ len(orig_loops) - num_par_dims : ]
		_iter_end = lambda _loop_end : self._resolve_lims(_loop_end, runtime_args)
		_ref = lambda obj : obj() if callable(obj) else obj

		# map which loops are to be executed in parallel 
		# sort them for maximum parallelisation 
		_par_loops = _id_par_loops( self.graph_node.payload.enclosing_loops, self.parallel_dims)
		_par_loops.sort(key= lambda obj : _iter_end(_ref(obj).record["for_limits"].limits["end"]), reverse=True)

		# Map parallelisable loops onto GPU hardware thread axes
		# and partition thread hierarchy according to device characteristics
		_hw_par_axes = [ _ref(_hw_tid_axis) for (_axis , _hw_tid_axis) in zip( hw_dim_axes , _par_loops) ]
		_iter_dom = Vec_CUDA_Domain( _hw_par_axes , runtime_args )
		_thread_partition = _iter_dom.partition()


		# Map outer loops carrying dependence for this statement and
		# loops which have to be executed sequentially within a kernel
		# due to lack of parallel hardware axes
		_seq_loop_dom = [ _iter_end( _ref(obj).record["for_limits"].limits["end"] ) \
			for obj in _dep_carrying_loops( self.graph_node.payload.enclosing_loops, self.parallel_dims) ]

		_kernel_loops =  [ _iter_end( _ref(_loop).record["for_limits"].limits["end"] ) \
								for _loop in _par_loops if _ref(_loop) not in _hw_par_axes ]
		_gpu_cost = GPU_Exec_Cost( _seq_loop_dom , \
						( _thread_partition, \
							[ _iter_end(_ref(_loop).record["for_limits"].limits["end"]) for _loop in _hw_par_axes] \
						), \
						_kernel_loops)
		return _gpu_cost

	def runtime_gen_drv(self, scevref_map, runtime_args, lvl=1):
		_scevref_arg_hdr = []
		for _arg in self.scevref_args :
			_scevarr_idx , _arg_idx = scevref_map.locate_var(_arg)
			_scevref_arr = "_scevref_arr_{0}".format(_arg.dtype)
			if _scevref_arr not in _scevref_arg_hdr :
				_scevref_arg_hdr += [_scevref_arr]

		_args = self._cuda_arg_str(True)
		for _arg in _scevref_arg_hdr:
			_args += ", _cvec{0}".format(_arg)

		_thread_partition = self._runtime_cuda_kernel_driver(runtime_args, lvl)
		self.raw_drv =  "{0}[{1}, {2}, stream]({3})".format("_gpu_" + self.graph_node.name , str(_thread_partition.hw_par_threads[0]), str(_thread_partition.hw_par_threads[1]) , _args )
		return _thread_partition


	# Although the core of the scevref kernel can be created at compile time
	# We have to patch the kernel core at runtime with scevref-arguments	
	def _patch_scevref_kernel_body(self, scevref_map ):
		_k_xfer_in = []
		_k_xfer_out = []
		_scevref_arg_hdr = [] 
		
		for _arg in self.scevref_args :
			_scevarr_idx , _arg_idx = scevref_map.locate_var(_arg)
			_scevref_arr = "scevref_arr_{0}".format(_arg.dtype)
			_k_xfer_in +=  [ "  {0} = {1}[{2}]".format( _arg.arg_name(False), _scevref_arr, _arg_idx)]
			_k_xfer_out += [ "  {0}[{1}] = {2}".format( _scevref_arr, _arg_idx, _arg.arg_name(False) )]
			if _scevref_arr not in _scevref_arg_hdr :
				_scevref_arg_hdr += [_scevref_arr]
				self.runtime_scevref_kernel_args.append( dag.Vec_Function_Argument( _scevref_arr
												, "[:]", _arg.dtype , "scev_array"))

		_args = self._cuda_arg_str(False)
		for _arg in _scevref_arg_hdr: 
			_args += ", {0}".format(_arg)


		return  [ "def _gpu_{0}({1}):".format(self.graph_node.name , _args) ] \
				+ _k_xfer_in \
				+ [ self.scevref_kernel_core ] \
				+ _k_xfer_out

	def patch_scevref_kernel(self, scevref_map, runtime_args ):
		self.raw_kernel = self._gen_kern_str( self._patch_scevref_kernel_body(scevref_map))

class Vec_CUDA_Domain :
	def __init__(self, loops, runtime_args=None ):
		# utility functions
		#self._iter_end = lambda _loop_end : _loop_end[1] if _loop_end[0] == 'imm' else self.place_holder()
		self._iter_end = lambda _loop_end : self._resolve_lims(_loop_end, runtime_args)
		self._ref = lambda obj : obj() if callable(obj) else obj
		self._dom_max = lambda obj : self._iter_end(self._ref(obj).record["for_limits"].limits["end"])

		self.loops = loops

	@property
	def loops(self):
		return self._loops

	@loops.setter
	def loops(self, loop_list):
		if type(loop_list) is list :
			if loop_list :
				self._loops = loop_list
			else :
				self._loops = []
		elif loop_list is None :
			self._loop_list = []
		else :
			self._loop_list = [loop_list]


	def _resolve_lims(self, lim, runtime_args=None):
		# This function is a place-holder for when I need to query the loop limit end
		# at run-time
		# for the moment assert
		if solve.phase.get_curr_stg_sym() == 'runtime':
			if lim[0] == 'lazy': 
				return getattr(runtime_args, lim[1], None)
			else :
				return lim[1]
		else : 
			if lim[0] == 'imm': 
				return lim[1]
		assert False , "Run-time for-loop end limit - cannot create kernel"

	# creates a tuple-of-lists for all the dimensions in the loop
	# initialises this to be 1 block-per-grid and iterdom_max for threads-per-grid
	# this will have to be pruned using _parition to fit the thread-domain
	# size constraints
	def _init_grid(self, loops ):
		# Condition where no parallelisation possible
		if len(loops) == 0 :
			return ([1],[min_tpb])
			
		_bpg = [ 1 for _loop in loops ]
		_tpb = [ self._dom_max(_loop) for _loop in loops ]
		_idx, _max_th = self._max_threads(_bpg, _tpb)
		#self._pad_gpu_threads(_bpg, _tpb, _idx , min_tpb)
		self._pad_gpu_threads(_bpg, _tpb, _idx , 2)
		return (_bpg , _tpb)

	# Pad GPU thread pool size up to a factor specified
	# For CUDA this is usually 32 viz min_tpb
	# but we use other values if no axis is greater than
	# the min_tpb
	def _pad_gpu_threads(self, bpg, tpb, idx, pad):
		_mult = (tpb[idx] + pad - 1) // pad
		tpb[idx] = _mult * pad


	# split the gpu thread numbers along a certain axis
	# so as to reduce the threads within a block and increment
	# the number of blocks
	def _split_dom(self, bpg, tpb, idx, pad):
		self._pad_gpu_threads(bpg,tpb, idx , pad)
		bpg[idx] = bpg[idx] * 2
		tpb[idx] = tpb[idx] // 2
		

	
	# Find axis with the minimum number of threads	
	# skip_min flag is used to indicate if we should
	# skip any value which is less than the
	# minimum-threads-per-block (min_tpb)
	def _min_threads(self, bpg, tpb, skip_min=False):
		_min_idx = 0
		_min_val = tpb[_min_idx]
		for _idx, _axis in enumerate(tpb):
			_curr = tpb_idx
			
			# pick the minimum thread value
			# with constraint greater than minimum threads
			if skip_min and _curr < min_tpb :
				continue

			if _curr < _min_val :
				_min_idx, _min_val = _idx , _curr
		return (_min_idx, _min_val)


	# Find axis with the maximum number of threads
	def _max_threads(self, bpg, tpb):
		_max_idx = 0
		_max_val = tpb[_max_idx]
		for _idx, _axis in enumerate(tpb):
			if tpb[_idx] > _max_val :
				_max_idx, _max_val = _idx, tpb[_idx]
		return (_max_idx, _max_val)


	def _blk_size(self, bpg, tpb):
		_tsize = 1
		for _dmax in tpb :
			_tsize *= _dmax
		return _tsize


		
	def _partition(self, iterdom):
		_bpg, _tpb = iterdom
		#print("bpg = {0}, tpb = {1}".format(_bpg,_tpb))
		while self._blk_size(_bpg, _tpb) > max_tpb :
			_idx, _dom_end = self._max_threads( _bpg, _tpb )
			self._split_dom( _bpg, _tpb, _idx , 2 )
			#print("bpg = {0}, tpb = {1}".format(_bpg,_tpb))
		return iterdom
	
		
	def partition(self, synchronize=False):
		_bpg, _tpb  = self._partition( self._init_grid(self.loops) )
		_threads = (tuple(_bpg), tuple(_tpb))
		if synchronize :
			return (_threads, "[{0}, {1}]".format(_threads[0], _threads[1]))
		return _threads




class Vec_CUDA_Txfr:
	def __init__(self, func_name , vec_list , scev_list , dynarg_list):
		self.vfunc_name = "_gpu_" + func_name
		self.vec_list = vec_list
		self.scev_list = scev_list
		self.dynarg_list = dynarg_list


	@property
	def vfunc_name(self):
		return self._vfunc_name
		
	@vfunc_name.setter
	def vfunc_name(self, name):
		self._vfunc_name = name

	@property
	def vec_list(self):
		return self._vec_list

	@vec_list.setter
	def vec_list(self, args):
		self._vec_list = args

	@property
	def scev_list(self):
		return self._scev_list

	@scev_list.setter
	def scev_list(self, args):
		self._scev_list = args

	@property
	def dynarg_list(self):
		return self._dynarg_list

	@dynarg_list.setter
	def dynarg_list(self, args):
		self._dynarg_list = args

	def prologue(self, lvl, pipe, dag_build, synchronize=False):
		_unpack = ""
		_txfr_args = ""
		_txfr_args_list = []

		# Add all vectors used in the loop to the arguments
		for _arg in self.vec_list :
			_unpack += "{0}{1} = arg_container.{2}\n".format(2*lvl*" " \
												, _arg.arg_name(False), _arg.arg_name(False))
			_vt, _rec = _arg.vtype
			if _vt not in  ["scev_src", "constant"] :
				if synchronize :
					_txfr_args += "{0}{1} = cuda.to_device({2})\n".format(\
									2*(lvl)*" ", _arg.arg_name(True), _arg.arg_name(False))
				else : 
					_txfr_args += "{0}{1} = cuda.to_device({2},{3})\n".format(\
									2*(lvl+1)*" ", _arg.arg_name(True), _arg.arg_name(False), pipe)
				_txfr_args_list.append(_arg)

		if synchronize : 
			_str = "def {0}(arg_container, _timer):\n".format(self.vfunc_name)
			_str += _unpack
			_str += "{0}_timer.start_xfer_to_dev()\n".format((2*(lvl))*" ")
			_str += _txfr_args
			_str += "{0}cuda.synchronize()\n".format((2*(lvl))*" ")
			_str += "{0}_timer.end_xfer_to_dev()\n".format((2*(lvl))*" ")
			_str += "{0}_timer.start_exec()\n".format((2*(lvl))*" ")
		else : 
			_str = "def {0}(arg_container, _timer):\n".format(self.vfunc_name)
			_str += _unpack
			_str += "{0}{1} = cuda.stream()\n".format(2*lvl*" ", pipe)
			_str += "{0}_timer.start_exec()\n".format((2*(lvl))*" ")
			_str += "{0}with {1}.auto_synchronize():\n".format((2*lvl)*" ", pipe)
			#_str += "{0}_timer.start_xfer_to_dev()\n".format((2*(lvl+1))*" ")
			_str += _txfr_args
			#_str += "{0}_timer.end_xfer_to_dev()\n".format((2*(lvl+1))*" ")
			#_str += "{0}_timer.start_exec()\n".format((2*(lvl+1))*" ")
		return (_str, _txfr_args_list)

	def epilogue(self, lvl, pipe, dag_build, synchronize=False):
		_pack = ""
		_txfr_args = ""
		_txfr_args_list = []
		for _arg in reversed(self.vec_list) :
			_vt, _rec = _arg.vtype
			if _vt not in  ["scev_src", "constant"] :
				_txfr_args += "{0}{1}.to_host({2})\n".format(\
										"  " + 2*lvl*" ", _arg.arg_name(True), pipe)
				_txfr_args_list.append(_arg)

			_pack += "{0}arg_container.{1} = {2}\n".format(2*lvl*" " \
												, _arg.arg_name(False), _arg.arg_name(False))

		# This is only ever useful in a debug scenario
		if synchronize : 
			return "{0}cuda.synchronize()\n".format(2*(lvl)*" ") \
					+ "{0}_timer.end_exec()\n".format(2*(lvl)*" ") \
					+ "{0}_timer.start_xfer_to_host()\n".format(2*(lvl)*" ") \
					+ _txfr_args \
					+ "{0}cuda.synchronize()\n".format(2*(lvl)*" ") \
					+ "{0}_timer.end_xfer_to_host()\n".format(2*(lvl)*" ") \
					+ _pack

		#return "{0}_timer.end_exec()\n".format("  " + 2*(lvl)*" ") \
		#		+ "{0}_timer.start_xfer_to_host()\n".format("  " + 2*(lvl)*" ") \
		#		+ _txfr_args \
		#		+ "{0}_timer.end_xfer_to_host()\n".format("  " + 2*(lvl)*" ") \
		#		+ _pack

		return ( _txfr_args \
				+ "{0}_timer.end_exec()\n".format(2*(lvl)*" ") \
				+ _pack , _txfr_args_list )

	def runtime_prologue(self, lvl, pipe, dag_build, scev_map, runtime_args, synchronize=False):
		_unpack = ""
		_txfr_args = ""
		_txfr_args_list = []
		_unique_vars = []

		# Add all vectors used in the loop to the arguments
		for _arg in self.vec_list :
			_var_name = _arg.arg_name(False)
			_unique_vars.append(_var_name)
			_unpack += "{0}{1} = arg_container.{1}\n".format(2*lvl*" " , _var_name )
			_vt, _rec = _arg.vtype
			if _vt not in  ["scev_src", "constant", "looplim_iv" ] :
				if synchronize : 
					_txfr_args += "{0}{1} = cuda.to_device({2})\n".format(\
									2*(lvl)*" ", _arg.arg_name(True), _arg.arg_name(False))
				else : 
					_txfr_args += "{0}{1} = cuda.to_device({2},{3})\n".format(\
									2*(lvl+1)*" ", _arg.arg_name(True), _arg.arg_name(False), pipe)
				_txfr_args_list.append(_arg)

		for _arg in self.dynarg_list :
			_var_name  = _arg.arg_name(False)
			if _var_name in _unique_vars :
				continue

			_unique_vars.append( _var_name )
			_unpack += "{0}{1} = arg_container.{1}\n".format(2*lvl*" " , _var_name)
			_vt, _rec = _arg.vtype
			if _vt not in  ["scev_src", "constant", "looplim_iv" ] :
				if synchronize :
					_txfr_args += "{0}{1} = cuda.to_device({2})\n".format(\
									2*(lvl)*" ", _arg.arg_name(True), _arg.arg_name(False))
				else : 
					_txfr_args += "{0}{1} = cuda.to_device({2},{3})\n".format(\
									2*(lvl+1)*" ", _arg.arg_name(True), _arg.arg_name(False), pipe)

		_scev_gen = "" 
		for _scev_arr in scev_map.type_map.keys() : 
			_type_idx = scev_map.type_map[_scev_arr]
			if len(scev_map.scev_array[_type_idx]) == 0 :
				continue
		
			_npobj = "["	
			for _var in scev_map.scev_array[_type_idx]:
				_unpack += "{0}{1} = arg_container.{1}\n".format(2*lvl*" " , _var.arg_name(True))
				_npobj += "{0}, ".format(_var.arg_name(True))
			else :
				_npobj += "]"
			_scev_gen += "{0}_scevref_arr_{1} = np.array({2}, dtype=np.{1}, copy=True)".format(2*lvl*" " \
																					, _scev_arr, _npobj)+ "\n"

			if synchronize : 
				_txfr_args += "{0}_cvec_scevref_arr_{1} = cuda.to_device(_scevref_arr_{1})\n".format(\
									2*(lvl)*" ", _scev_arr )
			else : 
				_txfr_args += "{0}_cvec_scevref_arr_{1} = cuda.to_device(_scevref_arr_{1},{2})\n".format(\
									2*(lvl+1)*" ", _scev_arr, pipe)


		if synchronize : 
			_str = "def {0}(arg_container, _timer):\n".format(self.vfunc_name)
			_str += _unpack + _scev_gen 
			_str += "{0}_timer.start_xfer_to_dev()\n".format((2*(lvl))*" ")
			_str += _txfr_args
			_str += "{0}cuda.synchronize()\n".format((2*(lvl))*" ")
			_str += "{0}_timer.end_xfer_to_dev()\n".format((2*(lvl))*" ")
			_str += "{0}_timer.start_exec()\n".format((2*(lvl))*" ")
		else :
			_str = "def {0}(arg_container, _timer):\n".format(self.vfunc_name)
			_str += _unpack + _scev_gen
			_str += "{0}{1} = cuda.stream()\n".format(2*lvl*" ", pipe)
			_str += "{0}_timer.start_exec()\n".format((2*(lvl))*" ")
			_str += "{0}with {1}.auto_synchronize():\n".format((2*lvl)*" ", pipe)
			#_str += "{0}_timer.start_xfer_to_dev()\n".format((2*(lvl+1))*" ")
			_str += _txfr_args
			#_str += "{0}_timer.end_xfer_to_dev()\n".format((2*(lvl+1))*" ")
			#_str += "{0}_timer.start_exec()\n".format((2*(lvl+1))*" ")
		return (_str, _txfr_args_list)


	def runtime_epilogue(self, lvl, pipe, dag_build, scev_map, runtime_args, synchronize=False):
		_pack = ""
		_txfr_args = ""
		_txfr_args_list = []
		_unique_vars = []


		for _scev_arr in scev_map.type_map.keys() :
			_type_idx = scev_map.type_map[_scev_arr]
			if len(scev_map.scev_array[_type_idx]) == 0 :
				continue
			for _scev_idx, _scev_var in enumerate(scev_map.scev_array[_type_idx]):
				_pack += "{0}arg_container.{1} = _scevref_arr_{2}[{3}]\n".format(2*lvl*" ", _scev_var.arg_name(True) , _scev_var.dtype, _scev_idx)
			if synchronize : 
				_txfr_args += "{0}_cvec_scevref_arr_{1}.to_host()\n".format(2*(lvl)*" ", _scev_arr)
			else :
				_txfr_args += "{0}_cvec_scevref_arr_{1}.to_host({2})\n".format("  " + 2*(lvl)*" ", _scev_arr, pipe)

		for _arg in reversed(self.dynarg_list) :
			_var_name = _arg.arg_name(False)
			if _var_name in _unique_vars :
				continue
			_unique_vars.append( _var_name )
			_pack += "{0}arg_container.{1} = {1}\n".format(2*lvl*" " , _var_name)

		for _arg in reversed(self.vec_list) :
			_var_name = _arg.arg_name(False)
			if _var_name in _unique_vars :
				continue
			_unique_vars.append( _var_name )
			_vt, _rec = _arg.vtype
			if _vt not in  ["scev_src", "constant"] :
				if synchronize : 
					_txfr_args += "{0}{1}.to_host()\n".format(\
											2*lvl*" ", _arg.arg_name(True))
				else : 
					_txfr_args += "{0}{1}.to_host({2})\n".format(\
											"  " + 2*lvl*" ", _arg.arg_name(True), pipe)
				_txfr_args_list.append(_arg)

			_pack += "{0}arg_container.{1} = {1}\n".format(2*lvl*" " , _var_name)


		if synchronize : 
			return "{0}cuda.synchronize()\n".format(2*(lvl)*" ") \
					+ "{0}_timer.end_exec()\n".format(2*(lvl)*" ") \
					+ "{0}_timer.start_xfer_to_host()\n".format(2*(lvl)*" ") \
					+ _txfr_args \
					+ "{0}cuda.synchronize()\n".format(2*(lvl)*" ") \
					+ "{0}_timer.end_xfer_to_host()\n".format(2*(lvl)*" ") \
					+ _pack

		#return "{0}_timer.end_exec()\n".format("  " + 2*(lvl)*" ") \
		#		+ "{0}_timer.start_xfer_to_host()\n".format("  " + 2*(lvl)*" ") \
		#		+ _txfr_args \
		#		+ "{0}_timer.end_xfer_to_host()\n".format("  " + 2*(lvl)*" ") \
		#		+ _pack

		return  ( _txfr_args \
				+ "{0}_timer.end_exec()\n".format(2*(lvl)*" ") \
				+ _pack , _txfr_args_list )

class GPU_Exec_Cost(hwb.Execution_Cost):
    def __init__(self, seq_loops, hw_par_loops, kern_loops):
        super().__init__(seq_loops)

        self.hw_par_threads = hw_par_loops[0]
        self.hw_par_loops = hw_par_loops[1]

        self.kernel_loops = kern_loops
        self.hw = 'gtx-1060'

    @property
    def hw_par_threads(self):
        return self._HW_par_threads

    @hw_par_threads.setter
    def hw_par_threads(self, hw_thread_sizes):
        self._HW_par_threads = hw_thread_sizes

    @property
    def hw_par_loops(self):
        return self._HW_parloops

    @hw_par_loops.setter
    def hw_par_loops(self, hw_loops):
        self._HW_parloops = hw_loops

    @property
    def kernel_loops(self):
        return self._kernel_loops

    @kernel_loops.setter
    def kernel_loops(self, kern_loops):
        self._kernel_loops = kern_loops

    def _execution_cost(self, cost_metric, icost_kern = 1.0, icost_gpu = 1.0):
        _pi = lambda x, y: x * y
        _pseq = functools.reduce(_pi, [1] + self.seq_loop_dom)

        _cost_ki = cost_metric["gpu"]["Cki_Cg_ratio"] if cost_metric else None
        _jit_speedup = cost_metric["gpu"]["Cint_Cgpu_ratio"] if cost_metric else 1.0
        if _jit_speedup == None :
            _jit_speedup = 1.0

        #cost of any loops executed in sequence to preserve dependences
        if _pseq == 1:
            return _cost_ki + self._parallel_cost( _cost_ki, _jit_speedup, icost_gpu)
        else:
            return _pseq * self._parallel_cost( _cost_ki, _jit_speedup, icost_gpu)

    def _parallel_cost(self, cost_ki, jit_speedup, icost):
        _pi = lambda x, y: x * y
        _pk = functools.reduce(_pi, [1] + self.kernel_loops)

        _phw = functools.reduce(_pi, [1] + self.hw_par_loops)
        _pblk = functools.reduce(_pi, (1,) + self.hw_par_threads[0])

        # Start with inner most kernel loops for this statement
        _cost = _pk * icost

        # multiply with serialized cost to account for over-lapping grids
        # in case there are more blocks than grids
        _num_sm = hw_parm[self.hw]['sm']
        _num_wsched = hw_parm[self.hw]['wsched']
        _cost *= ((_pblk + _num_sm - 1 )// _num_sm) \
            * ( max( hw_parm[self.hw]['ws'], _phw) / (_pblk * _num_wsched * hw_parm[self.hw]['ws']))

	# If not an installation time profiling run, 
	# check if we are starving the GPU 
        # Happens when we cannot invoke kernels fast enough in the interpreter
        # 
        if cost_ki != None :
            _cost = (cost_ki * icost) if (cost_ki * icost) > _cost else _cost
        return _cost / jit_speedup

    def parallel_cost(self, profile_map, icost = 1.0):
        _cost_ki = profile_map["gpu"]["Cki_Cg_ratio"] if profile_map else None
        _jit_speedup = profile_map["gpu"]["Cint_Cgpu_ratio"] if profile_map else 1.0
        if _jit_speedup  == None :
            _jit_speedup = 1.0
        return self._parallel_cost( _cost_ki, _jit_speedup, icost)

    def cost(self, cost_metric ):
        return self._execution_cost(cost_metric)


    def patch_types( self, stmt, scevref_map, runtime_args):
        pass

