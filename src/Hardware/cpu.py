#!/home/dejice/work/python-tutorial/ast-venv/bin/python3
#/usr/bin/python3

# ALPyNA : Automatic Loop Parallelisation in Python for Heterogeneous Architectures
# Copyright (C) <2020>  <Dejice Jacob>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# long with this program.  If not, see <https://www.gnu.org/licenses/>.


import weakref
import astor
import Dependency_Solver as solve
import functools
import numpy as np
from Hardware import hw_base as hwb

# Hardware description of nVidia Cards
# map with key = model. value = map of keys
# SM (streaming multiprocessors)
# Freq (frequency in MegaHertz)
cpu_hw_parm = {'avx2': {"reg_len": 32, "num_reg": 16}}


class Vec_Host_CPU:
    def __init__(self, graph_node, parallel_dims):
        self.graph_node = graph_node
        self.parallel_dims = parallel_dims
        self.raw_kernel = None

        self.scevref_kernel_core = None
        self.scevref_kernel = False
        self.raw_drv = None
        self.runtime_kernel = None

    @property
    def graph_node(self):
        return self._graph_node()

    @graph_node.setter
    def graph_node(self, node):
        self._graph_node = weakref.ref(node)

    @property
    def raw_kernel(self):
        return self._raw_kernel

    @raw_kernel.setter
    def raw_kernel(self, kern_str):
        self._raw_kernel = kern_str

    @property
    def raw_drv(self):
        return self._raw_drv

    @raw_drv.setter
    def raw_drv(self, drv):
        self._raw_drv = drv

    def _vec_stmt(self, indent):
        _pred = self.graph_node.payload.assign_stmt.predicate_ast
        _obj = self.graph_node.payload.assign_stmt.node
        if _pred:
            _code = ["{0}if {1}:".format(indent, astor.codegen.to_source(_pred).rstrip("\n"))]
            _code.append("{0}  {1}".format(indent, astor.codegen.to_source(_obj).rstrip("\n")))
        else:
            _code = ["{0}{1}".format(indent, astor.codegen.to_source(_obj).rstrip("\n"))]
        return _code

    @property
    def scevref_kernel_core(self):
        return self._scevref_kernel_core

    @scevref_kernel_core.setter
    def scevref_kernel_core(self, kern_str):
        self._scevref_kernel_core = kern_str

    def _check_scalar_ref(self, scev_ref):
        if len(scev_ref) > 0:
            _assign_trgts = self.graph_node.payload.assign_stmt.record["trgt"]
            for _arg in _assign_trgts:
                for _scev in scev_ref:
                    if _arg['var'] == _scev.name:
                        self.generate_phase = solve.phase.get_next_stg_sym()
                        self.scevref_kernel = True
        return    # Break out of all loops

    # If the limits are known at compile time, just query it
    # If the limits are known only at runtime, extract from
    # runtime environment
    def _resolve_lims(self, lim, runtime_args=None):
        if solve.phase.get_curr_stg_sym() == 'runtime':
            if lim[0] == 'lazy':
                return getattr(runtime_args, lim[1], None)
            else:
                return lim[1]
        else:
            if lim[0] == 'imm':
                return lim[1]
        assert False, "Run-time for-loop end limit - cannot create kernel"

    def _gen_kern_str(self, stmt_list):
        _code_txt = ""
        for _txt in stmt_list:
            _code_txt += _txt + "\n"
        return _code_txt

    def _cpu_kernel_body(self, runtime_args=None):
        _id_parloops = lambda orig_loops, num_par_dims: orig_loops[len(orig_loops) - num_par_dims:]
        _par_loops = _id_parloops(self.graph_node.payload.enclosing_loops, self.parallel_dims)
        _ref = lambda x: x() if callable(x) else x

        _body = []
        _nidx = None
        for _nidx, _for_stmt in enumerate(_par_loops):
            _body += [ "{0}for {1} in range({2},{3},{4}):".format("  "*_nidx \
                        , _ref(_for_stmt).record["for_targets"].target[0]
                        , _ref(_for_stmt).record["for_limits"].limits["start"][1]
                        , _ref(_for_stmt).record["for_limits"].limits["end"][1]
                        , _ref(_for_stmt).record["for_limits"].limits["step"][1])
                    ]
        else:
            _body += self._vec_stmt("  " * (0 if _nidx == None else _nidx + 1))
        return _body

    def gen_kernel(self, scev_ref, lvl=1):
        assert solve.phase.get_curr_stg_sym() == "aot", "exception at stage {0}".format(
            solve.phase.get_curr_stg_sym())
        # Only check whether we have a scev-ref.
        # If it is not a scev-ref kernel, the actual kernel will be generated
        # during driver prologue generation
        self._check_scalar_ref(scev_ref)
        return None

    def gen_drv(self, scev_ref, lvl=1):
        _iter_end = lambda _loop_end: self._resolve_lims(_loop_end)
        _ref = lambda obj: obj() if callable(obj) else obj
        _loops = self.graph_node.payload.enclosing_loops
        _cpu_cost = CPU_Exec_Cost(
            [_iter_end(_ref(_loop).record["for_limits"].limits["end"]) for _loop in _loops], _loops,
            self.parallel_dims)

        return _cpu_cost

    def runtime_gen_kernel(self, scev_ref, runtime_args, lvl=1):
        pass

    def runtime_gen_drv(self, scev_ref, runtime_args, lvl=1):
        _iter_end = lambda _loop_end: self._resolve_lims(_loop_end, runtime_args)
        _ref = lambda obj: obj() if callable(obj) else obj
        _loops = self.graph_node.payload.enclosing_loops
        _cpu_cost = CPU_Exec_Cost(
            [_iter_end(_ref(_loop).record["for_limits"].limits["end"]) for _loop in _loops], _loops,
            self.parallel_dims, runtime_args)
        return _cpu_cost

    # This function should only ever be called at runtime whether the kernel
    # is generated ahead of time or at runtime
    def patch_argtypes(self, scevref_map, runtime_args):
        return []

    def patch_scevref_kernel(self, scevref_map, runtime_args):
        pass


class Host_CPU_Txfr(hwb.Host_Base_Txfr):
    def __init__(self, func_name, vec_list, scev_list, dynarg_list):
        super().__init__( "_cpu_" + func_name \
             , vec_list , scev_list , dynarg_list)
        self.func_name = func_name

    def _cpu_kernel(self, dag_build, scevref_map):
        _ast_co = dag_build.loop_nest.boundary.start.node
        _unique_vars = []
        _k_xfer_in = ""
        _k_xfer_out = ""
        _scevref_arg_hdr = []

        _args = ""
        for _arg in self.vec_list:
            _var_name = _arg.arg_name(False)
            _unique_vars.append(_var_name)
            _args += "{0},".format(_arg.arg_name(False))

        for _arg in self.scev_list:
            _var_name = _arg.arg_name(False)
            if _var_name in _unique_vars:
                continue
            _unique_vars.append(_var_name)
            _scevarr_idx, _arg_idx = scevref_map.locate_var(_arg)
            _scevref_arr = "scevref_arr_{0}".format(_arg.dtype)
            _k_xfer_in += "    {0} = {1}[{2}]\n".format(
                _arg.arg_name(False), _scevref_arr, _arg_idx)
            _k_xfer_out += "    {0}[{1}] = {2}\n".format(_scevref_arr, _arg_idx,
                                                         _arg.arg_name(False))
            if _scevref_arr not in _scevref_arg_hdr:
                _scevref_arg_hdr.append(_scevref_arr)
                _args += "{0},".format(_scevref_arr)

        for _arg in self.dynarg_list:
            _var_name = _arg.arg_name(False)
            if _var_name in _unique_vars:
                continue
            _unique_vars.append(_var_name)
            _args += "{0},".format(_arg.arg_name(False))

        _kern_str = "def {0}({1}):\n".format("_cpukern_" + self.func_name, _args.rstrip(","))
        _kern_str += _k_xfer_in + "    "
        _kern_str += astor.codegen.to_source(_ast_co).replace("\n", "\n    ")
        _kern_str += "\n" + _k_xfer_out
        return _kern_str + "\n" if _k_xfer_out else _kern_str

    def prologue(self, lvl, pipe, dag_build):
        #_co_body = astor.codegen.to_source(_ast_co).replace("\n", "\n    ")
        _kern = self._cpu_kernel(dag_build, None)

        _unpack = ""
        _arg_list = ""
        for _arg in self.vec_list:
            _unpack += "{0}{1} = arg_container.{2}\n".format(2*lvl*" " \
                     , _arg.arg_name(False), _arg.arg_name(False))
            _arg_list += "{0},".format(_arg.arg_name(False))

        _str = "def {0}(arg_container, _timer):\n".format(self.vfunc_name)
        _str += _unpack + "{0}_timer.start_exec()\n".format(2 * lvl * " ")
        _kern_call = "{0}{1}{2}({3})".format(2 * lvl * " ", "_cpukern_", self.func_name,
                                             _arg_list.rstrip(","))
        return (_kern + _str + _kern_call + "\n", [])

    def runtime_prologue(self, lvl, pipe, dag_build, scev_map, runtime_args):
        _kern = self._cpu_kernel(dag_build, scev_map)
        _unpack = ""
        _arg_list = ""
        _npobj = ""
        _scev_gen = ""
        _unique_vars = []
        _scevref_arg_hdr = []

        for _arg in self.vec_list:
            _var_name = _arg.arg_name(False)
            _unique_vars.append(_var_name)
            _unpack += "{0}{1} = arg_container.{1}\n".format(2 * lvl * " ", _var_name)
            _arg_list += "{0},".format(_arg.arg_name(False))

        # For Scevs first generate arguments for kernel-call based on scev-arrays
        for _scev_arr in scev_map.type_map.keys():
            _type_idx = scev_map.type_map[_scev_arr]
            if len(scev_map.scev_array[_type_idx]) == 0:
                continue
            for _var in scev_map.scev_array[_type_idx]:
                _var_name = _var.arg_name(False)
                if _var_name in _unique_vars:
                    continue
                _unique_vars.append(_var_name)
                _unpack += "{0}{1} = arg_container.{1}\n".format(2 * lvl * " ", _var_name)
                _npobj += "{0},".format(_var_name)
            _scev_gen += "{0}_scevref_arr_{1} = np.array([{2}], dtype=np.{1},copy=True)\n".format(
                2 * lvl * " ", _scev_arr, _npobj, _npobj)

        # Add scevs in same order they were found for ABI compatibility
        for _arg in self.scev_list:
            _var_name = _arg.arg_name(False)
            _scevarr_idx, _arg_idx = scev_map.locate_var(_arg)
            _scevref_arr = "_scevref_arr_{0}".format(_arg.dtype)
            if _scevref_arr not in _scevref_arg_hdr:
                _scevref_arg_hdr.append(_scevref_arr)
                _arg_list += "{0},".format(_scevref_arr)

        for _arg in self.dynarg_list:
            _var_name = _arg.arg_name(False)
            if _var_name in _unique_vars:
                continue
            _unique_vars.append(_var_name)
            _unpack += "{0}{1} = arg_container.{1}\n".format(2 * lvl * " ", _var_name)
            _arg_list += "{0},".format(_arg.arg_name(False))

        _str = "def {0}(arg_container, _timer):\n".format(self.vfunc_name)
        _str += _unpack + _scev_gen + "{0}_timer.start_exec()\n".format(2 * lvl * " ")
        _kern_call = "{0}{1}{2}({3})".format(2 * lvl * " ", "_cpukern_", self.func_name,
                                             _arg_list.rstrip(","))
        return (_kern + _str + _kern_call, [])

    def epilogue(self, lvl, pipe, dag_build):
        _pack = "{0}_timer.end_exec()\n".format(2 * lvl * " ")
        for _arg in self.vec_list:
            _var_name = _arg.arg_name(False)
            _pack += "{0}arg_container.{1} = {1}\n".format(2 * lvl * " ", _var_name)
        return (_pack, [])

    def runtime_epilogue(self, lvl, pipe, dag_build, scev_map, runtime_args):
        _pack = "{0}_timer.end_exec()\n".format(2 * lvl * " ")
        _unique_vars = []

        for _arg in self.dynarg_list:
            _var_name = _arg.arg_name(False)
            _unique_vars.append(_var_name)
            _pack += "{0}arg_container.{1} = {1}\n".format(2 * lvl * " ", _var_name)

        for _arg in self.vec_list:
            _var_name = _arg.arg_name(False)
            if _var_name in _unique_vars:
                continue
            _unique_vars.append(_var_name)
            _pack += "{0}arg_container.{1} = {1}\n".format(2 * lvl * " ", _var_name)

        # For Scevs first generate arguments for kernel-call based on scev-arrays
        for _scev_arr in scev_map.type_map.keys():
            _type_idx = scev_map.type_map[_scev_arr]
            if len(scev_map.scev_array[_type_idx]) == 0:
                continue

            for _arr_idx, _var in enumerate(scev_map.scev_array[_type_idx]):
                _var_name = _var.arg_name(False)
                if _var_name in _unique_vars:
                    continue
                _unique_vars.append(_var_name)
                _pack += "{0}arg_container.{1} = _scevref_arr_{2}[{3}]\n".format(
                    2 * lvl * " ", _var_name, _scev_arr, _arr_idx)

        return (_pack, [])

    def runtime_drv_signature(self, lvl, scevref_map):
        _sig = "{0}@jit".format(2 * " " * lvl)
        _arg_str = ""
        _unique_vars = []
        _scevref_arg_hdr = []

        for _arg in self.vec_list:
            _var_name = _arg.arg_name(False)
            _unique_vars.append(_var_name)
            _arg_str += "{0},".format(_arg.dtype + _arg.dim_str)

        # Add scevs in same order they were found for ABI compatibility
        for _arg in self.scev_list:
            _var_name = _arg.arg_name(False)
            if _var_name in _unique_vars:
                continue
            _unique_vars.append(_var_name)
            _scevarr_idx, _arg_idx = scevref_map.locate_var(_arg)
            _scevref_arr = "_scevref_arr_{0}".format(_arg.dtype)
            if _scevref_arr not in _scevref_arg_hdr:
                _scevref_arg_hdr.append(_scevref_arr)
                _arg_str += "{0},".format(_arg.dtype + "[:]")

        for _arg in self.dynarg_list:
            _var_name = _arg.arg_name(False)
            if _var_name in _unique_vars:
                continue
            _unique_vars.append(_var_name)
            _arg_str += "{0},".format(_arg.dtype + _arg.dim_str)

        return "{0}('{1}')".format(_sig, _arg_str.rstrip(",")) if len(_arg_str) else _sig


class CPU_Exec_Cost(hwb.Execution_Cost):
    def __init__(self, seq_loops, loops, par_loops, runtime_args=None):
        super().__init__(seq_loops)
        self.hw = 'avx2'
        self.parallel_dom_size = self._init_par_domain(loops, par_loops, runtime_args)
        self.vector_factor = cpu_hw_parm[self.hw]['reg_len'] * cpu_hw_parm[self.hw]['num_reg']

    def _init_par_domain(self, loops, par_loops, runtime_args=None):
        _id_par_loops = lambda orig_loops, num_par_dims: orig_loops[len(orig_loops) - num_par_dims:]
        _ref = lambda obj: obj() if callable(obj) else obj
        _iter_end = lambda _loop_end: self._resolve_lims(_loop_end, runtime_args)

        par_domains = [ _iter_end(_ref(_loop).record['for_limits'].limits['end']) \
                            for _loop in _id_par_loops( loops, par_loops) ]
        return functools.reduce(lambda x, y: x * y, par_domains)

    # If the limits are known at compile time, just query it
    # If the limits are known only at runtime, extract from
    # runtime environment
    def _resolve_lims(self, lim, runtime_args=None):
        if solve.phase.get_curr_stg_sym() == 'runtime':
            if lim[0] == 'lazy':
                return getattr(runtime_args, lim[1], None)
            else:
                return lim[1]
        else:
            if lim[0] == 'imm':
                return lim[1]
        assert False, "Run-time for-loop end limit - cannot create kernel"

    def _execution_cost(self, jit_speedup, icost=1.0):
        _cost = icost * functools.reduce(lambda x, y: x * y, [1] + self.seq_loop_dom)
        return _cost / jit_speedup

    def parallel_cost(self, profile_map):
        _jit_speedup = profile_map["cpu"]["Cint_Ccpu_ratio"] if profile_map else 1.0
        # On a profile run, we are measuring jit-speedup
        if _jit_speedup == None:
            _jit_speedup = 1.0
        return self._execution_cost(_jit_speedup)

    def cost(self, cost_metric):
        _jit_speedup = cost_metric["cpu"]["Cint_Ccpu_ratio"] if cost_metric else 1.0
        if _jit_speedup == None:
            _jit_speedup = 1.0
        return self._execution_cost(_jit_speedup)

    def _deduce_vector_factor(self, vec_factor, var_list, runtime_args, trgt=False):
        for _sym in var_list:
            if not _sym['axes'] and trgt:
                # scalar value write within a loop causing
                # dependence carried by every loop
                return 1

            _var = getattr(runtime_args, _sym['var'], None)
            if type(_var) is np.ndarray:
                _type_size = _var.itemsize
            elif type(_var) in hwb.Native_types:
                _type_size = np.dtype(Native_Numpy_Map[type(_var)]).itemsize
            _new_vec_f = cpu_hw_parm[self.hw]['reg_len'] * cpu_hw_parm[self.hw][
                'num_reg'] / _type_size
            if vec_factor > _new_vec_f:
                vec_factor = _new_vec_f
        return vec_factor

    def patch_types(self, node, scevref_map, runtime_args):
        _var_list = node.assign_stmt.record['pred']
        # The cpu compiler will not vectorise
        # in the presence of conditionals
        if len(_var_list):
            self.vector_factor = 1
            return

        # First check target type for potential vectorisation size
        _var_list = node.assign_stmt.record['trgt']
        self.vector_factor = self._deduce_vector_factor(self.vector_factor, _var_list, runtime_args,
                                                        True)
        # Next - check source type for potential vectorisation size
        _var_list = node.assign_stmt.record['src'].depends
        self.vector_factor = self._deduce_vector_factor(self.vector_factor, _var_list, runtime_args,
                                                        True)

        _var_list = node.assign_stmt.record['src'].depends


class Host_VM:
    def __init__(self, graph_node, parallel_dims):
        self.graph_node = graph_node
        self.parallel_dims = parallel_dims
        self.raw_kernel = None
        self.scevref_kernel = False
        self.raw_drv = None
        self.runtime_kernel = None

    @property
    def graph_node(self):
        return self._graph_node()

    @graph_node.setter
    def graph_node(self, node):
        self._graph_node = weakref.ref(node)

    @property
    def raw_kernel(self):
        return self._raw_kernel

    @raw_kernel.setter
    def raw_kernel(self, kern_str):
        self._raw_kernel = kern_str

    @property
    def raw_drv(self):
        return self._raw_drv

    @raw_drv.setter
    def raw_drv(self, drv):
        self._raw_drv = drv

    def _resolve_lims(self, lim, runtime_args=None):
        # This function is a place-holder for when I need to query the loop limit end
        # at run-time
        # for the moment assert
        if solve.phase.get_curr_stg_sym() == 'runtime':
            if lim[0] == 'lazy':
                return getattr(runtime_args, lim[1], None)
            else:
                return lim[1]
        else:
            if lim[0] == 'imm':
                return lim[1]
        assert False, "Run-time for-loop end limit - cannot create kernel"

    # This method is only present to prevent an exception.
    # We will be generating the whole loop-nest in the prologue
    def gen_kernel(self, scev_ref, lvl=1):
        self.raw_kernel = ""

    # This method is only present to prevent an exception.
    # We will be generating the whole loop-nest in the prologue
    def gen_drv(self, scev_ref, lvl=1):
        self.raw_drv = ""
        _iter_end = lambda _loop_end: self._resolve_lims(_loop_end)
        _ref = lambda obj: obj() if callable(obj) else obj
        _vm_cost = VM_Exec_Cost( [ _iter_end(_ref(_loop).record["for_limits"].limits["end"]) \
                                        for _loop in self.graph_node.payload.enclosing_loops ] )

        return _vm_cost

    def runtime_gen_kernel(self, scev_ref, runtime_args, lvl=1):
        pass

    def runtime_gen_drv(self, scev_ref, runtime_args, lvl=1):
        _iter_end = lambda _loop_end: self._resolve_lims(_loop_end, runtime_args)
        _ref = lambda obj: obj() if callable(obj) else obj
        _vm_cost = VM_Exec_Cost( [ _iter_end(_ref(_loop).record["for_limits"].limits["end"]) \
                                        for _loop in self.graph_node.payload.enclosing_loops ] )
        return _vm_cost

    # This function should only ever be called at runtime whether the kernel
    # is generated ahead of time or at runtime
    def patch_argtypes(self, scevref_map, runtime_args):
        return []


class Host_VM_Txfr(hwb.Host_Base_Txfr):
    def __init__(self, func_name, vec_list, scev_list, dynarg_list):
        super().__init__( "_vm_" + func_name \
             , vec_list , scev_list , dynarg_list)

    def prologue(self, lvl, pipe, dag_build):
        _ast_co = dag_build.loop_nest.boundary.start.node
        _co_body = astor.codegen.to_source(_ast_co).replace("\n", "\n    ")

        _uniq = []
        _unpack = ""
        for _arg in self.vec_list + self.scev_list + self.dynarg_list:
            if _arg.arg_name(False) in _uniq:
                continue
            _uniq += [_arg.arg_name(False)]
            _unpack += "{0}{1} = arg_container.{2}\n".format(2*lvl*" " \
                     , _arg.arg_name(False), _arg.arg_name(False))

        _str = "def {0}(arg_container, _timer):\n".format(self.vfunc_name)
        _str += _unpack + "{0}_timer.start_exec()\n{0}".format(2 * lvl * " ")
        return (_str + _co_body + "\n", [])

    def epilogue(self, lvl, pipe, dag_build):
        _uniq = []
        _pack = "{0}_timer.end_exec()\n".format(2 * lvl * " ")
        for _arg in self.vec_list + self.scev_list + self.dynarg_list:
            if _arg.arg_name(False) in _uniq:
                continue
            _uniq += [_arg.arg_name(False)]
            _pack += "{0}arg_container.{1} = {2}\n".format(2*lvl*" " \
                     , _arg.arg_name(False), _arg.arg_name(False))
        return (_pack, [])


class VM_Exec_Cost(hwb.Execution_Cost):
    def __init__(self, seq_loops):
        super().__init__(seq_loops)

    def _execution_cost(self, _speedup):
        _icost = 1    # relative cost per instance execution
        return _icost * functools.reduce(lambda x, y: x * y, [1] + self.seq_loop_dom)

    def parallel_cost(self, profile_map):
        return self._execution_cost(1.0)

    def cost(self, cost_metric):
        return self._execution_cost(1.0)

    def patch_types(self, node, scevref_map, runtime_args):
        pass
